this.default_host = this.default_host || {};
(function(self) {
    var window = this;
    try {
        var defineProperty;
        var arr;
        var learn;
        var ea;
        var isPlainObject;
        var freeze;
        var cache;
        var matches;
        var sel;
        self.g;
        /** @type {Function} */
        defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function(obj, prop, desc) {
            if (desc.get || desc.set) {
                throw new TypeError("ES3 does not support getters and setters.");
            }
            if (obj != Array.prototype) {
                if (obj != Object.prototype) {
                    obj[prop] = desc.value;
                }
            }
        };
        arr = "undefined" != typeof window && window === this ? this : "undefined" != typeof window.global ? window.global : this;
        /**
         * @return {undefined}
         */
        learn = function() {
            /**
             * @return {undefined}
             */
            learn = function() {
            };
            if (!arr.Symbol) {
                arr.Symbol = isPlainObject;
            }
        };
        /** @type {number} */
        ea = 0;
        /**
         * @param {string} value
         * @return {?}
         */
        isPlainObject = function(value) {
            return "jscomp_symbol_" + (value || "") + ea++;
        };
        /**
         * @return {undefined}
         */
        self.ga = function() {
            learn();
            var methodName = arr.Symbol.iterator;
            if (!methodName) {
                methodName = arr.Symbol.iterator = arr.Symbol("iterator");
            }
            if ("function" != typeof Array.prototype[methodName]) {
                defineProperty(Array.prototype, methodName, {
                    configurable : true,
                    writable : true,
                    /**
                     * @return {?}
                     */
                    value : function() {
                        return self.fa(this);
                    }
                });
            }
            /**
             * @return {undefined}
             */
            self.ga = function() {
            };
        };
        /**
         * @param {Array} object
         * @return {?}
         */
        self.fa = function(object) {
            /** @type {number} */
            var i = 0;
            return freeze(function() {
                return i < object.length ? {
                    done : false,
                    value : object[i++]
                } : {
                    done : true
                };
            });
        };
        /**
         * @param {Object} object
         * @return {?}
         */
        freeze = function(object) {
            self.ga();
            object = {
                next : object
            };
            /**
             * @return {?}
             */
            object[arr.Symbol.iterator] = function() {
                return this;
            };
            return object;
        };
        cache = arr;
        /** @type {Array} */
        matches = ["String", "prototype", "repeat"];
        /** @type {number} */
        sel = 0;
        for (;sel < matches.length - 1;sel++) {
            var name = matches[sel];
            if (!(name in cache)) {
                cache[name] = {};
            }
            cache = cache[name];
        }
        var key = matches[matches.length - 1];
        var opt_locale = cache[key];
        var locale = opt_locale ? opt_locale : function(dataAndEvents) {
            var s;
            if (null == this) {
                throw new TypeError("The 'this' value for String.prototype.repeat must not be null or undefined");
            }
            /** @type {string} */
            s = this + "";
            if (0 > dataAndEvents || 1342177279 < dataAndEvents) {
                throw new window.RangeError("Invalid count value");
            }
            dataAndEvents |= 0;
            /** @type {string} */
            var slarge = "";
            for (;dataAndEvents;) {
                if (dataAndEvents & 1 && (slarge += s), dataAndEvents >>>= 1) {
                    s += s;
                }
            }
            return slarge;
        };
        if (locale != opt_locale) {
            if (null != locale) {
                defineProperty(cache, key, {
                    configurable : true,
                    writable : true,
                    value : locale
                });
            }
        }
        self.qa = self.qa || {};
        self.l = this;
        /** @type {string} */
        self.ra = "closure_uid_" + (1E9 * Math.random() >>> 0);
        /** @type {function (): number} */
        self.sa = Date.now || function() {
                return+new Date;
            };
    } catch (curr) {
        self._DumpException(curr);
    }
    try {
        var $goog$bindJs_$;
        var bound;
        /**
         * @param {string} x
         * @return {?}
         */
        self.m = function(x) {
            return void 0 !== x;
        };
        /**
         * @param {Function} matcherFunction
         * @param {?} $selfObj$$
         * @param {?} $var_args$$
         * @return {?}
         */
        $goog$bindJs_$ = function(matcherFunction, $selfObj$$, $var_args$$) {
            if (!matcherFunction) {
                throw Error();
            }
            if (2 < arguments.length) {
                /** @type {Array.<?>} */
                var args = Array.prototype.slice.call(arguments, 2);
                return function() {
                    /** @type {Array.<?>} */
                    var newArgs = Array.prototype.slice.call(arguments);
                    Array.prototype.unshift.apply(newArgs, args);
                    return matcherFunction.apply($selfObj$$, newArgs);
                };
            }
            return function() {
                return matcherFunction.apply($selfObj$$, arguments);
            };
        };
        /**
         * @param {Function} cb
         * @param {?} lower
         * @param {?} num
         * @return {?}
         */
        bound = function(cb, lower, num) {
            return cb.call.apply(cb.bind, arguments);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        self.va = function(value) {
            /** @type {string} */
            var $s = typeof value;
            if ("object" == $s) {
                if (value) {
                    if (value instanceof Array) {
                        return "array";
                    }
                    if (value instanceof Object) {
                        return $s;
                    }
                    /** @type {string} */
                    var isFunction = Object.prototype.toString.call(value);
                    if ("[object Window]" == isFunction) {
                        return "object";
                    }
                    if ("[object Array]" == isFunction || "number" == typeof value.length && ("undefined" != typeof value.splice && ("undefined" != typeof value.propertyIsEnumerable && !value.propertyIsEnumerable("splice")))) {
                        return "array";
                    }
                    if ("[object Function]" == isFunction || "undefined" != typeof value.call && ("undefined" != typeof value.propertyIsEnumerable && !value.propertyIsEnumerable("call"))) {
                        return "function";
                    }
                } else {
                    return "null";
                }
            } else {
                if ("function" == $s && "undefined" == typeof value.call) {
                    return "object";
                }
            }
            return $s;
        };
        /**
         * @param {Function} object
         * @param {Function} b
         * @return {undefined}
         */
        self.p = function(object, b) {
            /**
             * @return {undefined}
             */
            function __() {
            }
            __.prototype = b.prototype;
            object.D = b.prototype;
            object.prototype = new __;
            /** @type {Function} */
            object.prototype.constructor = object;
            /**
             * @param {?} opt_context
             * @param {?} name
             * @param {?} dataAndEvents
             * @return {?}
             */
            object.Ac = function(opt_context, name, dataAndEvents) {
                /** @type {Array} */
                var args = Array(arguments.length - 2);
                /** @type {number} */
                var i = 2;
                for (;i < arguments.length;i++) {
                    args[i - 2] = arguments[i];
                }
                return b.prototype[name].apply(opt_context, args);
            };
        };
        /**
         * @param {string} pair
         * @param {string} ll
         * @return {undefined}
         */
        self.wa = function(pair, ll) {
            var parts = pair.split(".");
            var cur = self.l;
            if (!(parts[0] in cur)) {
                if (!!cur.execScript) {
                    cur.execScript("var " + parts[0]);
                }
            }
            var part;
            for (;parts.length && (part = parts.shift());) {
                if (!parts.length && self.m(ll)) {
                    /** @type {string} */
                    cur[part] = ll;
                } else {
                    if (cur[part]) {
                        cur = cur[part];
                    } else {
                        cur = cur[part] = {};
                    }
                }
            }
        };
        /**
         * @param {Function} recurring
         * @param {number} ar
         * @return {?}
         */
        self.xa = function(recurring, ar) {
            /** @type {Array.<?>} */
            var args = Array.prototype.slice.call(arguments, 1);
            return function() {
                /** @type {Array.<?>} */
                var newArgs = args.slice();
                newArgs.push.apply(newArgs, arguments);
                return recurring.apply(this, newArgs);
            };
        };
        /**
         * @param {?} context
         * @param {?} off
         * @param {?} xy
         * @return {?}
         */
        self.r = function(context, off, xy) {
            self.r = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? bound : $goog$bindJs_$;
            return self.r.apply(null, arguments);
        };
        /**
         * @param {?} arg
         * @return {?}
         */
        self.ya = function(arg) {
            /** @type {string} */
            var type = typeof arg;
            return "object" == type && null != arg || "function" == type;
        };
        /**
         * @param {Function} object
         * @return {?}
         */
        self.za = function(object) {
            return "function" == self.va(object);
        };
        /**
         * @param {Object} value
         * @return {?}
         */
        self.Aa = function(value) {
            return "number" == typeof value;
        };
        /**
         * @param {string} arr
         * @return {?}
         */
        self.t = function(arr) {
            return "string" == typeof arr;
        };
        /**
         * @param {string} data
         * @return {?}
         */
        self.Ba = function(data) {
            var $type = self.va(data);
            return "array" == $type || "object" == $type && "number" == typeof data.length;
        };
        /**
         * @param {?} actual
         * @return {?}
         */
        self.Ca = function(actual) {
            return "array" == self.va(actual);
        };
        /**
         * @return {undefined}
         */
        self.Da = function() {
        };
        /**
         * @param {string} pair
         * @return {?}
         */
        self.Ea = function(pair) {
            pair = pair.split(".");
            var item = self.l;
            var root;
            for (;root = pair.shift();) {
                if (null != item[root]) {
                    item = item[root];
                } else {
                    return null;
                }
            }
            return item;
        };
        /**
         * @param {?} opt_msg
         * @return {undefined}
         */
        self.Fa = function(opt_msg) {
            if (Error.captureStackTrace) {
                Error.captureStackTrace(this, self.Fa);
            } else {
                /** @type {string} */
                var stack = Error().stack;
                if (stack) {
                    /** @type {string} */
                    this.stack = stack;
                }
            }
            if (opt_msg) {
                /** @type {string} */
                this.message = String(opt_msg);
            }
        };
        self.p(self.Fa, Error);
        /** @type {string} */
        self.Fa.prototype.name = "CustomError";
        var hasMembers;
        var escape;
        var r20;
        var rreturn;
        var regExclamation;
        var re;
        var regexTrim;
        var splatParam;
        var numbers;
        var equals;
        var posLess;
        /**
         * @param {string} str
         * @param {string} prefix
         * @return {?}
         */
        self.Ha = function(str, prefix) {
            return 0 == str.lastIndexOf(prefix, 0);
        };
        /**
         * @param {string} x
         * @param {string} type
         * @return {?}
         */
        self.Ia = function(x, type) {
            /** @type {number} */
            var id = x.length - type.length;
            return 0 <= id && x.indexOf(type, id) == id;
        };
        /**
         * @param {string} type
         * @return {?}
         */
        self.Ja = function(type) {
            return/^[\s\xa0]*$/.test(type);
        };
        /** @type {function (string): ?} */
        escape = String.prototype.trim ? function(buf) {
            return buf.trim();
        } : function(lastLine) {
            return lastLine.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "");
        };
        /**
         * @param {string} x
         * @return {?}
         */
        self.Sa = function(x) {
            if (!numbers.test(x)) {
                return x;
            }
            if (-1 != x.indexOf("&")) {
                x = x.replace(r20, "&amp;");
            }
            if (-1 != x.indexOf("<")) {
                x = x.replace(rreturn, "&lt;");
            }
            if (-1 != x.indexOf(">")) {
                x = x.replace(regExclamation, "&gt;");
            }
            if (-1 != x.indexOf('"')) {
                x = x.replace(re, "&quot;");
            }
            if (-1 != x.indexOf("'")) {
                x = x.replace(regexTrim, "&#39;");
            }
            if (-1 != x.indexOf("\x00")) {
                x = x.replace(splatParam, "&#0;");
            }
            return x;
        };
        /** @type {RegExp} */
        r20 = /&/g;
        /** @type {RegExp} */
        rreturn = /</g;
        /** @type {RegExp} */
        regExclamation = />/g;
        /** @type {RegExp} */
        re = /"/g;
        /** @type {RegExp} */
        regexTrim = /'/g;
        /** @type {RegExp} */
        splatParam = /\x00/g;
        /** @type {RegExp} */
        numbers = /[\x00&<>"']/;
        /** @type {function ((Object|string), number): ?} */
        equals = String.prototype.repeat ? function(opts, match) {
            return opts.repeat(match);
        } : function(delimit2, l) {
            return Array(l + 1).join(delimit2);
        };
        /**
         * @param {Object} r
         * @return {?}
         */
        self.Ua = function(r) {
            return null == r ? "" : String(r);
        };
        /**
         * @return {?}
         */
        self.Va = function() {
            return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ (0, self.sa)()).toString(36);
        };
        /**
         * @param {?} html
         * @param {string} obj
         * @return {?}
         */
        self.Xa = function(html, obj) {
            /** @type {number} */
            var year = 0;
            var v1Subs = escape(String(html)).split(".");
            var v2Subs = escape(String(obj)).split(".");
            /** @type {number} */
            var month = Math.max(v1Subs.length, v2Subs.length);
            /** @type {number} */
            var subIdx = 0;
            for (;0 == year && subIdx < month;subIdx++) {
                var value = v1Subs[subIdx] || "";
                var v2Sub = v2Subs[subIdx] || "";
                do {
                    /** @type {Array} */
                    value = /(\d*)(\D*)(.*)/.exec(value) || ["", "", "", ""];
                    /** @type {Array} */
                    v2Sub = /(\d*)(\D*)(.*)/.exec(v2Sub) || ["", "", "", ""];
                    if (0 == value[0].length && 0 == v2Sub[0].length) {
                        break;
                    }
                    year = posLess(0 == value[1].length ? 0 : (0, window.parseInt)(value[1], 10), 0 == v2Sub[1].length ? 0 : (0, window.parseInt)(v2Sub[1], 10)) || (posLess(0 == value[2].length, 0 == v2Sub[2].length) || posLess(value[2], v2Sub[2]));
                    value = value[3];
                    v2Sub = v2Sub[3];
                } while (0 == year);
            }
            return year;
        };
        /**
         * @param {(boolean|number|string)} a
         * @param {(boolean|number|string)} b
         * @return {?}
         */
        posLess = function(a, b) {
            return a < b ? -1 : a > b ? 1 : 0;
        };
        var func;
        /** @type {function (?, string, ?): ?} */
        func = Array.prototype.indexOf ? function(ary, object, graphics) {
            return Array.prototype.indexOf.call(ary, object, graphics);
        } : function(array, value, from) {
            from = null == from ? 0 : 0 > from ? Math.max(0, array.length + from) : from;
            if (self.t(array)) {
                return self.t(value) && 1 == value.length ? array.indexOf(value, from) : -1;
            }
            for (;from < array.length;from++) {
                if (from in array && array[from] === value) {
                    return from;
                }
            }
            return-1;
        };
        /** @type {function (string, Function, number): undefined} */
        self.u = Array.prototype.forEach ? function(x, type, expectedNumberOfNonCommentArgs) {
            Array.prototype.forEach.call(x, type, expectedNumberOfNonCommentArgs);
        } : function(x, type, expectedNumberOfNonCommentArgs) {
            var l = x.length;
            var arr2 = self.t(x) ? x.split("") : x;
            /** @type {number} */
            var i = 0;
            for (;i < l;i++) {
                if (i in arr2) {
                    type.call(expectedNumberOfNonCommentArgs, arr2[i], i, x);
                }
            }
        };
        /**
         * @param {string} arr
         * @param {Function} f
         * @return {undefined}
         */
        self.Za = function(arr, f) {
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = arr.length - 1;
            for (;0 <= i;--i) {
                if (i in arr2) {
                    f.call(void 0, arr2[i], i, arr);
                }
            }
        };
        /** @type {function (Object, ?, ?): ?} */
        self.$a = Array.prototype.filter ? function(a, mapper, graphics) {
            return Array.prototype.filter.call(a, mapper, graphics);
        } : function(arr, callback, T) {
            var e = arr.length;
            /** @type {Array} */
            var res = [];
            /** @type {number} */
            var resLength = 0;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2) {
                    var val = arr2[i];
                    if (callback.call(T, val, i, arr)) {
                        res[resLength++] = val;
                    }
                }
            }
            return res;
        };
        /** @type {function (?, ?, ?): ?} */
        self.ab = Array.prototype.map ? function(next_scope, mapper, graphics) {
            return Array.prototype.map.call(next_scope, mapper, graphics);
        } : function(arr, f, opt_obj) {
            var e = arr.length;
            /** @type {Array} */
            var res = Array(e);
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2) {
                    res[i] = f.call(opt_obj, arr2[i], i, arr);
                }
            }
            return res;
        };
        /** @type {function (?, ?, ?): ?} */
        self.bb = Array.prototype.some ? function(next_scope, mapper, graphics) {
            return Array.prototype.some.call(next_scope, mapper, graphics);
        } : function(arr, f, opt_obj) {
            var e = arr.length;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2 && f.call(opt_obj, arr2[i], i, arr)) {
                    return true;
                }
            }
            return false;
        };
        /**
         * @param {string} arr
         * @param {Function} callback
         * @return {?}
         */
        self.db = function(arr, callback) {
            var i = self.cb(arr, callback, void 0);
            return 0 > i ? null : self.t(arr) ? arr.charAt(i) : arr[i];
        };
        /**
         * @param {string} arr
         * @param {Function} f
         * @param {?} opt_obj
         * @return {?}
         */
        self.cb = function(arr, f, opt_obj) {
            var e = arr.length;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2 && f.call(opt_obj, arr2[i], i, arr)) {
                    return i;
                }
            }
            return-1;
        };
        /**
         * @param {?} data
         * @param {string} now
         * @return {?}
         */
        self.eb = function(data, now) {
            return 0 <= func(data, now);
        };
        /**
         * @param {?} value
         * @param {string} type
         * @return {?}
         */
        self.gb = function(value, type) {
            var n = func(value, type);
            var color;
            if (color = 0 <= n) {
                self.fb(value, n);
            }
            return color;
        };
        /**
         * @param {?} object
         * @param {number} i
         * @return {?}
         */
        self.fb = function(object, i) {
            return 1 == Array.prototype.splice.call(object, i, 1).length;
        };
        /**
         * @param {Array} object
         * @return {?}
         */
        self.hb = function(object) {
            return Array.prototype.concat.apply(Array.prototype, arguments);
        };
        /**
         * @param {Array} data
         * @return {?}
         */
        self.ib = function(data) {
            var n = data.length;
            if (0 < n) {
                /** @type {Array} */
                var e = Array(n);
                /** @type {number} */
                var i = 0;
                for (;i < n;i++) {
                    e[i] = data[i];
                }
                return e;
            }
            return[];
        };
        a: {
            var n = self.l.navigator;
            if (n) {
                var u = n.userAgent;
                if (u) {
                    self.jb = u;
                    break a;
                }
            }
            /** @type {string} */
            self.jb = "";
        }
        /**
         * @param {string} str
         * @return {?}
         */
        self.w = function(str) {
            return-1 != self.jb.indexOf(str);
        };
        var isEmpty;
        var indexOf;
        var vsprintf;
        var properties;
        /**
         * @param {Object} obj
         * @param {Function} fn
         * @param {?} elems
         * @return {undefined}
         */
        self.mb = function(obj, fn, elems) {
            var key;
            for (key in obj) {
                fn.call(elems, obj[key], key, obj);
            }
        };
        /**
         * @param {Object} obj
         * @param {Function} f
         * @return {?}
         */
        isEmpty = function(obj, f) {
            var key;
            for (key in obj) {
                if (f.call(void 0, obj[key], key, obj)) {
                    return true;
                }
            }
            return false;
        };
        /**
         * @param {Object} obj
         * @return {?}
         */
        indexOf = function(obj) {
            /** @type {Array} */
            var result = [];
            /** @type {number} */
            var ri = 0;
            var prop;
            for (prop in obj) {
                result[ri++] = obj[prop];
            }
            return result;
        };
        /**
         * @param {Array} argv
         * @return {?}
         */
        vsprintf = function(argv) {
            /** @type {Array} */
            var res = [];
            /** @type {number} */
            var resLength = 0;
            var key;
            for (key in argv) {
                /** @type {string} */
                res[resLength++] = key;
            }
            return res;
        };
        /**
         * @param {Object} helper
         * @return {?}
         */
        self.qb = function(helper) {
            var global = {};
            var key;
            for (key in helper) {
                global[key] = helper[key];
            }
            return global;
        };
        /** @type {Array.<string>} */
        properties = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
        /**
         * @param {Object} obj
         * @param {?} a
         * @return {undefined}
         */
        self.sb = function(obj, a) {
            var prop;
            var source;
            /** @type {number} */
            var i = 1;
            for (;i < arguments.length;i++) {
                source = arguments[i];
                for (prop in source) {
                    obj[prop] = source[prop];
                }
                /** @type {number} */
                var index = 0;
                for (;index < properties.length;index++) {
                    prop = properties[index];
                    if (Object.prototype.hasOwnProperty.call(source, prop)) {
                        obj[prop] = source[prop];
                    }
                }
            }
        };
        /**
         * @return {?}
         */
        var toVLQSigned = function() {
            return(self.w("Chrome") || self.w("CriOS")) && !self.w("Edge");
        };
        var fixedPosition;
        /**
         * @return {?}
         */
        fixedPosition = function() {
            return self.w("iPhone") && (!self.w("iPod") && !self.w("iPad"));
        };
        /**
         * @return {?}
         */
        self.vb = function() {
            return self.w("Macintosh");
        };
        /**
         * @return {?}
         */
        self.wb = function() {
            return self.w("Windows");
        };
        /**
         * @param {?} obj
         * @return {?}
         */
        var _ = function(obj) {
            _[" "](obj);
            return obj;
        };
        /** @type {function (): undefined} */
        _[" "] = self.Da;
        /**
         * @param {Object} type
         * @param {string} name
         * @return {?}
         */
        self.yb = function(type, name) {
            try {
                return _(type[name]), true;
            } catch (c) {
            }
            return false;
        };
        /**
         * @param {Object} result
         * @param {string} key
         * @param {Function} f
         * @return {?}
         */
        self.zb = function(result, key, f) {
            return Object.prototype.hasOwnProperty.call(result, key) ? result[key] : result[key] = f(key);
        };
        var versionOffset;
        var lastWallLeft;
        var navigator;
        var iframeCssFixes;
        var num;
        var arg;
        var expectationResult;
        var doc;
        var fromIndex;
        versionOffset = self.w("Opera");
        self.x = self.w("Trident") || self.w("MSIE");
        lastWallLeft = self.w("Edge");
        self.Cb = self.w("Gecko") && (!(-1 != self.jb.toLowerCase().indexOf("webkit") && !self.w("Edge")) && (!(self.w("Trident") || self.w("MSIE")) && !self.w("Edge")));
        /** @type {boolean} */
        self.Db = -1 != self.jb.toLowerCase().indexOf("webkit") && !self.w("Edge");
        navigator = self.l.navigator || null;
        self.Fb = navigator && navigator.platform || "";
        self.Gb = self.vb();
        self.Hb = self.wb();
        /**
         * @return {?}
         */
        iframeCssFixes = function() {
            var doc = self.l.document;
            return doc ? doc.documentMode : void 0;
        };
        a: {
            /** @type {string} */
            var desc = "";
            var namespaceMatch = function() {
                var xhtml = self.jb;
                if (self.Cb) {
                    return/rv\:([^\);]+)(\)|;)/.exec(xhtml);
                }
                if (lastWallLeft) {
                    return/Edge\/([\d\.]+)/.exec(xhtml);
                }
                if (self.x) {
                    return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(xhtml);
                }
                if (self.Db) {
                    return/WebKit\/(\S+)/.exec(xhtml);
                }
                if (versionOffset) {
                    return/(?:Version)[ \/]?(\S+)/.exec(xhtml);
                }
            }();
            if (namespaceMatch) {
                desc = namespaceMatch ? namespaceMatch[1] : "";
            }
            if (self.x) {
                var content = iframeCssFixes();
                if (null != content && content > (0, window.parseFloat)(desc)) {
                    /** @type {string} */
                    num = String(content);
                    break a;
                }
            }
            num = desc;
        }
        arg = num;
        expectationResult = {};
        /**
         * @param {string} length
         * @return {?}
         */
        self.Pb = function(length) {
            return self.zb(expectationResult, length, function() {
                return 0 <= self.Xa(arg, length);
            });
        };
        doc = self.l.document;
        fromIndex = doc && self.x ? iframeCssFixes() || ("CSS1Compat" == doc.compatMode ? (0, window.parseInt)(arg, 10) : 5) : void 0;
        var index = self.w("Firefox");
        var program = fixedPosition() || self.w("iPod");
        var inverse = self.w("iPad");
        var Vb = self.w("Android") && !(toVLQSigned() || (self.w("Firefox") || (self.w("Opera") || self.w("Silk"))));
        var vlq = toVLQSigned();
        var Xb = self.w("Safari") && (!(toVLQSigned() || (self.w("Coast") || (self.w("Opera") || (self.w("Edge") || (self.w("Silk") || self.w("Android")))))) && !(fixedPosition() || (self.w("iPad") || self.w("iPod"))));
        /** @type {null} */
        var props = null;
        var _FormParagraphs;
        var trim;
        var e;
        var str;
        var params;
        var rmultiDash;
        var iterator;
        /**
         * @param {string} text
         * @return {?}
         */
        _FormParagraphs = function(text) {
            return/^\s*$/.test(text) ? false : /^[\],:{}\s\u2028\u2029]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, "@").replace(/(?:"[^"\\\n\r\u2028\u2029\x00-\x08\x0a-\x1f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)[\s\u2028\u2029]*(?=:|,|]|}|$)/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""));
        };
        /**
         * @param {string} text
         * @return {?}
         */
        trim = function(text) {
            /** @type {string} */
            text = String(text);
            if (_FormParagraphs(text)) {
                try {
                    return eval("(" + text + ")");
                } catch (b) {
                }
            }
            throw Error("c`" + text);
        };
        /**
         * @param {Function} isXML
         * @param {Function} how
         * @return {?}
         */
        self.cc = function(isXML, how) {
            /** @type {Array} */
            var buf = [];
            str(new e(how), isXML, buf);
            return buf.join("");
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        e = function(a) {
            this.a = a;
        };
        /**
         * @param {Node} b
         * @param {Function} value
         * @param {Array} x
         * @return {undefined}
         */
        str = function(b, value, x) {
            if (null == value) {
                x.push("null");
            } else {
                if ("object" == typeof value) {
                    if (self.Ca(value)) {
                        /** @type {Function} */
                        var a = value;
                        value = a.length;
                        x.push("[");
                        /** @type {string} */
                        var c = "";
                        /** @type {number} */
                        var i = 0;
                        for (;i < value;i++) {
                            x.push(c);
                            c = a[i];
                            str(b, b.a ? b.a.call(a, String(i), c) : c, x);
                            /** @type {string} */
                            c = ",";
                        }
                        x.push("]");
                        return;
                    }
                    if (value instanceof String || (value instanceof Number || value instanceof Boolean)) {
                        /** @type {*} */
                        value = value.valueOf();
                    } else {
                        x.push("{");
                        /** @type {string} */
                        i = "";
                        for (a in value) {
                            if (Object.prototype.hasOwnProperty.call(value, a)) {
                                c = value[a];
                                if ("function" != typeof c) {
                                    x.push(i);
                                    iterator(a, x);
                                    x.push(":");
                                    str(b, b.a ? b.a.call(value, a, c) : c, x);
                                    /** @type {string} */
                                    i = ",";
                                }
                            }
                        }
                        x.push("}");
                        return;
                    }
                }
                switch(typeof value) {
                    case "string":
                        iterator(value, x);
                        break;
                    case "number":
                        x.push((0, window.isFinite)(value) && !(0, window.isNaN)(value) ? String(value) : "null");
                        break;
                    case "boolean":
                        x.push(String(value));
                        break;
                    case "function":
                        x.push("null");
                        break;
                    default:
                        throw Error("d`" + typeof value);;
                }
            }
        };
        params = {
            '"' : '\\"',
            "\\" : "\\\\",
            "/" : "\\/",
            "\b" : "\\b",
            "\f" : "\\f",
            "\n" : "\\n",
            "\r" : "\\r",
            "\t" : "\\t",
            "\x0B" : "\\u000b"
        };
        /** @type {RegExp} */
        rmultiDash = /\uffff/.test("\uffff") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g;
        /**
         * @param {string} key
         * @param {Array} value
         * @return {undefined}
         */
        iterator = function(key, value) {
            value.push('"', key.replace(rmultiDash, function(key) {
                var fn = params[key];
                if (!fn) {
                    /** @type {string} */
                    fn = "\\u" + (key.charCodeAt(0) | 65536).toString(16).substr(1);
                    /** @type {string} */
                    params[key] = fn;
                }
                return fn;
            }), '"');
        };
        var check;
        var conditional;
        var eql;
        /**
         * @return {undefined}
         */
        self.y = function() {
        };
        /** @type {boolean} */
        self.gc = "function" == typeof window.Uint8Array;
        /**
         * @param {Object} obj
         * @param {number} funcToCall
         * @param {string} value
         * @param {Object} recurring
         * @return {undefined}
         */
        self.z = function(obj, funcToCall, value, recurring) {
            /** @type {null} */
            obj.b = null;
            if (!funcToCall) {
                /** @type {Array} */
                funcToCall = value ? [value] : [];
            }
            /** @type {(string|undefined)} */
            obj.H = value ? String(value) : void 0;
            /** @type {number} */
            obj.h = 0 === value ? -1 : 0;
            /** @type {number} */
            obj.c = funcToCall;
            a: {
                if (obj.c.length && (funcToCall = obj.c.length - 1, (value = obj.c[funcToCall]) && ("object" == typeof value && (!self.Ca(value) && !(self.gc && value instanceof window.Uint8Array))))) {
                    /** @type {number} */
                    obj.j = funcToCall - obj.h;
                    /** @type {string} */
                    obj.g = value;
                    break a;
                }
                /** @type {number} */
                obj.j = Number.MAX_VALUE;
            }
            obj.A = {};
            if (recurring) {
                /** @type {number} */
                funcToCall = 0;
                for (;funcToCall < recurring.length;funcToCall++) {
                    value = recurring[funcToCall];
                    if (value < obj.j) {
                        value += obj.h;
                        obj.c[value] = obj.c[value] || self.hc;
                    } else {
                        obj.g[value] = obj.g[value] || self.hc;
                    }
                }
            }
        };
        /** @type {Array} */
        self.hc = [];
        /**
         * @param {string} x
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        self.A = function(x, expectedNumberOfNonCommentArgs) {
            if (expectedNumberOfNonCommentArgs < x.j) {
                var i = expectedNumberOfNonCommentArgs + x.h;
                var elt = x.c[i];
                return elt === self.hc ? x.c[i] = [] : elt;
            }
            elt = x.g[expectedNumberOfNonCommentArgs];
            return elt === self.hc ? x.g[expectedNumberOfNonCommentArgs] = [] : elt;
        };
        /**
         * @param {?} x
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {boolean} recurring
         * @return {undefined}
         */
        self.B = function(x, expectedNumberOfNonCommentArgs, recurring) {
            if (expectedNumberOfNonCommentArgs < x.j) {
                /** @type {boolean} */
                x.c[expectedNumberOfNonCommentArgs + x.h] = recurring;
            } else {
                /** @type {boolean} */
                x.g[expectedNumberOfNonCommentArgs] = recurring;
            }
        };
        /**
         * @param {string} x
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        self.C = function(x, type, expectedNumberOfNonCommentArgs) {
            if (!x.b) {
                x.b = {};
            }
            if (!x.b[expectedNumberOfNonCommentArgs]) {
                var item = self.A(x, expectedNumberOfNonCommentArgs);
                if (item) {
                    x.b[expectedNumberOfNonCommentArgs] = new type(item);
                }
            }
            return x.b[expectedNumberOfNonCommentArgs];
        };
        /**
         * @param {?} m
         * @param {number} index
         * @param {string} ll
         * @return {undefined}
         */
        self.E = function(m, index, ll) {
            if (!m.b) {
                m.b = {};
            }
            var recurring = ll ? self.D(ll) : ll;
            /** @type {string} */
            m.b[index] = ll;
            self.B(m, index, recurring);
        };
        /**
         * @param {string} object
         * @return {undefined}
         */
        check = function(object) {
            if (object.b) {
                var k;
                for (k in object.b) {
                    var ll = object.b[k];
                    if (self.Ca(ll)) {
                        /** @type {number} */
                        var i = 0;
                        for (;i < ll.length;i++) {
                            if (ll[i]) {
                                self.D(ll[i]);
                            }
                        }
                    } else {
                        if (ll) {
                            self.D(ll);
                        }
                    }
                }
            }
        };
        /**
         * @param {string} x
         * @return {?}
         */
        self.D = function(x) {
            check(x);
            return x.c;
        };
        conditional = self.l.JSON && self.l.JSON.stringify || "object" === typeof JSON && JSON.stringify;
        /** @type {function (): ?} */
        self.y.prototype.l = self.gc ? function() {
            /** @type {function (): ?} */
            var toJSON = window.Uint8Array.prototype.toJSON;
            /**
             * @return {?}
             */
            window.Uint8Array.prototype.toJSON = function() {
                if (!props) {
                    props = {};
                    /** @type {number} */
                    var p = 0;
                    for (;65 > p;p++) {
                        /** @type {string} */
                        props[p] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(p);
                    }
                }
                p = props;
                /** @type {Array} */
                var that = [];
                /** @type {number} */
                var len = 0;
                for (;len < this.length;len += 3) {
                    var i = this[len];
                    /** @type {boolean} */
                    var invoked = len + 1 < this.length;
                    var name = invoked ? this[len + 1] : 0;
                    /** @type {boolean} */
                    var perm = len + 2 < this.length;
                    var type = perm ? this[len + 2] : 0;
                    /** @type {number} */
                    var key = i >> 2;
                    /** @type {number} */
                    i = (i & 3) << 4 | name >> 4;
                    /** @type {number} */
                    name = (name & 15) << 2 | type >> 6;
                    /** @type {number} */
                    type = type & 63;
                    if (!perm) {
                        /** @type {number} */
                        type = 64;
                        if (!invoked) {
                            /** @type {number} */
                            name = 64;
                        }
                    }
                    that.push(p[key], p[i], p[name], p[type]);
                }
                return that.join("");
            };
            try {
                var b = conditional.call(null, self.D(this), serialize);
            } finally {
                /** @type {function (): ?} */
                window.Uint8Array.prototype.toJSON = toJSON;
            }
            return b;
        } : conditional ? function() {
            return conditional.call(null, self.D(this), serialize);
        } : function() {
            return self.cc(self.D(this), serialize);
        };
        /**
         * @param {?} opt
         * @param {number} object
         * @return {?}
         */
        var serialize = function(opt, object) {
            if (self.Aa(object)) {
                if ((0, window.isNaN)(object)) {
                    return "NaN";
                }
                if (window.Infinity === object) {
                    return "Infinity";
                }
                if (-window.Infinity === object) {
                    return "-Infinity";
                }
            }
            return object;
        };
        /**
         * @return {?}
         */
        self.y.prototype.toString = function() {
            check(this);
            return this.c.toString();
        };
        /**
         * @param {Object} a
         * @param {Object} obj
         * @return {?}
         */
        eql = function(a, obj) {
            a = a || {};
            obj = obj || {};
            var testSource = {};
            var name;
            for (name in a) {
                /** @type {number} */
                testSource[name] = 0;
            }
            for (name in obj) {
                /** @type {number} */
                testSource[name] = 0;
            }
            for (name in testSource) {
                if (!self.lc(a[name], obj[name])) {
                    return false;
                }
            }
            return true;
        };
        /**
         * @param {Array} a
         * @param {Array} b
         * @return {?}
         */
        self.lc = function(a, b) {
            if (a == b) {
                return true;
            }
            if (!self.ya(a) || (!self.ya(b) || a.constructor != b.constructor)) {
                return false;
            }
            if (self.gc && a.constructor === window.Uint8Array) {
                if (a.length != b.length) {
                    return false;
                }
                /** @type {number} */
                var i = 0;
                for (;i < a.length;i++) {
                    if (a[i] != b[i]) {
                        return false;
                    }
                }
                return true;
            }
            if (a.constructor === Array) {
                var ret = void 0;
                var initialValue = void 0;
                /** @type {number} */
                var padLength = Math.max(a.length, b.length);
                /** @type {number} */
                i = 0;
                for (;i < padLength;i++) {
                    var def = a[i];
                    var value = b[i];
                    if (def) {
                        if (def.constructor == Object) {
                            ret = def;
                            def = void 0;
                        }
                    }
                    if (value) {
                        if (value.constructor == Object) {
                            initialValue = value;
                            value = void 0;
                        }
                    }
                    if (!self.lc(def, value)) {
                        return false;
                    }
                }
                return ret || initialValue ? (ret = ret || {}, initialValue = initialValue || {}, eql(ret, initialValue)) : true;
            }
            if (a.constructor === Object) {
                return eql(a, b);
            }
            throw Error("e");
        };
        /**
         * @return {undefined}
         */
        self.F = function() {
            this.K = this.K;
            this.C = this.C;
        };
        /** @type {boolean} */
        self.F.prototype.K = false;
        /**
         * @return {undefined}
         */
        self.F.prototype.Z = function() {
            if (!this.K) {
                /** @type {boolean} */
                this.K = true;
                this.B();
            }
        };
        /**
         * @param {string} ll
         * @param {?} e
         * @return {undefined}
         */
        self.I = function(ll, e) {
            self.G(ll, self.xa(self.H, e));
        };
        /**
         * @param {string} x
         * @param {Function} type
         * @param {string} ll
         * @return {undefined}
         */
        self.G = function(x, type, ll) {
            if (x.K) {
                if (self.m(ll)) {
                    type.call(ll);
                } else {
                    type();
                }
            } else {
                if (!x.C) {
                    /** @type {Array} */
                    x.C = [];
                }
                x.C.push(self.m(ll) ? (0, self.r)(type, ll) : type);
            }
        };
        /**
         * @return {undefined}
         */
        self.F.prototype.B = function() {
            if (this.C) {
                for (;this.C.length;) {
                    this.C.shift()();
                }
            }
        };
        /**
         * @param {?} p
         * @return {undefined}
         */
        self.H = function(p) {
            if (p) {
                if ("function" == typeof p.Z) {
                    p.Z();
                }
            }
        };
        /** @type {boolean} */
        var err = !self.x || 9 <= Number(fromIndex);
        if (!(!self.Cb && !self.x)) {
            if (!(self.x && 9 <= Number(fromIndex))) {
                if (self.Cb) {
                    self.Pb("1.9.1");
                }
            }
        }
        if (self.x) {
            self.Pb("9");
        }
        /** @type {RegExp} */
        self.oc = /^(ar|ckb|dv|he|iw|fa|nqo|ps|sd|ug|ur|yi|.*[-_](Arab|Hebr|Thaa|Nkoo|Tfng))(?!.*[-_](Latn|Cyrl)($|-|_))($|-|_)/i;
        /**
         * @return {undefined}
         */
        self.qc = function() {
            /** @type {string} */
            this.a = "";
            this.c = self.pc;
            /** @type {null} */
            this.b = null;
        };
        /** @type {boolean} */
        self.qc.prototype.Wa = true;
        /**
         * @return {?}
         */
        self.qc.prototype.La = function() {
            return this.b;
        };
        /** @type {boolean} */
        self.qc.prototype.Xa = true;
        /**
         * @return {?}
         */
        self.qc.prototype.Va = function() {
            return this.a;
        };
        self.pc = {};
        /**
         * @param {string} n
         * @param {number} recurring
         * @return {?}
         */
        self.rc = function(n, recurring) {
            var params = new self.qc;
            /** @type {string} */
            params.a = n;
            /** @type {number} */
            params.b = recurring;
            return params;
        };
        self.rc("<!DOCTYPE html>", 0);
        self.rc("", 0);
        self.sc = self.rc("<br>", 0);
        var names;
        var init;
        var makeArray;
        /**
         * @param {?} target
         * @return {?}
         */
        self.vc = function(target) {
            return target ? new self.tc(self.uc(target)) : hasMembers || (hasMembers = new self.tc);
        };
        /**
         * @param {string} ll
         * @param {Node} ctx
         * @return {?}
         */
        self.xc = function(ll, ctx) {
            var context = ctx || window.document;
            /** @type {null} */
            var d = null;
            if (context.getElementsByClassName) {
                d = context.getElementsByClassName(ll)[0];
            } else {
                if (context.querySelectorAll && context.querySelector) {
                    d = context.querySelector("." + ll);
                } else {
                    d = self.wc(window.document, "*", ll, ctx)[0];
                }
            }
            return d || null;
        };
        /**
         * @param {Object} root
         * @param {string} tagName
         * @param {string} ll
         * @param {Object} arrayLike
         * @return {?}
         */
        self.wc = function(root, tagName, ll, arrayLike) {
            root = arrayLike || root;
            tagName = tagName && "*" != tagName ? tagName.toUpperCase() : "";
            if (root.querySelectorAll && (root.querySelector && (tagName || ll))) {
                return root.querySelectorAll(tagName + (ll ? "." + ll : ""));
            }
            if (ll && root.getElementsByClassName) {
                root = root.getElementsByClassName(ll);
                if (tagName) {
                    arrayLike = {};
                    /** @type {number} */
                    var len = 0;
                    /** @type {number} */
                    var i = 0;
                    var el;
                    for (;el = root[i];i++) {
                        if (tagName == el.nodeName) {
                            arrayLike[len++] = el;
                        }
                    }
                    /** @type {number} */
                    arrayLike.length = len;
                    return arrayLike;
                }
                return root;
            }
            root = root.getElementsByTagName(tagName || "*");
            if (ll) {
                arrayLike = {};
                /** @type {number} */
                i = len = 0;
                for (;el = root[i];i++) {
                    tagName = el.className;
                    if ("function" == typeof tagName.split) {
                        if (self.eb(tagName.split(/\s+/), ll)) {
                            arrayLike[len++] = el;
                        }
                    }
                }
                /** @type {number} */
                arrayLike.length = len;
                return arrayLike;
            }
            return root;
        };
        /**
         * @param {Element} element
         * @param {string} part
         * @return {undefined}
         */
        self.zc = function(element, part) {
            self.mb(part, function(val, key) {
                if ("style" == key) {
                    element.style.cssText = val;
                } else {
                    if ("class" == key) {
                        element.className = val;
                    } else {
                        if ("for" == key) {
                            element.htmlFor = val;
                        } else {
                            if (names.hasOwnProperty(key)) {
                                element.setAttribute(names[key], val);
                            } else {
                                if (self.Ha(key, "aria-") || self.Ha(key, "data-")) {
                                    element.setAttribute(key, val);
                                } else {
                                    element[key] = val;
                                }
                            }
                        }
                    }
                }
            });
        };
        names = {
            cellpadding : "cellPadding",
            cellspacing : "cellSpacing",
            colspan : "colSpan",
            frameborder : "frameBorder",
            height : "height",
            maxlength : "maxLength",
            nonce : "nonce",
            role : "role",
            rowspan : "rowSpan",
            type : "type",
            usemap : "useMap",
            valign : "vAlign",
            width : "width"
        };
        /**
         * @param {Document} doc
         * @param {Node} parent
         * @param {Object} args
         * @return {undefined}
         */
        init = function(doc, parent, args) {
            /**
             * @param {string} child
             * @return {undefined}
             */
            function add(child) {
                if (child) {
                    parent.appendChild(self.t(child) ? doc.createTextNode(child) : child);
                }
            }
            /** @type {number} */
            var i = 2;
            for (;i < args.length;i++) {
                var arg = args[i];
                if (!self.Ba(arg) || self.ya(arg) && 0 < arg.nodeType) {
                    add(arg);
                } else {
                    (0, self.u)(makeArray(arg) ? self.ib(arg) : arg, add);
                }
            }
        };
        /**
         * @param {HTMLElement} parent
         * @return {undefined}
         */
        self.Cc = function(parent) {
            var node;
            for (;node = parent.firstChild;) {
                parent.removeChild(node);
            }
        };
        /**
         * @param {Object} el
         * @return {?}
         */
        self.uc = function(el) {
            return 9 == el.nodeType ? el : el.ownerDocument || el.document;
        };
        /**
         * @param {Object} arg
         * @return {?}
         */
        makeArray = function(arg) {
            if (arg && "number" == typeof arg.length) {
                if (self.ya(arg)) {
                    return "function" == typeof arg.item || "string" == typeof arg.item;
                }
                if (self.za(arg)) {
                    return "function" == typeof arg.item;
                }
            }
            return false;
        };
        /**
         * @param {(Document|string)} a
         * @return {undefined}
         */
        self.tc = function(a) {
            this.a = a || (self.l.document || window.document);
        };
        self.g = self.tc.prototype;
        /** @type {function (?): ?} */
        self.g.ya = self.vc;
        /**
         * @param {string} arg
         * @return {?}
         */
        self.g.F = function(arg) {
            return self.t(arg) ? this.a.getElementById(arg) : arg;
        };
        /**
         * @param {?} dataAndEvents
         * @param {?} deepDataAndEvents
         * @param {?} ignoreMethodDoesntExist
         * @return {?}
         */
        self.g.na = function(dataAndEvents, deepDataAndEvents, ignoreMethodDoesntExist) {
            var doc = this.a;
            /** @type {Arguments} */
            var args = arguments;
            /** @type {string} */
            var data = String(args[0]);
            var body = args[1];
            if (!err && (body && (body.name || body.type))) {
                /** @type {Array} */
                data = ["<", data];
                if (body.name) {
                    data.push(' name="', self.Sa(body.name), '"');
                }
                if (body.type) {
                    data.push(' type="', self.Sa(body.type), '"');
                    var part = {};
                    self.sb(part, body);
                    delete part.type;
                    body = part;
                }
                data.push(">");
                /** @type {string} */
                data = data.join("");
            }
            data = doc.createElement(data);
            if (body) {
                if (self.t(body)) {
                    data.className = body;
                } else {
                    if (self.Ca(body)) {
                        data.className = body.join(" ");
                    } else {
                        self.zc(data, body);
                    }
                }
            }
            if (2 < args.length) {
                init(doc, data, args);
            }
            return data;
        };
        /**
         * @param {?} node
         * @param {?} childNode
         * @return {undefined}
         */
        self.g.appendChild = function(node, childNode) {
            node.appendChild(childNode);
        };
        /**
         * @param {undefined} parent
         * @param {Object} descendant
         * @return {?}
         */
        self.g.contains = function(parent, descendant) {
            if (!parent || !descendant) {
                return false;
            }
            if (parent.contains && 1 == descendant.nodeType) {
                return parent == descendant || parent.contains(descendant);
            }
            if ("undefined" != typeof parent.compareDocumentPosition) {
                return parent == descendant || !!(parent.compareDocumentPosition(descendant) & 16);
            }
            for (;descendant && parent != descendant;) {
                descendant = descendant.parentNode;
            }
            return descendant == parent;
        };
        self.Dc = function(recurring) {
            return function() {
                return recurring;
            };
        }(false);
        var s = "StopIteration" in self.l ? self.l.StopIteration : {
            message : "StopIteration",
            stack : ""
        };
        /**
         * @return {undefined}
         */
        var Stream = function() {
        };
        /**
         * @return {?}
         */
        Stream.prototype.next = function() {
            throw s;
        };
        /**
         * @return {?}
         */
        Stream.prototype.ia = function() {
            return this;
        };
        /**
         * @param {?} data
         * @return {?}
         */
        var makeCallback = function(data) {
            if (data instanceof Stream) {
                return data;
            }
            if ("function" == typeof data.ia) {
                return data.ia(false);
            }
            if (self.Ba(data)) {
                /** @type {number} */
                var pos = 0;
                var stream = new Stream;
                /**
                 * @return {?}
                 */
                stream.next = function() {
                    for (;;) {
                        if (pos >= data.length) {
                            throw s;
                        }
                        if (pos in data) {
                            return data[pos++];
                        }
                        pos++;
                    }
                };
                return stream;
            }
            throw Error("f");
        };
        /**
         * @param {Object} callback
         * @param {Function} error
         * @return {undefined}
         */
        var query = function(callback, error) {
            if (self.Ba(callback)) {
                try {
                    (0, self.u)(callback, error, void 0);
                } catch (c) {
                    if (c !== s) {
                        throw c;
                    }
                }
            } else {
                callback = makeCallback(callback);
                try {
                    for (;;) {
                        error.call(void 0, callback.next(), void 0, callback);
                    }
                } catch (c) {
                    if (c !== s) {
                        throw c;
                    }
                }
            }
        };
        /**
         * @param {Object} callback
         * @return {?}
         */
        var children = function(callback) {
            if (self.Ba(callback)) {
                return self.ib(callback);
            }
            callback = makeCallback(callback);
            /** @type {Array} */
            var children = [];
            query(callback, function(child) {
                children.push(child);
            });
            return children;
        };
        /**
         * @param {Object} val
         * @param {?} a
         * @return {undefined}
         */
        self.J = function(val, a) {
            this.b = {};
            /** @type {Array} */
            this.a = [];
            /** @type {number} */
            this.g = this.c = 0;
            /** @type {number} */
            var n = arguments.length;
            if (1 < n) {
                if (n % 2) {
                    throw Error("b");
                }
                /** @type {number} */
                var idx = 0;
                for (;idx < n;idx += 2) {
                    this.set(arguments[idx], arguments[idx + 1]);
                }
            } else {
                if (val) {
                    if (val instanceof self.J) {
                        n = val.X();
                        idx = val.V();
                    } else {
                        n = vsprintf(val);
                        idx = indexOf(val);
                    }
                    /** @type {number} */
                    var i = 0;
                    for (;i < n.length;i++) {
                        this.set(n[i], idx[i]);
                    }
                }
            }
        };
        /**
         * @return {?}
         */
        self.J.prototype.V = function() {
            each(this);
            /** @type {Array} */
            var eventPath = [];
            /** @type {number} */
            var i = 0;
            for (;i < this.a.length;i++) {
                eventPath.push(this.b[this.a[i]]);
            }
            return eventPath;
        };
        /**
         * @return {?}
         */
        self.J.prototype.X = function() {
            each(this);
            return this.a.concat();
        };
        /**
         * @param {?} item
         * @param {string} name
         * @return {?}
         */
        self.Lc = function(item, name) {
            return hasOwnProperty(item.b, name);
        };
        /**
         * @return {undefined}
         */
        self.J.prototype.clear = function() {
            this.b = {};
            /** @type {number} */
            this.g = this.c = this.a.length = 0;
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.J.prototype.remove = function(name) {
            return hasOwnProperty(this.b, name) ? (delete this.b[name], this.c--, this.g++, this.a.length > 2 * this.c && each(this), true) : false;
        };
        /**
         * @param {Object} args
         * @return {undefined}
         */
        var each = function(args) {
            if (args.c != args.a.length) {
                /** @type {number} */
                var i = 0;
                /** @type {number} */
                var len = 0;
                for (;i < args.a.length;) {
                    var type = args.a[i];
                    if (hasOwnProperty(args.b, type)) {
                        args.a[len++] = type;
                    }
                    i++;
                }
                /** @type {number} */
                args.a.length = len;
            }
            if (args.c != args.a.length) {
                var tests = {};
                /** @type {number} */
                len = i = 0;
                for (;i < args.a.length;) {
                    type = args.a[i];
                    if (!hasOwnProperty(tests, type)) {
                        args.a[len++] = type;
                        /** @type {number} */
                        tests[type] = 1;
                    }
                    i++;
                }
                /** @type {number} */
                args.a.length = len;
            }
        };
        /**
         * @param {string} name
         * @param {?} obj
         * @return {?}
         */
        self.J.prototype.get = function(name, obj) {
            return hasOwnProperty(this.b, name) ? this.b[name] : obj;
        };
        /**
         * @param {string} name
         * @param {?} s
         * @return {undefined}
         */
        self.J.prototype.set = function(name, s) {
            if (!hasOwnProperty(this.b, name)) {
                this.c++;
                this.a.push(name);
                this.g++;
            }
            this.b[name] = s;
        };
        /**
         * @param {Function} fn
         * @param {Object} bind
         * @return {undefined}
         */
        self.J.prototype.forEach = function(fn, bind) {
            var codeSegments = this.X();
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                var rvar = codeSegments[i];
                var val = this.get(rvar);
                fn.call(bind, val, rvar, this);
            }
        };
        /**
         * @param {boolean} opt_keys
         * @return {?}
         */
        self.J.prototype.ia = function(opt_keys) {
            each(this);
            /** @type {number} */
            var completed = 0;
            var g = this.g;
            var color = this;
            var stream = new Stream;
            /**
             * @return {?}
             */
            stream.next = function() {
                if (g != color.g) {
                    throw Error("g");
                }
                if (completed >= color.a.length) {
                    throw s;
                }
                var key = color.a[completed++];
                return opt_keys ? key : color.b[key];
            };
            return stream;
        };
        /**
         * @param {?} object
         * @param {string} keepData
         * @return {?}
         */
        var hasOwnProperty = function(object, keepData) {
            return Object.prototype.hasOwnProperty.call(object, keepData);
        };
        var send;
        /**
         * @param {string} buffer
         * @return {?}
         */
        self.Mc = function(buffer) {
            if (buffer.V && "function" == typeof buffer.V) {
                return buffer.V();
            }
            if (self.t(buffer)) {
                return buffer.split("");
            }
            if (self.Ba(buffer)) {
                /** @type {Array} */
                var results = [];
                var numberOfChannels = buffer.length;
                /** @type {number} */
                var j = 0;
                for (;j < numberOfChannels;j++) {
                    results.push(buffer[j]);
                }
                return results;
            }
            return indexOf(buffer);
        };
        /**
         * @param {(Array|string)} data
         * @param {Function} fn
         * @return {undefined}
         */
        send = function(data, fn) {
            if (data.forEach && "function" == typeof data.forEach) {
                data.forEach(fn, void 0);
            } else {
                if (self.Ba(data) || self.t(data)) {
                    (0, self.u)(data, fn, void 0);
                } else {
                    var attrs;
                    if (data.X && "function" == typeof data.X) {
                        attrs = data.X();
                    } else {
                        if (data.V && "function" == typeof data.V) {
                            attrs = void 0;
                        } else {
                            if (self.Ba(data) || self.t(data)) {
                                /** @type {Array} */
                                attrs = [];
                                var items = data.length;
                                /** @type {number} */
                                var i = 0;
                                for (;i < items;i++) {
                                    attrs.push(i);
                                }
                            } else {
                                attrs = vsprintf(data);
                            }
                        }
                    }
                    items = self.Mc(data);
                    i = items.length;
                    /** @type {number} */
                    var key = 0;
                    for (;key < i;key++) {
                        fn.call(void 0, items[key], attrs && attrs[key], data);
                    }
                }
            }
        };
        /**
         * @param {?} c
         * @param {?} h
         * @param {?} g
         * @return {undefined}
         */
        var Matrix = function(c, h, g) {
            this.g = g;
            this.c = c;
            this.h = h;
            /** @type {number} */
            this.b = 0;
            /** @type {null} */
            this.a = null;
        };
        /**
         * @return {?}
         */
        Matrix.prototype.get = function() {
            var a;
            if (0 < this.b) {
                this.b--;
                a = this.a;
                this.a = a.next;
                /** @type {null} */
                a.next = null;
            } else {
                a = this.c();
            }
            return a;
        };
        /**
         * @param {Object} node
         * @param {string} ll
         * @return {undefined}
         */
        var post = function(node, ll) {
            node.h(ll);
            if (node.b < node.g) {
                node.b++;
                ll.next = node.a;
                /** @type {string} */
                node.a = ll;
            }
        };
        var finish;
        /** @type {Array} */
        self.Qc = [];
        /** @type {Array} */
        self.Rc = [];
        /** @type {boolean} */
        self.Sc = false;
        /**
         * @param {Function} onComplete
         * @return {undefined}
         */
        finish = function(onComplete) {
            /** @type {Function} */
            self.Qc[self.Qc.length] = onComplete;
            if (self.Sc) {
                /** @type {number} */
                var i = 0;
                for (;i < self.Rc.length;i++) {
                    onComplete((0, self.r)(self.Rc[i].b, self.Rc[i]));
                }
            }
        };
        var show;
        var makeIterator;
        var create;
        var createCallback;
        /**
         * @param {?} callback
         * @return {undefined}
         */
        show = function(callback) {
            self.l.setTimeout(function() {
                throw callback;
            }, 0);
        };
        /**
         * @param {?} done
         * @param {?} thisObj
         * @return {undefined}
         */
        self.Yc = function(done, thisObj) {
            var callback = done;
            if (thisObj) {
                callback = (0, self.r)(done, thisObj);
            }
            callback = createCallback(callback);
            if (!self.za(self.l.setImmediate) || self.l.Window && (self.l.Window.prototype && (!self.w("Edge") && self.l.Window.prototype.setImmediate == self.l.setImmediate))) {
                if (!makeIterator) {
                    makeIterator = create();
                }
                makeIterator(callback);
            } else {
                self.l.setImmediate(callback);
            }
        };
        /**
         * @return {?}
         */
        create = function() {
            var Channel = self.l.MessageChannel;
            if ("undefined" === typeof Channel) {
                if ("undefined" !== typeof window) {
                    if (window.postMessage) {
                        if (window.addEventListener) {
                            if (!self.w("Presto")) {
                                /**
                                 * @return {undefined}
                                 */
                                Channel = function() {
                                    var d = window.document.createElement("IFRAME");
                                    /** @type {string} */
                                    d.style.display = "none";
                                    /** @type {string} */
                                    d.src = "";
                                    window.document.documentElement.appendChild(d);
                                    var w = d.contentWindow;
                                    d = w.document;
                                    d.open();
                                    d.write("");
                                    d.close();
                                    /** @type {string} */
                                    var ll = "callImmediate" + Math.random();
                                    /** @type {string} */
                                    var cycle = "file:" == w.location.protocol ? "*" : w.location.protocol + "//" + w.location.host;
                                    d = (0, self.r)(function(event) {
                                        if (("*" == cycle || event.origin == cycle) && event.data == ll) {
                                            this.port1.onmessage();
                                        }
                                    }, this);
                                    w.addEventListener("message", d, false);
                                    this.port1 = {};
                                    this.port2 = {
                                        /**
                                         * @return {undefined}
                                         */
                                        postMessage : function() {
                                            w.postMessage(ll, cycle);
                                        }
                                    };
                                };
                            }
                        }
                    }
                }
            }
            if ("undefined" !== typeof Channel && (!self.w("Trident") && !self.w("MSIE"))) {
                var channel = new Channel;
                var node = {};
                var current = node;
                /**
                 * @return {undefined}
                 */
                channel.port1.onmessage = function() {
                    if (self.m(node.next)) {
                        node = node.next;
                        var start = node.gb;
                        /** @type {null} */
                        node.gb = null;
                        start();
                    }
                };
                return function(gb) {
                    current.next = {
                        gb : gb
                    };
                    current = current.next;
                    channel.port2.postMessage(0);
                };
            }
            return "undefined" !== typeof window.document && "onreadystatechange" in window.document.createElement("SCRIPT") ? function(fetchOnlyFunction) {
                var s = window.document.createElement("SCRIPT");
                /**
                 * @return {undefined}
                 */
                s.onreadystatechange = function() {
                    /** @type {null} */
                    s.onreadystatechange = null;
                    s.parentNode.removeChild(s);
                    /** @type {null} */
                    s = null;
                    fetchOnlyFunction();
                    /** @type {null} */
                    fetchOnlyFunction = null;
                };
                window.document.documentElement.appendChild(s);
            } : function(funcToCall) {
                self.l.setTimeout(funcToCall, 0);
            };
        };
        /**
         * @param {?} func
         * @return {?}
         */
        createCallback = function(func) {
            return func;
        };
        finish(function(dataAndEvents) {
            /** @type {Function} */
            createCallback = dataAndEvents;
        });
        /**
         * @return {undefined}
         */
        var abc = function() {
            /** @type {null} */
            this.b = this.a = null;
        };
        var code = new Matrix(function() {
            return new a;
        }, function(record) {
            record.reset();
        }, 100);
        /**
         * @return {?}
         */
        abc.prototype.remove = function() {
            /** @type {null} */
            var removed = null;
            if (this.a) {
                removed = this.a;
                this.a = this.a.next;
                if (!this.a) {
                    /** @type {null} */
                    this.b = null;
                }
                /** @type {null} */
                removed.next = null;
            }
            return removed;
        };
        /**
         * @return {undefined}
         */
        var a = function() {
            /** @type {null} */
            this.next = this.b = this.a = null;
        };
        /**
         * @param {string} name
         * @param {?} s
         * @return {undefined}
         */
        a.prototype.set = function(name, s) {
            /** @type {string} */
            this.a = name;
            this.b = s;
            /** @type {null} */
            this.next = null;
        };
        /**
         * @return {undefined}
         */
        a.prototype.reset = function() {
            /** @type {null} */
            this.next = this.b = this.a = null;
        };
        var foo;
        var resolvePromise;
        var dd;
        var container;
        var done;
        /**
         * @param {string} optgroup
         * @param {?} source
         * @return {undefined}
         */
        self.fd = function(optgroup, source) {
            if (!foo) {
                resolvePromise();
            }
            if (!dd) {
                foo();
                /** @type {boolean} */
                dd = true;
            }
            var me = container;
            var t = code.get();
            t.set(optgroup, source);
            if (me.b) {
                me.b.next = t;
            } else {
                me.a = t;
            }
            me.b = t;
        };
        /**
         * @return {undefined}
         */
        resolvePromise = function() {
            if (self.l.Promise && self.l.Promise.resolve) {
                var currentPromise = self.l.Promise.resolve(void 0);
                /**
                 * @return {undefined}
                 */
                foo = function() {
                    currentPromise.then(done);
                };
            } else {
                /**
                 * @return {undefined}
                 */
                foo = function() {
                    self.Yc(done);
                };
            }
        };
        /** @type {boolean} */
        dd = false;
        container = new abc;
        /**
         * @return {undefined}
         */
        done = function() {
            var params;
            for (;params = container.remove();) {
                try {
                    params.a.call(params.b);
                } catch (restoreScript) {
                    show(restoreScript);
                }
                post(code, params);
            }
            /** @type {boolean} */
            dd = false;
        };
        /**
         * @param {Function} a
         * @return {undefined}
         */
        var execute = function(a) {
            a.prototype.then = a.prototype.then;
            /** @type {boolean} */
            a.prototype.$goog_Thenable = true;
        };
        /**
         * @param {?} f
         * @return {?}
         */
        var tryIt = function(f) {
            if (!f) {
                return false;
            }
            try {
                return!!f.$goog_Thenable;
            } catch (b) {
                return false;
            }
        };
        var Map;
        var seg;
        var after;
        var constructor;
        var filter;
        var debug;
        var animate;
        var dump;
        var getData;
        /**
         * @param {?} next_callback
         * @param {?} next_scope
         * @return {undefined}
         */
        self.kd = function(next_callback, next_scope) {
            /** @type {number} */
            this.a = 0;
            this.C = void 0;
            /** @type {null} */
            this.g = this.b = this.c = null;
            /** @type {boolean} */
            this.h = this.j = false;
            if (next_callback != self.Da) {
                try {
                    var rvar = this;
                    next_callback.call(next_scope, function(walkers) {
                        debug(rvar, 2, walkers);
                    }, function(walkers) {
                        debug(rvar, 3, walkers);
                    });
                } catch (suiteView) {
                    debug(this, 3, suiteView);
                }
            }
        };
        /**
         * @return {undefined}
         */
        Map = function() {
            /** @type {null} */
            this.next = this.context = this.b = this.g = this.a = null;
            /** @type {boolean} */
            this.c = false;
        };
        /**
         * @return {undefined}
         */
        Map.prototype.reset = function() {
            /** @type {null} */
            this.context = this.b = this.g = this.a = null;
            /** @type {boolean} */
            this.c = false;
        };
        seg = new Matrix(function() {
            return new Map;
        }, function(record) {
            record.reset();
        }, 100);
        /**
         * @param {number} t
         * @param {number} b
         * @param {Object} context
         * @return {?}
         */
        after = function(t, b, context) {
            var self = seg.get();
            /** @type {number} */
            self.g = t;
            /** @type {number} */
            self.b = b;
            /** @type {Object} */
            self.context = context;
            return self;
        };
        /**
         * @param {Object} c
         * @return {?}
         */
        self.od = function(c) {
            if (c instanceof self.kd) {
                return c;
            }
            var rvar = new self.kd(self.Da);
            debug(rvar, 2, c);
            return rvar;
        };
        /**
         * @return {?}
         */
        self.qd = function() {
            var text;
            var b = new self.kd(function(textAlt) {
                text = textAlt;
            });
            return new _overlap_interval(b, text);
        };
        /**
         * @param {Function} onComplete
         * @param {?} options
         * @param {Object} next
         * @return {?}
         */
        self.kd.prototype.then = function(onComplete, options, next) {
            return self.rd(this, self.za(onComplete) ? onComplete : null, self.za(options) ? options : null, next);
        };
        execute(self.kd);
        /**
         * @param {?} id
         * @return {undefined}
         */
        self.kd.prototype.cancel = function(id) {
            if (0 == this.a) {
                self.fd(function() {
                    var typePattern = new type(id);
                    constructor(this, typePattern);
                }, this);
            }
        };
        /**
         * @param {Object} obj
         * @param {Object} args
         * @return {undefined}
         */
        constructor = function(obj, args) {
            if (0 == obj.a) {
                if (obj.c) {
                    var c = obj.c;
                    if (c.b) {
                        /** @type {number} */
                        var node = 0;
                        /** @type {null} */
                        var udataCur = null;
                        /** @type {null} */
                        var n = null;
                        var b = c.b;
                        for (;b && (b.c || (node++, b.a == obj && (udataCur = b), !(udataCur && 1 < node)));b = b.next) {
                            if (!udataCur) {
                                n = b;
                            }
                        }
                        if (udataCur) {
                            if (0 == c.a && 1 == node) {
                                constructor(c, args);
                            } else {
                                if (n) {
                                    node = n;
                                    if (node.next == c.g) {
                                        c.g = node;
                                    }
                                    node.next = node.next.next;
                                } else {
                                    getData(c);
                                }
                                addClass(c, udataCur, 3, args);
                            }
                        }
                    }
                    /** @type {null} */
                    obj.c = null;
                } else {
                    debug(obj, 3, args);
                }
            }
        };
        /**
         * @param {Object} p
         * @param {?} v
         * @return {undefined}
         */
        filter = function(p, v) {
            if (!p.b) {
                if (!(2 != p.a && 3 != p.a)) {
                    dump(p);
                }
            }
            if (p.g) {
                p.g.next = v;
            } else {
                p.b = v;
            }
            p.g = v;
        };
        /**
         * @param {boolean} c
         * @param {boolean} g
         * @param {boolean} context
         * @param {Object} right
         * @return {?}
         */
        self.rd = function(c, g, context, right) {
            var args = after(null, null, null);
            args.a = new self.kd(function(func, iterator) {
                args.g = g ? function(x) {
                    try {
                        var ll = g.call(right, x);
                        func(ll);
                    } catch (subscription) {
                        iterator(subscription);
                    }
                } : func;
                args.b = context ? function(x) {
                    try {
                        var ll = context.call(right, x);
                        if (!self.m(ll) && x instanceof type) {
                            iterator(x);
                        } else {
                            func(ll);
                        }
                    } catch (subscription) {
                        iterator(subscription);
                    }
                } : iterator;
            });
            /** @type {boolean} */
            args.a.c = c;
            filter(c, args);
            return args.a;
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        self.kd.prototype.v = function(x) {
            /** @type {number} */
            this.a = 0;
            debug(this, 2, x);
        };
        /**
         * @param {string} str
         * @return {undefined}
         */
        self.kd.prototype.w = function(str) {
            /** @type {number} */
            this.a = 0;
            debug(this, 3, str);
        };
        /**
         * @param {Object} o
         * @param {string} opt_attributes
         * @param {Object} obj
         * @return {undefined}
         */
        debug = function(o, opt_attributes, obj) {
            if (0 == o.a) {
                if (o === obj) {
                    /** @type {number} */
                    opt_attributes = 3;
                    /** @type {TypeError} */
                    obj = new TypeError("Promise cannot resolve to itself");
                }
                /** @type {number} */
                o.a = 1;
                if (!self.yd(obj, o.v, o.w, o)) {
                    /** @type {Object} */
                    o.C = obj;
                    /** @type {string} */
                    o.a = opt_attributes;
                    /** @type {null} */
                    o.c = null;
                    dump(o);
                    if (!(3 != opt_attributes)) {
                        if (!(obj instanceof type)) {
                            search(o, obj);
                        }
                    }
                }
            }
        };
        /**
         * @param {?} arg
         * @param {string} done
         * @param {Function} callback
         * @param {Object} obj
         * @return {?}
         */
        self.yd = function(arg, done, callback, obj) {
            if (arg instanceof self.kd) {
                return filter(arg, after(done || self.Da, callback || null, obj)), true;
            }
            if (tryIt(arg)) {
                return arg.then(done, callback, obj), true;
            }
            if (self.ya(arg)) {
                try {
                    var onComplete = arg.then;
                    if (self.za(onComplete)) {
                        return animate(arg, onComplete, done, callback, obj), true;
                    }
                } catch (kValue) {
                    return callback.call(obj, kValue), true;
                }
            }
            return false;
        };
        /**
         * @param {?} variable
         * @param {Function} onComplete
         * @param {Function} callback
         * @param {Function} fn
         * @param {Object} scope
         * @return {undefined}
         */
        animate = function(variable, onComplete, callback, fn, scope) {
            /** @type {boolean} */
            var f = false;
            /**
             * @param {?} node
             * @return {undefined}
             */
            var animate = function(node) {
                if (!f) {
                    /** @type {boolean} */
                    f = true;
                    callback.call(scope, node);
                }
            };
            /**
             * @param {?} el
             * @return {undefined}
             */
            var text = function(el) {
                if (!f) {
                    /** @type {boolean} */
                    f = true;
                    fn.call(scope, el);
                }
            };
            try {
                onComplete.call(variable, animate, text);
            } catch (passes) {
                text(passes);
            }
        };
        /**
         * @param {Object} msg
         * @return {undefined}
         */
        dump = function(msg) {
            if (!msg.j) {
                /** @type {boolean} */
                msg.j = true;
                self.fd(msg.l, msg);
            }
        };
        /**
         * @param {Node} node
         * @return {?}
         */
        getData = function(node) {
            /** @type {null} */
            var v = null;
            if (node.b) {
                v = node.b;
                node.b = v.next;
                /** @type {null} */
                v.next = null;
            }
            if (!node.b) {
                /** @type {null} */
                node.g = null;
            }
            return v;
        };
        /**
         * @return {undefined}
         */
        self.kd.prototype.l = function() {
            var udataCur;
            for (;udataCur = getData(this);) {
                addClass(this, udataCur, this.a, this.C);
            }
            /** @type {boolean} */
            this.j = false;
        };
        /**
         * @param {Object} c
         * @param {Object} value
         * @param {number} walkers
         * @param {Object} els
         * @return {undefined}
         */
        var addClass = function(c, value, walkers, els) {
            if (3 == walkers && (value.b && !value.c)) {
                for (;c && c.h;c = c.c) {
                    /** @type {boolean} */
                    c.h = false;
                }
            }
            if (value.a) {
                /** @type {null} */
                value.a.c = null;
                isArray(value, walkers, els);
            } else {
                try {
                    if (value.c) {
                        value.g.call(value.context);
                    } else {
                        isArray(value, walkers, els);
                    }
                } catch (event) {
                    eventTrigger.call(null, event);
                }
            }
            post(seg, value);
        };
        /**
         * @param {Object} a
         * @param {number} obj
         * @param {Object} o
         * @return {undefined}
         */
        var isArray = function(a, obj, o) {
            if (2 == obj) {
                a.g.call(a.context, o);
            } else {
                if (a.b) {
                    a.b.call(a.context, o);
                }
            }
        };
        /**
         * @param {Object} opts
         * @param {Object} event
         * @return {undefined}
         */
        var search = function(opts, event) {
            /** @type {boolean} */
            opts.h = true;
            self.fd(function() {
                if (opts.h) {
                    eventTrigger.call(null, event);
                }
            });
        };
        /** @type {function (?): undefined} */
        var eventTrigger = show;
        /**
         * @param {?} obj
         * @return {undefined}
         */
        var type = function(obj) {
            self.Fa.call(this, obj);
        };
        self.p(type, self.Fa);
        /** @type {string} */
        type.prototype.name = "cancel";
        /**
         * @param {?} a
         * @param {?} b
         * @return {undefined}
         */
        var _overlap_interval = function(a, b) {
            this.a = a;
            this.b = b;
        };
        /**
         * @param {Function} dataAndEvents
         * @param {string} deepDataAndEvents
         * @return {undefined}
         */
        self.Dd = function(dataAndEvents, deepDataAndEvents) {
            /** @type {Array} */
            this.l = [];
            /** @type {Function} */
            this.L = dataAndEvents;
            this.A = deepDataAndEvents || null;
            /** @type {boolean} */
            this.j = this.c = false;
            this.g = void 0;
            /** @type {boolean} */
            this.G = this.M = this.w = false;
            /** @type {number} */
            this.v = 0;
            /** @type {null} */
            this.a = null;
            /** @type {number} */
            this.C = 0;
        };
        /**
         * @param {?} evt
         * @return {undefined}
         */
        self.Dd.prototype.cancel = function(evt) {
            if (this.c) {
                if (this.g instanceof self.Dd) {
                    this.g.cancel();
                }
            } else {
                if (this.a) {
                    var item = this.a;
                    delete this.a;
                    if (evt) {
                        item.cancel(evt);
                    } else {
                        item.C--;
                        if (0 >= item.C) {
                            item.cancel();
                        }
                    }
                }
                if (this.L) {
                    this.L.call(this.A, this);
                } else {
                    /** @type {boolean} */
                    this.G = true;
                }
                if (!this.c) {
                    this.H(new self.Ed);
                }
            }
        };
        /**
         * @param {boolean} m
         * @param {?} n
         * @return {undefined}
         */
        self.Dd.prototype.I = function(m, n) {
            /** @type {boolean} */
            this.w = false;
            castShadows(this, m, n);
        };
        /**
         * @param {Object} item
         * @param {boolean} recurring
         * @param {?} t
         * @return {undefined}
         */
        var castShadows = function(item, recurring, t) {
            /** @type {boolean} */
            item.c = true;
            item.g = t;
            /** @type {boolean} */
            item.j = !recurring;
            draw(item);
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        var clear = function(obj) {
            if (obj.c) {
                if (!obj.G) {
                    throw new state;
                }
                /** @type {boolean} */
                obj.G = false;
            }
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        self.Dd.prototype.b = function(x) {
            clear(this);
            castShadows(this, true, x);
        };
        /**
         * @param {?} n
         * @return {undefined}
         */
        self.Dd.prototype.H = function(n) {
            clear(this);
            castShadows(this, false, n);
        };
        /**
         * @param {?} isSorted
         * @param {Function} g
         * @param {Node} scope
         * @return {undefined}
         */
        self.Kd = function(isSorted, g, scope) {
            self.Jd(isSorted, g, null, scope);
        };
        /**
         * @param {Object} item
         * @param {?} x
         * @param {Object} idx
         * @param {(Node|string)} date
         * @return {undefined}
         */
        self.Jd = function(item, x, idx, date) {
            item.l.push([x, idx, date]);
            if (item.c) {
                draw(item);
            }
        };
        /**
         * @param {Function} onComplete
         * @param {Object} fn
         * @param {Object} next
         * @return {?}
         */
        self.Dd.prototype.then = function(onComplete, fn, next) {
            var text;
            var rejectPromise;
            var me = new self.kd(function(textAlt, reject) {
                text = textAlt;
                rejectPromise = reject;
            });
            self.Jd(this, text, function(r) {
                if (r instanceof self.Ed) {
                    me.cancel();
                } else {
                    rejectPromise(r);
                }
            });
            return me.then(onComplete, fn, next);
        };
        execute(self.Dd);
        /**
         * @param {?} isSorted
         * @param {Object} data
         * @return {undefined}
         */
        self.Ld = function(isSorted, data) {
            if (data instanceof self.Dd) {
                self.Kd(isSorted, (0, self.r)(data.h, data));
            } else {
                self.Kd(isSorted, function() {
                    return data;
                });
            }
        };
        /**
         * @param {string} x
         * @return {?}
         */
        self.Dd.prototype.h = function(x) {
            var e = new self.Dd;
            self.Jd(this, e.b, e.H, e);
            if (x) {
                e.a = this;
                this.C++;
            }
            return e;
        };
        /**
         * @param {Object} argv
         * @return {?}
         */
        var line = function(argv) {
            return(0, self.bb)(argv.l, function(r) {
                return self.za(r[1]);
            });
        };
        /**
         * @param {Object} data
         * @return {undefined}
         */
        var draw = function(data) {
            if (data.v && (data.c && line(data))) {
                var v = data.v;
                var value = values[v];
                if (value) {
                    self.l.clearTimeout(value.a);
                    delete values[v];
                }
                /** @type {number} */
                data.v = 0;
            }
            if (data.a) {
                data.a.C--;
                delete data.a;
            }
            v = data.g;
            /** @type {boolean} */
            var url = value = false;
            for (;data.l.length && !data.w;) {
                var context = data.l.shift();
                var fn = context[0];
                var f = context[1];
                context = context[2];
                if (fn = data.j ? f : fn) {
                    try {
                        var ll = fn.call(context || data.A, v);
                        if (self.m(ll)) {
                            data.j = data.j && (ll == v || ll instanceof Error);
                            data.g = v = ll;
                        }
                        if (tryIt(v) || "function" === typeof self.l.Promise && v instanceof self.l.Promise) {
                            /** @type {boolean} */
                            url = true;
                            /** @type {boolean} */
                            data.w = true;
                        }
                    } catch (o1) {
                        v = o1;
                        /** @type {boolean} */
                        data.j = true;
                        if (!line(data)) {
                            /** @type {boolean} */
                            value = true;
                        }
                    }
                }
            }
            data.g = v;
            if (url) {
                ll = (0, self.r)(data.I, data, true);
                url = (0, self.r)(data.I, data, false);
                if (v instanceof self.Dd) {
                    self.Jd(v, ll, url);
                    /** @type {boolean} */
                    v.M = true;
                } else {
                    v.then(ll, url);
                }
            }
            if (value) {
                v = new Color(v);
                values[v.a] = v;
                data.v = v.a;
            }
        };
        /**
         * @return {undefined}
         */
        var state = function() {
            self.Fa.call(this);
        };
        self.p(state, self.Fa);
        /** @type {string} */
        state.prototype.message = "Deferred has already fired";
        /** @type {string} */
        state.prototype.name = "AlreadyCalledError";
        /**
         * @return {undefined}
         */
        self.Ed = function() {
            self.Fa.call(this);
        };
        self.p(self.Ed, self.Fa);
        /** @type {string} */
        self.Ed.prototype.message = "Deferred was canceled";
        /** @type {string} */
        self.Ed.prototype.name = "CanceledError";
        /**
         * @param {?} b
         * @return {undefined}
         */
        var Color = function(b) {
            this.a = self.l.setTimeout((0, self.r)(this.c, this), 0);
            this.b = b;
        };
        /**
         * @return {undefined}
         */
        Color.prototype.c = function() {
            delete values[this.a];
            throw this.b;
        };
        var values = {};
        /** @type {boolean} */
        var named = !self.x || 9 <= Number(fromIndex);
        var Qd = self.x && !self.Pb("9");
        if (!!self.Db) {
            self.Pb("528");
        }
        if (!(self.Cb && self.Pb("1.9b"))) {
            if (!(self.x && self.Pb("8"))) {
                if (!(versionOffset && self.Pb("9.5"))) {
                    if (self.Db) {
                        self.Pb("528");
                    }
                }
            }
        }
        if (!(self.Cb && !self.Pb("8"))) {
            if (self.x) {
                self.Pb("9");
            }
        }
        /**
         * @param {?} x
         * @param {Object} value
         * @return {undefined}
         */
        self.K = function(x, value) {
            this.type = "undefined" != typeof self.Rd && x instanceof self.Rd ? String(x) : x;
            this.b = this.target = value;
            /** @type {boolean} */
            this.g = false;
            /** @type {boolean} */
            this.lb = true;
        };
        /**
         * @return {undefined}
         */
        self.K.prototype.l = function() {
            /** @type {boolean} */
            this.g = true;
        };
        /**
         * @return {undefined}
         */
        self.K.prototype.c = function() {
            /** @type {boolean} */
            this.lb = false;
        };
        /**
         * @param {Object} e
         * @param {?} b
         * @return {undefined}
         */
        var listener = function(e, b) {
            self.K.call(this, e ? e.type : "");
            /** @type {null} */
            this.b = this.target = null;
            /** @type {number} */
            this.keyCode = 0;
            /** @type {boolean} */
            this.C = this.v = this.h = this.j = false;
            /** @type {null} */
            this.a = this.state = null;
            if (e) {
                this.type = e.type;
                this.target = e.target || e.srcElement;
                this.b = b;
                var rel = e.relatedTarget;
                if (rel) {
                    if (self.Cb) {
                        self.yb(rel, "nodeName");
                    }
                }
                this.keyCode = e.keyCode || 0;
                this.j = e.ctrlKey;
                this.h = e.altKey;
                this.v = e.shiftKey;
                this.C = e.metaKey;
                this.state = e.state;
                /** @type {Object} */
                this.a = e;
                if (e.defaultPrevented) {
                    this.c();
                }
            }
        };
        self.p(listener, self.K);
        /**
         * @return {undefined}
         */
        listener.prototype.l = function() {
            listener.D.l.call(this);
            if (this.a.stopPropagation) {
                this.a.stopPropagation();
            } else {
                /** @type {boolean} */
                this.a.cancelBubble = true;
            }
        };
        /**
         * @return {undefined}
         */
        listener.prototype.c = function() {
            listener.D.c.call(this);
            var ev = this.a;
            if (ev.preventDefault) {
                ev.preventDefault();
            } else {
                if (ev.returnValue = false, Qd) {
                    try {
                        if (ev.ctrlKey || 112 <= ev.keyCode && 123 >= ev.keyCode) {
                            /** @type {number} */
                            ev.keyCode = -1;
                        }
                    } catch (b) {
                    }
                }
            }
        };
        /** @type {string} */
        var functionName = "closure_listenable_" + (1E6 * Math.random() | 0);
        /**
         * @param {Object} obj
         * @return {?}
         */
        var isFunction = function(obj) {
            return!(!obj || !obj[functionName]);
        };
        /** @type {number} */
        var nextKey = 0;
        /**
         * @param {Function} opt_parent
         * @param {?} url
         * @param {?} type
         * @param {?} is_root
         * @param {?} $0
         * @return {undefined}
         */
        var Node = function(opt_parent, url, type, is_root, $0) {
            /** @type {Function} */
            this.listener = opt_parent;
            /** @type {null} */
            this.proxy = null;
            this.src = url;
            this.type = type;
            /** @type {boolean} */
            this.wa = !!is_root;
            this.Na = $0;
            /** @type {number} */
            this.key = ++nextKey;
            /** @type {boolean} */
            this.removed = this.Ja = false;
        };
        /**
         * @param {Object} listener
         * @return {undefined}
         */
        var wrap = function(listener) {
            /** @type {boolean} */
            listener.removed = true;
            /** @type {null} */
            listener.listener = null;
            /** @type {null} */
            listener.proxy = null;
            /** @type {null} */
            listener.src = null;
            /** @type {null} */
            listener.Na = null;
        };
        /**
         * @param {?} src
         * @return {undefined}
         */
        var Tile = function(src) {
            this.src = src;
            this.a = {};
            /** @type {number} */
            this.b = 0;
        };
        /**
         * @param {Object} n
         * @param {Object} prop
         * @param {string} name
         * @param {boolean} recurring
         * @param {?} event
         * @param {boolean} fn
         * @return {?}
         */
        var value = function(n, prop, name, recurring, event, fn) {
            var i = prop.toString();
            prop = n.a[i];
            if (!prop) {
                /** @type {Array} */
                prop = n.a[i] = [];
                n.b++;
            }
            var p = trigger(prop, name, event, fn);
            if (-1 < p) {
                n = prop[p];
                if (!recurring) {
                    /** @type {boolean} */
                    n.Ja = false;
                }
            } else {
                n = new Node(name, n.src, i, !!event, fn);
                /** @type {boolean} */
                n.Ja = recurring;
                prop.push(n);
            }
            return n;
        };
        /**
         * @param {string} index
         * @param {number} name
         * @param {boolean} options
         * @param {boolean} element
         * @return {?}
         */
        Tile.prototype.remove = function(index, name, options, element) {
            index = index.toString();
            if (!(index in this.a)) {
                return false;
            }
            var frame = this.a[index];
            name = trigger(frame, name, options, element);
            return-1 < name ? (wrap(frame[name]), self.fb(frame, name), 0 == frame.length && (delete this.a[index], this.b--), true) : false;
        };
        /**
         * @param {Node} parent
         * @param {Object} fn
         * @return {undefined}
         */
        var setup = function(parent, fn) {
            var name = fn.type;
            if (name in parent.a) {
                if (self.gb(parent.a[name], fn)) {
                    wrap(fn);
                    if (0 == parent.a[name].length) {
                        delete parent.a[name];
                        parent.b--;
                    }
                }
            }
        };
        /**
         * @param {Object} dest
         * @return {?}
         */
        Tile.prototype.removeAll = function(dest) {
            dest = dest && dest.toString();
            /** @type {number} */
            var count = 0;
            var mat;
            for (mat in this.a) {
                if (!dest || mat == dest) {
                    var codeSegments = this.a[mat];
                    /** @type {number} */
                    var i = 0;
                    for (;i < codeSegments.length;i++) {
                        ++count;
                        wrap(codeSegments[i]);
                    }
                    delete this.a[mat];
                    this.b--;
                }
            }
            return count;
        };
        /**
         * @param {Object} input
         * @param {number} value
         * @param {?} index
         * @param {boolean} arg
         * @param {Element} name
         * @return {?}
         */
        var callback = function(input, value, index, arg, name) {
            input = input.a[value.toString()];
            /** @type {number} */
            value = -1;
            if (input) {
                value = trigger(input, index, arg, name);
            }
            return-1 < value ? input[value] : null;
        };
        /**
         * @param {Node} o
         * @return {?}
         */
        var compile = function(o) {
            /** @type {(string|undefined)} */
            var ll = self.m(void 0) ? "undefined" : void 0;
            var fn = self.m(ll);
            /** @type {string} */
            var typeName = fn ? ll.toString() : "";
            var easing = self.m(void 0);
            return isEmpty(o.a, function(codeSegments) {
                /** @type {number} */
                var i = 0;
                for (;i < codeSegments.length;++i) {
                    if (!(fn && codeSegments[i].type != typeName || easing && void 0 != codeSegments[i].wa)) {
                        return true;
                    }
                }
                return false;
            });
        };
        /**
         * @param {Array} data
         * @param {?} name
         * @param {boolean} args
         * @param {boolean} state
         * @return {?}
         */
        var trigger = function(data, name, args, state) {
            /** @type {number} */
            var i = 0;
            for (;i < data.length;++i) {
                var e = data[i];
                if (!e.removed && (e.listener == name && (e.wa == !!args && e.Na == state))) {
                    return i;
                }
            }
            return-1;
        };
        var _i;
        var args;
        var fe;
        var add;
        var delegate;
        var remove;
        var off;
        var bind;
        var cb;
        var loop;
        var extend;
        var prefix;
        var intersect;
        /** @type {string} */
        _i = "closure_lm_" + (1E6 * Math.random() | 0);
        args = {};
        /** @type {number} */
        fe = 0;
        /**
         * @param {Object} arg
         * @param {?} type
         * @param {string} idx
         * @param {?} opt_capt
         * @param {boolean} event
         * @return {?}
         */
        self.ge = function(arg, type, idx, opt_capt, event) {
            if (self.Ca(type)) {
                /** @type {number} */
                var i = 0;
                for (;i < type.length;i++) {
                    self.ge(arg, type[i], idx, opt_capt, event);
                }
                return null;
            }
            idx = intersect(idx);
            return isFunction(arg) ? arg.listen(type, idx, opt_capt, event) : add(arg, type, idx, false, opt_capt, event);
        };
        /**
         * @param {Object} src
         * @param {?} key
         * @param {Object} data
         * @param {Object} proxy
         * @param {?} listener
         * @param {boolean} selector
         * @return {?}
         */
        add = function(src, key, data, proxy, listener, selector) {
            if (!key) {
                throw Error("h");
            }
            /** @type {boolean} */
            var capture = !!listener;
            var d = extend(src);
            if (!d) {
                src[_i] = d = new Tile(src);
            }
            data = value(d, key, data, proxy, listener, selector);
            if (data.proxy) {
                return data;
            }
            proxy = delegate();
            /** @type {Object} */
            data.proxy = proxy;
            /** @type {Object} */
            proxy.src = src;
            /** @type {Object} */
            proxy.listener = data;
            if (src.addEventListener) {
                src.addEventListener(key.toString(), proxy, capture);
            } else {
                if (src.attachEvent) {
                    src.attachEvent(off(key.toString()), proxy);
                } else {
                    throw Error("i");
                }
            }
            fe++;
            return data;
        };
        /**
         * @return {?}
         */
        delegate = function() {
            var proxyCallbackFunction = loop;
            /** @type {function (?): ?} */
            var f = named ? function(eventObject) {
                return proxyCallbackFunction.call(f.src, f.listener, eventObject);
            } : function(index) {
                index = proxyCallbackFunction.call(f.src, f.listener, index);
                if (!index) {
                    return index;
                }
            };
            return f;
        };
        /**
         * @param {Object} view
         * @param {?} data
         * @param {Object} helpers
         * @param {?} node
         * @param {boolean} until
         * @return {?}
         */
        self.ne = function(view, data, helpers, node, until) {
            if (self.Ca(data)) {
                /** @type {number} */
                var i = 0;
                for (;i < data.length;i++) {
                    self.ne(view, data[i], helpers, node, until);
                }
                return null;
            }
            helpers = intersect(helpers);
            return isFunction(view) ? view.Ma(data, helpers, node, until) : add(view, data, helpers, true, node, until);
        };
        /**
         * @param {Object} o
         * @param {?} name
         * @param {?} model
         * @param {boolean} i
         * @param {Element} t
         * @return {undefined}
         */
        self.oe = function(o, name, model, i, t) {
            if (self.Ca(name)) {
                /** @type {number} */
                var c = 0;
                for (;c < name.length;c++) {
                    self.oe(o, name[c], model, i, t);
                }
            } else {
                model = intersect(model);
                if (isFunction(o)) {
                    o.ta(name, model, i, t);
                } else {
                    if (o) {
                        if (o = extend(o)) {
                            if (name = callback(o, name, model, !!i, t)) {
                                remove(name);
                            }
                        }
                    }
                }
            }
        };
        /**
         * @param {Object} listener
         * @return {undefined}
         */
        remove = function(listener) {
            if (self.Aa(listener) || (!listener || listener.removed)) {
                return;
            }
            var src = listener.src;
            if (isFunction(src)) {
                setup(src.c, listener);
                return;
            }
            var type = listener.type;
            var proxy = listener.proxy;
            if (src.removeEventListener) {
                src.removeEventListener(type, proxy, listener.wa);
            } else {
                if (src.detachEvent) {
                    src.detachEvent(off(type), proxy);
                }
            }
            fe--;
            if (type = extend(src)) {
                setup(type, listener);
                if (0 == type.b) {
                    /** @type {null} */
                    type.src = null;
                    /** @type {null} */
                    src[_i] = null;
                }
            } else {
                wrap(listener);
            }
        };
        /**
         * @param {string} name
         * @return {?}
         */
        off = function(name) {
            return name in args ? args[name] : args[name] = "on" + name;
        };
        /**
         * @param {number} opts
         * @param {Object} contents
         * @param {boolean} recurring
         * @param {?} type
         * @return {?}
         */
        bind = function(opts, contents, recurring, type) {
            /** @type {boolean} */
            var elem = true;
            if (opts = extend(opts)) {
                if (contents = opts.a[contents.toString()]) {
                    contents = contents.concat();
                    /** @type {number} */
                    opts = 0;
                    for (;opts < contents.length;opts++) {
                        var node = contents[opts];
                        if (node) {
                            if (node.wa == recurring) {
                                if (!node.removed) {
                                    node = cb(node, type);
                                    /** @type {boolean} */
                                    elem = elem && false !== node;
                                }
                            }
                        }
                    }
                }
            }
            return elem;
        };
        /**
         * @param {Object} node
         * @param {?} event
         * @return {?}
         */
        cb = function(node, event) {
            var l = node.listener;
            var who = node.Na || node.src;
            if (node.Ja) {
                remove(node);
            }
            return l.call(who, event);
        };
        /**
         * @param {Object} node
         * @param {string} step
         * @return {?}
         */
        loop = function(node, step) {
            if (node.removed) {
                return true;
            }
            if (!named) {
                var v = step || self.Ea("window.event");
                var t = new listener(v, this);
                /** @type {boolean} */
                var doCallBack = true;
                if (!(0 > v.keyCode || void 0 != v.returnValue)) {
                    a: {
                        /** @type {boolean} */
                        var current = false;
                        if (0 == v.keyCode) {
                            try {
                                /** @type {number} */
                                v.keyCode = -1;
                                break a;
                            } catch (n) {
                                /** @type {boolean} */
                                current = true;
                            }
                        }
                        if (current || void 0 == v.returnValue) {
                            /** @type {boolean} */
                            v.returnValue = true;
                        }
                    }
                    /** @type {Array} */
                    v = [];
                    current = t.b;
                    for (;current;current = current.parentNode) {
                        v.push(current);
                    }
                    current = node.type;
                    /** @type {number} */
                    var i = v.length - 1;
                    for (;!t.g && 0 <= i;i--) {
                        t.b = v[i];
                        var val = bind(v[i], current, true, t);
                        doCallBack = doCallBack && val;
                    }
                    /** @type {number} */
                    i = 0;
                    for (;!t.g && i < v.length;i++) {
                        t.b = v[i];
                        val = bind(v[i], current, false, t);
                        doCallBack = doCallBack && val;
                    }
                }
                return doCallBack;
            }
            return cb(node, new listener(step, this));
        };
        /**
         * @param {?} obj
         * @return {?}
         */
        extend = function(obj) {
            obj = obj[_i];
            return obj instanceof Tile ? obj : null;
        };
        /** @type {string} */
        prefix = "__closure_events_fn_" + (1E9 * Math.random() >>> 0);
        /**
         * @param {Function} a
         * @return {?}
         */
        intersect = function(a) {
            if (self.za(a)) {
                return a;
            }
            if (!a[prefix]) {
                /**
                 * @param {?} next_scope
                 * @return {?}
                 */
                a[prefix] = function(next_scope) {
                    return a.handleEvent(next_scope);
                };
            }
            return a[prefix];
        };
        finish(function(func) {
            loop = func(loop);
        });
        /**
         * @return {undefined}
         */
        self.L = function() {
            self.F.call(this);
            this.c = new Tile(this);
            this.ga = this;
            /** @type {null} */
            this.G = null;
        };
        self.p(self.L, self.F);
        /** @type {boolean} */
        self.L.prototype[functionName] = true;
        /**
         * @param {string} m
         * @return {undefined}
         */
        self.L.prototype.I = function(m) {
            /** @type {string} */
            this.G = m;
        };
        /**
         * @param {?} listener
         * @param {?} callback
         * @param {boolean} context
         * @param {Element} t
         * @return {undefined}
         */
        self.L.prototype.removeEventListener = function(listener, callback, context, t) {
            self.oe(this, listener, callback, context, t);
        };
        /**
         * @param {string} x
         * @param {Function} type
         * @return {?}
         */
        self.M = function(x, type) {
            var codeSegments;
            var src = x.G;
            if (src) {
                /** @type {Array} */
                codeSegments = [];
                for (;src;src = src.G) {
                    codeSegments.push(src);
                }
            }
            src = x.ga;
            /** @type {Function} */
            var e = type;
            var evt = e.type || e;
            if (self.t(e)) {
                e = new self.K(e, src);
            } else {
                if (e instanceof self.K) {
                    e.target = e.target || src;
                } else {
                    var ok = e;
                    e = new self.K(evt, src);
                    self.sb(e, ok);
                }
            }
            /** @type {boolean} */
            ok = true;
            var _this;
            if (codeSegments) {
                /** @type {number} */
                var i = codeSegments.length - 1;
                for (;!e.g && 0 <= i;i--) {
                    _this = e.b = codeSegments[i];
                    ok = _this.pa(evt, true, e) && ok;
                }
            }
            if (!e.g) {
                _this = e.b = src;
                ok = _this.pa(evt, true, e) && ok;
                if (!e.g) {
                    ok = _this.pa(evt, false, e) && ok;
                }
            }
            if (codeSegments) {
                /** @type {number} */
                i = 0;
                for (;!e.g && i < codeSegments.length;i++) {
                    _this = e.b = codeSegments[i];
                    ok = _this.pa(evt, false, e) && ok;
                }
            }
            return ok;
        };
        self.g = self.L.prototype;
        /**
         * @return {undefined}
         */
        self.g.B = function() {
            self.L.D.B.call(this);
            if (this.c) {
                this.c.removeAll(void 0);
            }
            /** @type {null} */
            this.G = null;
        };
        /**
         * @param {?} type
         * @param {string} name
         * @param {?} listener
         * @param {boolean} src
         * @return {?}
         */
        self.g.listen = function(type, name, listener, src) {
            return value(this.c, String(type), name, false, listener, src);
        };
        /**
         * @param {?} type
         * @param {string} data
         * @param {?} event
         * @param {boolean} arg
         * @return {?}
         */
        self.g.Ma = function(type, data, event, arg) {
            return value(this.c, String(type), data, true, event, arg);
        };
        /**
         * @param {?} target
         * @param {?} name
         * @param {boolean} id
         * @param {boolean} selector
         * @return {?}
         */
        self.g.ta = function(target, name, id, selector) {
            return this.c.remove(String(target), name, id, selector);
        };
        /**
         * @param {Array} a
         * @param {boolean} recurring
         * @param {?} eventObject
         * @return {?}
         */
        self.g.pa = function(a, recurring, eventObject) {
            a = this.c.a[String(a)];
            if (!a) {
                return true;
            }
            a = a.concat();
            /** @type {boolean} */
            var rv = true;
            /** @type {number} */
            var i = 0;
            for (;i < a.length;++i) {
                var listener = a[i];
                if (listener && (!listener.removed && listener.wa == recurring)) {
                    var listenerFn = listener.listener;
                    var listenerHandler = listener.Na || listener.src;
                    if (listener.Ja) {
                        setup(this.c, listener);
                    }
                    /** @type {boolean} */
                    rv = false !== listenerFn.call(listenerHandler, eventObject) && rv;
                }
            }
            return rv && 0 != eventObject.lb;
        };
        /**
         * @return {undefined}
         */
        var o = function() {
        };
        /** @type {null} */
        o.prototype.a = null;
        /**
         * @param {Node} data
         * @return {?}
         */
        var getJSON = function(data) {
            var a;
            if (!(a = data.a)) {
                a = {};
                if (request(data)) {
                    /** @type {boolean} */
                    a[0] = true;
                    /** @type {boolean} */
                    a[1] = true;
                }
                a = data.a = a;
            }
            return a;
        };
        var pdataCur;
        /**
         * @return {undefined}
         */
        var me = function() {
        };
        self.p(me, o);
        /**
         * @param {Element} data
         * @return {?}
         */
        var createRequest = function(data) {
            return(data = request(data)) ? new window.ActiveXObject(data) : new window.XMLHttpRequest;
        };
        /**
         * @param {Node} data
         * @return {?}
         */
        var request = function(data) {
            if (!data.b && ("undefined" == typeof window.XMLHttpRequest && "undefined" != typeof window.ActiveXObject)) {
                /** @type {Array} */
                var ca = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
                /** @type {number} */
                var i = 0;
                for (;i < ca.length;i++) {
                    var c = ca[i];
                    try {
                        return new window.ActiveXObject(c), data.b = c;
                    } catch (e) {
                    }
                }
                throw Error("j");
            }
            return data.b;
        };
        pdataCur = new me;
        /**
         * @param {number} h
         * @param {(Document|string)} g
         * @return {undefined}
         */
        self.ze = function(h, g) {
            self.L.call(this);
            this.h = h || 1;
            this.g = g || self.l;
            this.j = (0, self.r)(this.v, this);
            this.l = (0, self.sa)();
        };
        self.p(self.ze, self.L);
        /** @type {boolean} */
        self.ze.prototype.b = false;
        /** @type {null} */
        self.ze.prototype.a = null;
        /**
         * @param {?} funcToCall
         * @return {undefined}
         */
        self.ze.prototype.setInterval = function(funcToCall) {
            this.h = funcToCall;
            if (this.a && this.b) {
                this.stop();
                self.Ae(this);
            } else {
                if (this.a) {
                    this.stop();
                }
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.v = function() {
            if (this.b) {
                /** @type {number} */
                var y = (0, self.sa)() - this.l;
                if (0 < y && y < 0.8 * this.h) {
                    this.a = this.g.setTimeout(this.j, this.h - y);
                } else {
                    if (this.a) {
                        this.g.clearTimeout(this.a);
                        /** @type {null} */
                        this.a = null;
                    }
                    self.M(this, "tick");
                    if (this.b) {
                        this.a = this.g.setTimeout(this.j, this.h);
                        this.l = (0, self.sa)();
                    }
                }
            }
        };
        /**
         * @param {Object} exports
         * @return {undefined}
         */
        self.Ae = function(exports) {
            /** @type {boolean} */
            exports.b = true;
            if (!exports.a) {
                exports.a = exports.g.setTimeout(exports.j, exports.h);
                exports.l = (0, self.sa)();
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.stop = function() {
            /** @type {boolean} */
            this.b = false;
            if (this.a) {
                this.g.clearTimeout(this.a);
                /** @type {null} */
                this.a = null;
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.B = function() {
            self.ze.D.B.call(this);
            this.stop();
            delete this.g;
        };
        /**
         * @param {Function} callback
         * @param {number} time
         * @param {string} thisArg
         * @return {?}
         */
        self.Be = function(callback, time, thisArg) {
            if (self.za(callback)) {
                if (thisArg) {
                    callback = (0, self.r)(callback, thisArg);
                }
            } else {
                if (callback && "function" == typeof callback.handleEvent) {
                    callback = (0, self.r)(callback.handleEvent, callback);
                } else {
                    throw Error("k");
                }
            }
            return 2147483647 < Number(time) ? -1 : self.l.setTimeout(callback, time || 0);
        };
        /**
         * @param {?} timeoutKey
         * @return {undefined}
         */
        self.Ce = function(timeoutKey) {
            self.l.clearTimeout(timeoutKey);
        };
        /** @type {RegExp} */
        var delegateEventSplitter = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;
        /**
         * @param {string} src
         * @param {Function} callback
         * @return {undefined}
         */
        var handle = function(src, callback) {
            if (src) {
                var matches = src.split("&");
                /** @type {number} */
                var i = 0;
                for (;i < matches.length;i++) {
                    var indexOfEquals = matches[i].indexOf("=");
                    var name;
                    /** @type {null} */
                    var code = null;
                    if (0 <= indexOfEquals) {
                        name = matches[i].substring(0, indexOfEquals);
                        code = matches[i].substring(indexOfEquals + 1);
                    } else {
                        name = matches[i];
                    }
                    callback(name, code ? (0, window.decodeURIComponent)(code.replace(/\+/g, " ")) : "");
                }
            }
        };
        /**
         * @param {string} row_id
         * @return {undefined}
         */
        var item = function(row_id) {
            self.L.call(this);
            this.headers = new self.J;
            this.H = row_id || null;
            /** @type {boolean} */
            this.b = false;
            /** @type {null} */
            this.A = this.a = null;
            /** @type {string} */
            this.O = "";
            /** @type {number} */
            this.j = 0;
            /** @type {string} */
            this.h = "";
            /** @type {boolean} */
            this.g = this.M = this.v = this.L = false;
            /** @type {number} */
            this.l = 0;
            /** @type {null} */
            this.w = null;
            /** @type {string} */
            this.aa = "";
            /** @type {boolean} */
            this.T = this.W = false;
        };
        var regex;
        var camelKey;
        var initialize;
        var E;
        var set;
        var connect;
        var log;
        self.p(item, self.L);
        /** @type {RegExp} */
        regex = /^https?$/i;
        /** @type {Array} */
        camelKey = ["POST", "PUT"];
        /** @type {Array} */
        self.Ie = [];
        /**
         * @param {(number|string)} packet
         * @param {string} obj
         * @param {string} message
         * @param {string} msg
         * @param {string} port
         * @param {number} l
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        self.Je = function(packet, obj, message, msg, port, l, dataAndEvents) {
            var server = new item;
            self.Ie.push(server);
            if (obj) {
                server.listen("complete", obj);
            }
            server.Ma("ready", server.ha);
            if (l) {
                /** @type {number} */
                server.l = Math.max(0, l);
            }
            if (dataAndEvents) {
                /** @type {boolean} */
                server.W = dataAndEvents;
            }
            server.send(packet, message, msg, port);
        };
        /**
         * @return {undefined}
         */
        item.prototype.ha = function() {
            this.Z();
            self.gb(self.Ie, this);
        };
        /**
         * @param {number} data
         * @param {string} method
         * @param {string} payload
         * @param {(Array|string)} message
         * @return {undefined}
         */
        item.prototype.send = function(data, method, payload, message) {
            if (this.a) {
                throw Error("l`" + this.O + "`" + data);
            }
            method = method ? method.toUpperCase() : "GET";
            /** @type {number} */
            this.O = data;
            /** @type {string} */
            this.h = "";
            /** @type {number} */
            this.j = 0;
            /** @type {boolean} */
            this.L = false;
            /** @type {boolean} */
            this.b = true;
            this.a = this.H ? createRequest(this.H) : createRequest(pdataCur);
            this.A = this.H ? getJSON(this.H) : getJSON(pdataCur);
            this.a.onreadystatechange = (0, self.r)(this.$, this);
            try {
                /** @type {boolean} */
                this.M = true;
                this.a.open(method, String(data), true);
                /** @type {boolean} */
                this.M = false;
            } catch (oldconfig) {
                colorChanged(this, oldconfig);
                return;
            }
            data = payload || "";
            var res = new self.J(this.headers);
            if (message) {
                send(message, function(source, cookieName) {
                    res.set(cookieName, source);
                });
            }
            message = self.db(res.X(), onLoad);
            payload = self.l.FormData && data instanceof self.l.FormData;
            if (!!self.eb(camelKey, method)) {
                if (!message) {
                    if (!payload) {
                        res.set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                    }
                }
            }
            res.forEach(function(sValue, env) {
                this.a.setRequestHeader(env, sValue);
            }, this);
            if (this.aa) {
                this.a.responseType = this.aa;
            }
            if ("withCredentials" in this.a) {
                if (this.a.withCredentials !== this.W) {
                    this.a.withCredentials = this.W;
                }
            }
            try {
                set(this);
                if (0 < this.l) {
                    if (this.T = publish(this.a)) {
                        this.a.timeout = this.l;
                        this.a.ontimeout = (0, self.r)(this.Y, this);
                    } else {
                        this.w = self.Be(this.Y, this.l, this);
                    }
                }
                /** @type {boolean} */
                this.v = true;
                this.a.send(data);
                /** @type {boolean} */
                this.v = false;
            } catch (QUnit) {
                colorChanged(this, QUnit);
            }
        };
        /**
         * @param {?} config
         * @return {?}
         */
        var publish = function(config) {
            return self.x && (self.Pb(9) && (self.Aa(config.timeout) && self.m(config.ontimeout)));
        };
        /**
         * @param {Object} evt
         * @return {?}
         */
        var onLoad = function(evt) {
            return "content-type" == evt.toLowerCase();
        };
        /**
         * @return {undefined}
         */
        item.prototype.Y = function() {
            if ("undefined" != typeof self.qa) {
                if (this.a) {
                    /** @type {string} */
                    this.h = "Timed out after " + this.l + "ms, aborting";
                    /** @type {number} */
                    this.j = 8;
                    self.M(this, "timeout");
                    if (this.a) {
                        if (this.b) {
                            /** @type {boolean} */
                            this.b = false;
                            /** @type {boolean} */
                            this.g = true;
                            this.a.abort();
                            /** @type {boolean} */
                            this.g = false;
                            /** @type {number} */
                            this.j = 8;
                            self.M(this, "complete");
                            self.M(this, "abort");
                            E(this);
                        }
                    }
                }
            }
        };
        /**
         * @param {Object} me
         * @param {?} b
         * @return {undefined}
         */
        var colorChanged = function(me, b) {
            /** @type {boolean} */
            me.b = false;
            if (me.a) {
                /** @type {boolean} */
                me.g = true;
                me.a.abort();
                /** @type {boolean} */
                me.g = false;
            }
            me.h = b;
            /** @type {number} */
            me.j = 5;
            Deferred(me);
            E(me);
        };
        /**
         * @param {string} ll
         * @return {undefined}
         */
        var Deferred = function(ll) {
            if (!ll.L) {
                /** @type {boolean} */
                ll.L = true;
                self.M(ll, "complete");
                self.M(ll, "error");
            }
        };
        /**
         * @return {undefined}
         */
        item.prototype.B = function() {
            if (this.a) {
                if (this.b) {
                    /** @type {boolean} */
                    this.b = false;
                    /** @type {boolean} */
                    this.g = true;
                    this.a.abort();
                    /** @type {boolean} */
                    this.g = false;
                }
                E(this, true);
            }
            item.D.B.call(this);
        };
        /**
         * @return {undefined}
         */
        item.prototype.$ = function() {
            if (!this.K) {
                if (this.M || (this.v || this.g)) {
                    initialize(this);
                } else {
                    this.P();
                }
            }
        };
        /**
         * @return {undefined}
         */
        item.prototype.P = function() {
            initialize(this);
        };
        /**
         * @param {string} ll
         * @return {undefined}
         */
        initialize = function(ll) {
            if (ll.b && ("undefined" != typeof self.qa && (!ll.A[1] || (4 != connect(ll) || 2 != log(ll))))) {
                if (ll.v && 4 == connect(ll)) {
                    self.Be(ll.$, 0, ll);
                } else {
                    if (self.M(ll, "readystatechange"), 4 == connect(ll)) {
                        /** @type {boolean} */
                        ll.b = false;
                        try {
                            if (self.Te(ll)) {
                                self.M(ll, "complete");
                                self.M(ll, "success");
                            } else {
                                /** @type {number} */
                                ll.j = 6;
                                var expires;
                                try {
                                    expires = 2 < connect(ll) ? ll.a.statusText : "";
                                } catch (c) {
                                    /** @type {string} */
                                    expires = "";
                                }
                                /** @type {string} */
                                ll.h = expires + " [" + log(ll) + "]";
                                Deferred(ll);
                            }
                        } finally {
                            E(ll);
                        }
                    }
                }
            }
        };
        /**
         * @param {string} ll
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        E = function(ll, dataAndEvents) {
            if (ll.a) {
                set(ll);
                var el = ll.a;
                var successCallback = ll.A[0] ? self.Da : null;
                /** @type {null} */
                ll.a = null;
                /** @type {null} */
                ll.A = null;
                if (!dataAndEvents) {
                    self.M(ll, "ready");
                }
                try {
                    el.onreadystatechange = successCallback;
                } catch (e) {
                }
            }
        };
        /**
         * @param {Object} c
         * @return {undefined}
         */
        set = function(c) {
            if (c.a) {
                if (c.T) {
                    /** @type {null} */
                    c.a.ontimeout = null;
                }
            }
            if (self.Aa(c.w)) {
                self.Ce(c.w);
                /** @type {null} */
                c.w = null;
            }
        };
        /**
         * @param {string} x
         * @return {?}
         */
        self.Te = function(x) {
            var output = log(x);
            var fullPath;
            a: {
                switch(output) {
                    case 200:
                        ;
                    case 201:
                        ;
                    case 202:
                        ;
                    case 204:
                        ;
                    case 206:
                        ;
                    case 304:
                        ;
                    case 1223:
                        /** @type {boolean} */
                        fullPath = true;
                        break a;
                    default:
                        /** @type {boolean} */
                        fullPath = false;
                }
            }
            if (!fullPath) {
                if (output = 0 === output) {
                    /** @type {(null|string)} */
                    x = String(x.O).match(delegateEventSplitter)[1] || null;
                    if (!x) {
                        if (self.l.self) {
                            if (self.l.self.location) {
                                x = self.l.self.location.protocol;
                                x = x.substr(0, x.length - 1);
                            }
                        }
                    }
                    /** @type {boolean} */
                    output = !regex.test(x ? x.toLowerCase() : "");
                }
                /** @type {boolean} */
                fullPath = output;
            }
            return fullPath;
        };
        /**
         * @param {Node} object
         * @return {?}
         */
        connect = function(object) {
            return object.a ? object.a.readyState : 0;
        };
        /**
         * @param {Node} x
         * @return {?}
         */
        log = function(x) {
            try {
                return 2 < connect(x) ? x.a.status : -1;
            } catch (b) {
                return-1;
            }
        };
        /**
         * @param {Node} e
         * @return {?}
         */
        self.Ue = function(e) {
            try {
                return e.a ? e.a.responseText : "";
            } catch (b) {
                return "";
            }
        };
        finish(function($sanitize) {
            item.prototype.P = $sanitize(item.prototype.P);
        });
        var dispatch;
        /**
         * @param {string} val
         * @return {undefined}
         */
        self.N = function(val) {
            self.F.call(this);
            /** @type {string} */
            this.v = val;
            this.g = {};
        };
        self.p(self.N, self.F);
        /** @type {Array} */
        var pkgName = [];
        /**
         * @param {?} type
         * @param {string} name
         * @param {?} opt_fn
         * @param {boolean} callback
         * @return {?}
         */
        self.N.prototype.listen = function(type, name, opt_fn, callback) {
            if (!self.Ca(name)) {
                if (name) {
                    pkgName[0] = name.toString();
                }
                /** @type {Array} */
                name = pkgName;
            }
            /** @type {number} */
            var i = 0;
            for (;i < name.length;i++) {
                var ret = self.ge(type, name[i], opt_fn || this.handleEvent, callback || false, this.v || this);
                if (!ret) {
                    break;
                }
                this.g[ret.key] = ret;
            }
            return this;
        };
        /**
         * @param {Object} walkers
         * @param {Object} node
         * @param {?} inplace
         * @param {Function} report
         * @return {undefined}
         */
        self.Ze = function(walkers, node, inplace, report) {
            dispatch(walkers, node, inplace, report, void 0);
        };
        /**
         * @param {Object} obj
         * @param {Object} params
         * @param {?} data
         * @param {Object} report
         * @param {?} done
         * @param {Node} deepDataAndEvents
         * @return {undefined}
         */
        dispatch = function(obj, params, data, report, done, deepDataAndEvents) {
            if (self.Ca(data)) {
                /** @type {number} */
                var byteIndex = 0;
                for (;byteIndex < data.length;byteIndex++) {
                    dispatch(obj, params, data[byteIndex], report, done, deepDataAndEvents);
                }
            } else {
                params = self.ne(params, data, report || obj.handleEvent, done, deepDataAndEvents || (obj.v || obj));
                if (!params) {
                    return;
                }
                /** @type {Object} */
                obj.g[params.key] = params;
            }
        };
        /**
         * @param {Object} params
         * @param {Object} data
         * @param {Object} idx
         * @param {boolean} until
         * @param {Node} headers
         * @return {?}
         */
        self.N.prototype.ta = function(params, data, idx, until, headers) {
            if (self.Ca(data)) {
                /** @type {number} */
                var byteIndex = 0;
                for (;byteIndex < data.length;byteIndex++) {
                    this.ta(params, data[byteIndex], idx, until, headers);
                }
            } else {
                idx = idx || this.handleEvent;
                headers = headers || (this.v || this);
                idx = intersect(idx);
                /** @type {boolean} */
                until = !!until;
                data = isFunction(params) ? callback(params.c, String(data), idx, until, headers) : params ? (params = extend(params)) ? callback(params, data, idx, until, headers) : null : null;
                if (data) {
                    remove(data);
                    delete this.g[data.key];
                }
            }
            return this;
        };
        /**
         * @return {undefined}
         */
        self.N.prototype.removeAll = function() {
            self.mb(this.g, function(l, word) {
                if (this.g.hasOwnProperty(word)) {
                    remove(l);
                }
            }, this);
            this.g = {};
        };
        /**
         * @return {undefined}
         */
        self.N.prototype.B = function() {
            self.N.D.B.call(this);
            this.removeAll();
        };
        /**
         * @return {?}
         */
        self.N.prototype.handleEvent = function() {
            throw Error("m");
        };
        /**
         * @return {undefined}
         */
        var d = function() {
        };
        /**
         * @param {string} x
         * @return {?}
         */
        d.prototype.g = function(x) {
            if (this.a) {
                /** @type {number} */
                var i = 0;
                for (;i < this.a.length;++i) {
                    if (this.a[i] instanceof x) {
                        return this.a[i];
                    }
                }
            }
            return null;
        };
        /**
         * @param {Function} key
         * @return {undefined}
         */
        var deferProp = function(key) {
            /** @type {boolean} */
            (key ? key : function() {
            }).Ib = true;
        };
        /**
         * @return {undefined}
         */
        self.bf = function() {
        };
        self.p(self.bf, d);
        /**
         * @param {string} x
         * @return {?}
         */
        self.bf.prototype.b = function(x) {
            return x;
        };
        deferProp(self.bf.prototype.b);
        /**
         * @return {?}
         */
        self.bf.prototype.c = function() {
            return true;
        };
        deferProp(self.bf.prototype.c);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var elems = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(elems, self.y);
        var $;
        /**
         * @param {string} s
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        $ = function(s, expectedNumberOfNonCommentArgs) {
            var elm = new elems;
            self.B(elm, 1, expectedNumberOfNonCommentArgs);
            self.B(elm, 2, s);
            return elm;
        };
        self.ef = $("?", 0);
        self.ff = $("q", 1);
        self.gf = $("<", 2);
        self.hf = $(">", 3);
        self.jf = $("~", 4);
        self.kf = $("s", 5);
        /**
         * @param {string} x
         * @param {string} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {undefined}
         */
        self.O = function(x, type, expectedNumberOfNonCommentArgs) {
            /** @type {string} */
            this.c = x;
            this.b = type || null;
            this.a = expectedNumberOfNonCommentArgs || [];
        };
        /**
         * @return {?}
         */
        self.O.prototype.toString = function() {
            return this.c;
        };
        var assert;
        var getBrightness;
        var clone;
        var lambda;
        var quote;
        var replace;
        var pos;
        var HEREGEX_OMIT;
        var regAttrs;
        var pdataOld;
        var udataCur;
        var that;
        var test;
        /**
         * @param {Object} data
         * @param {string} ll
         * @return {undefined}
         */
        self.P = function(data, ll) {
            /** @type {string} */
            this.b = this.C = this.h = "";
            /** @type {null} */
            this.g = null;
            /** @type {string} */
            this.l = this.a = "";
            /** @type {boolean} */
            this.j = false;
            var item;
            if (data instanceof self.P) {
                this.j = self.m(ll) ? ll : data.j;
                assert(this, data.h);
                this.C = data.C;
                this.b = data.b;
                getBrightness(this, data.g);
                this.a = data.a;
                item = data.c;
                var top = new that;
                top.c = item.c;
                if (item.a) {
                    top.a = new self.J(item.a);
                    top.b = item.b;
                }
                clone(this, top);
                self.pf(this, data.l);
            } else {
                if (data && (item = String(data).match(delegateEventSplitter))) {
                    /** @type {boolean} */
                    this.j = !!ll;
                    assert(this, item[1] || "", true);
                    this.C = lambda(item[2] || "");
                    this.b = lambda(item[3] || "", true);
                    getBrightness(this, item[4]);
                    this.a = lambda(item[5] || "", true);
                    clone(this, item[6] || "", true);
                    self.pf(this, item[7] || "", true);
                } else {
                    /** @type {boolean} */
                    this.j = !!ll;
                    this.c = new that(null, 0, this.j);
                }
            }
        };
        /**
         * @return {?}
         */
        self.P.prototype.toString = function() {
            /** @type {Array} */
            var t = [];
            var k = this.h;
            if (k) {
                t.push(quote(k, pos, true), ":");
            }
            var b = this.b;
            if (b || "file" == k) {
                t.push("//");
                if (k = this.C) {
                    t.push(quote(k, pos, true), "@");
                }
                t.push((0, window.encodeURIComponent)(String(b)).replace(/%25([0-9a-fA-F]{2})/g, "%$1"));
                b = this.g;
                if (null != b) {
                    t.push(":", String(b));
                }
            }
            if (b = this.a) {
                if (this.b) {
                    if ("/" != b.charAt(0)) {
                        t.push("/");
                    }
                }
                t.push(quote(b, "/" == b.charAt(0) ? regAttrs : HEREGEX_OMIT, true));
            }
            if (b = this.c.toString()) {
                t.push("?", b);
            }
            if (b = this.l) {
                t.push("#", quote(b, udataCur));
            }
            return t.join("");
        };
        /**
         * @param {Object} message
         * @param {string} data
         * @param {boolean} preserve
         * @return {undefined}
         */
        assert = function(message, data, preserve) {
            message.h = preserve ? lambda(data, true) : data;
            if (message.h) {
                message.h = message.h.replace(/:$/, "");
            }
        };
        /**
         * @param {?} b
         * @param {number} g
         * @return {undefined}
         */
        getBrightness = function(b, g) {
            if (g) {
                /** @type {number} */
                g = Number(g);
                if ((0, window.isNaN)(g) || 0 > g) {
                    throw Error("r`" + g);
                }
                /** @type {number} */
                b.g = g;
            } else {
                /** @type {null} */
                b.g = null;
            }
        };
        /**
         * @param {Object} item
         * @param {string} obj
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        clone = function(item, obj, dataAndEvents) {
            if (obj instanceof that) {
                /** @type {string} */
                item.c = obj;
                clean(item.c, item.j);
            } else {
                if (!dataAndEvents) {
                    obj = quote(obj, pdataOld);
                }
                item.c = new that(obj, 0, item.j);
            }
        };
        /**
         * @param {Object} object
         * @param {string} name
         * @param {?} data
         * @return {undefined}
         */
        self.R = function(object, name, data) {
            object.c.set(name, data);
        };
        /**
         * @param {?} query
         * @param {string} data
         * @param {boolean} preserve
         * @return {undefined}
         */
        self.pf = function(query, data, preserve) {
            query.l = preserve ? lambda(data) : data;
        };
        /**
         * @param {string} value
         * @return {?}
         */
        self.yf = function(value) {
            return value instanceof self.P ? new self.P(value) : new self.P(value, void 0);
        };
        /**
         * @param {(Function|string)} radius
         * @param {string} res
         * @return {?}
         */
        self.zf = function(radius, res) {
            if (!(radius instanceof self.P)) {
                radius = self.yf(radius);
            }
            if (!(res instanceof self.P)) {
                res = self.yf(res);
            }
            /** @type {(Function|string)} */
            var arg = radius;
            /** @type {string} */
            var args = res;
            var options = new self.P(arg);
            /** @type {boolean} */
            var webm = !!args.h;
            if (webm) {
                assert(options, args.h);
            } else {
                /** @type {boolean} */
                webm = !!args.C;
            }
            if (webm) {
                options.C = args.C;
            } else {
                /** @type {boolean} */
                webm = !!args.b;
            }
            if (webm) {
                options.b = args.b;
            } else {
                /** @type {boolean} */
                webm = null != args.g;
            }
            var num = args.a;
            if (webm) {
                getBrightness(options, args.g);
            } else {
                if (webm = !!args.a) {
                    if ("/" != num.charAt(0) && (arg.b && !arg.a ? num = "/" + num : (arg = options.a.lastIndexOf("/"), -1 != arg && (num = options.a.substr(0, arg + 1) + num))), arg = num, ".." == arg || "." == arg) {
                        /** @type {string} */
                        num = "";
                    } else {
                        if (-1 != arg.indexOf("./") || -1 != arg.indexOf("/.")) {
                            num = self.Ha(arg, "/");
                            arg = arg.split("/");
                            /** @type {Array} */
                            var out = [];
                            /** @type {number} */
                            var a = 0;
                            for (;a < arg.length;) {
                                var copies = arg[a++];
                                if ("." == copies) {
                                    if (num) {
                                        if (a == arg.length) {
                                            out.push("");
                                        }
                                    }
                                } else {
                                    if (".." == copies) {
                                        if (1 < out.length || 1 == out.length && "" != out[0]) {
                                            out.pop();
                                        }
                                        if (num) {
                                            if (a == arg.length) {
                                                out.push("");
                                            }
                                        }
                                    } else {
                                        out.push(copies);
                                        /** @type {boolean} */
                                        num = true;
                                    }
                                }
                            }
                            /** @type {string} */
                            num = out.join("/");
                        } else {
                            num = arg;
                        }
                    }
                }
            }
            if (webm) {
                options.a = num;
            } else {
                /** @type {boolean} */
                webm = "" !== args.c.toString();
            }
            if (webm) {
                clone(options, lambda(args.c.toString()));
            } else {
                /** @type {boolean} */
                webm = !!args.l;
            }
            if (webm) {
                self.pf(options, args.l);
            }
            return options;
        };
        /**
         * @param {string} tmpl
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        lambda = function(tmpl, dataAndEvents) {
            return tmpl ? dataAndEvents ? (0, window.decodeURI)(tmpl.replace(/%25/g, "%2525")) : (0, window.decodeURIComponent)(tmpl) : "";
        };
        /**
         * @param {string} str
         * @param {?} value
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        quote = function(str, value, dataAndEvents) {
            return self.t(str) ? (str = (0, window.encodeURI)(str).replace(value, replace), dataAndEvents && (str = str.replace(/%25([0-9a-fA-F]{2})/g, "%$1")), str) : null;
        };
        /**
         * @param {string} n
         * @return {?}
         */
        replace = function(n) {
            n = n.charCodeAt(0);
            return "%" + (n >> 4 & 15).toString(16) + (n & 15).toString(16);
        };
        /** @type {RegExp} */
        pos = /[#\/\?@]/g;
        /** @type {RegExp} */
        HEREGEX_OMIT = /[\#\?:]/g;
        /** @type {RegExp} */
        regAttrs = /[\#\?]/g;
        /** @type {RegExp} */
        pdataOld = /[\#\?@]/g;
        /** @type {RegExp} */
        udataCur = /#/g;
        /**
         * @param {Node} c
         * @param {?} dataAndEvents
         * @param {?} trigger
         * @return {undefined}
         */
        that = function(c, dataAndEvents, trigger) {
            /** @type {null} */
            this.b = this.a = null;
            this.c = c || null;
            /** @type {boolean} */
            this.g = !!trigger;
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        test = function(obj) {
            if (!obj.a) {
                obj.a = new self.J;
                /** @type {number} */
                obj.b = 0;
                if (obj.c) {
                    handle(obj.c, function(messageFormat, vvar) {
                        var cookieName = (0, window.decodeURIComponent)(messageFormat.replace(/\+/g, " "));
                        test(obj);
                        /** @type {null} */
                        obj.c = null;
                        cookieName = get(obj, cookieName);
                        var assigns = obj.a.get(cookieName);
                        if (!assigns) {
                            obj.a.set(cookieName, assigns = []);
                        }
                        assigns.push(vvar);
                        obj.b += 1;
                    });
                }
            }
        };
        /**
         * @param {string} name
         * @return {?}
         */
        that.prototype.remove = function(name) {
            test(this);
            name = get(this, name);
            return self.Lc(this.a, name) ? (this.c = null, this.b -= this.a.get(name).length, this.a.remove(name)) : false;
        };
        /**
         * @return {undefined}
         */
        that.prototype.clear = function() {
            /** @type {null} */
            this.a = this.c = null;
            /** @type {number} */
            this.b = 0;
        };
        /**
         * @param {Object} obj
         * @param {string} key
         * @return {?}
         */
        var has = function(obj, key) {
            test(obj);
            key = get(obj, key);
            return self.Lc(obj.a, key);
        };
        /**
         * @return {?}
         */
        that.prototype.X = function() {
            test(this);
            var segs = this.a.V();
            var codeSegments = this.a.X();
            /** @type {Array} */
            var arrayOfArgs = [];
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                var seg = segs[i];
                /** @type {number} */
                var k = 0;
                for (;k < seg.length;k++) {
                    arrayOfArgs.push(codeSegments[i]);
                }
            }
            return arrayOfArgs;
        };
        /**
         * @param {string} key
         * @return {?}
         */
        that.prototype.V = function(key) {
            test(this);
            /** @type {Array} */
            var which = [];
            if (self.t(key)) {
                if (has(this, key)) {
                    which = self.hb(which, this.a.get(get(this, key)));
                }
            } else {
                key = this.a.V();
                /** @type {number} */
                var i = 0;
                for (;i < key.length;i++) {
                    which = self.hb(which, key[i]);
                }
            }
            return which;
        };
        /**
         * @param {string} name
         * @param {?} value
         * @return {?}
         */
        that.prototype.set = function(name, value) {
            test(this);
            /** @type {null} */
            this.c = null;
            name = get(this, name);
            if (has(this, name)) {
                this.b -= this.a.get(name).length;
            }
            this.a.set(name, [value]);
            this.b += 1;
            return this;
        };
        /**
         * @param {string} name
         * @param {?} obj
         * @return {?}
         */
        that.prototype.get = function(name, obj) {
            var data = name ? this.V(name) : [];
            return 0 < data.length ? String(data[0]) : obj;
        };
        /**
         * @param {Object} item
         * @param {string} name
         * @param {?} bind
         * @return {undefined}
         */
        self.Ef = function(item, name, bind) {
            item.remove(name);
            if (0 < bind.length) {
                /** @type {null} */
                item.c = null;
                item.a.set(get(item, name), self.ib(bind));
                item.b += bind.length;
            }
        };
        /**
         * @return {?}
         */
        that.prototype.toString = function() {
            if (this.c) {
                return this.c;
            }
            if (!this.a) {
                return "";
            }
            /** @type {Array} */
            var assigns = [];
            var items = this.a.X();
            /** @type {number} */
            var k = 0;
            for (;k < items.length;k++) {
                var values = items[k];
                var e = (0, window.encodeURIComponent)(String(values));
                values = this.V(values);
                /** @type {number} */
                var i = 0;
                for (;i < values.length;i++) {
                    var vvar = e;
                    if ("" !== values[i]) {
                        vvar += "=" + (0, window.encodeURIComponent)(String(values[i]));
                    }
                    assigns.push(vvar);
                }
            }
            return this.c = assigns.join("&");
        };
        /**
         * @param {Object} obj
         * @param {string} name
         * @return {?}
         */
        var get = function(obj, name) {
            /** @type {string} */
            var field = String(name);
            if (obj.g) {
                /** @type {string} */
                field = field.toLowerCase();
            }
            return field;
        };
        /**
         * @param {Object} obj
         * @param {?} res
         * @return {undefined}
         */
        var clean = function(obj, res) {
            if (res) {
                if (!obj.g) {
                    test(obj);
                    /** @type {null} */
                    obj.c = null;
                    obj.a.forEach(function(parsed, name) {
                        var platform = name.toLowerCase();
                        if (name != platform) {
                            this.remove(name);
                            self.Ef(this, platform, parsed);
                        }
                    }, obj);
                }
            }
            obj.g = res;
        };
        if (!self.bf.D) {
            self.p(self.bf, d);
        }
        /**
         * @param {number} opt_attributes
         * @return {?}
         */
        self.Ff = function(opt_attributes) {
            switch(opt_attributes) {
                case 1:
                    return "homepage";
                case 4:
                    return "Google Translator Toolkit";
                case 5:
                    return "Google Sites";
                case 6:
                    return "customersupport";
                case 9:
                    return "present";
                case 15:
                    return "hangout";
                case 18:
                    return "gmail";
                case 20:
                    return "docs";
                case 24:
                    return "docs_hangout";
                case 22:
                    return "hangout_lite";
                case 23:
                    return "aChromeExtension";
                case 25:
                    return "bigtop";
                case 27:
                    return "ChromeApp";
                case 36:
                    return "StartPage";
            }
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.Gf = function(funcToCall) {
            self.z(this, funcToCall, "uv_qm", null);
        };
        self.p(self.Gf, self.y);
        /** @type {string} */
        self.Gf.messageId = "uv_qm";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.Hf = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_rdm", null);
        };
        self.p(self.Hf, self.y);
        /** @type {string} */
        self.Hf.messageId = "uv_r_rdm";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.If = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_rr", null);
        };
        self.p(self.If, self.y);
        /** @type {string} */
        self.If.messageId = "uv_r_rr";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.Jf = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_frm", null);
        };
        self.p(self.Jf, self.y);
        /** @type {string} */
        self.Jf.messageId = "uv_r_frm";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.Kf = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(self.Kf, self.y);
        /**
         * @param {string} rows
         * @param {number} delta
         * @return {?}
         */
        var render = function(rows, delta) {
            /** @type {Array.<?>} */
            var args = Array.prototype.slice.call(arguments);
            var messageFormat = args.shift();
            if ("undefined" == typeof messageFormat) {
                throw Error("t");
            }
            return messageFormat.replace(/%([0\-\ \+]*)(\d+)?(\.(\d+))?([%sfdiu])/g, function(dataAndEvents, deepDataAndEvents, ignoreMethodDoesntExist, textAlt, keepData, methodname, opt_attributes, matcherFunction) {
                if ("%" == methodname) {
                    return "%";
                }
                var callback = args.shift();
                if ("undefined" == typeof callback) {
                    throw Error("u");
                }
                arguments[0] = callback;
                return el[methodname].apply(null, arguments);
            });
        };
        var el = {
            /**
             * @param {string} x
             * @param {string} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @return {?}
             */
            s : function(x, type, expectedNumberOfNonCommentArgs) {
                return(0, window.isNaN)(expectedNumberOfNonCommentArgs) || ("" == expectedNumberOfNonCommentArgs || x.length >= Number(expectedNumberOfNonCommentArgs)) ? x : x = -1 < type.indexOf("-", 0) ? x + equals(" ", Number(expectedNumberOfNonCommentArgs) - x.length) : equals(" ", Number(expectedNumberOfNonCommentArgs) - x.length) + x;
            },
            /**
             * @param {(number|string)} n
             * @param {string} key
             * @param {number} length
             * @param {string} result
             * @param {string} c
             * @return {?}
             */
            f : function(n, key, length, result, c) {
                result = n.toString();
                if (!(0, window.isNaN)(c)) {
                    if (!("" == c)) {
                        result = (0, window.parseFloat)(n).toFixed(c);
                    }
                }
                var padding;
                /** @type {string} */
                padding = 0 > Number(n) ? "-" : 0 <= key.indexOf("+") ? "+" : 0 <= key.indexOf(" ") ? " " : "";
                if (0 <= Number(n)) {
                    /** @type {string} */
                    result = padding + result;
                }
                if ((0, window.isNaN)(length) || result.length >= Number(length)) {
                    return result;
                }
                /** @type {string} */
                result = (0, window.isNaN)(c) ? Math.abs(Number(n)).toString() : Math.abs(Number(n)).toFixed(c);
                /** @type {number} */
                n = Number(length) - result.length - padding.length;
                return result = 0 <= key.indexOf("-", 0) ? padding + result + equals(" ", n) : padding + equals(0 <= key.indexOf("0", 0) ? "0" : " ", n) + result;
            },
            /**
             * @param {string} x
             * @param {string} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @param {string} callback
             * @param {Object} a
             * @param {?} f
             * @param {?} opts
             * @param {?} partials
             * @return {?}
             */
            d : function(x, type, expectedNumberOfNonCommentArgs, callback, a, f, opts, partials) {
                return el.f((0, window.parseInt)(x, 10), type, expectedNumberOfNonCommentArgs, callback, 0, f, opts, partials);
            }
        };
        /** @type {function (string, string, number, string, Object, ?, ?, ?): ?} */
        el.i = el.d;
        /** @type {function (string, string, number, string, Object, ?, ?, ?): ?} */
        el.u = el.d;
        /**
         * @param {string} j
         * @param {?} d
         * @return {undefined}
         */
        self.S = function(j, d) {
            /** @type {Array} */
            var arr = [];
            /** @type {number} */
            var i = 0;
            for (;i < arguments.length;i++) {
                if (arguments[i] instanceof Error) {
                    arr.push(arguments[i].stack);
                } else {
                    if ("object" == typeof arguments[i]) {
                        try {
                            arr.push(JSON.stringify(arguments[i]));
                        } catch (e) {
                            arr.push("_error_(" + e + ")");
                        }
                    } else {
                        arr.push(arguments[i]);
                    }
                }
            }
            if (window.console) {
                /** @type {Date} */
                i = new Date;
                /** @type {string} */
                i = render("%02d-%02d ", i.getMonth() + 1, i.getDate()) + i.toLocaleTimeString() + render(".%03d", i.getMilliseconds());
                window.console.log(i + " info: " + render.apply({}, arr));
            }
        };
        self.Nf = new self.O("pVbxBc");
        self.T = new self.O("nED8Yb");
        self.Of = new self.O("xclgJb");
        self.Pf = new self.O("Qz9Lj");
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        self.Rf = function(funcToCall) {
            self.z(this, funcToCall, "h_exts", recurring);
        };
        self.p(self.Rf, self.y);
        /** @type {Array} */
        var recurring = [2];
        /** @type {string} */
        self.Rf.messageId = "h_exts";
        /**
         * @param {(Function|string)} radius
         * @return {?}
         */
        self.Sf = function(radius) {
            return self.zf(radius, "/webchat/host-js");
        };
        /**
         * @param {string} elm
         * @return {?}
         */
        self.Tf = function(elm) {
            var which = self.Sf(self.A(elm, 2));
            self.R(which, "prop", self.Ff(27));
            self.R(which, "b", 1);
            self.R(which, "hl", self.A(elm, 3));
            if (self.A(elm, 4)) {
                self.R(which, "jsmode", self.A(elm, 4));
            }
            self.R(which, "zx", self.Va());
            return which.toString();
        };
        var isEqual;
        /**
         * @param {RegExp} that
         * @return {?}
         */
        isEqual = function(that) {
            return(that = that.exec(self.jb)) ? that[1] : "";
        };
        self.Vf = function() {
            if (index) {
                return isEqual(/Firefox\/([0-9.]+)/);
            }
            if (self.x || (lastWallLeft || versionOffset)) {
                return arg;
            }
            if (vlq) {
                return isEqual(/Chrome\/([0-9.]+)/);
            }
            if (Xb && !(fixedPosition() || (self.w("iPad") || self.w("iPod")))) {
                return isEqual(/Version\/([0-9.]+)/);
            }
            if (program || inverse) {
                /** @type {(Array.<string>|null)} */
                var result = /Version\/(\S+).*Mobile\/(\S+)/.exec(self.jb);
                if (result) {
                    return result[1] + "." + result[2];
                }
            } else {
                if (Vb) {
                    return(result = isEqual(/Android\s+([0-9.]+)/)) ? result : isEqual(/Version\/([0-9.]+)/);
                }
            }
            return "";
        }();
        /**
         * @param {string} node
         * @param {string} m
         * @return {undefined}
         */
        self.Wf = function(node, m) {
            this.l = node || "e";
            /** @type {string} */
            this.g = null != node && "p" == node ? "e" : "p";
            /** @type {null} */
            this.c = this.h = null;
            this.j = m || "u";
            /** @type {null} */
            this.a = this.C = null;
            /** @type {Array} */
            this.b = [];
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.message = function() {
            var c = {};
            c.i = this.a;
            c.m = this.b;
            c.r = this.j;
            c.o = this.C;
            c.s = this.c;
            c.st = this.g;
            c.d = this.h;
            c.dt = this.l;
            return c;
        };
        /** @type {function (): ?} */
        self.Wf.prototype.message = self.Wf.prototype.message;
        /**
         * @return {?}
         */
        self.Wf.prototype.source = function() {
            return this.c;
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.target = function() {
            return this.h;
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.content = function() {
            return this.b;
        };
        /**
         * @param {?} obj
         * @param {string} selection
         * @return {undefined}
         */
        var i = function(obj, selection) {
            self.K.call(this, obj, selection);
            /** @type {string} */
            this.S = selection;
        };
        self.p(i, self.K);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var which = function(funcToCall) {
            self.z(this, funcToCall, "capi_md1571", null);
        };
        self.p(which, self.y);
        /** @type {string} */
        which.messageId = "capi_md1571";
        var opts = {
            bb : 0,
            tc : 1,
            sc : 2,
            ab : 3,
            Qa : 4
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var ready = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(ready, self.y);
        /**
         * @return {?}
         */
        var rand = function() {
            /** @type {number} */
            var a = 10;
            /** @type {string} */
            var number = startPage;
            /** @type {number} */
            var n = number.length;
            /** @type {string} */
            var res = "";
            for (;0 < a--;) {
                res += number.charAt(Math.floor(Math.random() * n));
            }
            return res;
        };
        /** @type {string} */
        var startPage = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        /**
         * @param {?} l
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        self.cg = function(l, dataAndEvents) {
            self.F.call(this);
            this.j = dataAndEvents || rand();
            /** @type {string} */
            this.g = this.a = "";
            /** @type {null} */
            this.b = this.c = null;
            this.l = l;
            this.h = new self.N(this);
            self.I(this, this.h);
            this.h.listen(window, "message", this.v);
        };
        self.p(self.cg, self.F);
        /**
         * @param {string} second
         * @param {boolean} stop
         * @param {number} opt_attributes
         * @return {undefined}
         */
        self.cg.prototype.da = function(second, stop, opt_attributes) {
            /** @type {string} */
            this.g = second;
            /** @type {boolean} */
            this.c = stop;
            /** @type {number} */
            this.b = opt_attributes;
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        self.cg.prototype.v = function(x) {
            x = x.a;
            var subscription;
            subscription = x.origin;
            if (this.c && this.c != x.source || "*" != this.g && this.g != subscription) {
                /** @type {boolean} */
                subscription = false;
            } else {
                if (this.b) {
                    subscription = (new self.P(subscription)).b;
                    /** @type {boolean} */
                    subscription = self.Ia(subscription, this.b) ? subscription.length == this.b.length || ("." == this.b[0] || "." == subscription[subscription.length - 1 - this.b.length]) : false;
                } else {
                    /** @type {boolean} */
                    subscription = true;
                }
            }
            if (subscription && ((subscription = x.data) && (self.t(subscription) && "[" == subscription[0]))) {
                var ll;
                try {
                    if (ll = JSON.parse(subscription), !self.Ca(ll)) {
                        return;
                    }
                } catch (d) {
                    return;
                }
                if (0 != ll.length && ("capi_md1571" == ll[0] && (ll = new which(ll), self.A(ll, 1) == this.j))) {
                    subscription = self.A(ll, 3);
                    if (!this.a) {
                        this.a = subscription;
                    } else {
                        if (this.a != subscription) {
                            return;
                        }
                    }
                    if (!("*" != this.g && this.c)) {
                        this.da(x.origin, x.source);
                    }
                    if (1 == self.A(ll, 2)) {
                        this.send(2);
                    } else {
                        this.l(ll, x);
                    }
                }
            }
        };
        /**
         * @param {number} data
         * @param {string} ll
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {undefined}
         */
        self.cg.prototype.send = function(data, ll, expectedNumberOfNonCommentArgs) {
            if (this.a && this.c) {
                ll = ll || new which;
                self.B(ll, 3, this.j);
                self.B(ll, 1, this.a);
                self.B(ll, 2, data);
                /** @type {string} */
                var subscription = JSON.stringify(self.D(ll));
                var _ = this.c;
                var cycle = this.g;
                if (self.x) {
                    self.Be(function() {
                        _.postMessage(subscription, cycle);
                    });
                } else {
                    if (expectedNumberOfNonCommentArgs) {
                        _.postMessage(subscription, cycle, expectedNumberOfNonCommentArgs);
                    } else {
                        _.postMessage(subscription, cycle);
                    }
                }
            }
        };
        /**
         * @param {(Object|string)} o
         * @param {boolean} item
         * @param {?} t
         * @param {?} dt
         * @param {number} attributes
         * @return {undefined}
         */
        var tick = function(o, item, t, dt, attributes) {
            this.c = self.qd();
            this.a = new self.cg((0, self.r)(this.h, this), dt);
            this.a.da(o, item, attributes);
            this.a.a = t;
            this.b = new self.ze(50);
            self.Ae(this.b);
            self.ge(this.b, "tick", this.g, false, this);
            this.g();
            o = this.c.a;
            item = this.j;
            item = after(item, item, this);
            /** @type {boolean} */
            item.c = true;
            filter(o, item);
        };
        /**
         * @return {undefined}
         */
        tick.prototype.j = function() {
            self.H(this.a);
            self.oe(this.b, "tick", this.g, false, this);
            self.H(this.b);
        };
        /**
         * @return {undefined}
         */
        tick.prototype.g = function() {
            this.a.send(1);
        };
        /**
         * @param {string} x
         * @param {Function} type
         * @return {undefined}
         */
        tick.prototype.h = function(x, type) {
            if (2 == self.A(x, 2)) {
                this.c.b(type.origin);
            }
        };
        /**
         * @param {(Function|string)} s
         * @param {Text} v
         * @param {Object} a
         * @return {undefined}
         */
        var val = function(s, v, a) {
            self.F.call(this);
            /** @type {(Function|string)} */
            this.b = s;
            s = self.A(v, 3);
            v = self.A(v, 1);
            this.a = new self.cg((0, self.r)(this.c, this), v);
            /** @type {(Function|string)} */
            this.a.a = s;
            if (a) {
                this.a.da(a.origin, a.source);
            }
            self.I(this, this.a);
        };
        self.p(val, self.F);
        /**
         * @param {number} data
         * @return {undefined}
         */
        val.prototype.send = function(data) {
            var method = new which;
            self.E(method, 4, data);
            this.a.send(opts.bb, method);
        };
        /**
         * @param {string} o
         * @param {boolean} item
         * @return {undefined}
         */
        val.prototype.connect = function(o, item) {
            this.a.da(o, item);
            this.a.send(opts.ab);
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        val.prototype.c = function(x) {
            if (self.A(x, 2) == opts.bb) {
                x = self.C(x, ready, 4);
                this.b(x);
            }
        };
        /** @type {function (): undefined} */
        val.prototype.g = self.Da;
        /**
         * @param {(Function|string)} i
         * @param {string} array
         * @param {?} options
         * @return {undefined}
         */
        var controller = function(i, array, options) {
            self.F.call(this);
            /** @type {(Function|string)} */
            this.h = i;
            /** @type {null} */
            this.b = this.a = this.c = null;
            if (options) {
                this.b = options.ports[0];
                on(this);
            } else {
                this.a = new self.cg(self.Da, self.A(array, 1));
                i = self.A(array, 3);
                /** @type {(Function|string)} */
                this.a.a = i;
            }
        };
        self.p(controller, self.F);
        /**
         * @return {undefined}
         */
        controller.prototype.B = function() {
            controller.D.B.call(this);
            if (this.b) {
                this.b.close();
            }
            self.H(this.a);
        };
        /**
         * @return {undefined}
         */
        controller.prototype.g = function() {
            this.c = new self.N(this);
            self.I(this, this.c);
            on(this);
        };
        /**
         * @param {string} ll
         * @return {undefined}
         */
        controller.prototype.send = function(ll) {
            this.b.postMessage(self.D(ll));
        };
        /**
         * @param {string} o
         * @param {boolean} item
         * @return {undefined}
         */
        controller.prototype.connect = function(o, item) {
            var channel = new window.MessageChannel;
            this.b = channel.port1;
            on(this);
            var c = this.a;
            /** @type {null} */
            this.a = null;
            channel = channel.port2;
            c.da(o, item);
            c.send(opts.Qa, void 0, [channel]);
            self.H(c);
        };
        /**
         * @param {Object} item
         * @return {undefined}
         */
        var on = function(item) {
            if (item.b) {
                if (item.c) {
                    item.c.listen(item.b, "message", item.j);
                    item.b.start();
                }
            }
        };
        /**
         * @param {Object} a
         * @return {undefined}
         */
        controller.prototype.j = function(a) {
            a = a.a;
            var ll;
            try {
                ll = new ready(a.data);
            } catch (c) {
                return;
            }
            this.h(ll);
        };
        /**
         * @param {string} element
         * @param {string} x
         * @param {string} fn
         * @return {?}
         */
        var apply = function(element, x, fn) {
            /** @type {boolean} */
            var IS_TOUCH_DEVICE = false;
            if (self.m(self.A(x, 2))) {
                if (self.A(x, 2) == opts.Qa) {
                    /** @type {boolean} */
                    IS_TOUCH_DEVICE = true;
                }
            } else {
                IS_TOUCH_DEVICE = vlq;
            }
            return IS_TOUCH_DEVICE ? new controller(element, x, fn) : new val(element, x, fn);
        };
        var build;
        /**
         * @return {undefined}
         */
        self.ig = function() {
            self.L.call(this);
            /** @type {null} */
            this.h = null;
            /** @type {boolean} */
            this.l = false;
            /** @type {Array} */
            this.j = [];
            /** @type {Array} */
            this.g = [];
            self.G(this, this.w, this);
            /** @type {null} */
            this.b = null;
        };
        self.p(self.ig, self.L);
        /**
         * @return {undefined}
         */
        self.ig.prototype.w = function() {
            for (;this.g.length;) {
                this.g.shift()();
            }
        };
        /**
         * @param {Object} options
         * @return {undefined}
         */
        self.kg = function(options) {
            options.w();
            /** @type {Array} */
            options.j = [];
            options.b = new self.Dd;
            options.g.push((0, self.r)(options.b.cancel, options.b));
            /** @type {boolean} */
            options.l = false;
            self.Kd(options.b.h(), function(value) {
                if (compile(this.c) || this.G) {
                    logger(this);
                }
                this.g.push(self.xa(self.H, value));
            }, options);
        };
        self.g = self.ig.prototype;
        /**
         * @param {string} ll
         * @return {undefined}
         */
        self.g.sendMessage = function(ll) {
            this.xa(self.D(ll));
        };
        /**
         * @param {Object} recurring
         * @return {undefined}
         */
        self.g.xa = function(recurring) {
            if (this.b) {
                var buf = new ready;
                self.B(buf, 1, recurring);
                self.Kd(this.b.h(), function(ws) {
                    ws.send(buf);
                });
            }
        };
        /**
         * @param {Function} a
         * @return {undefined}
         */
        self.g.$a = function(a) {
            /** @type {Function} */
            this.h = a;
            self.G(this, function() {
                /** @type {null} */
                this.h = null;
            }, this);
            logger(this);
        };
        /**
         * @return {?}
         */
        self.g.J = function() {
            return this;
        };
        /**
         * @param {string} o
         * @param {boolean} stop
         * @param {number} opt_attributes
         * @return {undefined}
         */
        self.g.da = function(o, stop, opt_attributes) {
            this.a.da(o, stop, opt_attributes);
        };
        /**
         * @param {Object} next
         * @param {Object} method
         * @param {string} recurring
         * @param {string} key
         * @return {undefined}
         */
        self.mg = function(next, method, recurring, key) {
            self.kg(next);
            build(next, method, recurring, key).then(function(args) {
                var ll = new which;
                self.B(ll, 1, args.pc);
                self.B(ll, 3, recurring);
                ll = apply((0, self.r)(this.v, this), ll);
                ll.connect(args.origin, method);
                this.b.b(ll);
            }, void 0, next);
        };
        /**
         * @param {Object} result
         * @param {string} item
         * @param {string} recurring
         * @param {string} keepData
         * @return {?}
         */
        build = function(result, item, recurring, keepData) {
            var pc = rand();
            item = (new tick("*", item, recurring, pc, keepData)).c.a;
            result.g.push((0, self.r)(item.cancel, item));
            return item.then(function(origin) {
                return{
                    pc : pc,
                    origin : origin
                };
            });
        };
        /**
         * @param {string} array
         * @param {string} name
         * @return {undefined}
         */
        self.ig.prototype.H = function(array, name) {
            if (self.A(array, 2) == opts.ab || self.A(array, 2) == opts.Qa) {
                self.H(this.a);
                /** @type {null} */
                this.a = null;
                this.b.b(apply((0, self.r)(this.v, this), array, name));
            }
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        self.ig.prototype.v = function(x) {
            this.j.push(x);
            this.A();
        };
        /**
         * @return {undefined}
         */
        self.ig.prototype.A = function() {
            for (;move(this);) {
            }
        };
        /**
         * @param {string} ll
         * @return {?}
         */
        var move = function(ll) {
            if (ll.K || (!ll.l || 0 == ll.j.length)) {
                return false;
            }
            var recurring = self.A(ll.j.shift(), 1);
            if (ll.h) {
                ll.h.xa(recurring);
            }
            self.M(ll, new i(recurring[0], recurring));
            return true;
        };
        /**
         * @param {Node} scope
         * @return {undefined}
         */
        var logger = function(scope) {
            if (!scope.l) {
                /** @type {boolean} */
                scope.l = true;
                if (scope.b) {
                    self.Kd(scope.b.h(), function(rgba) {
                        rgba.g();
                        self.Be(this.A, 0, this);
                    }, scope);
                }
            }
        };
        /**
         * @param {?} type
         * @param {string} name
         * @param {?} opt_scope
         * @param {boolean} error
         * @return {?}
         */
        self.ig.prototype.listen = function(type, name, opt_scope, error) {
            logger(this);
            return self.ig.D.listen.call(this, type, name, opt_scope, error);
        };
        /**
         * @param {?} type
         * @param {Object} helpers
         * @param {?} xs
         * @param {boolean} until
         * @return {?}
         */
        self.ig.prototype.Ma = function(type, helpers, xs, until) {
            logger(this);
            return self.ig.D.Ma.call(this, type, helpers, xs, until);
        };
        /**
         * @param {Object} m
         * @return {?}
         */
        self.ig.prototype.I = function(m) {
            logger(this);
            return self.ig.D.I.call(this, m);
        };
        /**
         * @param {?} b
         * @return {undefined}
         */
        var valueRef = function(b) {
            this.b = b;
            /** @type {string} */
            this.g = "empty";
            /** @type {null} */
            this.a = null;
            this.c = {};
        };
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        valueRef.prototype.setVersion = function(deepDataAndEvents) {
            this.g = deepDataAndEvents;
            return this;
        };
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        valueRef.prototype.Oa = function(deepDataAndEvents) {
            this.a = deepDataAndEvents;
            return this;
        };
        /**
         * @param {string} caption
         * @return {?}
         */
        valueRef.prototype.Za = function(caption) {
            this.c = self.qb(caption);
            return this;
        };
        /**
         * @return {undefined}
         */
        var y = function() {
        };
        /**
         * @return {undefined}
         */
        var config = function() {
        };
        self.p(config, y);
        /**
         * @return {undefined}
         */
        config.prototype.clear = function() {
            var r20 = children(this.ia(true));
            var tracker = this;
            (0, self.u)(r20, function(name) {
                tracker.remove(name);
            });
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        var k = function(a) {
            this.a = a;
        };
        self.p(k, config);
        self.g = k.prototype;
        /**
         * @return {?}
         */
        self.g.isAvailable = function() {
            if (!this.a) {
                return false;
            }
            try {
                return this.a.setItem("__sak", "1"), this.a.removeItem("__sak"), true;
            } catch (a) {
                return false;
            }
        };
        /**
         * @param {string} name
         * @param {?} string
         * @return {undefined}
         */
        self.g.set = function(name, string) {
            try {
                this.a.setItem(name, string);
            } catch (c) {
                if (0 == this.a.length) {
                    throw "Storage mechanism: Storage disabled";
                }
                throw "Storage mechanism: Quota exceeded";
            }
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.g.get = function(name) {
            name = this.a.getItem(name);
            if (!self.t(name) && null !== name) {
                throw "Storage mechanism: Invalid value was encountered";
            }
            return name;
        };
        /**
         * @param {string} keepData
         * @return {undefined}
         */
        self.g.remove = function(keepData) {
            this.a.removeItem(keepData);
        };
        /**
         * @param {boolean} recurring
         * @return {?}
         */
        self.g.ia = function(recurring) {
            /** @type {number} */
            var i = 0;
            var storage = this.a;
            var stream = new Stream;
            /**
             * @return {?}
             */
            stream.next = function() {
                if (i >= storage.length) {
                    throw s;
                }
                var key = storage.key(i++);
                if (recurring) {
                    return key;
                }
                key = storage.getItem(key);
                if (!self.t(key)) {
                    throw "Storage mechanism: Invalid value was encountered";
                }
                return key;
            };
            return stream;
        };
        /**
         * @return {undefined}
         */
        self.g.clear = function() {
            this.a.clear();
        };
        /**
         * @param {number} index
         * @return {?}
         */
        self.g.key = function(index) {
            return this.a.key(index);
        };
        /**
         * @return {undefined}
         */
        var now = function() {
            /** @type {null} */
            var a = null;
            try {
                a = window.localStorage || null;
            } catch (b) {
            }
            this.a = a;
        };
        self.p(now, k);
        /**
         * @param {?} index
         * @param {?} place
         * @return {undefined}
         */
        var go = function(index, place) {
            this.c = index;
            /** @type {Array} */
            this.a = [];
            /** @type {null} */
            this.b = null;
            if (place) {
                this.b = new now;
            }
            if (null != this.b && this.b.isAvailable()) {
                var source = this.b.get("__webmonitoring_RateThrottler_history_hourlyRate");
                if (null != source) {
                    try {
                        this.a = trim(source) || [];
                    } catch (d) {
                    }
                }
            }
            /** @type {boolean} */
            source = false;
            if (!self.Ca(this.a)) {
                /** @type {Array} */
                this.a = [];
                /** @type {boolean} */
                source = true;
            }
            for (;this.a.length > this.c;) {
                this.a.shift();
                /** @type {boolean} */
                source = true;
            }
            if (source) {
                getter(this);
            }
        };
        /**
         * @param {Node} obj
         * @return {undefined}
         */
        var getter = function(obj) {
            if (null != obj.b && obj.b.isAvailable()) {
                try {
                    obj.b.set("__webmonitoring_RateThrottler_history_hourlyRate", self.cc(obj.a));
                } catch (b) {
                }
            }
        };
        /**
         * @param {?} e
         * @return {undefined}
         */
        var fn = function(e) {
            self.F.call(this);
            this.a = e;
            /** @type {boolean} */
            this.h = false;
        };
        self.p(fn, self.F);
        /**
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        fn.prototype.setEnabled = function(dataAndEvents) {
            /** @type {boolean} */
            this.h = dataAndEvents;
        };
        /**
         * @param {?} i
         * @return {undefined}
         */
        var data = function(i) {
            fn.call(this, i);
            /** @type {number} */
            this.w = 100;
            this.v = {};
            /** @type {null} */
            this.R = null;
            /** @type {string} */
            this.c = "";
            /** @type {boolean} */
            this.G = true;
            /** @type {number} */
            this.j = 10;
            /** @type {null} */
            this.b = null;
            /** @type {boolean} */
            this.g = false;
        };
        self.p(data, fn);
        /**
         * @param {?} recurring
         * @return {?}
         */
        data.prototype.pb = function(recurring) {
            this.G = recurring;
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setUseLocalStorage = data.prototype.pb;
        /**
         * @param {?} a
         * @return {?}
         */
        data.prototype.oc = function(a) {
            /** @type {number} */
            this.j = Math.min(a, 100);
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setMaxErrorsPerHour = data.prototype.oc;
        /**
         * @param {string} opt_attributes
         * @return {?}
         */
        data.prototype.nb = function(opt_attributes) {
            /** @type {string} */
            this.w = opt_attributes;
            return this;
        };
        /** @type {function (string): ?} */
        data.prototype.setGlobalSampling = data.prototype.nb;
        /**
         * @param {?} key
         * @param {?} x
         * @return {?}
         */
        data.prototype.eb = function(key, x) {
            this.v[key] = x;
            return this;
        };
        /** @type {function (?, ?): ?} */
        data.prototype.addPerErrorMessageSampling = data.prototype.eb;
        /**
         * @param {string} cur
         * @return {?}
         */
        data.prototype.mb = function(cur) {
            /** @type {string} */
            this.R = cur;
            return this;
        };
        /** @type {function (string): ?} */
        data.prototype.setCallback = data.prototype.mb;
        /**
         * @param {?} compiler
         * @return {?}
         */
        data.prototype.nc = function(compiler) {
            this.c = compiler;
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setClientId = data.prototype.nc;
        /**
         * @return {?}
         */
        data.prototype.enable = function() {
            if (this.h) {
                return false;
            }
            this.l = window.onerror;
            window.onerror = this.A.bind(this);
            this.setEnabled(true);
            return true;
        };
        /** @type {function (): ?} */
        data.prototype.enable = data.prototype.enable;
        /**
         * @param {string} x
         * @param {Object} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {(Object|string)} start
         * @param {Object} error
         * @return {undefined}
         */
        data.prototype.jb = function(x, type, expectedNumberOfNonCommentArgs, start, error) {
            var config = self.yf(window.location.href);
            x = null != error && null != error.message ? error.message : x;
            var data;
            if (data = (!self.za(this.R) || this.R(config.a, x, type, expectedNumberOfNonCommentArgs, start, error)) && this.g) {
                /** @type {Array.<string>} */
                data = Object.keys(this.v);
                /** @type {number} */
                var name = 100;
                /** @type {number} */
                var s = 0;
                for (;s < data.length;++s) {
                    /** @type {string} */
                    var value = data[s];
                    if ((new RegExp(value)).test(x)) {
                        name = this.v[value];
                    }
                }
                /** @type {boolean} */
                data = (100 * Math.random()).toFixed(6) >= this.w * name / 100 ? false : true;
            }
            if (data) {
                if (0 <= this.j) {
                    if (null === this.b) {
                        this.b = new go(this.j, this.G);
                    }
                }
                if (null === this.b) {
                    /** @type {number} */
                    data = 0;
                } else {
                    a: {
                        data = this.b;
                        name = (0, self.sa)();
                        if (data.a.length == data.c) {
                            if (data.a[0] < name - 36E5) {
                                data.a.shift();
                            } else {
                                /** @type {boolean} */
                                data = false;
                                break a;
                            }
                        }
                        data.a.push(name);
                        getter(data);
                        /** @type {boolean} */
                        data = true;
                    }
                    /** @type {boolean} */
                    data = !data;
                }
                /** @type {boolean} */
                data = !data;
            }
            if (data) {
                if (this.g) {
                    data = self.qb(this.a.c);
                    data.url = config.a;
                    /** @type {string} */
                    data.type = "JavascriptError";
                    /** @type {string} */
                    data.error_message = x;
                    /** @type {string} */
                    name = "Other";
                    if (versionOffset) {
                        /** @type {string} */
                        name = "Opera";
                    } else {
                        if (self.x) {
                            /** @type {string} */
                            name = "Internet Explorer";
                        } else {
                            if (index) {
                                /** @type {string} */
                                name = "Firefox";
                            } else {
                                if (vlq) {
                                    /** @type {string} */
                                    name = "Chrome";
                                } else {
                                    if (Xb) {
                                        /** @type {string} */
                                        name = "Safari";
                                    }
                                }
                            }
                        }
                    }
                    /** @type {string} */
                    data.browser = name;
                    data.browser_version = self.Vf;
                    /** @type {string} */
                    data.os = self.w("CrOS") ? "Chrome OS" : self.w("Linux") ? "Linux" : self.wb() ? "Windows" : self.w("Android") ? "Android" : fixedPosition() ? "iPhone" : self.w("iPad") ? "iPad" : self.w("iPod") ? "iPod" : self.vb() ? "Mac" : "Unknown";
                    name = self.jb;
                    /** @type {string} */
                    s = "";
                    if (self.wb()) {
                        /** @type {RegExp} */
                        s = /Windows (?:NT|Phone) ([0-9.]+)/;
                        /** @type {string} */
                        s = (name = s.exec(name)) ? name[1] : "0.0";
                    } else {
                        if (fixedPosition() || (self.w("iPad") || self.w("iPod"))) {
                            /** @type {RegExp} */
                            s = /(?:iPhone|iPod|iPad|CPU)\s+OS\s+(\S+)/;
                            /** @type {(null|string)} */
                            s = (name = s.exec(name)) && name[1].replace(/_/g, ".");
                        } else {
                            if (self.vb()) {
                                /** @type {RegExp} */
                                s = /Mac OS X ([0-9_.]+)/;
                                /** @type {string} */
                                s = (name = s.exec(name)) ? name[1].replace(/_/g, ".") : "10";
                            } else {
                                if (self.w("Android")) {
                                    /** @type {RegExp} */
                                    s = /Android\s+([^\);]+)(\)|;)/;
                                    /** @type {(null|string)} */
                                    s = (name = s.exec(name)) && name[1];
                                } else {
                                    if (self.w("CrOS")) {
                                        /** @type {RegExp} */
                                        s = /(?:CrOS\s+(?:i686|x86_64)\s+([0-9.]+))/;
                                        /** @type {(null|string)} */
                                        s = (name = s.exec(name)) && name[1];
                                    }
                                }
                            }
                        }
                    }
                    /** @type {string} */
                    data.os_version = s || "";
                    if (null != this.a.a) {
                        data.channel = this.a.a;
                    }
                    if (!self.Ja(this.c)) {
                        data.guid = this.c;
                    }
                    if (null != type) {
                        /** @type {Object} */
                        data.src = type;
                    }
                    if (null != expectedNumberOfNonCommentArgs) {
                        /** @type {number} */
                        data.line = expectedNumberOfNonCommentArgs;
                    }
                    if (null != start) {
                        /** @type {(Object|string)} */
                        data.column = start;
                    }
                    data.prod = this.a.b;
                    data.ver = this.a.g;
                    type = normalize(data);
                    error = null != error ? self.Ua(error.stack) : "";
                    if (!self.Ja(x)) {
                        expectedNumberOfNonCommentArgs = error.split("\n");
                        if (-1 < expectedNumberOfNonCommentArgs[0].indexOf(x)) {
                            expectedNumberOfNonCommentArgs.splice(0, 1);
                            error = expectedNumberOfNonCommentArgs.join("\n");
                        }
                    }
                    ajax(this, "https://clients2.google.com/cr/staging_report", type, error);
                    config = {
                        product : this.a.b,
                        url : config.a,
                        js_errors_count : "1"
                    };
                    x = self.Ua(this.a.a);
                    if (!self.Ja(x)) {
                        /** @type {string} */
                        config.version = x;
                    }
                    ajax(this, "https://clients2.google.com/cr/staging_perf", normalize(config));
                }
            }
        };
        /** @type {function (string, Object, number, (Object|string), Object): undefined} */
        data.prototype.reportError = data.prototype.jb;
        /**
         * @param {string} x
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {number} ll
         * @param {string} arr
         * @param {Object} config
         * @return {undefined}
         */
        data.prototype.A = function(x, expectedNumberOfNonCommentArgs, ll, arr, config) {
            this.jb(x, expectedNumberOfNonCommentArgs, ll, arr, config);
            if (null != this.l) {
                if (self.za(this.l)) {
                    this.l(x, expectedNumberOfNonCommentArgs, ll, arr, config);
                }
            }
        };
        /**
         * @param {Object} a
         * @return {?}
         */
        var normalize = function(a) {
            /** @type {string} */
            var out = "";
            var prefix;
            for (prefix in a) {
                out += self.Ja(out) ? "?" : "&";
                out += prefix + "=" + (0, window.encodeURIComponent)(a[prefix]);
            }
            return out;
        };
        /**
         * @param {Object} options
         * @param {string} i
         * @param {string} path
         * @param {string} type
         * @return {undefined}
         */
        var ajax = function(options, i, path, type) {
            type = type || "";
            /** @type {({Content-Type: string}|{})} */
            var next = self.Ja(type) ? {} : {
                "Content-Type" : "text/plain"
            };
            self.Je(i + path, options.I.bind(options, i), "POST", type, next, 6E4, true);
        };
        /**
         * @param {Object} m
         * @param {?} e
         * @return {undefined}
         */
        data.prototype.I = function(m, e) {
            self.Te(e.target);
        };
        /**
         * @param {?} val
         * @return {undefined}
         */
        var b = function(val) {
            fn.call(this, val);
            /** @type {null} */
            this.R = null;
            /** @type {number} */
            this.b = 0;
        };
        self.p(b, fn);
        /**
         * @param {string} cur
         * @return {?}
         */
        b.prototype.mb = function(cur) {
            /** @type {string} */
            this.R = cur;
            return this;
        };
        /** @type {Array} */
        var codeSegments = [{
            age : 6E4,
            name : "1m"
        }, {
            age : 15E5,
            name : "25m"
        }, {
            age : 36E5,
            name : "1h"
        }, {
            age : 36E6,
            name : "10h"
        }];
        /**
         * @return {?}
         */
        b.prototype.enable = function() {
            if (this.h) {
                return false;
            }
            if (window.performance && window.performance.memory) {
                var e = new self.ze(6E4);
                self.I(this, e);
                var v = new self.N(this);
                self.I(this, v);
                v.listen(e, "tick", this.c);
                self.Ae(e);
                this.setEnabled(true);
                return true;
            }
            return false;
        };
        /**
         * @return {undefined}
         */
        b.prototype.c = function() {
            this.b += 6E4;
            /** @type {string} */
            var data = "";
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;++i) {
                if (this.b == codeSegments[i].age) {
                    data = codeSegments[i].name;
                    break;
                }
            }
            if ("" != data) {
                /** @type {string} */
                i = rhtml.test(self.jb) ? "" : "32_bit_";
                var res = window.performance.memory;
                if (res.totalJSHeapSize && res.usedJSHeapSize) {
                    /** @type {Array} */
                    data = [{
                        name : "total_" + i + "js_heap_size_mb_" + data + "_raw",
                        displayName : "Total JS heap (MB)",
                        value : Math.round(res.totalJSHeapSize / 1048576)
                    }, {
                        name : "used_" + i + "js_heap_size_mb_" + data + "_raw",
                        displayName : "Used JS heap (MB)",
                        value : Math.round(res.usedJSHeapSize / 1048576)
                    }];
                    i = self.yf(window.location.href).a;
                    res = "?product=" + (0, window.encodeURIComponent)(this.a.b) + "&url=" + (0, window.encodeURIComponent)(i);
                    var j = self.Ua(this.a.a);
                    if (!self.Ja(j)) {
                        res += "&version=" + (0, window.encodeURIComponent)(j);
                    }
                    /** @type {number} */
                    j = 0;
                    for (;j < data.length;++j) {
                        res += "&" + (0, window.encodeURIComponent)(data[j].name) + "=" + data[j].value;
                        if (self.za(this.R)) {
                            this.R(i, data[j].displayName, data[j].value);
                        }
                    }
                    self.Je("https://clients2.google.com/cr/staging_perf" + res, drop, "POST", "", null, 6E4, true);
                }
            }
        };
        /**
         * @param {Event} ev
         * @return {undefined}
         */
        var drop = function(ev) {
            self.Te(ev.target);
        };
        /** @type {RegExp} */
        var rhtml = /(x86_64|Win64)/;
        /**
         * @param {?} n
         * @return {undefined}
         */
        var x = function(n) {
            self.F.call(this);
            this.b = new valueRef(n);
            this.a = {};
        };
        self.p(x, self.F);
        self.wa("webmonitoring.Monitoring", x);
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        x.prototype.setVersion = function(deepDataAndEvents) {
            this.b.setVersion(deepDataAndEvents);
            return this;
        };
        /** @type {function (?): ?} */
        x.prototype.setVersion = x.prototype.setVersion;
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        x.prototype.Oa = function(deepDataAndEvents) {
            this.b.Oa(deepDataAndEvents);
            return this;
        };
        /** @type {function (?): ?} */
        x.prototype.setChannel = x.prototype.Oa;
        /**
         * @param {string} caption
         * @return {?}
         */
        x.prototype.Za = function(caption) {
            this.b.Za(caption);
            return this;
        };
        /** @type {function (string): ?} */
        x.prototype.setContext = x.prototype.Za;
        /**
         * @return {?}
         */
        x.prototype.Ka = function() {
            if (!(null != this.a[2])) {
                this.a[2] = new data(this.b);
            }
            return this.a[2];
        };
        /** @type {function (): ?} */
        x.prototype.createJsErrorsReporter = x.prototype.Ka;
        /**
         * @param {?} id
         * @return {undefined}
         */
        var r = function(id) {
            x.call(this, id);
        };
        self.p(r, x);
        /**
         * @return {?}
         */
        r.prototype.Ka = function() {
            var cleanCache = r.D.Ka.call(this);
            /** @type {boolean} */
            cleanCache.g = true;
            return cleanCache;
        };
        /**
         * @return {undefined}
         */
        self.Eg = function() {
            var that = new r("Google_Hangouts_Chrome");
            that.Oa(window.chrome.runtime.id);
            that.setVersion(window.chrome.runtime.getManifest().version);
            if (!(null != that.a[1])) {
                that.a[1] = new b(that.b);
            }
            that.a[1].enable();
            that.Ka().pb(false).nb(100).eb("Script error.", 0).enable();
        };
        /**
         * @param {?} c
         * @param {?} b
         * @return {undefined}
         */
        var t = function(c, b) {
            this.c = c;
            this.b = b;
            if (!this.constructor.fb) {
                this.constructor.fb = {};
            }
            this.constructor.fb[this.toString()] = this;
        };
        /**
         * @return {?}
         */
        t.prototype.toString = function() {
            if (!this.a) {
                /** @type {string} */
                this.a = this.c.a + ":" + this.b;
            }
            return this.a;
        };
        /**
         * @param {?} item
         * @param {?} cb
         * @return {undefined}
         */
        var op = function(item, cb) {
            t.call(this, item, cb);
        };
        self.p(op, t);
        /**
         * @param {?} a
         * @return {undefined}
         */
        var array_to_hash = function(a) {
            this.a = a;
        };
        new array_to_hash("lib");
        var dataAttr;
        var template;
        var Game;
        var songs;
        /**
         * @param {Object} data
         * @return {undefined}
         */
        self.Jg = function(data) {
            self.F.call(this);
            this.g = {};
            this.j = {};
            this.l = {};
            this.a = {};
            this.c = {};
            this.I = {};
            this.v = data ? data.J() : new self.L;
            /** @type {boolean} */
            this.P = !data;
            /** @type {null} */
            this.b = null;
            if (data) {
                /** @type {Object} */
                this.b = data;
                this.l = data.l;
                this.a = data.a;
                this.j = data.j;
                this.c = data.c;
            } else {
                (0, self.sa)();
            }
            data = dataAttr(this);
            if (this != data) {
                if (data.h) {
                    data.h.push(this);
                } else {
                    /** @type {Array} */
                    data.h = [this];
                }
            }
        };
        self.p(self.Jg, self.F);
        /** @type {boolean} */
        self.Kg = 0.05 > Math.random();
        /**
         * @param {Object} data
         * @return {?}
         */
        dataAttr = function(data) {
            for (;data.b;) {
                data = data.b;
            }
            return data;
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.Jg.prototype.get = function(name) {
            var win = self.Lg(this, name);
            if (null == win) {
                throw new Test(name);
            }
            return win;
        };
        /**
         * @param {string} ll
         * @param {string} name
         * @return {?}
         */
        self.Lg = function(ll, name) {
            /** @type {string} */
            var data = ll;
            for (;data;data = data.b) {
                if (data.K) {
                    throw Error("v");
                }
                if (data.g[name]) {
                    return data.g[name][0];
                }
                if (data.I[name]) {
                    break;
                }
            }
            if (data = ll.l[name]) {
                data = data(ll);
                if (null == data) {
                    throw Error("w`" + name);
                }
                self.Ng(ll, name, data);
                return data;
            }
            return null;
        };
        /**
         * @param {string} x
         * @param {string} type
         * @return {?}
         */
        self.Jg.prototype.G = function(x, type) {
            var ll = self.Lg(this, x);
            if (null == ll) {
                if (this.c[x]) {
                    var isSorted = this.c[x].h();
                    self.Kd(isSorted, (0, self.r)(this.G, this, x, type));
                    return isSorted;
                }
                throw new match(x, type, "Module loaded but service or factory not registered with app contexts.");
            }
            return ll.Ta ? (isSorted = new self.Dd, self.Ld(isSorted, ll.Ta()), isSorted.b(ll), self.Kd(isSorted, (0, self.r)(this.w, this, x)), isSorted) : this.w(x);
        };
        /**
         * @param {string} str
         * @return {?}
         */
        self.Jg.prototype.w = function(str) {
            if (this.c[str]) {
                delete this.c[str];
            }
            return this.get(str);
        };
        /**
         * @param {string} a
         * @param {string} mod
         * @param {string} d
         * @return {?}
         */
        self.Jg.prototype.L = function(a, mod, d) {
            return d instanceof self.Ed ? d : new handler(a, mod, d);
        };
        /**
         * @param {Object} data
         * @param {?} name
         * @param {?} fn
         * @param {(Array|string)} result
         * @return {?}
         */
        self.Ng = function(data, name, fn, result) {
            if (data.K) {
                if (!result) {
                    self.H(fn);
                }
            } else {
                /** @type {Array} */
                data.g[name] = [fn, !result];
                result = template(data, data, name);
                /** @type {number} */
                var i = 0;
                for (;i < result.length;i++) {
                    result[i].b(null);
                }
                delete data.j[name];
                return fn;
            }
        };
        /**
         * @param {Object} data
         * @param {string} view
         * @param {?} name
         * @return {?}
         */
        template = function(data, view, name) {
            /** @type {Array} */
            var str = [];
            var caption = data.a[name];
            if (caption) {
                self.Za(caption, function(a) {
                    var b;
                    a: {
                        b = a.Sa;
                        for (;b;) {
                            if (b == view) {
                                /** @type {boolean} */
                                b = true;
                                break a;
                            }
                            b = b.b;
                        }
                        /** @type {boolean} */
                        b = false;
                    }
                    if (b) {
                        str.push(a.d);
                        self.gb(caption, a);
                    }
                });
                if (0 == caption.length) {
                    delete data.a[name];
                }
            }
            return str;
        };
        /**
         * @param {Node} config
         * @param {?} dataAndEvents
         * @return {undefined}
         */
        Game = function(config, dataAndEvents) {
            if (config.a) {
                send(config.a, function(caption, off, buf) {
                    self.Za(caption, function(ready) {
                        if (ready.Sa == dataAndEvents) {
                            self.gb(caption, ready);
                        }
                    });
                    if (0 == caption.length) {
                        delete buf[off];
                    }
                });
            }
        };
        /**
         * @param {?} n
         * @param {?} d
         * @return {undefined}
         */
        self.Jg.prototype.H = function(n, d) {
            var which = this.a && this.a[n];
            if (which) {
                /** @type {number} */
                var i = 0;
                for (;i < which.length;++i) {
                    if (which[i].Sa == this && which[i].d == d) {
                        self.fb(which, i);
                        break;
                    }
                }
                if (0 == which.length) {
                    delete this.a[n];
                }
            }
        };
        /**
         * @return {undefined}
         */
        self.Jg.prototype.B = function() {
            if (dataAttr(this) == this) {
                var h = this.h;
                if (h) {
                    for (;h.length;) {
                        h[0].Z();
                    }
                }
            } else {
                h = dataAttr(this).h;
                /** @type {number} */
                var i = 0;
                for (;i < h.length;i++) {
                    if (h[i] == this) {
                        h.splice(i, 1);
                        break;
                    }
                }
            }
            var hi;
            for (hi in this.g) {
                h = this.g[hi];
                if (h[1]) {
                    if (h[0].Z) {
                        h[0].Z();
                    }
                }
            }
            /** @type {null} */
            this.g = null;
            if (this.P) {
                this.v.Z();
            }
            Game(this, this);
            /** @type {null} */
            this.a = null;
            self.H(this.O);
            /** @type {null} */
            this.I = this.O = null;
            self.Jg.D.B.call(this);
        };
        /**
         * @return {?}
         */
        self.Jg.prototype.J = function() {
            return this.v;
        };
        /**
         * @param {string} id
         * @return {undefined}
         */
        var Test = function(id) {
            self.Fa.call(this);
            /** @type {string} */
            this.id = id;
            /** @type {string} */
            this.message = 'Service for "' + id + '" is not registered';
        };
        self.p(Test, self.Fa);
        /**
         * @param {string} token
         * @param {string} execAsap
         * @param {?} err
         * @return {undefined}
         */
        var handler = function(token, execAsap, err) {
            self.Fa.call(this);
            /** @type {string} */
            this.message = 'Module "' + execAsap + '" failed to load when requesting the service "' + token + '" [cause: ' + err + "]";
            /** @type {string} */
            this.stack = err.stack + "\nWRAPPED BY:\n" + this.stack;
        };
        self.p(handler, self.Fa);
        /**
         * @param {string} consume
         * @param {string} other
         * @param {string} args
         * @return {undefined}
         */
        var match = function(consume, other, args) {
            self.Fa.call(this);
            /** @type {string} */
            this.message = 'Configuration error when loading the module "' + other + '" for the service "' + consume + '": ' + args;
        };
        self.p(match, self.Fa);
        songs = new array_to_hash("fva");
        self.Tg = new op(songs, 1);
    } catch (conf) {
        self._DumpException(conf);
    }
    try {
        /** @type {RegExp} */
        var whitespaceRegex = /\s*;\s*/;
        /**
         * @return {?}
         */
        var objEquiv = function() {
            return[];
        };
        /**
         * @param {Object} c
         * @return {?}
         */
        var update = function(c) {
            c = (c.a.cookie || "").split(whitespaceRegex);
            /** @type {Array} */
            var keys = [];
            /** @type {Array} */
            var values = [];
            var index;
            var part;
            /** @type {number} */
            var i = 0;
            for (;part = c[i];i++) {
                index = part.indexOf("=");
                if (-1 == index) {
                    keys.push("");
                    values.push(part);
                } else {
                    keys.push(part.substring(0, index));
                    values.push(part.substring(index + 1));
                }
            }
            return{
                keys : keys,
                values : values
            };
        };
        /**
         * @param {string} method
         * @param {Object} collection
         * @param {Array} codeSegments
         * @param {?} u
         * @return {?}
         */
        var success = function(method, collection, codeSegments, u) {
            var go;
            if (codeSegments.length) {
                if (u) {
                    /**
                     * @param {?} index
                     * @return {?}
                     */
                    go = function(index) {
                        var obj = this.a[codeSegments[0]];
                        return obj ? obj[method].apply(this.a[codeSegments[0]], arguments) : this.ba[codeSegments[0]].prototype[method].apply(this, arguments);
                    };
                } else {
                    if (collection[method].Kb) {
                        /**
                         * @param {?} index
                         * @return {?}
                         */
                        go = function(index) {
                            var data;
                            a: {
                                /** @type {Array.<?>} */
                                data = Array.prototype.slice.call(arguments, 0);
                                /** @type {number} */
                                var i = 0;
                                for (;i < codeSegments.length;++i) {
                                    var selector = this.a[codeSegments[i]];
                                    if (selector = selector ? selector[method].apply(selector, data) : this.ba[codeSegments[i]].prototype[method].apply(this, data)) {
                                        data = selector;
                                        break a;
                                    }
                                }
                                /** @type {boolean} */
                                data = false;
                            }
                            return data;
                        };
                    } else {
                        if (collection[method].Jb) {
                            /**
                             * @param {?} index
                             * @return {?}
                             */
                            go = function(index) {
                                var data;
                                a: {
                                    /** @type {Array.<?>} */
                                    data = Array.prototype.slice.call(arguments, 0);
                                    /** @type {number} */
                                    var i = 0;
                                    for (;i < codeSegments.length;++i) {
                                        var selector = this.a[codeSegments[i]];
                                        selector = selector ? selector[method].apply(selector, data) : this.ba[codeSegments[i]].prototype[method].apply(this, data);
                                        if (null != selector) {
                                            data = selector;
                                            break a;
                                        }
                                    }
                                    data = void 0;
                                }
                                return data;
                            };
                        } else {
                            if (collection[method].$b) {
                                /**
                                 * @param {?} index
                                 * @return {undefined}
                                 */
                                go = function(index) {
                                    /** @type {Array.<?>} */
                                    var args = Array.prototype.slice.call(arguments, 0);
                                    /** @type {number} */
                                    var i = 0;
                                    for (;i < codeSegments.length;++i) {
                                        var reporter = this.a[codeSegments[i]];
                                        if (reporter) {
                                            reporter[method].apply(reporter, args);
                                        } else {
                                            this.ba[codeSegments[i]].prototype[method].apply(this, args);
                                        }
                                    }
                                };
                            } else {
                                /**
                                 * @param {?} index
                                 * @return {?}
                                 */
                                go = function(index) {
                                    /** @type {Array.<?>} */
                                    var args = Array.prototype.slice.call(arguments, 0);
                                    /** @type {Array} */
                                    var self = [];
                                    /** @type {number} */
                                    var i = 0;
                                    for (;i < codeSegments.length;++i) {
                                        var value = this.a[codeSegments[i]];
                                        self.push(value ? value[method].apply(value, args) : this.ba[codeSegments[i]].prototype[method].apply(this, args));
                                    }
                                    return self;
                                };
                            }
                        }
                    }
                }
            } else {
                if (u || (collection[method].Kb || (collection[method].Jb || collection[method].$b))) {
                    /** @type {null} */
                    go = null;
                } else {
                    /** @type {function (): ?} */
                    go = objEquiv;
                }
            }
            return go;
        };
        /**
         * @param {string} name
         * @param {Object} data
         * @param {Array} args
         * @param {boolean} input
         * @return {?}
         */
        var c = function(name, data, args, input) {
            /** @type {Array} */
            var bProperties = [];
            /** @type {number} */
            var i = 0;
            for (;i < args.length && (args[i].prototype[name] === data[name] || (bProperties.push(i), !input));++i) {
            }
            return bProperties;
        };
        /**
         * @param {Array} buffer
         * @param {?} start
         * @return {undefined}
         */
        var inc = function(buffer, start) {
            /** @type {number} */
            var i = 1;
            for (;i < arguments.length;i++) {
                var data = arguments[i];
                if (self.Ba(data)) {
                    var index = buffer.length || 0;
                    var length = data.length || 0;
                    buffer.length = index + length;
                    /** @type {number} */
                    var k = 0;
                    for (;k < length;k++) {
                        buffer[index + k] = data[k];
                    }
                } else {
                    buffer.push(data);
                }
            }
        };
        /** @type {Array} */
        var parameter = [1];
        /**
         * @param {Object} a
         * @param {string} optgroup
         * @return {?}
         */
        var require = function(a, optgroup) {
            return a.c.get(optgroup);
        };
        /**
         * @param {(Document|string)} a
         * @return {undefined}
         */
        var C = function(a) {
            this.a = a || {
                    cookie : ""
                };
        };
        self.g = C.prototype;
        /**
         * @param {string} name
         * @param {?} value
         * @param {string} ll
         * @param {boolean} domain
         * @param {boolean} path
         * @param {boolean} secure
         * @return {undefined}
         */
        self.g.set = function(name, value, ll, domain, path, secure) {
            if (/[;=\s]/.test(name)) {
                throw Error("p`" + name);
            }
            if (/[;\r\n]/.test(value)) {
                throw Error("q`" + value);
            }
            if (!self.m(ll)) {
                /** @type {number} */
                ll = -1;
            }
            /** @type {string} */
            path = path ? ";domain=" + path : "";
            /** @type {string} */
            domain = domain ? ";path=" + domain : "";
            /** @type {string} */
            secure = secure ? ";secure" : "";
            /** @type {string} */
            ll = 0 > ll ? "" : 0 == ll ? ";expires=" + (new Date(1970, 1, 1)).toUTCString() : ";expires=" + (new Date((0, self.sa)() + 1E3 * ll)).toUTCString();
            /** @type {string} */
            this.a.cookie = name + "=" + value + path + domain + ll + secure;
        };
        /**
         * @param {string} name
         * @param {?} string
         * @return {?}
         */
        self.g.get = function(name, string) {
            /** @type {string} */
            var nameEq = name + "=";
            var parts = (this.a.cookie || "").split(whitespaceRegex);
            /** @type {number} */
            var i = 0;
            var part;
            for (;part = parts[i];i++) {
                if (0 == part.lastIndexOf(nameEq, 0)) {
                    return part.substr(nameEq.length);
                }
                if (part == name) {
                    return "";
                }
            }
            return string;
        };
        /**
         * @param {string} cookieName
         * @param {Object} keepData
         * @param {boolean} path
         * @return {?}
         */
        self.g.remove = function(cookieName, keepData, path) {
            var isRemoved = self.m(this.get(cookieName));
            this.set(cookieName, "", 0, keepData, path);
            return isRemoved;
        };
        /**
         * @return {?}
         */
        self.g.X = function() {
            return update(this).keys;
        };
        /**
         * @return {?}
         */
        self.g.V = function() {
            return update(this).values;
        };
        /**
         * @return {undefined}
         */
        self.g.clear = function() {
            var keys = update(this).keys;
            /** @type {number} */
            var i = keys.length - 1;
            for (;0 <= i;i--) {
                this.remove(keys[i]);
            }
        };
        /**
         * @param {string} x
         * @return {?}
         */
        var g = function(x) {
            return this.Ua.g(x);
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var prop = function(funcToCall) {
            self.z(this, funcToCall, "ci:ec", null);
        };
        self.p(prop, self.y);
        /** @type {string} */
        prop.messageId = "ci:ec";
        var obj = {
            a : self.ef,
            G : self.ff,
            v : self.gf,
            w : self.hf,
            C : self.jf,
            I : self.kf
        };
        /**
         * @param {(Function|string)} object
         * @return {?}
         */
        var keys = function(object) {
            var value = object.oa;
            /**
             * @param {?} n
             * @return {undefined}
             */
            var e = function(n) {
                e.D.constructor.call(this, n);
                var valuesLen = this.ba.length;
                /** @type {Array} */
                this.a = [];
                /** @type {number} */
                var i = 0;
                for (;i < valuesLen;++i) {
                    if (!this.ba[i].Cc) {
                        this.a[i] = new this.ba[i](n);
                    }
                }
            };
            self.p(e, value);
            /** @type {Array} */
            var b = [];
            for (;object;) {
                if (value = object.oa) {
                    if (value.ba) {
                        inc(b, value.ba);
                    }
                    var data = value.prototype;
                    var i;
                    for (i in data) {
                        if (data.hasOwnProperty(i) && (self.za(data[i]) && data[i] !== value)) {
                            /** @type {boolean} */
                            var u = !!data[i].Ib;
                            var r = c(i, data, b, u);
                            if (u = success(i, data, r, u)) {
                                e.prototype[i] = u;
                            }
                        }
                    }
                }
                object = object.D && object.D.constructor;
            }
            /** @type {Array} */
            e.prototype.ba = b;
            return e;
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var seen = function(funcToCall) {
            self.z(this, funcToCall, "capi:r_rstr", null);
        };
        self.p(seen, self.y);
        /** @type {string} */
        seen.messageId = "capi:r_rstr";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var connection = function(funcToCall) {
            self.z(this, funcToCall, "capi:r_recl", null);
        };
        self.p(connection, self.y);
        /** @type {string} */
        connection.messageId = "capi:r_recl";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var result = function(funcToCall) {
            self.z(this, funcToCall, "capi:r_clre", null);
        };
        self.p(result, self.y);
        /** @type {string} */
        result.messageId = "capi:r_clre";
        /**
         * @param {Object} args
         * @param {?} ms
         * @return {undefined}
         */
        var format = function(args, ms) {
            var types = {};
            try {
                /** @type {*} */
                types = JSON.parse(ms) || {};
            } catch (e) {
            }
            var type;
            for (type in types) {
                switch(type) {
                    case "r":
                        args.j = types[type];
                        break;
                    case "i":
                        args.a = types[type];
                        break;
                    case "m":
                        args.b = types[type];
                        break;
                    case "o":
                        args.C = types[type];
                        break;
                    case "s":
                        args.c = types[type];
                        break;
                    case "d":
                        args.h = types[type];
                        break;
                    case "st":
                        args.g = types[type];
                        break;
                    case "dt":
                        args.l = types[type];
                }
            }
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var fix = function(funcToCall) {
            self.z(this, funcToCall, 0, parameter);
        };
        self.p(fix, self.y);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var scripts = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_hrd", null);
        };
        self.p(scripts, self.y);
        /** @type {string} */
        scripts.messageId = "uv_r_hrd";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var message = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_rrr", null);
        };
        self.p(message, self.y);
        /** @type {string} */
        message.messageId = "uv_r_rrr";
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var msg = function(funcToCall) {
            self.z(this, funcToCall, "uv_r_rdr", null);
        };
        self.p(msg, self.y);
        /** @type {string} */
        msg.messageId = "uv_r_rdr";
        /**
         * @param {Object} options
         * @param {boolean} batches
         * @param {(Object|string)} req
         * @return {undefined}
         */
        var run = function(options, batches, req) {
            if (!this.Ua) {
                var value = this.constructor;
                for (;value && !value.oa;) {
                    value = value.D && value.D.constructor;
                }
                if (!value.oa.hb) {
                    value.oa.hb = keys(value);
                }
                this.Ua = new value.oa.hb(this);
                if (!this.g) {
                    /** @type {function (string): ?} */
                    this.g = g;
                }
            }
            var arg = this.Ua.b(options.clientBaseUrl || "https://talkgadget.google.com/talkgadget/");
            if (self.Ha(arg, "https://talkgadget.google.com/talkgadget/")) {
                value = (new C(window.document)).get("WCSDBG", "");
                value = value.split(";")[0];
                value = value.split("|")[0];
                /** @type {Array} */
                var camelKey = ["plus.google.com"];
                /** @type {RegExp} */
                var rchecked = /^[-_a-zA-Z0-9\.]+\.talkgadget\.google\.com$/;
                if (0 < value.length) {
                    if (!self.eb(camelKey, value)) {
                        if (rchecked.test(value)) {
                            arg = self.yf(arg);
                            arg = self.yf("https://" + value + arg.a).toString();
                        }
                    }
                }
            }
            this.ma = arg;
            this.c = req || new prop;
            if (options.notificationsOnly) {
                self.B(this.c, 1, true);
            }
            /** @type {string} */
            this.b = "0";
            if (null != options.authUser) {
                this.b = options.authUser;
            } else {
                req = require(new self.P(window.location.href), "authuser");
                if (null != req) {
                    /** @type {(Object|string)} */
                    this.b = req;
                }
            }
            this.v = options.clientUrl || "notifierclient";
            req = require(self.yf(this.v), "authuser");
            if (null != req) {
                /** @type {(Object|string)} */
                this.b = req;
            }
            this.Ra = options.propertyName || "Google Talk";
            this.G = options.cssUrl;
            this.locale = options.locale;
            this.C = options.debug;
            this.Ca = options.userJid;
            this.Ga = options.uiVersion;
            this.Ub = null != options.rosterExpanded ? options.rosterExpanded : true;
            this.Y = options.hostLogMinValue;
            this.Da = options.hideChat;
            this.Nb = options.refresh;
            this.O = options.hideProfileCard;
            this.W = options.hideStatusMsgs;
            this.T = options.hideRosterOptions;
            this.P = options.hideRecentConversations;
            this.M = options.hideOTR;
            this.Hb = options.hostServerMessageCallback || false;
            this.aa = options.isCentralRoster;
            this.Mb = options.isFullFrame || false;
            this.Tb = options.rosterClass || "talk_roster";
            this.Vb = options.rosterId || "talk_roster";
            /** @type {string} */
            this.a = "gtn-roster-iframe-id";
            if (options.rosterFrameId) {
                if (options.rosterFrameId.match(/[\w-]/)) {
                    this.a = options.rosterFrameId;
                }
            }
            if (options.rosterIframeIdB) {
                options.rosterIframeIdB.match(/[\w-]/);
            }
            this.va = options.parentToken;
            this.Rb = batches || false;
            this.j = self.m(options.isRtl) ? options.isRtl : self.oc.test(this.locale);
            this.position = options.position || "l";
            if (!options.keyboardShortcuts) {
                /** @type {Array} */
                batches = [];
                var property;
                for (property in obj) {
                    batches.push(self.D(obj[property]));
                }
            }
            this.A = options.entityId;
            this.Ha = options.useQuasarPanels || false;
            this.Ea = options.silent || false;
            this.Lb = options.ignoreCentralRoster || false;
            this.Qb = options.lazy || false;
            this.Gb = options.handleProfileLinks || false;
            this.$ = options.hostSettings;
            this.l = options.screen;
            this.I = options.docsConfig;
            this.Ob = options.jsmode;
            this.ua = options.moleWidth;
            this.ha = options.moleHeight;
            this.ga = options.minimizedMoleHeight || 36;
            this.Wb = options.derp;
            /** @type {string} */
            this.Fa = window.location.protocol + "//" + window.location.host;
            /** @type {string} */
            this.h = "";
            this.L = options.optOutAllowed;
            this.H = options.legacyTalkSignedIn;
            this.Ia = options.useSpareMole;
            this.w = options.conversationId;
        };
        run.oa = self.bf;
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var cycle = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(cycle, self.y);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var deferred = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(deferred, self.y);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var dblclick = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(dblclick, self.y);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var EVENT_READY = function(funcToCall) {
            self.z(this, funcToCall, 0, null);
        };
        self.p(EVENT_READY, self.y);
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var instance = function(funcToCall) {
            self.z(this, funcToCall, "h_cc", null);
        };
        self.p(instance, self.y);
        /** @type {string} */
        instance.messageId = "h_cc";
        /**
         * @param {boolean} f
         * @param {Object} millis
         * @return {undefined}
         */
        var defer = function(f, millis) {
            self.rd(f, null, millis, void 0);
        };
        /**
         * @param {string} x
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {boolean} recurring
         * @return {?}
         */
        var parseInt = function(x, expectedNumberOfNonCommentArgs, recurring) {
            x = self.A(x, expectedNumberOfNonCommentArgs);
            return null == x ? recurring : x;
        };
        /**
         * @param {number} value
         * @param {Object} recurring
         * @param {Function} error
         * @return {undefined}
         */
        var formatError = function(value, recurring, error) {
            if (!self.yd(value, recurring, error, null)) {
                self.fd(self.xa(recurring, value));
            }
        };
        /**
         * @param {Array} base
         * @return {?}
         */
        var resolve = function(base) {
            return new self.kd(function(fire, done) {
                var baseSize = base.length;
                /** @type {Array} */
                var memory = [];
                if (baseSize) {
                    /**
                     * @param {number} key
                     * @param {?} value
                     * @return {undefined}
                     */
                    var recurring = function(key, value) {
                        baseSize--;
                        memory[key] = value;
                        if (0 == baseSize) {
                            fire(memory);
                        }
                    };
                    /**
                     * @param {?} err
                     * @return {undefined}
                     */
                    var error = function(err) {
                        done(err);
                    };
                    /** @type {number} */
                    var r = 0;
                    var udataCur;
                    for (;r < base.length;r++) {
                        udataCur = base[r];
                        formatError(udataCur, self.xa(recurring, r), error);
                    }
                } else {
                    fire(memory);
                }
            });
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        var slice = function(a) {
            this.a = a;
            if (!self.x) {
                delete this.a.lpu;
                delete this.a.ppu;
            }
        };
        /** @type {Array} */
        var resultItems = ["cn", "tp"];
        /**
         * @param {?} err
         * @param {?} items
         * @return {?}
         */
        var complete = function(err, items) {
            var data;
            if (self.za(window.document.postMessage) || (self.za(window.postMessage) || self.x && window.postMessage)) {
                /** @type {number} */
                data = 1;
            } else {
                if (self.Cb) {
                    /** @type {number} */
                    data = 2;
                } else {
                    var item;
                    if (item = self.x) {
                        /** @type {boolean} */
                        item = false;
                        try {
                            data = window.opener;
                            window.opener = {};
                            item = self.yb(window, "opener");
                            window.opener = data;
                        } catch (e) {
                        }
                    }
                    /** @type {number} */
                    data = item ? 6 : 4;
                }
            }
            item = {};
            /** @type {string} */
            item.cn = Math.floor(2147483648 * Math.random()).toString(36);
            /** @type {number} */
            item.tp = data;
            item.ifrid = items;
            item.pu = err;
            return new slice(item);
        };
        self.wa("GTalk_IFrameChannelConfig.build", complete);
        /**
         * @param {Object} options
         * @param {string} body
         * @param {number} i
         * @param {boolean} node
         * @param {Object} result
         * @return {undefined}
         */
        var parse = function(options, body, i, node, result) {
            self.F.call(this);
            /** @type {Object} */
            this.a = options;
            this.b = node || window.document;
            this.c = new self.N(this);
            self.I(this, this.c);
            var data = self.jb;
            node = new self.P(options.ma + options.v);
            if (!parseInt(options.c, 2, false)) {
                self.R(node, "ts", "0");
                self.R(node, "re", options.Ub);
                self.R(node, "ref", options.Nb);
            }
            self.R(node, "client", "sm");
            self.R(node, "prop", options.Ra);
            self.R(node, "nav", "true");
            self.R(node, "fid", options.a);
            if (options.C) {
                self.R(node, "debug", options.C);
            }
            self.R(node, "os", self.Fb);
            self.R(node, "stime", i);
            if (options.Da) {
                self.R(node, "hc", options.Da);
            }
            /** @type {number} */
            i = 0;
            for (;i < resultItems.length;++i) {
            }
            body = self.cc(body.a);
            self.R(node, "xpc", body);
            self.R(node, "ec", self.cc(self.D(options.c)));
            if (options.l) {
                self.R(node, "screen", options.l);
            }
            if (-1 != data.indexOf("MSIE")) {
                self.R(node, "ua", data);
            }
            if (options.va) {
                self.R(node, "pvt", options.va);
            }
            if (options.Hb) {
                self.R(node, "hsmc", "true");
            }
            if (self.m(options.Y)) {
                self.R(node, "logl", options.Y);
            }
            body = window.location.href;
            if (0 > body.indexOf("?rel=1")) {
                body = body.split("?");
                /** @type {string} */
                body[1] = 1 == body.length ? "rel=1" : ["rel=1&", body[1]].join("");
                self.R(node, "href", body.join("?"));
            }
            body = new self.P(window.location.href);
            if ("false" == require(body, "uacheck")) {
                self.R(node, "uacheck", false);
            }
            if (data = options.Ob) {
                self.R(node, "jsmode", data);
            }
            if (data = require(body, "cssmode")) {
                self.R(node, "cssmode", data);
            }
            if (data = require(body, "convid")) {
                self.R(node, "convid", data);
            }
            if (data = body.c.V("convp")) {
                if (!self.Ca(data)) {
                    /** @type {Array} */
                    data = [String(data)];
                }
                self.Ef(node.c, "convp", data);
            }
            if (data = require(body, "smexp")) {
                self.R(node, "smexp", data);
            }
            data = body.c.V("mods");
            if (options.Qb) {
                data.push("lazy");
            }
            if (result) {
                data = self.hb(data, result);
            }
            if (0 < data.length) {
                self.R(node, "mods", data.join(","));
            }
            if (!options.ma || 0 > options.ma.indexOf("/u/")) {
                if (result = require(body, "authuser")) {
                    self.R(node, "authuser", result);
                } else {
                    if (null != options.b) {
                        self.R(node, "authuser", options.b);
                    }
                }
            }
            result = require(body, "eid");
            if (!self.Ja(self.Ua(result))) {
                self.R(node, "eid", result);
            }
            if (!self.Ja(self.Ua(this.a.A))) {
                self.R(node, "eid", this.a.A);
            }
            if (null != this.a.position) {
                self.R(node, "pos", this.a.position);
            }
            if (self.m(options.Ga)) {
                self.R(node, "uiv", this.a.Ga);
            }
            if (options.G) {
                self.R(node, "css", options.G);
            }
            result = options.locale || require(body, "hl");
            if (null != result) {
                self.R(node, "hl", result);
            }
            if (self.m(options.Ca)) {
                self.R(node, "uj", options.Ca);
            }
            if (null != options.O) {
                self.R(node, "hpc", options.O);
            }
            if (null != options.W) {
                self.R(node, "hsm", options.W);
            }
            if (null != options.T) {
                self.R(node, "hro", options.T);
            }
            if (null != options.P) {
                self.R(node, "hrc", options.P);
            }
            if (null != options.M) {
                self.R(node, "hotr", options.M);
            }
            if (null != options.aa) {
                self.R(node, "pop", options.aa);
            }
            if (options.Rb) {
                self.R(node, "pal", "1");
            }
            if (null != options.Ha) {
                self.R(node, "uqp", options.Ha);
            }
            if (null != options.L) {
                self.R(node, "gooa", options.L);
            }
            if (null != options.H) {
                self.R(node, "gltsi", options.H);
            }
            if (null != options.Ia) {
                self.R(node, "gusm", options.Ia);
            }
            if (null != options.Ea) {
                self.R(node, "sl", options.Ea);
            }
            if (options.Lb) {
                self.R(node, "icr", "1");
            }
            if (options.Gb) {
                self.R(node, "hpl", "1");
            }
            if (options.$) {
                self.R(node, "hs", options.$);
            }
            if (options.I) {
                self.R(node, "dc", self.cc(self.D(options.I)));
            }
            if (null != options.ua) {
                self.R(node, "molew", options.ua);
            }
            if (null != options.ha) {
                self.R(node, "moleh", options.ha);
            }
            if (null != options.ga) {
                self.R(node, "mmoleh", options.ga);
            }
            if (options.Wb) {
                self.R(node, "derp", "1");
            }
            if (options.h) {
                self.R(node, "fcho", options.h);
            }
            if (null != options.Fa) {
                self.R(node, "two", options.Fa);
            }
            if (null != options.w) {
                self.R(node, "convid", options.w);
            }
            self.R(node, "host", "1");
            self.R(node, "zx", self.Va());
            this.g = node.toString();
        };
        self.p(parse, self.F);
        /**
         * @param {Node} data
         * @param {?} id
         * @param {Function} parentRequire
         * @param {Function} cb
         * @return {undefined}
         */
        var load = function(data, id, parentRequire, cb) {
            var s = data.b.getElementById(id);
            if (!s) {
                s = data.b.createElement("div");
                s.id = id;
                s.className = data.a.Tb;
                data.b.body.appendChild(s);
            }
            data = (0, self.r)(function() {
                /** @type {string} */
                var styles = "";
                if (!this.a.Mb) {
                    /** @type {string} */
                    styles = "height:400px";
                }
                var container = this.b.createElement("iframe");
                self.zc(container, {
                    id : this.a.a,
                    name : this.a.a,
                    "class" : "talk_iframe",
                    frameborder : "0",
                    style : styles,
                    "aria-hidden" : "true"
                });
                if (!self.xc("talk_loading_msg", s)) {
                    self.Cc(s);
                }
                s.appendChild(container);
                var el = this.b.getElementById(this.a.a);
                self.Ze(this.c, el, ["load", "error"], function(outErr) {
                    el.contentWindow.postMessage("AllowFrame: Chat", "*");
                    cb(outErr);
                });
                el.src = this.g.toString();
                parentRequire();
            }, data);
            window.setTimeout(data, 0);
        };
        var optgroup = new self.O("q");
        var rvar = new self.O("r");
        /**
         * @param {Object} n
         * @return {undefined}
         */
        var root = function(n) {
            self.L.call(this);
            this.a = n.get(rvar);
            n = new self.N(this);
            self.I(this, n);
            n.listen(this.a.J(), "uv_r_frm", this.b);
            n.listen(this.a.J(), "uv_r_rdm", this.g);
        };
        self.p(root, self.L);
        /**
         * @param {Object} recurring
         * @return {undefined}
         */
        root.prototype.reload = function(recurring) {
            var ll = new self.If;
            self.B(ll, 1, recurring);
            this.a.sendMessage(ll);
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        root.prototype.b = function(x) {
            x = new self.Jf(x.S);
            self.M(this, new m(!!self.A(x, 1)));
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        root.prototype.g = function(x) {
            x = new self.Hf(x.S);
            self.M(this, new onComplete(self.A(x, 1)));
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        var m = function(a) {
            self.K.call(this, "uv_r_frm");
            this.a = a;
        };
        self.p(m, self.K);
        /**
         * @param {?} a
         * @return {undefined}
         */
        var onComplete = function(a) {
            self.K.call(this, "uv_r_rdm");
            this.a = a;
        };
        self.p(onComplete, self.K);
        /**
         * @param {string} name
         * @return {?}
         */
        var newContext = function(name) {
            var e = self.l.GTalkNotifier;
            return e ? self.od(e) : new self.kd(function(onload, $sanitize) {
                var timeoutKey = self.Be(function() {
                    return $sanitize("chat.host.Client is not available!");
                }, 25E3);
                var s = window.document.createElement("SCRIPT");
                s.src = self.Tf(name);
                s.addEventListener("load", function() {
                    self.Ce(timeoutKey);
                    if (e = self.l.GTalkNotifier) {
                        onload(e);
                    } else {
                        $sanitize("chat.host.Client is not available!");
                    }
                });
                s.addEventListener("error", function() {
                    self.Ce(timeoutKey);
                    $sanitize("chat.host.Client is not available!");
                });
                window.document.body.appendChild(s);
            });
        };
        /**
         * @param {Object} m
         * @return {undefined}
         */
        var p = function(m) {
            self.L.call(this);
            this.a = m.get(self.T);
            m = new self.N(this);
            self.I(m, m);
            m.listen(this.a.J(), "capi:r_clre", this.h);
            m.listen(this.a.J(), "capi:r_rrst", this.b);
            m.listen(this.a.J(), "capi:r_rers", this.g);
        };
        self.p(p, self.L);
        /**
         * @param {string} x
         * @return {undefined}
         */
        p.prototype.h = function(x) {
            x = new result(x.S);
            self.M(this, new map(self.C(x, fix, 1)));
        };
        /**
         * @return {undefined}
         */
        p.prototype.g = function() {
            self.M(this, "I");
        };
        /**
         * @return {undefined}
         */
        p.prototype.b = function() {
            self.M(this, "H");
        };
        /**
         * @param {?} array
         * @return {undefined}
         */
        var map = function(array) {
            self.K.call(this, "G");
            this.a = array;
        };
        self.p(map, self.K);
        /**
         * @param {Node} options
         * @return {undefined}
         */
        var src = function(options) {
            self.N.call(this);
            this.b = options.get(self.Pf);
            this.a = options.get(optgroup);
            this.listen(this.a, "uv_r_rdm", this.Eb);
            this.listen(this.a, "uv_r_frm", this.Db);
            this.listen(this.b, "G", this.ic);
            this.listen(this.b, "H", this.Cb);
            this.listen(this.b, "I", this.Fb);
        };
        self.p(src, self.N);
        self.g = src.prototype;
        /**
         * @param {string} ll
         * @return {undefined}
         */
        self.g.ic = function(ll) {
            /** @type {Array} */
            ll = (ll = ll.a) ? [self.D(ll)] : [void 0];
            this.a.reload(ll);
        };
        /**
         * @return {undefined}
         */
        self.g.Cb = function() {
            var w = this.a;
            var ll = new msg;
            w.a.sendMessage(ll);
        };
        /**
         * @param {boolean} opts
         * @return {undefined}
         */
        self.g.Db = function(opts) {
            var b = this.b;
            /** @type {boolean} */
            opts = !!opts.a;
            var ll = new connection;
            self.B(ll, 1, !!opts);
            b.a.sendMessage(ll);
        };
        /**
         * @param {Object} node
         * @return {undefined}
         */
        self.g.Eb = function(node) {
            if (node.a && (node = node.a[0])) {
                node = new fix(node);
                var b = this.b;
                var m = new seen;
                self.E(m, 1, node);
                b.a.sendMessage(m);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.Fb = function() {
            var w = this.a;
            var ll = new message;
            w.a.sendMessage(ll);
        };
        /**
         * @param {(Function|string)} value
         * @return {?}
         */
        var stat = function(value) {
            return new self.kd(function(handler, success) {
                self.Je(self.zf(value, "/webchat/extension-start"), function(ev) {
                    if (!self.Te(ev.target)) {
                        success();
                    }
                    try {
                        /** @type {*} */
                        var e = JSON.parse(self.Ue(ev.target));
                        handler(new self.Rf(e));
                    } catch (v) {
                        success(v.message);
                    }
                });
            });
        };
        /**
         * @param {string} row
         * @return {undefined}
         */
        var tree = function(row) {
            self.K.call(this, row[0]);
            /** @type {string} */
            this.S = row;
        };
        self.p(tree, self.K);
        /**
         * @param {string} ll
         * @param {Function} Class
         * @return {undefined}
         */
        var process = function(ll, Class) {
            self.F.call(this);
            this.v = (0, self.sa)();
            var options = {};
            var subscription = parseInt(ll, 1, "hangouts.google.com");
            /** @type {string} */
            var p = "";
            if (0 > subscription.indexOf("://")) {
                /** @type {string} */
                p = "https://";
                if (0 <= subscription.indexOf(":")) {
                    if (23 != self.A(ll, 2)) {
                        if (27 != self.A(ll, 2)) {
                            /** @type {string} */
                            p = "http://";
                        }
                    }
                }
            }
            if (-1 != subscription.indexOf("talkgadget.")) {
                /** @type {string} */
                options.clientBaseUrl = p + subscription + "/talkgadget/_/";
                /** @type {string} */
                options.clientUrl = "chat";
            } else {
                /** @type {string} */
                options.clientBaseUrl = p + subscription + "/webchat/u/" + parseInt(ll, 8, "0") + "/";
                /** @type {string} */
                options.clientUrl = "load";
            }
            options.authUser = parseInt(ll, 8, "0");
            options.propertyName = self.Ff(self.A(ll, 2));
            options.locale = self.A(ll, 4);
            /** @type {boolean} */
            options.hideProfileCard = true;
            options.parentToken = self.A(ll, 3);
            options.isRtl = parseInt(ll, 6, false);
            options.lazy = parseInt(ll, 7, false);
            options.entityId = self.A(ll, 9);
            options.hostSettings = self.A(ll, 5);
            options.jsmode = self.A(ll, 11);
            options.isFullFrame = parseInt(ll, 13, false);
            options.rosterId = parseInt(ll, 12, "talk_roster");
            options.rosterIframeIdB = parseInt(ll, 15, "gtn-roster-iframe-id-b");
            /** @type {boolean} */
            options.derp = !!parseInt(ll, 16, false);
            subscription = self.C(ll, cycle, 14);
            if (null != subscription) {
                options.isCentralRoster = parseInt(subscription, 1, false);
                options.useQuasarPanels = parseInt(subscription, 2, false);
            }
            subscription = self.C(ll, EVENT_READY, 10);
            if (null != subscription) {
                options.moleManagerLeftMargin = self.A(subscription, 1);
                options.moleManagerMiddleMargin = self.A(subscription, 2);
                options.moleManagerRightMargin = self.A(subscription, 3);
                options.moleManagerNoMoleAreaMargin = self.A(subscription, 4);
                options.moleManagerBoundingElementId = self.A(subscription, 5);
                options.moleWidth = self.A(subscription, 6);
                options.moleHeight = self.A(subscription, 7);
                options.minimizedMoleHeight = self.A(subscription, 8);
            }
            subscription = self.C(ll, dblclick, 18);
            if (null != subscription) {
                options.optOutAllowed = parseInt(subscription, 1, true);
                p = self.C(subscription, deferred, 3);
                if (null != p) {
                    options.legacyTalkSignedIn = parseInt(p, 1, true);
                }
                subscription = self.A(subscription, 4);
                if (null != subscription) {
                    options.useSpareMole = subscription;
                }
            }
            /** @type {number} */
            options.uiVersion = 2;
            /** @type {boolean} */
            options.hideStatusMsgs = true;
            /** @type {boolean} */
            options.hideRecentConversations = true;
            subscription = new prop;
            self.B(subscription, 2, true);
            self.B(subscription, 1, true);
            self.B(subscription, 3, false);
            options.endpointConfig = subscription;
            options.ignoreAutomatedRoster = parseInt(ll, 17, false);
            options.conversationId = self.A(ll, 19);
            this.g = options;
            options = this.a = new run(this.g, true, this.g.endpointConfig);
            var v = this.a.ma;
            subscription = this.a.b;
            if (-1 != v.indexOf("talkgadget.")) {
                p = self.yf(v);
                var a = p.a;
                if (0 <= a.indexOf("/u/")) {
                    subscription = v;
                } else {
                    v = a.lastIndexOf("_/");
                    if (0 > v || v != a.length - 2) {
                        a += "_/";
                    }
                    /** @type {string} */
                    p.a = "/u/" + subscription + a;
                    subscription = p.toString();
                }
            } else {
                subscription = v;
            }
            options.ma = subscription;
            this.a.h = window.location.origin;
            /** @type {null} */
            this.b = null;
            this.l = new self.L;
            options = self.qb(this.g);
            options.endpointConfig = self.D(this.a.c);
            this.h = new Class(options, true);
            this.j = new self.ig;
            self.I(this, this.j);
            refresh(this);
        };
        self.p(process, self.F);
        /**
         * @param {string} ll
         * @return {undefined}
         */
        process.prototype.sendMessage = function(ll) {
            this.xa(self.D(ll));
        };
        /**
         * @param {Object} fix
         * @return {undefined}
         */
        process.prototype.xa = function(fix) {
            this.c("_h", fix);
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        process.prototype.$a = function(a) {
            this.b = a;
            self.G(this, function() {
                /** @type {null} */
                this.b = null;
            }, this);
        };
        /**
         * @return {?}
         */
        process.prototype.J = function() {
            return this.l;
        };
        /**
         * @param {Object} e
         * @return {undefined}
         */
        var refresh = function(e) {
            var value = complete(e.a.ma, e.a.a);
            value = new parse(e.a, value, e.v, window.document);
            self.I(e, value);
            load(value, e.a.Vb, function() {
            }, function() {
                return onload(e);
            });
        };
        /**
         * @param {Object} item
         * @return {undefined}
         */
        var onload = function(item) {
            self.mg(item.j, window.document.getElementById(item.a.a).contentWindow, "31ACFC0_", "google.com");
            item.c("_addClientMessageCallback", function(recurring) {
                if (item.b) {
                    item.b.xa(recurring);
                }
                self.M(item.l, new tree(recurring));
            });
            item.c("_rosterIframeLoaded");
        };
        /**
         * @param {string} x
         * @param {Function} type
         * @return {undefined}
         */
        process.prototype.c = function(x, type) {
            /** @type {Array} */
            var callbackArgs = [];
            /** @type {number} */
            var i = 1;
            for (;i < arguments.length;i++) {
                callbackArgs.push(arguments[i]);
            }
            if (this.h[x]) {
                this.h[x].apply(this.h, callbackArgs);
            }
        };
        /**
         * @param {number} funcToCall
         * @return {undefined}
         */
        var l = function(funcToCall) {
            self.z(this, funcToCall, "h_ext", null);
        };
        self.p(l, self.y);
        /** @type {string} */
        l.messageId = "h_ext";
        /**
         * @return {undefined}
         */
        var f = function() {
            self.N.call(this);
            self.S("Starting ...");
            this.a = new self.Jg;
            self.I(this, this.a);
            /** @type {null} */
            this.j = null;
            /** @type {Array} */
            this.h = [];
            this.listen(window, "unload", this.Z);
            this.c = new self.Kf(JSON.parse((0, window.decodeURI)(window.location.hash.substr(1))));
            var server = self.A(this.c, 1);
            var s = new self.ig;
            self.kg(s);
            s.a = new self.cg((0, self.r)(s.H, s), server);
            s.a.da("*", void 0, void 0);
            s.g.push(self.xa(self.H, s.a));
            this.b = s;
            self.Ng(this.a, rvar, this.b);
            this.listen(this.b.J(), "uv_qm", this.l);
            next(this);
        };
        var ll;
        self.p(f, self.N);
        /**
         * @param {Object} num
         * @param {?} callback
         * @return {undefined}
         */
        var write = function(num, callback) {
            if (null == num.h) {
                callback();
            } else {
                num.h.push(callback);
            }
        };
        /**
         * @param {Object} result
         * @return {undefined}
         */
        var next = function(result) {
            self.S("Initializing ...");
            defer(resolve([stat(self.A(result.c, 2)).then(result.w, void 0, result), newContext(result.c)]).then(function(x) {
                self.ga();
                var method = x[window.Symbol.iterator];
                x = method ? method.call(x) : self.fa(x);
                var ret = x.next().value;
                x = x.next().value;
                if (result.K) {
                    /** @type {null} */
                    x = null;
                } else {
                    method = new instance;
                    var right = self.yf(self.A(result.c, 2));
                    var b = right.b;
                    if (null != right.g) {
                        b += ":" + right.g;
                    }
                    self.B(method, 1, b);
                    self.B(method, 2, 27);
                    self.B(method, 13, true);
                    self.B(method, 17, true);
                    ret = self.A(ret, 1);
                    self.B(method, 3, ret);
                    ret = self.A(result.c, 4);
                    self.B(method, 11, ret);
                    ret = self.A(result.c, 3) || "en";
                    self.B(method, 4, ret);
                    ret = new cycle;
                    self.B(ret, 2, true);
                    self.B(ret, 1, true);
                    self.E(method, 14, ret);
                    x = new process(method, x);
                }
                return x;
            }).then(function(d) {
                if (!result.K) {
                    d.$a(result.b);
                    result.b.$a(d);
                    /** @type {Object} */
                    result.j = d;
                    self.Ng(result.a, self.T, result.j);
                    d = new root(result.a);
                    self.Ng(result.a, optgroup, d);
                    self.Ng(result.a, self.Pf, new p(result.a));
                    new src(result.a);
                    result.b.sendMessage(new scripts);
                    for (;result.h.length;) {
                        result.h.shift()();
                    }
                    /** @type {null} */
                    result.h = null;
                }
            }), function(err) {
                self.S("Failed to start : %s.", err || "");
            });
        };
        /**
         * @param {string} str
         * @return {?}
         */
        f.prototype.w = function(str) {
            var w = this;
            return new self.kd(function(toString, $sanitize) {
                w.b.sendMessage(str);
                if (!self.A(str, 1)) {
                    $sanitize("Unauthenticated");
                }
                toString(str);
            });
        };
        /**
         * @param {string} x
         * @return {undefined}
         */
        f.prototype.l = function(x) {
            x = new self.Gf(x.S);
            var e = new self.Wf;
            format(e, self.A(x, 1));
            if ("onSendHostMessage" == e.a) {
                write(this, (0, self.r)(function() {
                    var ll = new l;
                    var recurring = e.content().toString();
                    self.B(ll, 1, recurring);
                    this.j.sendMessage(ll);
                }, this));
            }
        };
        self.wa("default_host._DumpException", function(e) {
            return window.console.error(e.stack);
        });
        self.Eg();
        if (!self.m(ll) || (null == ll || ll.K)) {
            ll = new f;
        }
    } catch (fieldset) {
        self._DumpException(fieldset);
    }
}).call(this, this.default_host);
