this.default_main = this.default_main || {};
(function(self) {
    var window = this;
    try {
        var defineProperty;
        var arr;
        var learn;
        var ea;
        var isPlainObject;
        var freeze;
        var proto;
        var matches;
        var sel;
        self.g;
        /** @type {Function} */
        defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function(obj, prop, desc) {
            if (desc.get || desc.set) {
                throw new TypeError("ES3 does not support getters and setters.");
            }
            if (obj != Array.prototype) {
                if (obj != Object.prototype) {
                    obj[prop] = desc.value;
                }
            }
        };
        arr = "undefined" != typeof window && window === this ? this : "undefined" != typeof window.global ? window.global : this;
        /**
         * @return {undefined}
         */
        learn = function() {
            /**
             * @return {undefined}
             */
            learn = function() {
            };
            if (!arr.Symbol) {
                arr.Symbol = isPlainObject;
            }
        };
        /** @type {number} */
        ea = 0;
        /**
         * @param {string} value
         * @return {?}
         */
        isPlainObject = function(value) {
            return "jscomp_symbol_" + (value || "") + ea++;
        };
        /**
         * @return {undefined}
         */
        self.ga = function() {
            learn();
            var methodName = arr.Symbol.iterator;
            if (!methodName) {
                methodName = arr.Symbol.iterator = arr.Symbol("iterator");
            }
            if ("function" != typeof Array.prototype[methodName]) {
                defineProperty(Array.prototype, methodName, {
                    configurable : true,
                    writable : true,
                    /**
                     * @return {?}
                     */
                    value : function() {
                        return self.fa(this);
                    }
                });
            }
            /**
             * @return {undefined}
             */
            self.ga = function() {
            };
        };
        /**
         * @param {string} types
         * @return {?}
         */
        self.fa = function(types) {
            /** @type {number} */
            var i = 0;
            return freeze(function() {
                return i < types.length ? {
                    done : false,
                    value : types[i++]
                } : {
                    done : true
                };
            });
        };
        /**
         * @param {Object} object
         * @return {?}
         */
        freeze = function(object) {
            self.ga();
            object = {
                next : object
            };
            /**
             * @return {?}
             */
            object[arr.Symbol.iterator] = function() {
                return this;
            };
            return object;
        };
        proto = arr;
        /** @type {Array} */
        matches = ["String", "prototype", "repeat"];
        /** @type {number} */
        sel = 0;
        for (;sel < matches.length - 1;sel++) {
            var ext = matches[sel];
            if (!(ext in proto)) {
                proto[ext] = {};
            }
            proto = proto[ext];
        }
        var name = matches[matches.length - 1];
        var opt_locale = proto[name];
        var locale = opt_locale ? opt_locale : function(types) {
            var s;
            if (null == this) {
                throw new TypeError("The 'this' value for String.prototype.repeat must not be null or undefined");
            }
            /** @type {string} */
            s = this + "";
            if (0 > types || 1342177279 < types) {
                throw new window.RangeError("Invalid count value");
            }
            types |= 0;
            /** @type {string} */
            var slarge = "";
            for (;types;) {
                if (types & 1 && (slarge += s), types >>>= 1) {
                    s += s;
                }
            }
            return slarge;
        };
        if (locale != opt_locale) {
            if (null != locale) {
                defineProperty(proto, name, {
                    configurable : true,
                    writable : true,
                    value : locale
                });
            }
        }
        self.qa = self.qa || {};
        self.l = this;
        /** @type {string} */
        self.ra = "closure_uid_" + (1E9 * Math.random() >>> 0);
        /** @type {function (): number} */
        self.sa = Date.now || function() {
                return+new Date;
            };
    } catch (conf) {
        self._DumpException(conf);
    }
    try {
        var $goog$bindJs_$;
        var on;
        /**
         * @param {string} types
         * @return {?}
         */
        self.m = function(types) {
            return void 0 !== types;
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        $goog$bindJs_$ = function(types, type, expectedNumberOfNonCommentArgs) {
            if (!types) {
                throw Error();
            }
            if (2 < arguments.length) {
                /** @type {Array.<?>} */
                var args = Array.prototype.slice.call(arguments, 2);
                return function() {
                    /** @type {Array.<?>} */
                    var newArgs = Array.prototype.slice.call(arguments);
                    Array.prototype.unshift.apply(newArgs, args);
                    return types.apply(type, newArgs);
                };
            }
            return function() {
                return types.apply(type, arguments);
            };
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        on = function(types, type, expectedNumberOfNonCommentArgs) {
            return types.call.apply(types.bind, arguments);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        self.va = function(value) {
            /** @type {string} */
            var $s = typeof value;
            if ("object" == $s) {
                if (value) {
                    if (value instanceof Array) {
                        return "array";
                    }
                    if (value instanceof Object) {
                        return $s;
                    }
                    /** @type {string} */
                    var isFunction = Object.prototype.toString.call(value);
                    if ("[object Window]" == isFunction) {
                        return "object";
                    }
                    if ("[object Array]" == isFunction || "number" == typeof value.length && ("undefined" != typeof value.splice && ("undefined" != typeof value.propertyIsEnumerable && !value.propertyIsEnumerable("splice")))) {
                        return "array";
                    }
                    if ("[object Function]" == isFunction || "undefined" != typeof value.call && ("undefined" != typeof value.propertyIsEnumerable && !value.propertyIsEnumerable("call"))) {
                        return "function";
                    }
                } else {
                    return "null";
                }
            } else {
                if ("function" == $s && "undefined" == typeof value.call) {
                    return "object";
                }
            }
            return $s;
        };
        /**
         * @param {Function} type
         * @param {Function} b
         * @return {undefined}
         */
        self.p = function(type, b) {
            /**
             * @return {undefined}
             */
            function __() {
            }
            __.prototype = b.prototype;
            type.D = b.prototype;
            type.prototype = new __;
            /** @type {Function} */
            type.prototype.constructor = type;
            /**
             * @param {?} opt_context
             * @param {?} name
             * @param {?} dataAndEvents
             * @return {?}
             */
            type.Ac = function(opt_context, name, dataAndEvents) {
                /** @type {Array} */
                var args = Array(arguments.length - 2);
                /** @type {number} */
                var i = 2;
                for (;i < arguments.length;i++) {
                    args[i - 2] = arguments[i];
                }
                return b.prototype[name].apply(opt_context, args);
            };
        };
        /**
         * @param {string} pair
         * @param {string} ready
         * @return {undefined}
         */
        self.wa = function(pair, ready) {
            var parts = pair.split(".");
            var cur = self.l;
            if (!(parts[0] in cur)) {
                if (!!cur.execScript) {
                    cur.execScript("var " + parts[0]);
                }
            }
            var part;
            for (;parts.length && (part = parts.shift());) {
                if (!parts.length && self.m(ready)) {
                    /** @type {string} */
                    cur[part] = ready;
                } else {
                    if (cur[part]) {
                        cur = cur[part];
                    } else {
                        cur = cur[part] = {};
                    }
                }
            }
        };
        /**
         * @param {Function} callback
         * @param {Function} keepData
         * @return {?}
         */
        self.xa = function(callback, keepData) {
            /** @type {Array.<?>} */
            var args = Array.prototype.slice.call(arguments, 1);
            return function() {
                /** @type {Array.<?>} */
                var newArgs = args.slice();
                newArgs.push.apply(newArgs, arguments);
                return callback.apply(this, newArgs);
            };
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        self.r = function(types, type, expectedNumberOfNonCommentArgs) {
            self.r = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? on : $goog$bindJs_$;
            return self.r.apply(null, arguments);
        };
        /**
         * @param {?} object
         * @return {?}
         */
        self.ya = function(object) {
            /** @type {string} */
            var $s = typeof object;
            return "object" == $s && null != object || "function" == $s;
        };
        /**
         * @param {Function} arg
         * @return {?}
         */
        self.za = function(arg) {
            return "function" == self.va(arg);
        };
        /**
         * @param {Object} value
         * @return {?}
         */
        self.Aa = function(value) {
            return "number" == typeof value;
        };
        /**
         * @param {string} val
         * @return {?}
         */
        self.t = function(val) {
            return "string" == typeof val;
        };
        /**
         * @param {string} arg
         * @return {?}
         */
        self.Ba = function(arg) {
            var type = self.va(arg);
            return "array" == type || "object" == type && "number" == typeof arg.length;
        };
        /**
         * @param {?} type
         * @return {?}
         */
        self.Ca = function(type) {
            return "array" == self.va(type);
        };
        /**
         * @return {undefined}
         */
        self.Da = function() {
        };
        /**
         * @param {string} w
         * @return {?}
         */
        self.Ea = function(w) {
            w = w.split(".");
            var item = self.l;
            var root;
            for (;root = w.shift();) {
                if (null != item[root]) {
                    item = item[root];
                } else {
                    return null;
                }
            }
            return item;
        };
        /**
         * @param {?} opt_msg
         * @return {undefined}
         */
        self.Fa = function(opt_msg) {
            if (Error.captureStackTrace) {
                Error.captureStackTrace(this, self.Fa);
            } else {
                /** @type {string} */
                var stack = Error().stack;
                if (stack) {
                    /** @type {string} */
                    this.stack = stack;
                }
            }
            if (opt_msg) {
                /** @type {string} */
                this.message = String(opt_msg);
            }
        };
        self.p(self.Fa, Error);
        /** @type {string} */
        self.Fa.prototype.name = "CustomError";
        var tag;
        var indentMultiline;
        var rreturn;
        var splitter;
        var regExclamation;
        var regexTrim;
        var splatParam;
        var unsafe_chars;
        var reWhitespace;
        var equals;
        var posLess;
        /**
         * @param {?} str
         * @param {string} prefix
         * @return {?}
         */
        self.Ha = function(str, prefix) {
            return 0 == str.lastIndexOf(prefix, 0);
        };
        /**
         * @param {string} a
         * @param {string} b
         * @return {?}
         */
        self.Ia = function(a, b) {
            /** @type {number} */
            var startIndex = a.length - b.length;
            return 0 <= startIndex && a.indexOf(b, startIndex) == startIndex;
        };
        /**
         * @param {string} key
         * @return {?}
         */
        self.Ja = function(key) {
            return/^[\s\xa0]*$/.test(key);
        };
        /** @type {function (string): ?} */
        indentMultiline = String.prototype.trim ? function(buf) {
            return buf.trim();
        } : function(lastLine) {
            return lastLine.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "");
        };
        /**
         * @param {string} types
         * @return {?}
         */
        self.Sa = function(types) {
            if (!reWhitespace.test(types)) {
                return types;
            }
            if (-1 != types.indexOf("&")) {
                types = types.replace(rreturn, "&amp;");
            }
            if (-1 != types.indexOf("<")) {
                types = types.replace(splitter, "&lt;");
            }
            if (-1 != types.indexOf(">")) {
                types = types.replace(regExclamation, "&gt;");
            }
            if (-1 != types.indexOf('"')) {
                types = types.replace(regexTrim, "&quot;");
            }
            if (-1 != types.indexOf("'")) {
                types = types.replace(splatParam, "&#39;");
            }
            if (-1 != types.indexOf("\x00")) {
                types = types.replace(unsafe_chars, "&#0;");
            }
            return types;
        };
        /** @type {RegExp} */
        rreturn = /&/g;
        /** @type {RegExp} */
        splitter = /</g;
        /** @type {RegExp} */
        regExclamation = />/g;
        /** @type {RegExp} */
        regexTrim = /"/g;
        /** @type {RegExp} */
        splatParam = /'/g;
        /** @type {RegExp} */
        unsafe_chars = /\x00/g;
        /** @type {RegExp} */
        reWhitespace = /[\x00&<>"']/;
        /** @type {function ((Object|string), number): ?} */
        equals = String.prototype.repeat ? function(opts, match) {
            return opts.repeat(match);
        } : function(delimit2, l) {
            return Array(l + 1).join(delimit2);
        };
        /**
         * @param {number} s
         * @return {?}
         */
        self.Ua = function(s) {
            return null == s ? "" : String(s);
        };
        /**
         * @return {?}
         */
        self.Va = function() {
            return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ (0, self.sa)()).toString(36);
        };
        /**
         * @param {?} obj
         * @param {(number|string)} opt_attributes
         * @return {?}
         */
        self.Xa = function(obj, opt_attributes) {
            /** @type {number} */
            var year = 0;
            var v1Subs = indentMultiline(String(obj)).split(".");
            var v2Subs = indentMultiline(String(opt_attributes)).split(".");
            /** @type {number} */
            var month = Math.max(v1Subs.length, v2Subs.length);
            /** @type {number} */
            var subIdx = 0;
            for (;0 == year && subIdx < month;subIdx++) {
                var value = v1Subs[subIdx] || "";
                var v2Sub = v2Subs[subIdx] || "";
                do {
                    /** @type {Array} */
                    value = /(\d*)(\D*)(.*)/.exec(value) || ["", "", "", ""];
                    /** @type {Array} */
                    v2Sub = /(\d*)(\D*)(.*)/.exec(v2Sub) || ["", "", "", ""];
                    if (0 == value[0].length && 0 == v2Sub[0].length) {
                        break;
                    }
                    year = posLess(0 == value[1].length ? 0 : (0, window.parseInt)(value[1], 10), 0 == v2Sub[1].length ? 0 : (0, window.parseInt)(v2Sub[1], 10)) || (posLess(0 == value[2].length, 0 == v2Sub[2].length) || posLess(value[2], v2Sub[2]));
                    value = value[3];
                    v2Sub = v2Sub[3];
                } while (0 == year);
            }
            return year;
        };
        /**
         * @param {(boolean|number|string)} a
         * @param {(boolean|number|string)} b
         * @return {?}
         */
        posLess = function(a, b) {
            return a < b ? -1 : a > b ? 1 : 0;
        };
        var register;
        /** @type {function (?, string, ?): ?} */
        register = Array.prototype.indexOf ? function(xs, tests, graphics) {
            return Array.prototype.indexOf.call(xs, tests, graphics);
        } : function(array, value, from) {
            from = null == from ? 0 : 0 > from ? Math.max(0, array.length + from) : from;
            if (self.t(array)) {
                return self.t(value) && 1 == value.length ? array.indexOf(value, from) : -1;
            }
            for (;from < array.length;from++) {
                if (from in array && array[from] === value) {
                    return from;
                }
            }
            return-1;
        };
        /** @type {function (string, Function, number): undefined} */
        self.u = Array.prototype.forEach ? function(types, type, expectedNumberOfNonCommentArgs) {
            Array.prototype.forEach.call(types, type, expectedNumberOfNonCommentArgs);
        } : function(types, type, expectedNumberOfNonCommentArgs) {
            var valuesLen = types.length;
            var arr2 = self.t(types) ? types.split("") : types;
            /** @type {number} */
            var i = 0;
            for (;i < valuesLen;i++) {
                if (i in arr2) {
                    type.call(expectedNumberOfNonCommentArgs, arr2[i], i, types);
                }
            }
        };
        /**
         * @param {string} arr
         * @param {Function} f
         * @return {undefined}
         */
        self.Za = function(arr, f) {
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = arr.length - 1;
            for (;0 <= i;--i) {
                if (i in arr2) {
                    f.call(void 0, arr2[i], i, arr);
                }
            }
        };
        /** @type {function (?, ?, ?): ?} */
        self.$a = Array.prototype.filter ? function(next_scope, mapper, graphics) {
            return Array.prototype.filter.call(next_scope, mapper, graphics);
        } : function(arr, fn, thisv) {
            var e = arr.length;
            /** @type {Array} */
            var res = [];
            /** @type {number} */
            var resLength = 0;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2) {
                    var val = arr2[i];
                    if (fn.call(thisv, val, i, arr)) {
                        res[resLength++] = val;
                    }
                }
            }
            return res;
        };
        /** @type {function (?, ?, ?): ?} */
        self.ab = Array.prototype.map ? function(next_scope, mapper, graphics) {
            return Array.prototype.map.call(next_scope, mapper, graphics);
        } : function(arr, f, opt_obj) {
            var e = arr.length;
            /** @type {Array} */
            var res = Array(e);
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2) {
                    res[i] = f.call(opt_obj, arr2[i], i, arr);
                }
            }
            return res;
        };
        /** @type {function (?, ?, ?): ?} */
        self.bb = Array.prototype.some ? function(next_scope, mapper, graphics) {
            return Array.prototype.some.call(next_scope, mapper, graphics);
        } : function(arr, f, opt_obj) {
            var e = arr.length;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2 && f.call(opt_obj, arr2[i], i, arr)) {
                    return true;
                }
            }
            return false;
        };
        /**
         * @param {string} arr
         * @param {Function} name
         * @return {?}
         */
        self.db = function(arr, name) {
            var i = self.cb(arr, name, void 0);
            return 0 > i ? null : self.t(arr) ? arr.charAt(i) : arr[i];
        };
        /**
         * @param {string} arr
         * @param {Function} f
         * @param {?} opt_obj
         * @return {?}
         */
        self.cb = function(arr, f, opt_obj) {
            var e = arr.length;
            var arr2 = self.t(arr) ? arr.split("") : arr;
            /** @type {number} */
            var i = 0;
            for (;i < e;i++) {
                if (i in arr2 && f.call(opt_obj, arr2[i], i, arr)) {
                    return i;
                }
            }
            return-1;
        };
        /**
         * @param {?} value
         * @param {string} name
         * @return {?}
         */
        self.eb = function(value, name) {
            return 0 <= register(value, name);
        };
        /**
         * @param {?} value
         * @param {string} type
         * @return {?}
         */
        self.gb = function(value, type) {
            var q = register(value, type);
            var color;
            if (color = 0 <= q) {
                self.fb(value, q);
            }
            return color;
        };
        /**
         * @param {?} object
         * @param {?} n
         * @return {?}
         */
        self.fb = function(object, n) {
            return 1 == Array.prototype.splice.call(object, n, 1).length;
        };
        /**
         * @param {Array} onComplete
         * @return {?}
         */
        self.hb = function(onComplete) {
            return Array.prototype.concat.apply(Array.prototype, arguments);
        };
        /**
         * @param {?} data
         * @return {?}
         */
        self.ib = function(data) {
            var length = data.length;
            if (0 < length) {
                /** @type {Array} */
                var result = Array(length);
                /** @type {number} */
                var i = 0;
                for (;i < length;i++) {
                    result[i] = data[i];
                }
                return result;
            }
            return[];
        };
        a: {
            var navigation = self.l.navigator;
            if (navigation) {
                var userAgent = navigation.userAgent;
                if (userAgent) {
                    self.jb = userAgent;
                    break a;
                }
            }
            /** @type {string} */
            self.jb = "";
        }
        /**
         * @param {string} str
         * @return {?}
         */
        self.w = function(str) {
            return-1 != self.jb.indexOf(str);
        };
        var isEmpty;
        var HOP;
        var _isArray;
        var segments;
        /**
         * @param {Object} obj
         * @param {Function} f
         * @param {?} opt_obj
         * @return {undefined}
         */
        self.mb = function(obj, f, opt_obj) {
            var key;
            for (key in obj) {
                f.call(opt_obj, obj[key], key, obj);
            }
        };
        /**
         * @param {Object} obj
         * @param {Function} f
         * @return {?}
         */
        isEmpty = function(obj, f) {
            var key;
            for (key in obj) {
                if (f.call(void 0, obj[key], key, obj)) {
                    return true;
                }
            }
            return false;
        };
        /**
         * @param {Object} obj
         * @return {?}
         */
        HOP = function(obj) {
            /** @type {Array} */
            var res = [];
            /** @type {number} */
            var resLength = 0;
            var key;
            for (key in obj) {
                res[resLength++] = obj[key];
            }
            return res;
        };
        /**
         * @param {string} obj
         * @return {?}
         */
        _isArray = function(obj) {
            /** @type {Array} */
            var res = [];
            /** @type {number} */
            var resLength = 0;
            var key;
            for (key in obj) {
                /** @type {string} */
                res[resLength++] = key;
            }
            return res;
        };
        /**
         * @param {Object} object
         * @return {?}
         */
        self.qb = function(object) {
            var old = {};
            var name;
            for (name in object) {
                old[name] = object[name];
            }
            return old;
        };
        /** @type {Array.<string>} */
        segments = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");
        /**
         * @param {?} obj
         * @param {?} a
         * @return {undefined}
         */
        self.sb = function(obj, a) {
            var name;
            var source;
            /** @type {number} */
            var i = 1;
            for (;i < arguments.length;i++) {
                source = arguments[i];
                for (name in source) {
                    obj[name] = source[name];
                }
                /** @type {number} */
                var x = 0;
                for (;x < segments.length;x++) {
                    name = segments[x];
                    if (Object.prototype.hasOwnProperty.call(source, name)) {
                        obj[name] = source[name];
                    }
                }
            }
        };
        /**
         * @return {?}
         */
        var Edge = function() {
            return(self.w("Chrome") || self.w("CriOS")) && !self.w("Edge");
        };
        var fixedPosition;
        /**
         * @return {?}
         */
        fixedPosition = function() {
            return self.w("iPhone") && (!self.w("iPod") && !self.w("iPad"));
        };
        /**
         * @return {?}
         */
        self.vb = function() {
            return self.w("Macintosh");
        };
        /**
         * @return {?}
         */
        self.wb = function() {
            return self.w("Windows");
        };
        /**
         * @param {?} cur
         * @return {?}
         */
        var sibling = function(cur) {
            sibling[" "](cur);
            return cur;
        };
        /** @type {function (): undefined} */
        sibling[" "] = self.Da;
        /**
         * @param {Object} el
         * @param {string} nodeName
         * @return {?}
         */
        self.yb = function(el, nodeName) {
            try {
                return sibling(el[nodeName]), true;
            } catch (c) {
            }
            return false;
        };
        /**
         * @param {Object} obj
         * @param {string} name
         * @param {Function} definition
         * @return {?}
         */
        self.zb = function(obj, name, definition) {
            return Object.prototype.hasOwnProperty.call(obj, name) ? obj[name] : obj[name] = definition(name);
        };
        var versionOffset;
        var lastWallLeft;
        var navigator;
        var iframeCssFixes;
        var direction;
        var suiteView;
        var initial;
        var doc;
        var fromIndex;
        versionOffset = self.w("Opera");
        self.x = self.w("Trident") || self.w("MSIE");
        lastWallLeft = self.w("Edge");
        self.Cb = self.w("Gecko") && (!(-1 != self.jb.toLowerCase().indexOf("webkit") && !self.w("Edge")) && (!(self.w("Trident") || self.w("MSIE")) && !self.w("Edge")));
        /** @type {boolean} */
        self.Db = -1 != self.jb.toLowerCase().indexOf("webkit") && !self.w("Edge");
        navigator = self.l.navigator || null;
        self.Fb = navigator && navigator.platform || "";
        self.Gb = self.vb();
        self.Hb = self.wb();
        /**
         * @return {?}
         */
        iframeCssFixes = function() {
            var doc = self.l.document;
            return doc ? doc.documentMode : void 0;
        };
        a: {
            /** @type {string} */
            var desc = "";
            var namespaceMatch = function() {
                var xhtml = self.jb;
                if (self.Cb) {
                    return/rv\:([^\);]+)(\)|;)/.exec(xhtml);
                }
                if (lastWallLeft) {
                    return/Edge\/([\d\.]+)/.exec(xhtml);
                }
                if (self.x) {
                    return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(xhtml);
                }
                if (self.Db) {
                    return/WebKit\/(\S+)/.exec(xhtml);
                }
                if (versionOffset) {
                    return/(?:Version)[ \/]?(\S+)/.exec(xhtml);
                }
            }();
            if (namespaceMatch) {
                desc = namespaceMatch ? namespaceMatch[1] : "";
            }
            if (self.x) {
                var fmt = iframeCssFixes();
                if (null != fmt && fmt > (0, window.parseFloat)(desc)) {
                    /** @type {string} */
                    direction = String(fmt);
                    break a;
                }
            }
            direction = desc;
        }
        suiteView = direction;
        initial = {};
        /**
         * @param {string} opt_attributes
         * @return {?}
         */
        self.Pb = function(opt_attributes) {
            return self.zb(initial, opt_attributes, function() {
                return 0 <= self.Xa(suiteView, opt_attributes);
            });
        };
        doc = self.l.document;
        fromIndex = doc && self.x ? iframeCssFixes() || ("CSS1Compat" == doc.compatMode ? (0, window.parseInt)(suiteView, 10) : 5) : void 0;
        var index = self.w("Firefox");
        var program = fixedPosition() || self.w("iPod");
        var inverse = self.w("iPad");
        var Vb = self.w("Android") && !(Edge() || (self.w("Firefox") || (self.w("Opera") || self.w("Silk"))));
        var tmp = Edge();
        var Xb = self.w("Safari") && (!(Edge() || (self.w("Coast") || (self.w("Opera") || (self.w("Edge") || (self.w("Silk") || self.w("Android")))))) && !(fixedPosition() || (self.w("iPad") || self.w("iPod"))));
        /** @type {null} */
        var props = null;
        var _FormParagraphs;
        var trim;
        var e;
        var str;
        var flags;
        var rSlash;
        var getValue;
        /**
         * @param {string} text
         * @return {?}
         */
        _FormParagraphs = function(text) {
            return/^\s*$/.test(text) ? false : /^[\],:{}\s\u2028\u2029]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, "@").replace(/(?:"[^"\\\n\r\u2028\u2029\x00-\x08\x0a-\x1f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)[\s\u2028\u2029]*(?=:|,|]|}|$)/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""));
        };
        /**
         * @param {string} text
         * @return {?}
         */
        trim = function(text) {
            /** @type {string} */
            text = String(text);
            if (_FormParagraphs(text)) {
                try {
                    return eval("(" + text + ")");
                } catch (b) {
                }
            }
            throw Error("c`" + text);
        };
        /**
         * @param {Function} value
         * @param {Function} how
         * @return {?}
         */
        self.cc = function(value, how) {
            /** @type {Array} */
            var buf = [];
            str(new e(how), value, buf);
            return buf.join("");
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        e = function(a) {
            this.a = a;
        };
        /**
         * @param {Node} b
         * @param {Function} value
         * @param {Array} x
         * @return {undefined}
         */
        str = function(b, value, x) {
            if (null == value) {
                x.push("null");
            } else {
                if ("object" == typeof value) {
                    if (self.Ca(value)) {
                        /** @type {Function} */
                        var a = value;
                        value = a.length;
                        x.push("[");
                        /** @type {string} */
                        var c = "";
                        /** @type {number} */
                        var i = 0;
                        for (;i < value;i++) {
                            x.push(c);
                            c = a[i];
                            str(b, b.a ? b.a.call(a, String(i), c) : c, x);
                            /** @type {string} */
                            c = ",";
                        }
                        x.push("]");
                        return;
                    }
                    if (value instanceof String || (value instanceof Number || value instanceof Boolean)) {
                        /** @type {*} */
                        value = value.valueOf();
                    } else {
                        x.push("{");
                        /** @type {string} */
                        i = "";
                        for (a in value) {
                            if (Object.prototype.hasOwnProperty.call(value, a)) {
                                c = value[a];
                                if ("function" != typeof c) {
                                    x.push(i);
                                    getValue(a, x);
                                    x.push(":");
                                    str(b, b.a ? b.a.call(value, a, c) : c, x);
                                    /** @type {string} */
                                    i = ",";
                                }
                            }
                        }
                        x.push("}");
                        return;
                    }
                }
                switch(typeof value) {
                    case "string":
                        getValue(value, x);
                        break;
                    case "number":
                        x.push((0, window.isFinite)(value) && !(0, window.isNaN)(value) ? String(value) : "null");
                        break;
                    case "boolean":
                        x.push(String(value));
                        break;
                    case "function":
                        x.push("null");
                        break;
                    default:
                        throw Error("d`" + typeof value);;
                }
            }
        };
        flags = {
            '"' : '\\"',
            "\\" : "\\\\",
            "/" : "\\/",
            "\b" : "\\b",
            "\f" : "\\f",
            "\n" : "\\n",
            "\r" : "\\r",
            "\t" : "\\t",
            "\x0B" : "\\u000b"
        };
        /** @type {RegExp} */
        rSlash = /\uffff/.test("\uffff") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g;
        /**
         * @param {string} s
         * @param {Array} code
         * @return {undefined}
         */
        getValue = function(s, code) {
            code.push('"', s.replace(rSlash, function(key) {
                var value = flags[key];
                if (!value) {
                    /** @type {string} */
                    value = "\\u" + (key.charCodeAt(0) | 65536).toString(16).substr(1);
                    /** @type {string} */
                    flags[key] = value;
                }
                return value;
            }), '"');
        };
        var promote;
        var conditional;
        var eql;
        /**
         * @return {undefined}
         */
        self.y = function() {
        };
        /** @type {boolean} */
        self.gc = "function" == typeof window.Uint8Array;
        /**
         * @param {Object} item
         * @param {number} value
         * @param {Object} mayParseLabeledStatementInstead
         * @param {Object} recurring
         * @return {undefined}
         */
        self.z = function(item, value, mayParseLabeledStatementInstead, recurring) {
            /** @type {null} */
            item.b = null;
            if (!value) {
                /** @type {Array} */
                value = mayParseLabeledStatementInstead ? [mayParseLabeledStatementInstead] : [];
            }
            /** @type {(string|undefined)} */
            item.H = mayParseLabeledStatementInstead ? String(mayParseLabeledStatementInstead) : void 0;
            /** @type {number} */
            item.h = 0 === mayParseLabeledStatementInstead ? -1 : 0;
            /** @type {number} */
            item.c = value;
            a: {
                if (item.c.length && (value = item.c.length - 1, (mayParseLabeledStatementInstead = item.c[value]) && ("object" == typeof mayParseLabeledStatementInstead && (!self.Ca(mayParseLabeledStatementInstead) && !(self.gc && mayParseLabeledStatementInstead instanceof window.Uint8Array))))) {
                    /** @type {number} */
                    item.j = value - item.h;
                    /** @type {Object} */
                    item.g = mayParseLabeledStatementInstead;
                    break a;
                }
                /** @type {number} */
                item.j = Number.MAX_VALUE;
            }
            item.A = {};
            if (recurring) {
                /** @type {number} */
                value = 0;
                for (;value < recurring.length;value++) {
                    mayParseLabeledStatementInstead = recurring[value];
                    if (mayParseLabeledStatementInstead < item.j) {
                        mayParseLabeledStatementInstead += item.h;
                        item.c[mayParseLabeledStatementInstead] = item.c[mayParseLabeledStatementInstead] || self.hc;
                    } else {
                        item.g[mayParseLabeledStatementInstead] = item.g[mayParseLabeledStatementInstead] || self.hc;
                    }
                }
            }
        };
        /** @type {Array} */
        self.hc = [];
        /**
         * @param {string} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        self.A = function(type, expectedNumberOfNonCommentArgs) {
            if (expectedNumberOfNonCommentArgs < type.j) {
                var i = expectedNumberOfNonCommentArgs + type.h;
                var elt = type.c[i];
                return elt === self.hc ? type.c[i] = [] : elt;
            }
            elt = type.g[expectedNumberOfNonCommentArgs];
            return elt === self.hc ? type.g[expectedNumberOfNonCommentArgs] = [] : elt;
        };
        /**
         * @param {Object} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {Object} recurring
         * @return {undefined}
         */
        self.B = function(type, expectedNumberOfNonCommentArgs, recurring) {
            if (expectedNumberOfNonCommentArgs < type.j) {
                /** @type {Object} */
                type.c[expectedNumberOfNonCommentArgs + type.h] = recurring;
            } else {
                /** @type {Object} */
                type.g[expectedNumberOfNonCommentArgs] = recurring;
            }
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        self.C = function(types, type, expectedNumberOfNonCommentArgs) {
            if (!types.b) {
                types.b = {};
            }
            if (!types.b[expectedNumberOfNonCommentArgs]) {
                var normalized = self.A(types, expectedNumberOfNonCommentArgs);
                if (normalized) {
                    types.b[expectedNumberOfNonCommentArgs] = new type(normalized);
                }
            }
            return types.b[expectedNumberOfNonCommentArgs];
        };
        /**
         * @param {?} fn
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {string} type
         * @return {undefined}
         */
        self.E = function(fn, expectedNumberOfNonCommentArgs, type) {
            if (!fn.b) {
                fn.b = {};
            }
            var recurring = type ? self.D(type) : type;
            /** @type {string} */
            fn.b[expectedNumberOfNonCommentArgs] = type;
            self.B(fn, expectedNumberOfNonCommentArgs, recurring);
        };
        /**
         * @param {string} parent
         * @return {undefined}
         */
        promote = function(parent) {
            if (parent.b) {
                var k;
                for (k in parent.b) {
                    var ready = parent.b[k];
                    if (self.Ca(ready)) {
                        /** @type {number} */
                        var i = 0;
                        for (;i < ready.length;i++) {
                            if (ready[i]) {
                                self.D(ready[i]);
                            }
                        }
                    } else {
                        if (ready) {
                            self.D(ready);
                        }
                    }
                }
            }
        };
        /**
         * @param {string} types
         * @return {?}
         */
        self.D = function(types) {
            promote(types);
            return types.c;
        };
        conditional = self.l.JSON && self.l.JSON.stringify || "object" === typeof JSON && JSON.stringify;
        /** @type {function (): ?} */
        self.y.prototype.l = self.gc ? function() {
            /** @type {function (): ?} */
            var toJSON = window.Uint8Array.prototype.toJSON;
            /**
             * @return {?}
             */
            window.Uint8Array.prototype.toJSON = function() {
                if (!props) {
                    props = {};
                    /** @type {number} */
                    var p = 0;
                    for (;65 > p;p++) {
                        /** @type {string} */
                        props[p] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(p);
                    }
                }
                p = props;
                /** @type {Array} */
                var that = [];
                /** @type {number} */
                var len = 0;
                for (;len < this.length;len += 3) {
                    var i = this[len];
                    /** @type {boolean} */
                    var invoked = len + 1 < this.length;
                    var name = invoked ? this[len + 1] : 0;
                    /** @type {boolean} */
                    var perm = len + 2 < this.length;
                    var type = perm ? this[len + 2] : 0;
                    /** @type {number} */
                    var key = i >> 2;
                    /** @type {number} */
                    i = (i & 3) << 4 | name >> 4;
                    /** @type {number} */
                    name = (name & 15) << 2 | type >> 6;
                    /** @type {number} */
                    type = type & 63;
                    if (!perm) {
                        /** @type {number} */
                        type = 64;
                        if (!invoked) {
                            /** @type {number} */
                            name = 64;
                        }
                    }
                    that.push(p[key], p[i], p[name], p[type]);
                }
                return that.join("");
            };
            try {
                var b = conditional.call(null, self.D(this), serialize);
            } finally {
                /** @type {function (): ?} */
                window.Uint8Array.prototype.toJSON = toJSON;
            }
            return b;
        } : conditional ? function() {
            return conditional.call(null, self.D(this), serialize);
        } : function() {
            return self.cc(self.D(this), serialize);
        };
        /**
         * @param {?} opt
         * @param {number} object
         * @return {?}
         */
        var serialize = function(opt, object) {
            if (self.Aa(object)) {
                if ((0, window.isNaN)(object)) {
                    return "NaN";
                }
                if (window.Infinity === object) {
                    return "Infinity";
                }
                if (-window.Infinity === object) {
                    return "-Infinity";
                }
            }
            return object;
        };
        /**
         * @return {?}
         */
        self.y.prototype.toString = function() {
            promote(this);
            return this.c.toString();
        };
        /**
         * @param {Object} a
         * @param {Object} obj
         * @return {?}
         */
        eql = function(a, obj) {
            a = a || {};
            obj = obj || {};
            var testSource = {};
            var name;
            for (name in a) {
                /** @type {number} */
                testSource[name] = 0;
            }
            for (name in obj) {
                /** @type {number} */
                testSource[name] = 0;
            }
            for (name in testSource) {
                if (!self.lc(a[name], obj[name])) {
                    return false;
                }
            }
            return true;
        };
        /**
         * @param {Array} a
         * @param {Array} b
         * @return {?}
         */
        self.lc = function(a, b) {
            if (a == b) {
                return true;
            }
            if (!self.ya(a) || (!self.ya(b) || a.constructor != b.constructor)) {
                return false;
            }
            if (self.gc && a.constructor === window.Uint8Array) {
                if (a.length != b.length) {
                    return false;
                }
                /** @type {number} */
                var i = 0;
                for (;i < a.length;i++) {
                    if (a[i] != b[i]) {
                        return false;
                    }
                }
                return true;
            }
            if (a.constructor === Array) {
                var ret = void 0;
                var initialValue = void 0;
                /** @type {number} */
                var padLength = Math.max(a.length, b.length);
                /** @type {number} */
                i = 0;
                for (;i < padLength;i++) {
                    var def = a[i];
                    var value = b[i];
                    if (def) {
                        if (def.constructor == Object) {
                            ret = def;
                            def = void 0;
                        }
                    }
                    if (value) {
                        if (value.constructor == Object) {
                            initialValue = value;
                            value = void 0;
                        }
                    }
                    if (!self.lc(def, value)) {
                        return false;
                    }
                }
                return ret || initialValue ? (ret = ret || {}, initialValue = initialValue || {}, eql(ret, initialValue)) : true;
            }
            if (a.constructor === Object) {
                return eql(a, b);
            }
            throw Error("e");
        };
        /**
         * @return {undefined}
         */
        self.F = function() {
            this.K = this.K;
            this.C = this.C;
        };
        /** @type {boolean} */
        self.F.prototype.K = false;
        /**
         * @return {undefined}
         */
        self.F.prototype.Z = function() {
            if (!this.K) {
                /** @type {boolean} */
                this.K = true;
                this.B();
            }
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        self.I = function(types, type) {
            self.G(types, self.xa(self.H, type));
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @param {(number|string)} orig
         * @return {undefined}
         */
        self.G = function(types, type, orig) {
            if (types.K) {
                if (self.m(orig)) {
                    type.call(orig);
                } else {
                    type();
                }
            } else {
                if (!types.C) {
                    /** @type {Array} */
                    types.C = [];
                }
                types.C.push(self.m(orig) ? (0, self.r)(type, orig) : type);
            }
        };
        /**
         * @return {undefined}
         */
        self.F.prototype.B = function() {
            if (this.C) {
                for (;this.C.length;) {
                    this.C.shift()();
                }
            }
        };
        /**
         * @param {?} spy
         * @return {undefined}
         */
        self.H = function(spy) {
            if (spy) {
                if ("function" == typeof spy.Z) {
                    spy.Z();
                }
            }
        };
        /** @type {boolean} */
        var err = !self.x || 9 <= Number(fromIndex);
        if (!(!self.Cb && !self.x)) {
            if (!(self.x && 9 <= Number(fromIndex))) {
                if (self.Cb) {
                    self.Pb("1.9.1");
                }
            }
        }
        if (self.x) {
            self.Pb("9");
        }
        /** @type {RegExp} */
        self.oc = /^(ar|ckb|dv|he|iw|fa|nqo|ps|sd|ug|ur|yi|.*[-_](Arab|Hebr|Thaa|Nkoo|Tfng))(?!.*[-_](Latn|Cyrl)($|-|_))($|-|_)/i;
        /**
         * @return {undefined}
         */
        self.qc = function() {
            /** @type {string} */
            this.a = "";
            this.c = self.pc;
            /** @type {null} */
            this.b = null;
        };
        /** @type {boolean} */
        self.qc.prototype.Wa = true;
        /**
         * @return {?}
         */
        self.qc.prototype.La = function() {
            return this.b;
        };
        /** @type {boolean} */
        self.qc.prototype.Xa = true;
        /**
         * @return {?}
         */
        self.qc.prototype.Va = function() {
            return this.a;
        };
        self.pc = {};
        /**
         * @param {string} s
         * @param {number} recurring
         * @return {?}
         */
        self.rc = function(s, recurring) {
            var object = new self.qc;
            /** @type {string} */
            object.a = s;
            /** @type {number} */
            object.b = recurring;
            return object;
        };
        self.rc("<!DOCTYPE html>", 0);
        self.rc("", 0);
        self.sc = self.rc("<br>", 0);
        var names;
        var access;
        var isNumber;
        /**
         * @param {?} path
         * @return {?}
         */
        self.vc = function(path) {
            return path ? new self.tc(self.uc(path)) : tag || (tag = new self.tc);
        };
        /**
         * @param {string} ready
         * @param {Node} ctx
         * @return {?}
         */
        self.xc = function(ready, ctx) {
            var context = ctx || window.document;
            /** @type {null} */
            var d = null;
            if (context.getElementsByClassName) {
                d = context.getElementsByClassName(ready)[0];
            } else {
                if (context.querySelectorAll && context.querySelector) {
                    d = context.querySelector("." + ready);
                } else {
                    d = self.wc(window.document, "*", ready, ctx)[0];
                }
            }
            return d || null;
        };
        /**
         * @param {Object} t
         * @param {string} name
         * @param {string} ready
         * @param {Object} n
         * @return {?}
         */
        self.wc = function(t, name, ready, n) {
            t = n || t;
            name = name && "*" != name ? name.toUpperCase() : "";
            if (t.querySelectorAll && (t.querySelector && (name || ready))) {
                return t.querySelectorAll(name + (ready ? "." + ready : ""));
            }
            if (ready && t.getElementsByClassName) {
                t = t.getElementsByClassName(ready);
                if (name) {
                    n = {};
                    /** @type {number} */
                    var j = 0;
                    /** @type {number} */
                    var i = 0;
                    var target;
                    for (;target = t[i];i++) {
                        if (name == target.nodeName) {
                            n[j++] = target;
                        }
                    }
                    /** @type {number} */
                    n.length = j;
                    return n;
                }
                return t;
            }
            t = t.getElementsByTagName(name || "*");
            if (ready) {
                n = {};
                /** @type {number} */
                i = j = 0;
                for (;target = t[i];i++) {
                    name = target.className;
                    if ("function" == typeof name.split) {
                        if (self.eb(name.split(/\s+/), ready)) {
                            n[j++] = target;
                        }
                    }
                }
                /** @type {number} */
                n.length = j;
                return n;
            }
            return t;
        };
        /**
         * @param {Element} element
         * @param {string} part
         * @return {undefined}
         */
        self.zc = function(element, part) {
            self.mb(part, function(val, key) {
                if ("style" == key) {
                    element.style.cssText = val;
                } else {
                    if ("class" == key) {
                        element.className = val;
                    } else {
                        if ("for" == key) {
                            element.htmlFor = val;
                        } else {
                            if (names.hasOwnProperty(key)) {
                                element.setAttribute(names[key], val);
                            } else {
                                if (self.Ha(key, "aria-") || self.Ha(key, "data-")) {
                                    element.setAttribute(key, val);
                                } else {
                                    element[key] = val;
                                }
                            }
                        }
                    }
                }
            });
        };
        names = {
            cellpadding : "cellPadding",
            cellspacing : "cellSpacing",
            colspan : "colSpan",
            frameborder : "frameBorder",
            height : "height",
            maxlength : "maxLength",
            nonce : "nonce",
            role : "role",
            rowspan : "rowSpan",
            type : "type",
            usemap : "useMap",
            valign : "vAlign",
            width : "width"
        };
        /**
         * @param {Document} context
         * @param {Node} value
         * @param {Object} vals
         * @return {undefined}
         */
        access = function(context, value, vals) {
            /**
             * @param {string} child
             * @return {undefined}
             */
            function fn(child) {
                if (child) {
                    value.appendChild(self.t(child) ? context.createTextNode(child) : child);
                }
            }
            /** @type {number} */
            var i = 2;
            for (;i < vals.length;i++) {
                var val = vals[i];
                if (!self.Ba(val) || self.ya(val) && 0 < val.nodeType) {
                    fn(val);
                } else {
                    (0, self.u)(isNumber(val) ? self.ib(val) : val, fn);
                }
            }
        };
        /**
         * @param {Node} content
         * @return {undefined}
         */
        self.Cc = function(content) {
            var text;
            for (;text = content.firstChild;) {
                content.removeChild(text);
            }
        };
        /**
         * @param {Object} el
         * @return {?}
         */
        self.uc = function(el) {
            return 9 == el.nodeType ? el : el.ownerDocument || el.document;
        };
        /**
         * @param {Object} arg
         * @return {?}
         */
        isNumber = function(arg) {
            if (arg && "number" == typeof arg.length) {
                if (self.ya(arg)) {
                    return "function" == typeof arg.item || "string" == typeof arg.item;
                }
                if (self.za(arg)) {
                    return "function" == typeof arg.item;
                }
            }
            return false;
        };
        /**
         * @param {(Document|string)} a
         * @return {undefined}
         */
        self.tc = function(a) {
            this.a = a || (self.l.document || window.document);
        };
        self.g = self.tc.prototype;
        /** @type {function (?): ?} */
        self.g.ya = self.vc;
        /**
         * @param {string} str
         * @return {?}
         */
        self.g.F = function(str) {
            return self.t(str) ? this.a.getElementById(str) : str;
        };
        /**
         * @param {?} dataAndEvents
         * @param {?} deepDataAndEvents
         * @param {?} ignoreMethodDoesntExist
         * @return {?}
         */
        self.g.na = function(dataAndEvents, deepDataAndEvents, ignoreMethodDoesntExist) {
            var clone = this.a;
            /** @type {Arguments} */
            var args = arguments;
            /** @type {string} */
            var data = String(args[0]);
            var body = args[1];
            if (!err && (body && (body.name || body.type))) {
                /** @type {Array} */
                data = ["<", data];
                if (body.name) {
                    data.push(' name="', self.Sa(body.name), '"');
                }
                if (body.type) {
                    data.push(' type="', self.Sa(body.type), '"');
                    var part = {};
                    self.sb(part, body);
                    delete part.type;
                    body = part;
                }
                data.push(">");
                /** @type {string} */
                data = data.join("");
            }
            data = clone.createElement(data);
            if (body) {
                if (self.t(body)) {
                    data.className = body;
                } else {
                    if (self.Ca(body)) {
                        data.className = body.join(" ");
                    } else {
                        self.zc(data, body);
                    }
                }
            }
            if (2 < args.length) {
                access(clone, data, args);
            }
            return data;
        };
        /**
         * @param {?} node
         * @param {?} childNode
         * @return {undefined}
         */
        self.g.appendChild = function(node, childNode) {
            node.appendChild(childNode);
        };
        /**
         * @param {Object} parent
         * @param {Object} descendant
         * @return {?}
         */
        self.g.contains = function(parent, descendant) {
            if (!parent || !descendant) {
                return false;
            }
            if (parent.contains && 1 == descendant.nodeType) {
                return parent == descendant || parent.contains(descendant);
            }
            if ("undefined" != typeof parent.compareDocumentPosition) {
                return parent == descendant || !!(parent.compareDocumentPosition(descendant) & 16);
            }
            for (;descendant && parent != descendant;) {
                descendant = descendant.parentNode;
            }
            return descendant == parent;
        };
        self.Dc = function(recurring) {
            return function() {
                return recurring;
            };
        }(false);
        var delimiterCode = "StopIteration" in self.l ? self.l.StopIteration : {
            message : "StopIteration",
            stack : ""
        };
        /**
         * @return {undefined}
         */
        var Stream = function() {
        };
        /**
         * @return {?}
         */
        Stream.prototype.next = function() {
            throw delimiterCode;
        };
        /**
         * @return {?}
         */
        Stream.prototype.ia = function() {
            return this;
        };
        /**
         * @param {?} data
         * @return {?}
         */
        var decode = function(data) {
            if (data instanceof Stream) {
                return data;
            }
            if ("function" == typeof data.ia) {
                return data.ia(false);
            }
            if (self.Ba(data)) {
                /** @type {number} */
                var pos = 0;
                var s = new Stream;
                /**
                 * @return {?}
                 */
                s.next = function() {
                    for (;;) {
                        if (pos >= data.length) {
                            throw delimiterCode;
                        }
                        if (pos in data) {
                            return data[pos++];
                        }
                        pos++;
                    }
                };
                return s;
            }
            throw Error("f");
        };
        /**
         * @param {Object} text
         * @param {Function} html
         * @return {undefined}
         */
        var exec = function(text, html) {
            if (self.Ba(text)) {
                try {
                    (0, self.u)(text, html, void 0);
                } catch (c) {
                    if (c !== delimiterCode) {
                        throw c;
                    }
                }
            } else {
                text = decode(text);
                try {
                    for (;;) {
                        html.call(void 0, text.next(), void 0, text);
                    }
                } catch (c) {
                    if (c !== delimiterCode) {
                        throw c;
                    }
                }
            }
        };
        /**
         * @param {string} text
         * @return {?}
         */
        var encode = function(text) {
            if (self.Ba(text)) {
                return self.ib(text);
            }
            text = decode(text);
            /** @type {Array} */
            var out = [];
            exec(text, function(copies) {
                out.push(copies);
            });
            return out;
        };
        /**
         * @param {string} walkers
         * @param {?} a
         * @return {undefined}
         */
        self.J = function(walkers, a) {
            this.b = {};
            /** @type {Array} */
            this.a = [];
            /** @type {number} */
            this.g = this.c = 0;
            /** @type {number} */
            var ii = arguments.length;
            if (1 < ii) {
                if (ii % 2) {
                    throw Error("b");
                }
                /** @type {number} */
                var $i$$70_values$$inline_90 = 0;
                for (;$i$$70_values$$inline_90 < ii;$i$$70_values$$inline_90 += 2) {
                    this.set(arguments[$i$$70_values$$inline_90], arguments[$i$$70_values$$inline_90 + 1]);
                }
            } else {
                if (walkers) {
                    if (walkers instanceof self.J) {
                        ii = walkers.X();
                        $i$$70_values$$inline_90 = walkers.V();
                    } else {
                        ii = _isArray(walkers);
                        $i$$70_values$$inline_90 = HOP(walkers);
                    }
                    /** @type {number} */
                    var i = 0;
                    for (;i < ii.length;i++) {
                        this.set(ii[i], $i$$70_values$$inline_90[i]);
                    }
                }
            }
        };
        /**
         * @return {?}
         */
        self.J.prototype.V = function() {
            each(this);
            /** @type {Array} */
            var eventPath = [];
            /** @type {number} */
            var i = 0;
            for (;i < this.a.length;i++) {
                eventPath.push(this.b[this.a[i]]);
            }
            return eventPath;
        };
        /**
         * @return {?}
         */
        self.J.prototype.X = function() {
            each(this);
            return this.a.concat();
        };
        /**
         * @param {?} item
         * @param {string} key
         * @return {?}
         */
        self.Lc = function(item, key) {
            return hasOwnProperty(item.b, key);
        };
        /**
         * @return {undefined}
         */
        self.J.prototype.clear = function() {
            this.b = {};
            /** @type {number} */
            this.g = this.c = this.a.length = 0;
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.J.prototype.remove = function(name) {
            return hasOwnProperty(this.b, name) ? (delete this.b[name], this.c--, this.g++, this.a.length > 2 * this.c && each(this), true) : false;
        };
        /**
         * @param {Object} args
         * @return {undefined}
         */
        var each = function(args) {
            if (args.c != args.a.length) {
                /** @type {number} */
                var i = 0;
                /** @type {number} */
                var len = 0;
                for (;i < args.a.length;) {
                    var type = args.a[i];
                    if (hasOwnProperty(args.b, type)) {
                        args.a[len++] = type;
                    }
                    i++;
                }
                /** @type {number} */
                args.a.length = len;
            }
            if (args.c != args.a.length) {
                var tests = {};
                /** @type {number} */
                len = i = 0;
                for (;i < args.a.length;) {
                    type = args.a[i];
                    if (!hasOwnProperty(tests, type)) {
                        args.a[len++] = type;
                        /** @type {number} */
                        tests[type] = 1;
                    }
                    i++;
                }
                /** @type {number} */
                args.a.length = len;
            }
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {?}
         */
        self.J.prototype.get = function(key, val) {
            return hasOwnProperty(this.b, key) ? this.b[key] : val;
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {undefined}
         */
        self.J.prototype.set = function(key, val) {
            if (!hasOwnProperty(this.b, key)) {
                this.c++;
                this.a.push(key);
                this.g++;
            }
            /** @type {Object} */
            this.b[key] = val;
        };
        /**
         * @param {Function} callback
         * @param {Object} obj
         * @return {undefined}
         */
        self.J.prototype.forEach = function(callback, obj) {
            var codeSegments = this.X();
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                var camelKey = codeSegments[i];
                var err = this.get(camelKey);
                callback.call(obj, err, camelKey, this);
            }
        };
        /**
         * @param {boolean} opt_keys
         * @return {?}
         */
        self.J.prototype.ia = function(opt_keys) {
            each(this);
            /** @type {number} */
            var completed = 0;
            var g = this.g;
            var color = this;
            var stream = new Stream;
            /**
             * @return {?}
             */
            stream.next = function() {
                if (g != color.g) {
                    throw Error("g");
                }
                if (completed >= color.a.length) {
                    throw delimiterCode;
                }
                var key = color.a[completed++];
                return opt_keys ? key : color.b[key];
            };
            return stream;
        };
        /**
         * @param {?} object
         * @param {string} keepData
         * @return {?}
         */
        var hasOwnProperty = function(object, keepData) {
            return Object.prototype.hasOwnProperty.call(object, keepData);
        };
        var send;
        /**
         * @param {string} str
         * @return {?}
         */
        self.Mc = function(str) {
            if (str.V && "function" == typeof str.V) {
                return str.V();
            }
            if (self.t(str)) {
                return str.split("");
            }
            if (self.Ba(str)) {
                /** @type {Array} */
                var whitespace = [];
                var len = str.length;
                /** @type {number} */
                var i = 0;
                for (;i < len;i++) {
                    whitespace.push(str[i]);
                }
                return whitespace;
            }
            return HOP(str);
        };
        /**
         * @param {string} data
         * @param {Function} fn
         * @return {undefined}
         */
        send = function(data, fn) {
            if (data.forEach && "function" == typeof data.forEach) {
                data.forEach(fn, void 0);
            } else {
                if (self.Ba(data) || self.t(data)) {
                    (0, self.u)(data, fn, void 0);
                } else {
                    var attrs;
                    if (data.X && "function" == typeof data.X) {
                        attrs = data.X();
                    } else {
                        if (data.V && "function" == typeof data.V) {
                            attrs = void 0;
                        } else {
                            if (self.Ba(data) || self.t(data)) {
                                /** @type {Array} */
                                attrs = [];
                                var items = data.length;
                                /** @type {number} */
                                var i = 0;
                                for (;i < items;i++) {
                                    attrs.push(i);
                                }
                            } else {
                                attrs = _isArray(data);
                            }
                        }
                    }
                    items = self.Mc(data);
                    i = items.length;
                    /** @type {number} */
                    var key = 0;
                    for (;key < i;key++) {
                        fn.call(void 0, items[key], attrs && attrs[key], data);
                    }
                }
            }
        };
        /**
         * @param {?} c
         * @param {?} h
         * @param {?} g
         * @return {undefined}
         */
        var Matrix = function(c, h, g) {
            this.g = g;
            this.c = c;
            this.h = h;
            /** @type {number} */
            this.b = 0;
            /** @type {null} */
            this.a = null;
        };
        /**
         * @return {?}
         */
        Matrix.prototype.get = function() {
            var a;
            if (0 < this.b) {
                this.b--;
                a = this.a;
                this.a = a.next;
                /** @type {null} */
                a.next = null;
            } else {
                a = this.c();
            }
            return a;
        };
        /**
         * @param {Object} o
         * @param {string} ready
         * @return {undefined}
         */
        var _getGradient = function(o, ready) {
            o.h(ready);
            if (o.b < o.g) {
                o.b++;
                ready.next = o.a;
                /** @type {string} */
                o.a = ready;
            }
        };
        var traverseNode;
        /** @type {Array} */
        self.Qc = [];
        /** @type {Array} */
        self.Rc = [];
        /** @type {boolean} */
        self.Sc = false;
        /**
         * @param {Function} fun
         * @return {undefined}
         */
        traverseNode = function(fun) {
            /** @type {Function} */
            self.Qc[self.Qc.length] = fun;
            if (self.Sc) {
                /** @type {number} */
                var i = 0;
                for (;i < self.Rc.length;i++) {
                    fun((0, self.r)(self.Rc[i].b, self.Rc[i]));
                }
            }
        };
        var getAll;
        var MAP;
        var create;
        var last_stat;
        /**
         * @param {?} context
         * @return {undefined}
         */
        getAll = function(context) {
            self.l.setTimeout(function() {
                throw context;
            }, 0);
        };
        /**
         * @param {Function} name
         * @param {?} dataAndEvents
         * @return {undefined}
         */
        self.Yc = function(name, dataAndEvents) {
            /** @type {Function} */
            var t = name;
            if (dataAndEvents) {
                t = (0, self.r)(name, dataAndEvents);
            }
            t = last_stat(t);
            if (!self.za(self.l.setImmediate) || self.l.Window && (self.l.Window.prototype && (!self.w("Edge") && self.l.Window.prototype.setImmediate == self.l.setImmediate))) {
                if (!MAP) {
                    MAP = create();
                }
                MAP(t);
            } else {
                self.l.setImmediate(t);
            }
        };
        /**
         * @return {?}
         */
        create = function() {
            var Channel = self.l.MessageChannel;
            if ("undefined" === typeof Channel) {
                if ("undefined" !== typeof window) {
                    if (window.postMessage) {
                        if (window.addEventListener) {
                            if (!self.w("Presto")) {
                                /**
                                 * @return {undefined}
                                 */
                                Channel = function() {
                                    var doc = window.document.createElement("IFRAME");
                                    /** @type {string} */
                                    doc.style.display = "none";
                                    /** @type {string} */
                                    doc.src = "";
                                    window.document.documentElement.appendChild(doc);
                                    var win = doc.contentWindow;
                                    doc = win.document;
                                    doc.open();
                                    doc.write("");
                                    doc.close();
                                    /** @type {string} */
                                    var messageName = "callImmediate" + Math.random();
                                    /** @type {string} */
                                    var url = "file:" == win.location.protocol ? "*" : win.location.protocol + "//" + win.location.host;
                                    doc = (0, self.r)(function(event) {
                                        if (("*" == url || event.origin == url) && event.data == messageName) {
                                            this.port1.onmessage();
                                        }
                                    }, this);
                                    win.addEventListener("message", doc, false);
                                    this.port1 = {};
                                    this.port2 = {
                                        /**
                                         * @return {undefined}
                                         */
                                        postMessage : function() {
                                            win.postMessage(messageName, url);
                                        }
                                    };
                                };
                            }
                        }
                    }
                }
            }
            if ("undefined" !== typeof Channel && (!self.w("Trident") && !self.w("MSIE"))) {
                var channel = new Channel;
                var node = {};
                var current = node;
                /**
                 * @return {undefined}
                 */
                channel.port1.onmessage = function() {
                    if (self.m(node.next)) {
                        node = node.next;
                        var start = node.gb;
                        /** @type {null} */
                        node.gb = null;
                        start();
                    }
                };
                return function(gb) {
                    current.next = {
                        gb : gb
                    };
                    current = current.next;
                    channel.port2.postMessage(0);
                };
            }
            return "undefined" !== typeof window.document && "onreadystatechange" in window.document.createElement("SCRIPT") ? function(fetchOnlyFunction) {
                var s = window.document.createElement("SCRIPT");
                /**
                 * @return {undefined}
                 */
                s.onreadystatechange = function() {
                    /** @type {null} */
                    s.onreadystatechange = null;
                    s.parentNode.removeChild(s);
                    /** @type {null} */
                    s = null;
                    fetchOnlyFunction();
                    /** @type {null} */
                    fetchOnlyFunction = null;
                };
                window.document.documentElement.appendChild(s);
            } : function(funcToCall) {
                self.l.setTimeout(funcToCall, 0);
            };
        };
        /**
         * @param {?} b
         * @return {?}
         */
        last_stat = function(b) {
            return b;
        };
        traverseNode(function(dataAndEvents) {
            /** @type {Function} */
            last_stat = dataAndEvents;
        });
        /**
         * @return {undefined}
         */
        var abc = function() {
            /** @type {null} */
            this.b = this.a = null;
        };
        var sb = new Matrix(function() {
            return new sprite;
        }, function(record) {
            record.reset();
        }, 100);
        /**
         * @return {?}
         */
        abc.prototype.remove = function() {
            /** @type {null} */
            var removed = null;
            if (this.a) {
                removed = this.a;
                this.a = this.a.next;
                if (!this.a) {
                    /** @type {null} */
                    this.b = null;
                }
                /** @type {null} */
                removed.next = null;
            }
            return removed;
        };
        /**
         * @return {undefined}
         */
        var sprite = function() {
            /** @type {null} */
            this.next = this.b = this.a = null;
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {undefined}
         */
        sprite.prototype.set = function(key, val) {
            /** @type {string} */
            this.a = key;
            /** @type {Object} */
            this.b = val;
            /** @type {null} */
            this.next = null;
        };
        /**
         * @return {undefined}
         */
        sprite.prototype.reset = function() {
            /** @type {null} */
            this.next = this.b = this.a = null;
        };
        var resolveOne;
        var resolvePromise;
        var dd;
        var selfObj;
        var transition;
        /**
         * @param {string} storageKey
         * @param {Object} ar
         * @return {undefined}
         */
        self.fd = function(storageKey, ar) {
            if (!resolveOne) {
                resolvePromise();
            }
            if (!dd) {
                resolveOne();
                /** @type {boolean} */
                dd = true;
            }
            var self = selfObj;
            var a = sb.get();
            a.set(storageKey, ar);
            if (self.b) {
                self.b.next = a;
            } else {
                self.a = a;
            }
            self.b = a;
        };
        /**
         * @return {undefined}
         */
        resolvePromise = function() {
            if (self.l.Promise && self.l.Promise.resolve) {
                var n = self.l.Promise.resolve(void 0);
                /**
                 * @return {undefined}
                 */
                resolveOne = function() {
                    n.then(transition);
                };
            } else {
                /**
                 * @return {undefined}
                 */
                resolveOne = function() {
                    self.Yc(transition);
                };
            }
        };
        /** @type {boolean} */
        dd = false;
        selfObj = new abc;
        /**
         * @return {undefined}
         */
        transition = function() {
            var parent;
            for (;parent = selfObj.remove();) {
                try {
                    parent.a.call(parent.b);
                } catch (fragment) {
                    getAll(fragment);
                }
                _getGradient(sb, parent);
            }
            /** @type {boolean} */
            dd = false;
        };
        /**
         * @param {Function} a
         * @return {undefined}
         */
        var execute = function(a) {
            a.prototype.then = a.prototype.then;
            /** @type {boolean} */
            a.prototype.$goog_Thenable = true;
        };
        /**
         * @param {?} object
         * @return {?}
         */
        var getType = function(object) {
            if (!object) {
                return false;
            }
            try {
                return!!object.$goog_Thenable;
            } catch (b) {
                return false;
            }
        };
        var valueRef;
        var els;
        var contains;
        var ondata;
        var bindCallbacks;
        var debug;
        var walk;
        var unfoldSoak;
        var rgb2hex;
        /**
         * @param {?} next_callback
         * @param {?} next_scope
         * @return {undefined}
         */
        self.kd = function(next_callback, next_scope) {
            /** @type {number} */
            this.a = 0;
            this.C = void 0;
            /** @type {null} */
            this.g = this.b = this.c = null;
            /** @type {boolean} */
            this.h = this.j = false;
            if (next_callback != self.Da) {
                try {
                    var rvar = this;
                    next_callback.call(next_scope, function(_) {
                        debug(rvar, 2, _);
                    }, function(_) {
                        debug(rvar, 3, _);
                    });
                } catch (camelKey) {
                    debug(this, 3, camelKey);
                }
            }
        };
        /**
         * @return {undefined}
         */
        valueRef = function() {
            /** @type {null} */
            this.next = this.context = this.b = this.g = this.a = null;
            /** @type {boolean} */
            this.c = false;
        };
        /**
         * @return {undefined}
         */
        valueRef.prototype.reset = function() {
            /** @type {null} */
            this.context = this.b = this.g = this.a = null;
            /** @type {boolean} */
            this.c = false;
        };
        els = new Matrix(function() {
            return new valueRef;
        }, function(record) {
            record.reset();
        }, 100);
        /**
         * @param {Object} v
         * @param {Object} b
         * @param {Object} context
         * @return {?}
         */
        contains = function(v, b, context) {
            var ret = els.get();
            /** @type {Object} */
            ret.g = v;
            /** @type {Object} */
            ret.b = b;
            /** @type {Object} */
            ret.context = context;
            return ret;
        };
        /**
         * @param {boolean} recurring
         * @return {?}
         */
        self.od = function(recurring) {
            if (recurring instanceof self.kd) {
                return recurring;
            }
            var rvar = new self.kd(self.Da);
            debug(rvar, 2, recurring);
            return rvar;
        };
        /**
         * @return {?}
         */
        self.qd = function() {
            var text;
            var b = new self.kd(function(textAlt) {
                text = textAlt;
            });
            return new selector_sortOrder(b, text);
        };
        /**
         * @param {Function} callback
         * @param {?} opt_attributes
         * @param {Object} node
         * @return {?}
         */
        self.kd.prototype.then = function(callback, opt_attributes, node) {
            return self.rd(this, self.za(callback) ? callback : null, self.za(opt_attributes) ? opt_attributes : null, node);
        };
        execute(self.kd);
        /**
         * @param {?} event
         * @return {undefined}
         */
        self.kd.prototype.cancel = function(event) {
            if (0 == this.a) {
                self.fd(function() {
                    var val = new clone(event);
                    ondata(this, val);
                }, this);
            }
        };
        /**
         * @param {Object} start
         * @param {Object} val
         * @return {undefined}
         */
        ondata = function(start, val) {
            if (0 == start.a) {
                if (start.c) {
                    var c = start.c;
                    if (c.b) {
                        /** @type {number} */
                        var node = 0;
                        /** @type {null} */
                        var expectationResult = null;
                        /** @type {null} */
                        var n = null;
                        var b = c.b;
                        for (;b && (b.c || (node++, b.a == start && (expectationResult = b), !(expectationResult && 1 < node)));b = b.next) {
                            if (!expectationResult) {
                                n = b;
                            }
                        }
                        if (expectationResult) {
                            if (0 == c.a && 1 == node) {
                                ondata(c, val);
                            } else {
                                if (n) {
                                    node = n;
                                    if (node.next == c.g) {
                                        c.g = node;
                                    }
                                    node.next = node.next.next;
                                } else {
                                    rgb2hex(c);
                                }
                                slice(c, expectationResult, 3, val);
                            }
                        }
                    }
                    /** @type {null} */
                    start.c = null;
                } else {
                    debug(start, 3, val);
                }
            }
        };
        /**
         * @param {Object} o
         * @param {?} v
         * @return {undefined}
         */
        bindCallbacks = function(o, v) {
            if (!o.b) {
                if (!(2 != o.a && 3 != o.a)) {
                    unfoldSoak(o);
                }
            }
            if (o.g) {
                o.g.next = v;
            } else {
                o.b = v;
            }
            o.g = v;
        };
        /**
         * @param {?} a
         * @param {boolean} fn
         * @param {boolean} jQuery
         * @param {Object} elems
         * @return {?}
         */
        self.rd = function(a, fn, jQuery, elems) {
            var obj = contains(null, null, null);
            obj.a = new self.kd(function($, Event) {
                obj.g = fn ? function(types) {
                    try {
                        var orig = fn.call(elems, types);
                        $(orig);
                    } catch (ready) {
                        Event(ready);
                    }
                } : $;
                obj.b = jQuery ? function(type) {
                    try {
                        var ready = jQuery.call(elems, type);
                        if (!self.m(ready) && type instanceof clone) {
                            Event(type);
                        } else {
                            $(ready);
                        }
                    } catch (orig) {
                        Event(orig);
                    }
                } : Event;
            });
            obj.a.c = a;
            bindCallbacks(a, obj);
            return obj.a;
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        self.kd.prototype.v = function(types) {
            /** @type {number} */
            this.a = 0;
            debug(this, 2, types);
        };
        /**
         * @param {string} str
         * @return {undefined}
         */
        self.kd.prototype.w = function(str) {
            /** @type {number} */
            this.a = 0;
            debug(this, 3, str);
        };
        /**
         * @param {Object} o
         * @param {number} opt_attributes
         * @param {Object} key
         * @return {undefined}
         */
        debug = function(o, opt_attributes, key) {
            if (0 == o.a) {
                if (o === key) {
                    /** @type {number} */
                    opt_attributes = 3;
                    /** @type {TypeError} */
                    key = new TypeError("Promise cannot resolve to itself");
                }
                /** @type {number} */
                o.a = 1;
                if (!self.yd(key, o.v, o.w, o)) {
                    /** @type {Object} */
                    o.C = key;
                    /** @type {number} */
                    o.a = opt_attributes;
                    /** @type {null} */
                    o.c = null;
                    unfoldSoak(o);
                    if (!(3 != opt_attributes)) {
                        if (!(key instanceof clone)) {
                            setData(o, key);
                        }
                    }
                }
            }
        };
        /**
         * @param {?} a
         * @param {string} callback
         * @param {Function} cb
         * @param {Object} target
         * @return {?}
         */
        self.yd = function(a, callback, cb, target) {
            if (a instanceof self.kd) {
                return bindCallbacks(a, contains(callback || self.Da, cb || null, target)), true;
            }
            if (getType(a)) {
                return a.then(callback, cb, target), true;
            }
            if (self.ya(a)) {
                try {
                    var property = a.then;
                    if (self.za(property)) {
                        return walk(a, property, callback, cb, target), true;
                    }
                } catch (r) {
                    return cb.call(target, r), true;
                }
            }
            return false;
        };
        /**
         * @param {?} source
         * @param {Function} property
         * @param {Function} cb
         * @param {Function} callback
         * @param {Object} el
         * @return {undefined}
         */
        walk = function(source, property, cb, callback, el) {
            /** @type {boolean} */
            var f = false;
            /**
             * @param {?} r
             * @return {undefined}
             */
            var check = function(r) {
                if (!f) {
                    /** @type {boolean} */
                    f = true;
                    cb.call(el, r);
                }
            };
            /**
             * @param {?} args
             * @return {undefined}
             */
            var match = function(args) {
                if (!f) {
                    /** @type {boolean} */
                    f = true;
                    callback.call(el, args);
                }
            };
            try {
                property.call(source, check, match);
            } catch (typePattern) {
                match(typePattern);
            }
        };
        /**
         * @param {Object} o
         * @return {undefined}
         */
        unfoldSoak = function(o) {
            if (!o.j) {
                /** @type {boolean} */
                o.j = true;
                self.fd(o.l, o);
            }
        };
        /**
         * @param {Node} b
         * @return {?}
         */
        rgb2hex = function(b) {
            /** @type {null} */
            var end = null;
            if (b.b) {
                end = b.b;
                b.b = end.next;
                /** @type {null} */
                end.next = null;
            }
            if (!b.b) {
                /** @type {null} */
                b.g = null;
            }
            return end;
        };
        /**
         * @return {undefined}
         */
        self.kd.prototype.l = function() {
            var expectationResult;
            for (;expectationResult = rgb2hex(this);) {
                slice(this, expectationResult, this.a, this.C);
            }
            /** @type {boolean} */
            this.j = false;
        };
        /**
         * @param {Object} c
         * @param {Object} result
         * @param {number} options
         * @param {Object} a
         * @return {undefined}
         */
        var slice = function(c, result, options, a) {
            if (3 == options && (result.b && !result.c)) {
                for (;c && c.h;c = c.c) {
                    /** @type {boolean} */
                    c.h = false;
                }
            }
            if (result.a) {
                /** @type {null} */
                result.a.c = null;
                report(result, options, a);
            } else {
                try {
                    if (result.c) {
                        result.g.call(result.context);
                    } else {
                        report(result, options, a);
                    }
                } catch (key) {
                    rep.call(null, key);
                }
            }
            _getGradient(els, result);
        };
        /**
         * @param {Object} result
         * @param {number} failing_message
         * @param {Object} d
         * @return {undefined}
         */
        var report = function(result, failing_message, d) {
            if (2 == failing_message) {
                result.g.call(result.context, d);
            } else {
                if (result.b) {
                    result.b.call(result.context, d);
                }
            }
        };
        /**
         * @param {Object} o
         * @param {Object} key
         * @return {undefined}
         */
        var setData = function(o, key) {
            /** @type {boolean} */
            o.h = true;
            self.fd(function() {
                if (o.h) {
                    rep.call(null, key);
                }
            });
        };
        /** @type {function (?): undefined} */
        var rep = getAll;
        /**
         * @param {?} deepDataAndEvents
         * @return {undefined}
         */
        var clone = function(deepDataAndEvents) {
            self.Fa.call(this, deepDataAndEvents);
        };
        self.p(clone, self.Fa);
        /** @type {string} */
        clone.prototype.name = "cancel";
        /**
         * @param {?} a
         * @param {?} b
         * @return {undefined}
         */
        var selector_sortOrder = function(a, b) {
            this.a = a;
            this.b = b;
        };
        /**
         * @param {string} dataAndEvents
         * @param {string} deepDataAndEvents
         * @return {undefined}
         */
        self.Dd = function(dataAndEvents, deepDataAndEvents) {
            /** @type {Array} */
            this.l = [];
            /** @type {string} */
            this.L = dataAndEvents;
            this.A = deepDataAndEvents || null;
            /** @type {boolean} */
            this.j = this.c = false;
            this.g = void 0;
            /** @type {boolean} */
            this.G = this.M = this.w = false;
            /** @type {number} */
            this.v = 0;
            /** @type {null} */
            this.a = null;
            /** @type {number} */
            this.C = 0;
        };
        /**
         * @param {?} event
         * @return {undefined}
         */
        self.Dd.prototype.cancel = function(event) {
            if (this.c) {
                if (this.g instanceof self.Dd) {
                    this.g.cancel();
                }
            } else {
                if (this.a) {
                    var item = this.a;
                    delete this.a;
                    if (event) {
                        item.cancel(event);
                    } else {
                        item.C--;
                        if (0 >= item.C) {
                            item.cancel();
                        }
                    }
                }
                if (this.L) {
                    this.L.call(this.A, this);
                } else {
                    /** @type {boolean} */
                    this.G = true;
                }
                if (!this.c) {
                    this.H(new self.Ed);
                }
            }
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        self.Dd.prototype.I = function(types, type) {
            /** @type {boolean} */
            this.w = false;
            _(this, types, type);
        };
        /**
         * @param {Object} scope
         * @param {boolean} keepData
         * @param {Function} obj
         * @return {undefined}
         */
        var _ = function(scope, keepData, obj) {
            /** @type {boolean} */
            scope.c = true;
            /** @type {Function} */
            scope.g = obj;
            /** @type {boolean} */
            scope.j = !keepData;
            loop(scope);
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        var isWindow = function(obj) {
            if (obj.c) {
                if (!obj.G) {
                    throw new state;
                }
                /** @type {boolean} */
                obj.G = false;
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        self.Dd.prototype.b = function(types) {
            isWindow(this);
            _(this, true, types);
        };
        /**
         * @param {?} spy
         * @return {undefined}
         */
        self.Dd.prototype.H = function(spy) {
            isWindow(this);
            _(this, false, spy);
        };
        /**
         * @param {?} arg
         * @param {Function} ready
         * @param {Node} scope
         * @return {undefined}
         */
        self.Kd = function(arg, ready, scope) {
            self.Jd(arg, ready, null, scope);
        };
        /**
         * @param {Object} s
         * @param {Function} type
         * @param {Object} resp
         * @param {Node} date
         * @return {undefined}
         */
        self.Jd = function(s, type, resp, date) {
            s.l.push([type, resp, date]);
            if (s.c) {
                loop(s);
            }
        };
        /**
         * @param {Function} callback
         * @param {?} opt_attributes
         * @param {Object} f
         * @return {?}
         */
        self.Dd.prototype.then = function(callback, opt_attributes, f) {
            var text;
            var rejectPromise;
            var c = new self.kd(function(textAlt, reject) {
                text = textAlt;
                rejectPromise = reject;
            });
            self.Jd(this, text, function(r) {
                if (r instanceof self.Ed) {
                    c.cancel();
                } else {
                    rejectPromise(r);
                }
            });
            return c.then(callback, opt_attributes, f);
        };
        execute(self.Dd);
        /**
         * @param {?} arg
         * @param {?} data
         * @return {undefined}
         */
        self.Ld = function(arg, data) {
            if (data instanceof self.Dd) {
                self.Kd(arg, (0, self.r)(data.h, data));
            } else {
                self.Kd(arg, function() {
                    return data;
                });
            }
        };
        /**
         * @param {string} types
         * @return {?}
         */
        self.Dd.prototype.h = function(types) {
            var e = new self.Dd;
            self.Jd(this, e.b, e.H, e);
            if (types) {
                e.a = this;
                this.C++;
            }
            return e;
        };
        /**
         * @param {Object} msg
         * @return {?}
         */
        var ok = function(msg) {
            return(0, self.bb)(msg.l, function(r) {
                return self.za(r[1]);
            });
        };
        /**
         * @param {Object} scope
         * @return {undefined}
         */
        var loop = function(scope) {
            if (scope.v && (scope.c && ok(scope))) {
                var data = scope.v;
                var result = cache[data];
                if (result) {
                    self.l.clearTimeout(result.a);
                    delete cache[data];
                }
                /** @type {number} */
                scope.v = 0;
            }
            if (scope.a) {
                scope.a.C--;
                delete scope.a;
            }
            data = scope.g;
            /** @type {boolean} */
            var attributes = result = false;
            for (;scope.l.length && !scope.w;) {
                var results = scope.l.shift();
                var r = results[0];
                var r1 = results[1];
                results = results[2];
                if (r = scope.j ? r1 : r) {
                    try {
                        var ready = r.call(results || scope.A, data);
                        if (self.m(ready)) {
                            scope.j = scope.j && (ready == data || ready instanceof Error);
                            scope.g = data = ready;
                        }
                        if (getType(data) || "function" === typeof self.l.Promise && data instanceof self.l.Promise) {
                            /** @type {boolean} */
                            attributes = true;
                            /** @type {boolean} */
                            scope.w = true;
                        }
                    } catch (tmp) {
                        data = tmp;
                        /** @type {boolean} */
                        scope.j = true;
                        if (!ok(scope)) {
                            /** @type {boolean} */
                            result = true;
                        }
                    }
                }
            }
            scope.g = data;
            if (attributes) {
                ready = (0, self.r)(scope.I, scope, true);
                attributes = (0, self.r)(scope.I, scope, false);
                if (data instanceof self.Dd) {
                    self.Jd(data, ready, attributes);
                    /** @type {boolean} */
                    data.M = true;
                } else {
                    data.then(ready, attributes);
                }
            }
            if (result) {
                data = new Line(data);
                cache[data.a] = data;
                scope.v = data.a;
            }
        };
        /**
         * @return {undefined}
         */
        var state = function() {
            self.Fa.call(this);
        };
        self.p(state, self.Fa);
        /** @type {string} */
        state.prototype.message = "Deferred has already fired";
        /** @type {string} */
        state.prototype.name = "AlreadyCalledError";
        /**
         * @return {undefined}
         */
        self.Ed = function() {
            self.Fa.call(this);
        };
        self.p(self.Ed, self.Fa);
        /** @type {string} */
        self.Ed.prototype.message = "Deferred was canceled";
        /** @type {string} */
        self.Ed.prototype.name = "CanceledError";
        /**
         * @param {?} pointB
         * @return {undefined}
         */
        var Line = function(pointB) {
            this.a = self.l.setTimeout((0, self.r)(this.c, this), 0);
            this.b = pointB;
        };
        /**
         * @return {undefined}
         */
        Line.prototype.c = function() {
            delete cache[this.a];
            throw this.b;
        };
        var cache = {};
        /** @type {boolean} */
        var named = !self.x || 9 <= Number(fromIndex);
        var Qd = self.x && !self.Pb("9");
        if (!!self.Db) {
            self.Pb("528");
        }
        if (!(self.Cb && self.Pb("1.9b"))) {
            if (!(self.x && self.Pb("8"))) {
                if (!(versionOffset && self.Pb("9.5"))) {
                    if (self.Db) {
                        self.Pb("528");
                    }
                }
            }
        }
        if (!(self.Cb && !self.Pb("8"))) {
            if (self.x) {
                self.Pb("9");
            }
        }
        /**
         * @param {?} x
         * @param {Object} value
         * @return {undefined}
         */
        self.K = function(x, value) {
            this.type = "undefined" != typeof self.Rd && x instanceof self.Rd ? String(x) : x;
            this.b = this.target = value;
            /** @type {boolean} */
            this.g = false;
            /** @type {boolean} */
            this.lb = true;
        };
        /**
         * @return {undefined}
         */
        self.K.prototype.l = function() {
            /** @type {boolean} */
            this.g = true;
        };
        /**
         * @return {undefined}
         */
        self.K.prototype.c = function() {
            /** @type {boolean} */
            this.lb = false;
        };
        /**
         * @param {Object} e
         * @param {?} b
         * @return {undefined}
         */
        var listener = function(e, b) {
            self.K.call(this, e ? e.type : "");
            /** @type {null} */
            this.b = this.target = null;
            /** @type {number} */
            this.keyCode = 0;
            /** @type {boolean} */
            this.C = this.v = this.h = this.j = false;
            /** @type {null} */
            this.a = this.state = null;
            if (e) {
                this.type = e.type;
                this.target = e.target || e.srcElement;
                this.b = b;
                var rel = e.relatedTarget;
                if (rel) {
                    if (self.Cb) {
                        self.yb(rel, "nodeName");
                    }
                }
                this.keyCode = e.keyCode || 0;
                this.j = e.ctrlKey;
                this.h = e.altKey;
                this.v = e.shiftKey;
                this.C = e.metaKey;
                this.state = e.state;
                /** @type {Object} */
                this.a = e;
                if (e.defaultPrevented) {
                    this.c();
                }
            }
        };
        self.p(listener, self.K);
        /**
         * @return {undefined}
         */
        listener.prototype.l = function() {
            listener.D.l.call(this);
            if (this.a.stopPropagation) {
                this.a.stopPropagation();
            } else {
                /** @type {boolean} */
                this.a.cancelBubble = true;
            }
        };
        /**
         * @return {undefined}
         */
        listener.prototype.c = function() {
            listener.D.c.call(this);
            var ev = this.a;
            if (ev.preventDefault) {
                ev.preventDefault();
            } else {
                if (ev.returnValue = false, Qd) {
                    try {
                        if (ev.ctrlKey || 112 <= ev.keyCode && 123 >= ev.keyCode) {
                            /** @type {number} */
                            ev.keyCode = -1;
                        }
                    } catch (b) {
                    }
                }
            }
        };
        /** @type {string} */
        var method = "closure_listenable_" + (1E6 * Math.random() | 0);
        /**
         * @param {Object} value
         * @return {?}
         */
        var isF = function(value) {
            return!(!value || !value[method]);
        };
        /** @type {number} */
        var nextKey = 0;
        /**
         * @param {Function} _arg
         * @param {?} src
         * @param {?} eventType
         * @param {?} location
         * @param {?} stream
         * @return {undefined}
         */
        var _Class = function(_arg, src, eventType, location, stream) {
            /** @type {Function} */
            this.listener = _arg;
            /** @type {null} */
            this.proxy = null;
            this.src = src;
            this.type = eventType;
            /** @type {boolean} */
            this.wa = !!location;
            this.Na = stream;
            /** @type {number} */
            this.key = ++nextKey;
            /** @type {boolean} */
            this.removed = this.Ja = false;
        };
        /**
         * @param {Object} listener
         * @return {undefined}
         */
        var removeListener = function(listener) {
            /** @type {boolean} */
            listener.removed = true;
            /** @type {null} */
            listener.listener = null;
            /** @type {null} */
            listener.proxy = null;
            /** @type {null} */
            listener.src = null;
            /** @type {null} */
            listener.Na = null;
        };
        /**
         * @param {?} src
         * @return {undefined}
         */
        var ctor = function(src) {
            this.src = src;
            this.a = {};
            /** @type {number} */
            this.b = 0;
        };
        /**
         * @param {Object} node
         * @param {Object} data
         * @param {string} value
         * @param {boolean} event
         * @param {(Range|TextRange)} opt_attributes
         * @param {boolean} obj
         * @return {?}
         */
        var filter = function(node, data, value, event, opt_attributes, obj) {
            var i = data.toString();
            data = node.a[i];
            if (!data) {
                /** @type {Array} */
                data = node.a[i] = [];
                node.b++;
            }
            var key = attr(data, value, opt_attributes, obj);
            if (-1 < key) {
                node = data[key];
                if (!event) {
                    /** @type {boolean} */
                    node.Ja = false;
                }
            } else {
                node = new _Class(value, node.src, i, !!opt_attributes, obj);
                /** @type {boolean} */
                node.Ja = event;
                data.push(node);
            }
            return node;
        };
        /**
         * @param {string} name
         * @param {Function} c
         * @param {boolean} attributes
         * @param {boolean} m
         * @return {?}
         */
        ctor.prototype.remove = function(name, c, attributes, m) {
            name = name.toString();
            if (!(name in this.a)) {
                return false;
            }
            var value = this.a[name];
            c = attr(value, c, attributes, m);
            return-1 < c ? (removeListener(value[c]), self.fb(value, c), 0 == value.length && (delete this.a[name], this.b--), true) : false;
        };
        /**
         * @param {Node} object
         * @param {Object} listener
         * @return {undefined}
         */
        var removeEvent = function(object, listener) {
            var name = listener.type;
            if (name in object.a) {
                if (self.gb(object.a[name], listener)) {
                    removeListener(listener);
                    if (0 == object.a[name].length) {
                        delete object.a[name];
                        object.b--;
                    }
                }
            }
        };
        /**
         * @param {Object} dest
         * @return {?}
         */
        ctor.prototype.removeAll = function(dest) {
            dest = dest && dest.toString();
            /** @type {number} */
            var count = 0;
            var mat;
            for (mat in this.a) {
                if (!dest || mat == dest) {
                    var codeSegments = this.a[mat];
                    /** @type {number} */
                    var i = 0;
                    for (;i < codeSegments.length;i++) {
                        ++count;
                        removeListener(codeSegments[i]);
                    }
                    delete this.a[mat];
                    this.b--;
                }
            }
            return count;
        };
        /**
         * @param {Object} obj
         * @param {number} v
         * @param {Function} k
         * @param {boolean} string
         * @param {Element} headers
         * @return {?}
         */
        var sprintf = function(obj, v, k, string, headers) {
            obj = obj.a[v.toString()];
            /** @type {number} */
            v = -1;
            if (obj) {
                v = attr(obj, k, string, headers);
            }
            return-1 < v ? obj[v] : null;
        };
        /**
         * @param {Node} o
         * @return {?}
         */
        var onSuccess = function(o) {
            /** @type {(string|undefined)} */
            var ready = self.m(void 0) ? "undefined" : void 0;
            var ret = self.m(ready);
            /** @type {string} */
            var typeName = ret ? ready.toString() : "";
            var e = self.m(void 0);
            return isEmpty(o.a, function(codeSegments) {
                /** @type {number} */
                var i = 0;
                for (;i < codeSegments.length;++i) {
                    if (!(ret && codeSegments[i].type != typeName || e && void 0 != codeSegments[i].wa)) {
                        return true;
                    }
                }
                return false;
            });
        };
        /**
         * @param {Array} op
         * @param {Function} name
         * @param {boolean} opt_attributes
         * @param {boolean} value
         * @return {?}
         */
        var attr = function(op, name, opt_attributes, value) {
            /** @type {number} */
            var i = 0;
            for (;i < op.length;++i) {
                var item = op[i];
                if (!item.removed && (item.listener == name && (item.wa == !!opt_attributes && item.Na == value))) {
                    return i;
                }
            }
            return-1;
        };
        var _i;
        var appenderMap;
        var fe;
        var addEventListener;
        var delegate;
        var unbind;
        var removeEventListener;
        var append;
        var errorHandler;
        var handle;
        var isString;
        var functionName;
        var encodeURIComponent;
        /** @type {string} */
        _i = "closure_lm_" + (1E6 * Math.random() | 0);
        appenderMap = {};
        /** @type {number} */
        fe = 0;
        /**
         * @param {Object} arg
         * @param {?} type
         * @param {Object} value
         * @param {(Range|TextRange)} opt_attributes
         * @param {boolean} event
         * @return {?}
         */
        self.ge = function(arg, type, value, opt_attributes, event) {
            if (self.Ca(type)) {
                /** @type {number} */
                var i = 0;
                for (;i < type.length;i++) {
                    self.ge(arg, type[i], value, opt_attributes, event);
                }
                return null;
            }
            value = encodeURIComponent(value);
            return isF(arg) ? arg.listen(type, value, opt_attributes, event) : addEventListener(arg, type, value, false, opt_attributes, event);
        };
        /**
         * @param {Object} target
         * @param {?} callback
         * @param {Object} fn
         * @param {Object} listener
         * @param {(Range|TextRange)} opt_attributes
         * @param {boolean} types
         * @return {?}
         */
        addEventListener = function(target, callback, fn, listener, opt_attributes, types) {
            if (!callback) {
                throw Error("h");
            }
            /** @type {boolean} */
            var capture = !!opt_attributes;
            var result = isString(target);
            if (!result) {
                target[_i] = result = new ctor(target);
            }
            fn = filter(result, callback, fn, listener, opt_attributes, types);
            if (fn.proxy) {
                return fn;
            }
            listener = delegate();
            /** @type {Object} */
            fn.proxy = listener;
            /** @type {Object} */
            listener.src = target;
            /** @type {Object} */
            listener.listener = fn;
            if (target.addEventListener) {
                target.addEventListener(callback.toString(), listener, capture);
            } else {
                if (target.attachEvent) {
                    target.attachEvent(removeEventListener(callback.toString()), listener);
                } else {
                    throw Error("i");
                }
            }
            fe++;
            return fn;
        };
        /**
         * @return {?}
         */
        delegate = function() {
            var proxyCallbackFunction = handle;
            /** @type {function (?): ?} */
            var f = named ? function(eventObject) {
                return proxyCallbackFunction.call(f.src, f.listener, eventObject);
            } : function(index) {
                index = proxyCallbackFunction.call(f.src, f.listener, index);
                if (!index) {
                    return index;
                }
            };
            return f;
        };
        /**
         * @param {Object} arg
         * @param {?} type
         * @param {Object} idx
         * @param {(Range|TextRange)} payload
         * @param {boolean} event
         * @return {?}
         */
        self.ne = function(arg, type, idx, payload, event) {
            if (self.Ca(type)) {
                /** @type {number} */
                var i = 0;
                for (;i < type.length;i++) {
                    self.ne(arg, type[i], idx, payload, event);
                }
                return null;
            }
            idx = encodeURIComponent(idx);
            return isF(arg) ? arg.Ma(type, idx, payload, event) : addEventListener(arg, type, idx, true, payload, event);
        };
        /**
         * @param {Object} value
         * @param {?} type
         * @param {?} text
         * @param {boolean} recurring
         * @param {Element} headers
         * @return {undefined}
         */
        self.oe = function(value, type, text, recurring, headers) {
            if (self.Ca(type)) {
                /** @type {number} */
                var i = 0;
                for (;i < type.length;i++) {
                    self.oe(value, type[i], text, recurring, headers);
                }
            } else {
                text = encodeURIComponent(text);
                if (isF(value)) {
                    value.ta(type, text, recurring, headers);
                } else {
                    if (value) {
                        if (value = isString(value)) {
                            if (type = sprintf(value, type, text, !!recurring, headers)) {
                                unbind(type);
                            }
                        }
                    }
                }
            }
        };
        /**
         * @param {Object} listener
         * @return {undefined}
         */
        unbind = function(listener) {
            if (self.Aa(listener) || (!listener || listener.removed)) {
                return;
            }
            var src = listener.src;
            if (isF(src)) {
                removeEvent(src.c, listener);
                return;
            }
            var type = listener.type;
            var proxy = listener.proxy;
            if (src.removeEventListener) {
                src.removeEventListener(type, proxy, listener.wa);
            } else {
                if (src.detachEvent) {
                    src.detachEvent(removeEventListener(type), proxy);
                }
            }
            fe--;
            if (type = isString(src)) {
                removeEvent(type, listener);
                if (0 == type.b) {
                    /** @type {null} */
                    type.src = null;
                    /** @type {null} */
                    src[_i] = null;
                }
            } else {
                removeListener(listener);
            }
        };
        /**
         * @param {string} type
         * @return {?}
         */
        removeEventListener = function(type) {
            return type in appenderMap ? appenderMap[type] : appenderMap[type] = "on" + type;
        };
        /**
         * @param {number} cur
         * @param {Object} text
         * @param {boolean} recurring
         * @param {?} e
         * @return {?}
         */
        append = function(cur, text, recurring, e) {
            /** @type {boolean} */
            var a = true;
            if (cur = isString(cur)) {
                if (text = cur.a[text.toString()]) {
                    text = text.concat();
                    /** @type {number} */
                    cur = 0;
                    for (;cur < text.length;cur++) {
                        var c = text[cur];
                        if (c) {
                            if (c.wa == recurring) {
                                if (!c.removed) {
                                    c = errorHandler(c, e);
                                    /** @type {boolean} */
                                    a = a && false !== c;
                                }
                            }
                        }
                    }
                }
            }
            return a;
        };
        /**
         * @param {Object} data
         * @param {?} err
         * @return {?}
         */
        errorHandler = function(data, err) {
            var callback = data.listener;
            var T = data.Na || data.src;
            if (data.Ja) {
                unbind(data);
            }
            return callback.call(T, err);
        };
        /**
         * @param {Object} data
         * @param {string} options
         * @return {?}
         */
        handle = function(data, options) {
            if (data.removed) {
                return true;
            }
            if (!named) {
                var opts = options || self.Ea("window.event");
                var d = new listener(opts, this);
                /** @type {boolean} */
                var val = true;
                if (!(0 > opts.keyCode || void 0 != opts.returnValue)) {
                    a: {
                        /** @type {boolean} */
                        var b = false;
                        if (0 == opts.keyCode) {
                            try {
                                /** @type {number} */
                                opts.keyCode = -1;
                                break a;
                            } catch (n) {
                                /** @type {boolean} */
                                b = true;
                            }
                        }
                        if (b || void 0 == opts.returnValue) {
                            /** @type {boolean} */
                            opts.returnValue = true;
                        }
                    }
                    /** @type {Array} */
                    opts = [];
                    b = d.b;
                    for (;b;b = b.parentNode) {
                        opts.push(b);
                    }
                    b = data.type;
                    /** @type {number} */
                    var j = opts.length - 1;
                    for (;!d.g && 0 <= j;j--) {
                        d.b = opts[j];
                        var bc = append(opts[j], b, true, d);
                        val = val && bc;
                    }
                    /** @type {number} */
                    j = 0;
                    for (;!d.g && j < opts.length;j++) {
                        d.b = opts[j];
                        bc = append(opts[j], b, false, d);
                        val = val && bc;
                    }
                }
                return val;
            }
            return errorHandler(data, new listener(options, this));
        };
        /**
         * @param {?} obj
         * @return {?}
         */
        isString = function(obj) {
            obj = obj[_i];
            return obj instanceof ctor ? obj : null;
        };
        /** @type {string} */
        functionName = "__closure_events_fn_" + (1E9 * Math.random() >>> 0);
        /**
         * @param {Function} x
         * @return {?}
         */
        encodeURIComponent = function(x) {
            if (self.za(x)) {
                return x;
            }
            if (!x[functionName]) {
                /**
                 * @param {?} evt
                 * @return {?}
                 */
                x[functionName] = function(evt) {
                    return x.handleEvent(evt);
                };
            }
            return x[functionName];
        };
        traverseNode(function($) {
            handle = $(handle);
        });
        /**
         * @return {undefined}
         */
        self.L = function() {
            self.F.call(this);
            this.c = new ctor(this);
            this.ga = this;
            /** @type {null} */
            this.G = null;
        };
        self.p(self.L, self.F);
        /** @type {boolean} */
        self.L.prototype[method] = true;
        /**
         * @param {string} types
         * @return {undefined}
         */
        self.L.prototype.I = function(types) {
            /** @type {string} */
            this.G = types;
        };
        /**
         * @param {?} listener
         * @param {?} type
         * @param {boolean} recurring
         * @param {Element} callback
         * @return {undefined}
         */
        self.L.prototype.removeEventListener = function(listener, type, recurring, callback) {
            self.oe(this, listener, type, recurring, callback);
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {?}
         */
        self.M = function(types, type) {
            var codeSegments;
            var src = types.G;
            if (src) {
                /** @type {Array} */
                codeSegments = [];
                for (;src;src = src.G) {
                    codeSegments.push(src);
                }
            }
            src = types.ga;
            /** @type {Function} */
            var e = type;
            var evt = e.type || e;
            if (self.t(e)) {
                e = new self.K(e, src);
            } else {
                if (e instanceof self.K) {
                    e.target = e.target || src;
                } else {
                    var ok = e;
                    e = new self.K(evt, src);
                    self.sb(e, ok);
                }
            }
            /** @type {boolean} */
            ok = true;
            var _this;
            if (codeSegments) {
                /** @type {number} */
                var i = codeSegments.length - 1;
                for (;!e.g && 0 <= i;i--) {
                    _this = e.b = codeSegments[i];
                    ok = _this.pa(evt, true, e) && ok;
                }
            }
            if (!e.g) {
                _this = e.b = src;
                ok = _this.pa(evt, true, e) && ok;
                if (!e.g) {
                    ok = _this.pa(evt, false, e) && ok;
                }
            }
            if (codeSegments) {
                /** @type {number} */
                i = 0;
                for (;!e.g && i < codeSegments.length;i++) {
                    _this = e.b = codeSegments[i];
                    ok = _this.pa(evt, false, e) && ok;
                }
            }
            return ok;
        };
        self.g = self.L.prototype;
        /**
         * @return {undefined}
         */
        self.g.B = function() {
            self.L.D.B.call(this);
            if (this.c) {
                this.c.removeAll(void 0);
            }
            /** @type {null} */
            this.G = null;
        };
        /**
         * @param {?} type
         * @param {string} callback
         * @param {?} opt_attributes
         * @param {boolean} types
         * @return {?}
         */
        self.g.listen = function(type, callback, opt_attributes, types) {
            return filter(this.c, String(type), callback, false, opt_attributes, types);
        };
        /**
         * @param {?} type
         * @param {string} thisObj
         * @param {(Range|TextRange)} opt_attributes
         * @param {boolean} walkers
         * @return {?}
         */
        self.g.Ma = function(type, thisObj, opt_attributes, walkers) {
            return filter(this.c, String(type), thisObj, true, opt_attributes, walkers);
        };
        /**
         * @param {string} data
         * @param {?} type
         * @param {Object} opt_attributes
         * @param {boolean} message
         * @return {?}
         */
        self.g.ta = function(data, type, opt_attributes, message) {
            return this.c.remove(String(data), type, opt_attributes, message);
        };
        /**
         * @param {?} a
         * @param {boolean} recurring
         * @param {?} eventObject
         * @return {?}
         */
        self.g.pa = function(a, recurring, eventObject) {
            a = this.c.a[String(a)];
            if (!a) {
                return true;
            }
            a = a.concat();
            /** @type {boolean} */
            var rv = true;
            /** @type {number} */
            var i = 0;
            for (;i < a.length;++i) {
                var listener = a[i];
                if (listener && (!listener.removed && listener.wa == recurring)) {
                    var listenerFn = listener.listener;
                    var listenerHandler = listener.Na || listener.src;
                    if (listener.Ja) {
                        removeEvent(this.c, listener);
                    }
                    /** @type {boolean} */
                    rv = false !== listenerFn.call(listenerHandler, eventObject) && rv;
                }
            }
            return rv && 0 != eventObject.lb;
        };
        /**
         * @return {undefined}
         */
        var config = function() {
        };
        /** @type {null} */
        config.prototype.a = null;
        /**
         * @param {Node} data
         * @return {?}
         */
        var getJSON = function(data) {
            var a;
            if (!(a = data.a)) {
                a = {};
                if (request(data)) {
                    /** @type {boolean} */
                    a[0] = true;
                    /** @type {boolean} */
                    a[1] = true;
                }
                a = data.a = a;
            }
            return a;
        };
        var camelKey;
        /**
         * @return {undefined}
         */
        var p = function() {
        };
        self.p(p, config);
        /**
         * @param {Element} data
         * @return {?}
         */
        var createRequest = function(data) {
            return(data = request(data)) ? new window.ActiveXObject(data) : new window.XMLHttpRequest;
        };
        /**
         * @param {Node} data
         * @return {?}
         */
        var request = function(data) {
            if (!data.b && ("undefined" == typeof window.XMLHttpRequest && "undefined" != typeof window.ActiveXObject)) {
                /** @type {Array} */
                var ca = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
                /** @type {number} */
                var i = 0;
                for (;i < ca.length;i++) {
                    var c = ca[i];
                    try {
                        return new window.ActiveXObject(c), data.b = c;
                    } catch (e) {
                    }
                }
                throw Error("j");
            }
            return data.b;
        };
        camelKey = new p;
        /**
         * @param {number} h
         * @param {(Document|string)} g
         * @return {undefined}
         */
        self.ze = function(h, g) {
            self.L.call(this);
            this.h = h || 1;
            this.g = g || self.l;
            this.j = (0, self.r)(this.v, this);
            this.l = (0, self.sa)();
        };
        self.p(self.ze, self.L);
        /** @type {boolean} */
        self.ze.prototype.b = false;
        /** @type {null} */
        self.ze.prototype.a = null;
        /**
         * @param {Function} code
         * @return {undefined}
         */
        self.ze.prototype.setInterval = function(code) {
            /** @type {Function} */
            this.h = code;
            if (this.a && this.b) {
                this.stop();
                self.Ae(this);
            } else {
                if (this.a) {
                    this.stop();
                }
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.v = function() {
            if (this.b) {
                /** @type {number} */
                var y = (0, self.sa)() - this.l;
                if (0 < y && y < 0.8 * this.h) {
                    this.a = this.g.setTimeout(this.j, this.h - y);
                } else {
                    if (this.a) {
                        this.g.clearTimeout(this.a);
                        /** @type {null} */
                        this.a = null;
                    }
                    self.M(this, "tick");
                    if (this.b) {
                        this.a = this.g.setTimeout(this.j, this.h);
                        this.l = (0, self.sa)();
                    }
                }
            }
        };
        /**
         * @param {Object} a
         * @return {undefined}
         */
        self.Ae = function(a) {
            /** @type {boolean} */
            a.b = true;
            if (!a.a) {
                a.a = a.g.setTimeout(a.j, a.h);
                a.l = (0, self.sa)();
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.stop = function() {
            /** @type {boolean} */
            this.b = false;
            if (this.a) {
                this.g.clearTimeout(this.a);
                /** @type {null} */
                this.a = null;
            }
        };
        /**
         * @return {undefined}
         */
        self.ze.prototype.B = function() {
            self.ze.D.B.call(this);
            this.stop();
            delete this.g;
        };
        /**
         * @param {Function} fn
         * @param {number} expectedHashCode
         * @param {Node} obj
         * @return {?}
         */
        self.Be = function(fn, expectedHashCode, obj) {
            if (self.za(fn)) {
                if (obj) {
                    fn = (0, self.r)(fn, obj);
                }
            } else {
                if (fn && "function" == typeof fn.handleEvent) {
                    fn = (0, self.r)(fn.handleEvent, fn);
                } else {
                    throw Error("k");
                }
            }
            return 2147483647 < Number(expectedHashCode) ? -1 : self.l.setTimeout(fn, expectedHashCode || 0);
        };
        /**
         * @param {?} timeoutKey
         * @return {undefined}
         */
        self.Ce = function(timeoutKey) {
            self.l.clearTimeout(timeoutKey);
        };
        /** @type {RegExp} */
        var dattr = /^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;
        /**
         * @param {string} docPath
         * @param {Function} callback
         * @return {undefined}
         */
        var expand = function(docPath, callback) {
            if (docPath) {
                var matches = docPath.split("&");
                /** @type {number} */
                var i = 0;
                for (;i < matches.length;i++) {
                    var indexOfEquals = matches[i].indexOf("=");
                    var name;
                    /** @type {null} */
                    var code = null;
                    if (0 <= indexOfEquals) {
                        name = matches[i].substring(0, indexOfEquals);
                        code = matches[i].substring(indexOfEquals + 1);
                    } else {
                        name = matches[i];
                    }
                    callback(name, code ? (0, window.decodeURIComponent)(code.replace(/\+/g, " ")) : "");
                }
            }
        };
        /**
         * @param {string} row_id
         * @return {undefined}
         */
        var item = function(row_id) {
            self.L.call(this);
            this.headers = new self.J;
            this.H = row_id || null;
            /** @type {boolean} */
            this.b = false;
            /** @type {null} */
            this.A = this.a = null;
            /** @type {string} */
            this.O = "";
            /** @type {number} */
            this.j = 0;
            /** @type {string} */
            this.h = "";
            /** @type {boolean} */
            this.g = this.M = this.v = this.L = false;
            /** @type {number} */
            this.l = 0;
            /** @type {null} */
            this.w = null;
            /** @type {string} */
            this.aa = "";
            /** @type {boolean} */
            this.T = this.W = false;
        };
        var regex;
        var silentOptions;
        var handleResponse;
        var onload;
        var stringify;
        var valueOf;
        var successCallback;
        self.p(item, self.L);
        /** @type {RegExp} */
        regex = /^https?$/i;
        /** @type {Array} */
        silentOptions = ["POST", "PUT"];
        /** @type {Array} */
        self.Ie = [];
        /**
         * @param {?} e
         * @param {string} events
         * @param {string} type
         * @param {string} payload
         * @param {Object} callback
         * @param {number} opt_attributes
         * @param {string} dataAndEvents
         * @return {undefined}
         */
        self.Je = function(e, events, type, payload, callback, opt_attributes, dataAndEvents) {
            var that = new item;
            self.Ie.push(that);
            if (events) {
                that.listen("complete", events);
            }
            that.Ma("ready", that.ha);
            if (opt_attributes) {
                /** @type {number} */
                that.l = Math.max(0, opt_attributes);
            }
            if (dataAndEvents) {
                /** @type {string} */
                that.W = dataAndEvents;
            }
            that.send(e, type, payload, callback);
        };
        /**
         * @return {undefined}
         */
        item.prototype.ha = function() {
            this.Z();
            self.gb(self.Ie, this);
        };
        /**
         * @param {number} message
         * @param {string} method
         * @param {string} payload
         * @param {(Array|string)} result
         * @return {undefined}
         */
        item.prototype.send = function(message, method, payload, result) {
            if (this.a) {
                throw Error("l`" + this.O + "`" + message);
            }
            method = method ? method.toUpperCase() : "GET";
            /** @type {number} */
            this.O = message;
            /** @type {string} */
            this.h = "";
            /** @type {number} */
            this.j = 0;
            /** @type {boolean} */
            this.L = false;
            /** @type {boolean} */
            this.b = true;
            this.a = this.H ? createRequest(this.H) : createRequest(camelKey);
            this.A = this.H ? getJSON(this.H) : getJSON(camelKey);
            this.a.onreadystatechange = (0, self.r)(this.$, this);
            try {
                /** @type {boolean} */
                this.M = true;
                this.a.open(method, String(message), true);
                /** @type {boolean} */
                this.M = false;
            } catch (failuresLink) {
                RGBToHSB(this, failuresLink);
                return;
            }
            message = payload || "";
            var res = new self.J(this.headers);
            if (result) {
                send(result, function(ar, storageKey) {
                    res.set(storageKey, ar);
                });
            }
            result = self.db(res.X(), table);
            payload = self.l.FormData && message instanceof self.l.FormData;
            if (!!self.eb(silentOptions, method)) {
                if (!result) {
                    if (!payload) {
                        res.set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                    }
                }
            }
            res.forEach(function(sValue, env) {
                this.a.setRequestHeader(env, sValue);
            }, this);
            if (this.aa) {
                this.a.responseType = this.aa;
            }
            if ("withCredentials" in this.a) {
                if (this.a.withCredentials !== this.W) {
                    this.a.withCredentials = this.W;
                }
            }
            try {
                stringify(this);
                if (0 < this.l) {
                    if (this.T = publish(this.a)) {
                        this.a.timeout = this.l;
                        this.a.ontimeout = (0, self.r)(this.Y, this);
                    } else {
                        this.w = self.Be(this.Y, this.l, this);
                    }
                }
                /** @type {boolean} */
                this.v = true;
                this.a.send(message);
                /** @type {boolean} */
                this.v = false;
            } catch (passesLink) {
                RGBToHSB(this, passesLink);
            }
        };
        /**
         * @param {?} config
         * @return {?}
         */
        var publish = function(config) {
            return self.x && (self.Pb(9) && (self.Aa(config.timeout) && self.m(config.ontimeout)));
        };
        /**
         * @param {Object} header
         * @return {?}
         */
        var table = function(header) {
            return "content-type" == header.toLowerCase();
        };
        /**
         * @return {undefined}
         */
        item.prototype.Y = function() {
            if ("undefined" != typeof self.qa) {
                if (this.a) {
                    /** @type {string} */
                    this.h = "Timed out after " + this.l + "ms, aborting";
                    /** @type {number} */
                    this.j = 8;
                    self.M(this, "timeout");
                    if (this.a) {
                        if (this.b) {
                            /** @type {boolean} */
                            this.b = false;
                            /** @type {boolean} */
                            this.g = true;
                            this.a.abort();
                            /** @type {boolean} */
                            this.g = false;
                            /** @type {number} */
                            this.j = 8;
                            self.M(this, "complete");
                            self.M(this, "abort");
                            onload(this);
                        }
                    }
                }
            }
        };
        /**
         * @param {Object} e
         * @param {?} el
         * @return {undefined}
         */
        var RGBToHSB = function(e, el) {
            /** @type {boolean} */
            e.b = false;
            if (e.a) {
                /** @type {boolean} */
                e.g = true;
                e.a.abort();
                /** @type {boolean} */
                e.g = false;
            }
            e.h = el;
            /** @type {number} */
            e.j = 5;
            _callback(e);
            onload(e);
        };
        /**
         * @param {string} ready
         * @return {undefined}
         */
        var _callback = function(ready) {
            if (!ready.L) {
                /** @type {boolean} */
                ready.L = true;
                self.M(ready, "complete");
                self.M(ready, "error");
            }
        };
        /**
         * @return {undefined}
         */
        item.prototype.B = function() {
            if (this.a) {
                if (this.b) {
                    /** @type {boolean} */
                    this.b = false;
                    /** @type {boolean} */
                    this.g = true;
                    this.a.abort();
                    /** @type {boolean} */
                    this.g = false;
                }
                onload(this, true);
            }
            item.D.B.call(this);
        };
        /**
         * @return {undefined}
         */
        item.prototype.$ = function() {
            if (!this.K) {
                if (this.M || (this.v || this.g)) {
                    handleResponse(this);
                } else {
                    this.P();
                }
            }
        };
        /**
         * @return {undefined}
         */
        item.prototype.P = function() {
            handleResponse(this);
        };
        /**
         * @param {string} ready
         * @return {undefined}
         */
        handleResponse = function(ready) {
            if (ready.b && ("undefined" != typeof self.qa && (!ready.A[1] || (4 != valueOf(ready) || 2 != successCallback(ready))))) {
                if (ready.v && 4 == valueOf(ready)) {
                    self.Be(ready.$, 0, ready);
                } else {
                    if (self.M(ready, "readystatechange"), 4 == valueOf(ready)) {
                        /** @type {boolean} */
                        ready.b = false;
                        try {
                            if (self.Te(ready)) {
                                self.M(ready, "complete");
                                self.M(ready, "success");
                            } else {
                                /** @type {number} */
                                ready.j = 6;
                                var expires;
                                try {
                                    expires = 2 < valueOf(ready) ? ready.a.statusText : "";
                                } catch (c) {
                                    /** @type {string} */
                                    expires = "";
                                }
                                /** @type {string} */
                                ready.h = expires + " [" + successCallback(ready) + "]";
                                _callback(ready);
                            }
                        } finally {
                            onload(ready);
                        }
                    }
                }
            }
        };
        /**
         * @param {string} ready
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        onload = function(ready, dataAndEvents) {
            if (ready.a) {
                stringify(ready);
                var el = ready.a;
                var successCallback = ready.A[0] ? self.Da : null;
                /** @type {null} */
                ready.a = null;
                /** @type {null} */
                ready.A = null;
                if (!dataAndEvents) {
                    self.M(ready, "ready");
                }
                try {
                    el.onreadystatechange = successCallback;
                } catch (e) {
                }
            }
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        stringify = function(obj) {
            if (obj.a) {
                if (obj.T) {
                    /** @type {null} */
                    obj.a.ontimeout = null;
                }
            }
            if (self.Aa(obj.w)) {
                self.Ce(obj.w);
                /** @type {null} */
                obj.w = null;
            }
        };
        /**
         * @param {string} a
         * @return {?}
         */
        self.Te = function(a) {
            var fn = successCallback(a);
            var bulk;
            a: {
                switch(fn) {
                    case 200:
                        ;
                    case 201:
                        ;
                    case 202:
                        ;
                    case 204:
                        ;
                    case 206:
                        ;
                    case 304:
                        ;
                    case 1223:
                        /** @type {boolean} */
                        bulk = true;
                        break a;
                    default:
                        /** @type {boolean} */
                        bulk = false;
                }
            }
            if (!bulk) {
                if (fn = 0 === fn) {
                    /** @type {(null|string)} */
                    a = String(a.O).match(dattr)[1] || null;
                    if (!a) {
                        if (self.l.self) {
                            if (self.l.self.location) {
                                a = self.l.self.location.protocol;
                                a = a.substr(0, a.length - 1);
                            }
                        }
                    }
                    /** @type {boolean} */
                    fn = !regex.test(a ? a.toLowerCase() : "");
                }
                /** @type {boolean} */
                bulk = fn;
            }
            return bulk;
        };
        /**
         * @param {Node} object
         * @return {?}
         */
        valueOf = function(object) {
            return object.a ? object.a.readyState : 0;
        };
        /**
         * @param {Node} result
         * @return {?}
         */
        successCallback = function(result) {
            try {
                return 2 < valueOf(result) ? result.a.status : -1;
            } catch (b) {
                return-1;
            }
        };
        /**
         * @param {Object} e
         * @return {?}
         */
        self.Ue = function(e) {
            try {
                return e.a ? e.a.responseText : "";
            } catch (b) {
                return "";
            }
        };
        traverseNode(function($sanitize) {
            item.prototype.P = $sanitize(item.prototype.P);
        });
        var notify;
        /**
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        self.N = function(dataAndEvents) {
            self.F.call(this);
            /** @type {boolean} */
            this.v = dataAndEvents;
            this.g = {};
        };
        self.p(self.N, self.F);
        /** @type {Array} */
        var pkgName = [];
        /**
         * @param {?} type
         * @param {string} name
         * @param {?} opt_attributes
         * @param {boolean} callback
         * @return {?}
         */
        self.N.prototype.listen = function(type, name, opt_attributes, callback) {
            if (!self.Ca(name)) {
                if (name) {
                    pkgName[0] = name.toString();
                }
                /** @type {Array} */
                name = pkgName;
            }
            /** @type {number} */
            var i = 0;
            for (;i < name.length;i++) {
                var ret = self.ge(type, name[i], opt_attributes || this.handleEvent, callback || false, this.v || this);
                if (!ret) {
                    break;
                }
                this.g[ret.key] = ret;
            }
            return this;
        };
        /**
         * @param {Object} msg
         * @param {Object} errmsg
         * @param {?} e
         * @param {Function} next_callback
         * @return {undefined}
         */
        self.Ze = function(msg, errmsg, e, next_callback) {
            notify(msg, errmsg, e, next_callback, void 0);
        };
        /**
         * @param {Object} obj
         * @param {Object} msg
         * @param {?} arg
         * @param {Object} callback
         * @param {(Range|TextRange)} type
         * @param {Node} deepDataAndEvents
         * @return {undefined}
         */
        notify = function(obj, msg, arg, callback, type, deepDataAndEvents) {
            if (self.Ca(arg)) {
                /** @type {number} */
                var a = 0;
                for (;a < arg.length;a++) {
                    notify(obj, msg, arg[a], callback, type, deepDataAndEvents);
                }
            } else {
                msg = self.ne(msg, arg, callback || obj.handleEvent, type, deepDataAndEvents || (obj.v || obj));
                if (!msg) {
                    return;
                }
                /** @type {Object} */
                obj.g[msg.key] = msg;
            }
        };
        /**
         * @param {string} val
         * @param {?} arg
         * @param {Object} title
         * @param {boolean} message
         * @param {Node} headers
         * @return {?}
         */
        self.N.prototype.ta = function(val, arg, title, message, headers) {
            if (self.Ca(arg)) {
                /** @type {number} */
                var a = 0;
                for (;a < arg.length;a++) {
                    this.ta(val, arg[a], title, message, headers);
                }
            } else {
                title = title || this.handleEvent;
                headers = headers || (this.v || this);
                title = encodeURIComponent(title);
                /** @type {boolean} */
                message = !!message;
                arg = isF(val) ? sprintf(val.c, String(arg), title, message, headers) : val ? (val = isString(val)) ? sprintf(val, arg, title, message, headers) : null : null;
                if (arg) {
                    unbind(arg);
                    delete this.g[arg.key];
                }
            }
            return this;
        };
        /**
         * @return {undefined}
         */
        self.N.prototype.removeAll = function() {
            self.mb(this.g, function(element, word) {
                if (this.g.hasOwnProperty(word)) {
                    unbind(element);
                }
            }, this);
            this.g = {};
        };
        /**
         * @return {undefined}
         */
        self.N.prototype.B = function() {
            self.N.D.B.call(this);
            this.removeAll();
        };
        /**
         * @return {?}
         */
        self.N.prototype.handleEvent = function() {
            throw Error("m");
        };
        /**
         * @return {undefined}
         */
        var ret = function() {
        };
        /**
         * @param {string} types
         * @return {?}
         */
        ret.prototype.g = function(types) {
            if (this.a) {
                /** @type {number} */
                var i = 0;
                for (;i < this.a.length;++i) {
                    if (this.a[i] instanceof types) {
                        return this.a[i];
                    }
                }
            }
            return null;
        };
        /**
         * @param {Function} key
         * @return {undefined}
         */
        var deferProp = function(key) {
            /** @type {boolean} */
            (key ? key : function() {
            }).Ib = true;
        };
        /**
         * @return {undefined}
         */
        self.bf = function() {
        };
        self.p(self.bf, ret);
        /**
         * @param {string} types
         * @return {?}
         */
        self.bf.prototype.b = function(types) {
            return types;
        };
        deferProp(self.bf.prototype.b);
        /**
         * @return {?}
         */
        self.bf.prototype.c = function() {
            return true;
        };
        deferProp(self.bf.prototype.c);
        /**
         * @param {number} value
         * @return {undefined}
         */
        var subject = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(subject, self.y);
        var parseInt;
        /**
         * @param {string} value
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        parseInt = function(value, expectedNumberOfNonCommentArgs) {
            var result = new subject;
            self.B(result, 1, expectedNumberOfNonCommentArgs);
            self.B(result, 2, value);
            return result;
        };
        self.ef = parseInt("?", 0);
        self.ff = parseInt("q", 1);
        self.gf = parseInt("<", 2);
        self.hf = parseInt(">", 3);
        self.jf = parseInt("~", 4);
        self.kf = parseInt("s", 5);
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {undefined}
         */
        self.O = function(types, type, expectedNumberOfNonCommentArgs) {
            /** @type {string} */
            this.c = types;
            this.b = type || null;
            this.a = expectedNumberOfNonCommentArgs || [];
        };
        /**
         * @return {?}
         */
        self.O.prototype.toString = function() {
            return this.c;
        };
        var post;
        var rgb;
        var invoke;
        var lambda;
        var quote;
        var _char;
        var url;
        var HEREGEX_OMIT;
        var regAttrs;
        var defaults;
        var varName;
        var that;
        var trigger;
        /**
         * @param {string} types
         * @param {string} type
         * @return {undefined}
         */
        self.P = function(types, type) {
            /** @type {string} */
            this.b = this.C = this.h = "";
            /** @type {null} */
            this.g = null;
            /** @type {string} */
            this.l = this.a = "";
            /** @type {boolean} */
            this.j = false;
            var m;
            if (types instanceof self.P) {
                this.j = self.m(type) ? type : types.j;
                post(this, types.h);
                this.C = types.C;
                this.b = types.b;
                rgb(this, types.g);
                this.a = types.a;
                m = types.c;
                var top = new that;
                top.c = m.c;
                if (m.a) {
                    top.a = new self.J(m.a);
                    top.b = m.b;
                }
                invoke(this, top);
                self.pf(this, types.l);
            } else {
                if (types && (m = String(types).match(dattr))) {
                    /** @type {boolean} */
                    this.j = !!type;
                    post(this, m[1] || "", true);
                    this.C = lambda(m[2] || "");
                    this.b = lambda(m[3] || "", true);
                    rgb(this, m[4]);
                    this.a = lambda(m[5] || "", true);
                    invoke(this, m[6] || "", true);
                    self.pf(this, m[7] || "", true);
                } else {
                    /** @type {boolean} */
                    this.j = !!type;
                    this.c = new that(null, 0, this.j);
                }
            }
        };
        /**
         * @return {?}
         */
        self.P.prototype.toString = function() {
            /** @type {Array} */
            var t = [];
            var k = this.h;
            if (k) {
                t.push(quote(k, url, true), ":");
            }
            var b = this.b;
            if (b || "file" == k) {
                t.push("//");
                if (k = this.C) {
                    t.push(quote(k, url, true), "@");
                }
                t.push((0, window.encodeURIComponent)(String(b)).replace(/%25([0-9a-fA-F]{2})/g, "%$1"));
                b = this.g;
                if (null != b) {
                    t.push(":", String(b));
                }
            }
            if (b = this.a) {
                if (this.b) {
                    if ("/" != b.charAt(0)) {
                        t.push("/");
                    }
                }
                t.push(quote(b, "/" == b.charAt(0) ? regAttrs : HEREGEX_OMIT, true));
            }
            if (b = this.c.toString()) {
                t.push("?", b);
            }
            if (b = this.l) {
                t.push("#", quote(b, varName));
            }
            return t.join("");
        };
        /**
         * @param {Object} args
         * @param {string} data
         * @param {boolean} callback
         * @return {undefined}
         */
        post = function(args, data, callback) {
            args.h = callback ? lambda(data, true) : data;
            if (args.h) {
                args.h = args.h.replace(/:$/, "");
            }
        };
        /**
         * @param {?} b
         * @param {number} g
         * @return {undefined}
         */
        rgb = function(b, g) {
            if (g) {
                /** @type {number} */
                g = Number(g);
                if ((0, window.isNaN)(g) || 0 > g) {
                    throw Error("r`" + g);
                }
                /** @type {number} */
                b.g = g;
            } else {
                /** @type {null} */
                b.g = null;
            }
        };
        /**
         * @param {Object} args
         * @param {Object} obj
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        invoke = function(args, obj, dataAndEvents) {
            if (obj instanceof that) {
                /** @type {Object} */
                args.c = obj;
                end(args.c, args.j);
            } else {
                if (!dataAndEvents) {
                    obj = quote(obj, defaults);
                }
                args.c = new that(obj, 0, args.j);
            }
        };
        /**
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {string} storageKey
         * @param {Object} now
         * @return {undefined}
         */
        self.R = function(expectedNumberOfNonCommentArgs, storageKey, now) {
            expectedNumberOfNonCommentArgs.c.set(storageKey, now);
        };
        /**
         * @param {Node} query
         * @param {string} data
         * @param {boolean} preserve
         * @return {undefined}
         */
        self.pf = function(query, data, preserve) {
            query.l = preserve ? lambda(data) : data;
        };
        /**
         * @param {string} value
         * @return {?}
         */
        self.yf = function(value) {
            return value instanceof self.P ? new self.P(value) : new self.P(value, void 0);
        };
        /**
         * @param {string} radius
         * @param {string} res
         * @return {?}
         */
        self.zf = function(radius, res) {
            if (!(radius instanceof self.P)) {
                radius = self.yf(radius);
            }
            if (!(res instanceof self.P)) {
                res = self.yf(res);
            }
            /** @type {string} */
            var arg = radius;
            /** @type {string} */
            var args = res;
            var options = new self.P(arg);
            /** @type {boolean} */
            var webm = !!args.h;
            if (webm) {
                post(options, args.h);
            } else {
                /** @type {boolean} */
                webm = !!args.C;
            }
            if (webm) {
                options.C = args.C;
            } else {
                /** @type {boolean} */
                webm = !!args.b;
            }
            if (webm) {
                options.b = args.b;
            } else {
                /** @type {boolean} */
                webm = null != args.g;
            }
            var num = args.a;
            if (webm) {
                rgb(options, args.g);
            } else {
                if (webm = !!args.a) {
                    if ("/" != num.charAt(0) && (arg.b && !arg.a ? num = "/" + num : (arg = options.a.lastIndexOf("/"), -1 != arg && (num = options.a.substr(0, arg + 1) + num))), arg = num, ".." == arg || "." == arg) {
                        /** @type {string} */
                        num = "";
                    } else {
                        if (-1 != arg.indexOf("./") || -1 != arg.indexOf("/.")) {
                            num = self.Ha(arg, "/");
                            arg = arg.split("/");
                            /** @type {Array} */
                            var out = [];
                            /** @type {number} */
                            var a = 0;
                            for (;a < arg.length;) {
                                var copies = arg[a++];
                                if ("." == copies) {
                                    if (num) {
                                        if (a == arg.length) {
                                            out.push("");
                                        }
                                    }
                                } else {
                                    if (".." == copies) {
                                        if (1 < out.length || 1 == out.length && "" != out[0]) {
                                            out.pop();
                                        }
                                        if (num) {
                                            if (a == arg.length) {
                                                out.push("");
                                            }
                                        }
                                    } else {
                                        out.push(copies);
                                        /** @type {boolean} */
                                        num = true;
                                    }
                                }
                            }
                            /** @type {string} */
                            num = out.join("/");
                        } else {
                            num = arg;
                        }
                    }
                }
            }
            if (webm) {
                options.a = num;
            } else {
                /** @type {boolean} */
                webm = "" !== args.c.toString();
            }
            if (webm) {
                invoke(options, lambda(args.c.toString()));
            } else {
                /** @type {boolean} */
                webm = !!args.l;
            }
            if (webm) {
                self.pf(options, args.l);
            }
            return options;
        };
        /**
         * @param {string} tmpl
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        lambda = function(tmpl, dataAndEvents) {
            return tmpl ? dataAndEvents ? (0, window.decodeURI)(tmpl.replace(/%25/g, "%2525")) : (0, window.decodeURIComponent)(tmpl) : "";
        };
        /**
         * @param {string} str
         * @param {?} s
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        quote = function(str, s, dataAndEvents) {
            return self.t(str) ? (str = (0, window.encodeURI)(str).replace(s, _char), dataAndEvents && (str = str.replace(/%25([0-9a-fA-F]{2})/g, "%$1")), str) : null;
        };
        /**
         * @param {string} n
         * @return {?}
         */
        _char = function(n) {
            n = n.charCodeAt(0);
            return "%" + (n >> 4 & 15).toString(16) + (n & 15).toString(16);
        };
        /** @type {RegExp} */
        url = /[#\/\?@]/g;
        /** @type {RegExp} */
        HEREGEX_OMIT = /[\#\?:]/g;
        /** @type {RegExp} */
        regAttrs = /[\#\?]/g;
        /** @type {RegExp} */
        defaults = /[\#\?@]/g;
        /** @type {RegExp} */
        varName = /#/g;
        /**
         * @param {Node} c
         * @param {?} dataAndEvents
         * @param {?} trigger
         * @return {undefined}
         */
        that = function(c, dataAndEvents, trigger) {
            /** @type {null} */
            this.b = this.a = null;
            this.c = c || null;
            /** @type {boolean} */
            this.g = !!trigger;
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        trigger = function(obj) {
            if (!obj.a) {
                obj.a = new self.J;
                /** @type {number} */
                obj.b = 0;
                if (obj.c) {
                    expand(obj.c, function(messageFormat, part) {
                        var key = (0, window.decodeURIComponent)(messageFormat.replace(/\+/g, " "));
                        trigger(obj);
                        /** @type {null} */
                        obj.c = null;
                        key = join(obj, key);
                        var values = obj.a.get(key);
                        if (!values) {
                            obj.a.set(key, values = []);
                        }
                        values.push(part);
                        obj.b += 1;
                    });
                }
            }
        };
        /**
         * @param {string} name
         * @return {?}
         */
        that.prototype.remove = function(name) {
            trigger(this);
            name = join(this, name);
            return self.Lc(this.a, name) ? (this.c = null, this.b -= this.a.get(name).length, this.a.remove(name)) : false;
        };
        /**
         * @return {undefined}
         */
        that.prototype.clear = function() {
            /** @type {null} */
            this.a = this.c = null;
            /** @type {number} */
            this.b = 0;
        };
        /**
         * @param {Object} obj
         * @param {string} key
         * @return {?}
         */
        var hOP = function(obj, key) {
            trigger(obj);
            key = join(obj, key);
            return self.Lc(obj.a, key);
        };
        /**
         * @return {?}
         */
        that.prototype.X = function() {
            trigger(this);
            var segs = this.a.V();
            var codeSegments = this.a.X();
            /** @type {Array} */
            var arrayOfArgs = [];
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                var seg = segs[i];
                /** @type {number} */
                var k = 0;
                for (;k < seg.length;k++) {
                    arrayOfArgs.push(codeSegments[i]);
                }
            }
            return arrayOfArgs;
        };
        /**
         * @param {(Array|string)} key
         * @return {?}
         */
        that.prototype.V = function(key) {
            trigger(this);
            /** @type {Array} */
            var onComplete = [];
            if (self.t(key)) {
                if (hOP(this, key)) {
                    onComplete = self.hb(onComplete, this.a.get(join(this, key)));
                }
            } else {
                key = this.a.V();
                /** @type {number} */
                var i = 0;
                for (;i < key.length;i++) {
                    onComplete = self.hb(onComplete, key[i]);
                }
            }
            return onComplete;
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {?}
         */
        that.prototype.set = function(key, val) {
            trigger(this);
            /** @type {null} */
            this.c = null;
            key = join(this, key);
            if (hOP(this, key)) {
                this.b -= this.a.get(key).length;
            }
            this.a.set(key, [val]);
            this.b += 1;
            return this;
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {?}
         */
        that.prototype.get = function(key, val) {
            var values = key ? this.V(key) : [];
            return 0 < values.length ? String(values[0]) : val;
        };
        /**
         * @param {Object} c
         * @param {string} name
         * @param {?} bind
         * @return {undefined}
         */
        self.Ef = function(c, name, bind) {
            c.remove(name);
            if (0 < bind.length) {
                /** @type {null} */
                c.c = null;
                c.a.set(join(c, name), self.ib(bind));
                c.b += bind.length;
            }
        };
        /**
         * @return {?}
         */
        that.prototype.toString = function() {
            if (this.c) {
                return this.c;
            }
            if (!this.a) {
                return "";
            }
            /** @type {Array} */
            var assigns = [];
            var items = this.a.X();
            /** @type {number} */
            var k = 0;
            for (;k < items.length;k++) {
                var values = items[k];
                var e = (0, window.encodeURIComponent)(String(values));
                values = this.V(values);
                /** @type {number} */
                var i = 0;
                for (;i < values.length;i++) {
                    var vvar = e;
                    if ("" !== values[i]) {
                        vvar += "=" + (0, window.encodeURIComponent)(String(values[i]));
                    }
                    assigns.push(vvar);
                }
            }
            return this.c = assigns.join("&");
        };
        /**
         * @param {Object} col
         * @param {string} keepData
         * @return {?}
         */
        var join = function(col, keepData) {
            /** @type {string} */
            var str = String(keepData);
            if (col.g) {
                /** @type {string} */
                str = str.toLowerCase();
            }
            return str;
        };
        /**
         * @param {Object} e
         * @param {?} index
         * @return {undefined}
         */
        var end = function(e, index) {
            if (index) {
                if (!e.g) {
                    trigger(e);
                    /** @type {null} */
                    e.c = null;
                    e.a.forEach(function(parsed, klass) {
                        var which = klass.toLowerCase();
                        if (klass != which) {
                            this.remove(klass);
                            self.Ef(this, which, parsed);
                        }
                    }, e);
                }
            }
            e.g = index;
        };
        if (!self.bf.D) {
            self.p(self.bf, ret);
        }
        /**
         * @param {number} opt_attributes
         * @return {?}
         */
        self.Ff = function(opt_attributes) {
            switch(opt_attributes) {
                case 1:
                    return "homepage";
                case 4:
                    return "Google Translator Toolkit";
                case 5:
                    return "Google Sites";
                case 6:
                    return "customersupport";
                case 9:
                    return "present";
                case 15:
                    return "hangout";
                case 18:
                    return "gmail";
                case 20:
                    return "docs";
                case 24:
                    return "docs_hangout";
                case 22:
                    return "hangout_lite";
                case 23:
                    return "aChromeExtension";
                case 25:
                    return "bigtop";
                case 27:
                    return "ChromeApp";
                case 36:
                    return "StartPage";
            }
        };
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        self.Gf = function(reqUrl) {
            self.z(this, reqUrl, "uv_qm", null);
        };
        self.p(self.Gf, self.y);
        /** @type {string} */
        self.Gf.messageId = "uv_qm";
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        self.Hf = function(reqUrl) {
            self.z(this, reqUrl, "uv_r_rdm", null);
        };
        self.p(self.Hf, self.y);
        /** @type {string} */
        self.Hf.messageId = "uv_r_rdm";
        /**
         * @param {number} body
         * @return {undefined}
         */
        self.If = function(body) {
            self.z(this, body, "uv_r_rr", null);
        };
        self.p(self.If, self.y);
        /** @type {string} */
        self.If.messageId = "uv_r_rr";
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        self.Jf = function(reqUrl) {
            self.z(this, reqUrl, "uv_r_frm", null);
        };
        self.p(self.Jf, self.y);
        /** @type {string} */
        self.Jf.messageId = "uv_r_frm";
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        self.Kf = function(reqUrl) {
            self.z(this, reqUrl, 0, null);
        };
        self.p(self.Kf, self.y);
        /**
         * @param {string} rows
         * @param {number} delta
         * @return {?}
         */
        var render = function(rows, delta) {
            /** @type {Array.<?>} */
            var args = Array.prototype.slice.call(arguments);
            var messageFormat = args.shift();
            if ("undefined" == typeof messageFormat) {
                throw Error("t");
            }
            return messageFormat.replace(/%([0\-\ \+]*)(\d+)?(\.(\d+))?([%sfdiu])/g, function(dataAndEvents, deepDataAndEvents, ignoreMethodDoesntExist, textAlt, keepData, methodname, opt_attributes, matcherFunction) {
                if ("%" == methodname) {
                    return "%";
                }
                var callback = args.shift();
                if ("undefined" == typeof callback) {
                    throw Error("u");
                }
                arguments[0] = callback;
                return el[methodname].apply(null, arguments);
            });
        };
        var el = {
            /**
             * @param {string} types
             * @param {Function} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @return {?}
             */
            s : function(types, type, expectedNumberOfNonCommentArgs) {
                return(0, window.isNaN)(expectedNumberOfNonCommentArgs) || ("" == expectedNumberOfNonCommentArgs || types.length >= Number(expectedNumberOfNonCommentArgs)) ? types : types = -1 < type.indexOf("-", 0) ? types + equals(" ", Number(expectedNumberOfNonCommentArgs) - types.length) : equals(" ", Number(expectedNumberOfNonCommentArgs) - types.length) + types;
            },
            /**
             * @param {(number|string)} n
             * @param {string} key
             * @param {number} length
             * @param {string} result
             * @param {string} c
             * @return {?}
             */
            f : function(n, key, length, result, c) {
                result = n.toString();
                if (!(0, window.isNaN)(c)) {
                    if (!("" == c)) {
                        result = (0, window.parseFloat)(n).toFixed(c);
                    }
                }
                var padding;
                /** @type {string} */
                padding = 0 > Number(n) ? "-" : 0 <= key.indexOf("+") ? "+" : 0 <= key.indexOf(" ") ? " " : "";
                if (0 <= Number(n)) {
                    /** @type {string} */
                    result = padding + result;
                }
                if ((0, window.isNaN)(length) || result.length >= Number(length)) {
                    return result;
                }
                /** @type {string} */
                result = (0, window.isNaN)(c) ? Math.abs(Number(n)).toString() : Math.abs(Number(n)).toFixed(c);
                /** @type {number} */
                n = Number(length) - result.length - padding.length;
                return result = 0 <= key.indexOf("-", 0) ? padding + result + equals(" ", n) : padding + equals(0 <= key.indexOf("0", 0) ? "0" : " ", n) + result;
            },
            /**
             * @param {string} types
             * @param {Function} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @param {(Object|string)} callback
             * @param {Error} opts
             * @param {?} f
             * @param {?} partials
             * @param {?} v
             * @return {?}
             */
            d : function(types, type, expectedNumberOfNonCommentArgs, callback, opts, f, partials, v) {
                return el.f((0, window.parseInt)(types, 10), type, expectedNumberOfNonCommentArgs, callback, 0, f, partials, v);
            }
        };
        /** @type {function (string, Function, number, (Object|string), Error, ?, ?, ?): ?} */
        el.i = el.d;
        /** @type {function (string, Function, number, (Object|string), Error, ?, ?, ?): ?} */
        el.u = el.d;
        /**
         * @param {string} j
         * @param {?} keepData
         * @return {undefined}
         */
        self.S = function(j, keepData) {
            /** @type {Array} */
            var arr = [];
            /** @type {number} */
            var i = 0;
            for (;i < arguments.length;i++) {
                if (arguments[i] instanceof Error) {
                    arr.push(arguments[i].stack);
                } else {
                    if ("object" == typeof arguments[i]) {
                        try {
                            arr.push(JSON.stringify(arguments[i]));
                        } catch (e) {
                            arr.push("_error_(" + e + ")");
                        }
                    } else {
                        arr.push(arguments[i]);
                    }
                }
            }
            if (window.console) {
                /** @type {Date} */
                i = new Date;
                /** @type {string} */
                i = render("%02d-%02d ", i.getMonth() + 1, i.getDate()) + i.toLocaleTimeString() + render(".%03d", i.getMilliseconds());
                window.console.log(i + " info: " + render.apply({}, arr));
            }
        };
        self.Nf = new self.O("pVbxBc");
        self.T = new self.O("nED8Yb");
        self.Of = new self.O("xclgJb");
        self.Pf = new self.O("Qz9Lj");
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        self.Rf = function(reqUrl) {
            self.z(this, reqUrl, "h_exts", parameter);
        };
        self.p(self.Rf, self.y);
        /** @type {Array} */
        var parameter = [2];
        /** @type {string} */
        self.Rf.messageId = "h_exts";
        /**
         * @param {string} radius
         * @return {?}
         */
        self.Sf = function(radius) {
            return self.zf(radius, "/webchat/host-js");
        };
        /**
         * @param {string} complete
         * @return {?}
         */
        self.Tf = function(complete) {
            var which = self.Sf(self.A(complete, 2));
            self.R(which, "prop", self.Ff(27));
            self.R(which, "b", 1);
            self.R(which, "hl", self.A(complete, 3));
            if (self.A(complete, 4)) {
                self.R(which, "jsmode", self.A(complete, 4));
            }
            self.R(which, "zx", self.Va());
            return which.toString();
        };
        var isEqual;
        /**
         * @param {RegExp} that
         * @return {?}
         */
        isEqual = function(that) {
            return(that = that.exec(self.jb)) ? that[1] : "";
        };
        self.Vf = function() {
            if (index) {
                return isEqual(/Firefox\/([0-9.]+)/);
            }
            if (self.x || (lastWallLeft || versionOffset)) {
                return suiteView;
            }
            if (tmp) {
                return isEqual(/Chrome\/([0-9.]+)/);
            }
            if (Xb && !(fixedPosition() || (self.w("iPad") || self.w("iPod")))) {
                return isEqual(/Version\/([0-9.]+)/);
            }
            if (program || inverse) {
                /** @type {(Array.<string>|null)} */
                var result = /Version\/(\S+).*Mobile\/(\S+)/.exec(self.jb);
                if (result) {
                    return result[1] + "." + result[2];
                }
            } else {
                if (Vb) {
                    return(result = isEqual(/Android\s+([0-9.]+)/)) ? result : isEqual(/Version\/([0-9.]+)/);
                }
            }
            return "";
        }();
        /**
         * @param {string} node
         * @param {string} m
         * @return {undefined}
         */
        self.Wf = function(node, m) {
            this.l = node || "e";
            /** @type {string} */
            this.g = null != node && "p" == node ? "e" : "p";
            /** @type {null} */
            this.c = this.h = null;
            this.j = m || "u";
            /** @type {null} */
            this.a = this.C = null;
            /** @type {Array} */
            this.b = [];
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.message = function() {
            var c = {};
            c.i = this.a;
            c.m = this.b;
            c.r = this.j;
            c.o = this.C;
            c.s = this.c;
            c.st = this.g;
            c.d = this.h;
            c.dt = this.l;
            return c;
        };
        /** @type {function (): ?} */
        self.Wf.prototype.message = self.Wf.prototype.message;
        /**
         * @return {?}
         */
        self.Wf.prototype.source = function() {
            return this.c;
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.target = function() {
            return this.h;
        };
        /**
         * @return {?}
         */
        self.Wf.prototype.content = function() {
            return this.b;
        };
        /**
         * @param {?} reason
         * @param {(Object|boolean|number|string)} request
         * @return {undefined}
         */
        var close = function(reason, request) {
            self.K.call(this, reason, request);
            /** @type {(Object|boolean|number|string)} */
            this.S = request;
        };
        self.p(close, self.K);
        /**
         * @param {number} listener
         * @return {undefined}
         */
        var Event = function(listener) {
            self.z(this, listener, "capi_md1571", null);
        };
        self.p(Event, self.y);
        /** @type {string} */
        Event.messageId = "capi_md1571";
        var opts = {
            bb : 0,
            tc : 1,
            sc : 2,
            ab : 3,
            Qa : 4
        };
        /**
         * @param {number} g
         * @return {undefined}
         */
        var dblclick = function(g) {
            self.z(this, g, 0, null);
        };
        self.p(dblclick, self.y);
        /**
         * @return {?}
         */
        var rand = function() {
            /** @type {number} */
            var a = 10;
            /** @type {string} */
            var number = startPage;
            /** @type {number} */
            var n = number.length;
            /** @type {string} */
            var res = "";
            for (;0 < a--;) {
                res += number.charAt(Math.floor(Math.random() * n));
            }
            return res;
        };
        /** @type {string} */
        var startPage = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        /**
         * @param {?} l
         * @param {(Document|string)} dataAndEvents
         * @return {undefined}
         */
        self.cg = function(l, dataAndEvents) {
            self.F.call(this);
            this.j = dataAndEvents || rand();
            /** @type {string} */
            this.g = this.a = "";
            /** @type {null} */
            this.b = this.c = null;
            this.l = l;
            this.h = new self.N(this);
            self.I(this, this.h);
            this.h.listen(window, "message", this.v);
        };
        self.p(self.cg, self.F);
        /**
         * @param {?} label
         * @param {boolean} thisObj
         * @param {?} px
         * @return {undefined}
         */
        self.cg.prototype.da = function(label, thisObj, px) {
            this.g = label;
            /** @type {boolean} */
            this.c = thisObj;
            this.b = px;
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        self.cg.prototype.v = function(type) {
            type = type.a;
            var b;
            b = type.origin;
            if (this.c && this.c != type.source || "*" != this.g && this.g != b) {
                /** @type {boolean} */
                b = false;
            } else {
                if (this.b) {
                    b = (new self.P(b)).b;
                    /** @type {boolean} */
                    b = self.Ia(b, this.b) ? b.length == this.b.length || ("." == this.b[0] || "." == b[b.length - 1 - this.b.length]) : false;
                } else {
                    /** @type {boolean} */
                    b = true;
                }
            }
            if (b && ((b = type.data) && (self.t(b) && "[" == b[0]))) {
                var ready;
                try {
                    if (ready = JSON.parse(b), !self.Ca(ready)) {
                        return;
                    }
                } catch (d) {
                    return;
                }
                if (0 != ready.length && ("capi_md1571" == ready[0] && (ready = new Event(ready), self.A(ready, 1) == this.j))) {
                    b = self.A(ready, 3);
                    if (!this.a) {
                        this.a = b;
                    } else {
                        if (this.a != b) {
                            return;
                        }
                    }
                    if (!("*" != this.g && this.c)) {
                        this.da(type.origin, type.source);
                    }
                    if (1 == self.A(ready, 2)) {
                        this.send(2);
                    } else {
                        this.l(ready, type);
                    }
                }
            }
        };
        /**
         * @param {number} error
         * @param {string} ready
         * @param {?} callback
         * @return {undefined}
         */
        self.cg.prototype.send = function(error, ready, callback) {
            if (this.a && this.c) {
                ready = ready || new Event;
                self.B(ready, 3, this.j);
                self.B(ready, 1, this.a);
                self.B(ready, 2, error);
                /** @type {string} */
                var that = JSON.stringify(self.D(ready));
                var target = this.c;
                var otherWindow = this.g;
                if (self.x) {
                    self.Be(function() {
                        target.postMessage(that, otherWindow);
                    });
                } else {
                    if (callback) {
                        target.postMessage(that, otherWindow, callback);
                    } else {
                        target.postMessage(that, otherWindow);
                    }
                }
            }
        };
        /**
         * @param {(Object|string)} a
         * @param {Object} target
         * @param {?} v
         * @param {?} data
         * @param {?} px
         * @return {undefined}
         */
        var copyPixel = function(a, target, v, data, px) {
            this.c = self.qd();
            this.a = new self.cg((0, self.r)(this.h, this), data);
            this.a.da(a, target, px);
            this.a.a = v;
            this.b = new self.ze(50);
            self.Ae(this.b);
            self.ge(this.b, "tick", this.g, false, this);
            this.g();
            a = this.c.a;
            target = this.j;
            target = contains(target, target, this);
            /** @type {boolean} */
            target.c = true;
            bindCallbacks(a, target);
        };
        /**
         * @return {undefined}
         */
        copyPixel.prototype.j = function() {
            self.H(this.a);
            self.oe(this.b, "tick", this.g, false, this);
            self.H(this.b);
        };
        /**
         * @return {undefined}
         */
        copyPixel.prototype.g = function() {
            this.a.send(1);
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        copyPixel.prototype.h = function(types, type) {
            if (2 == self.A(types, 2)) {
                this.c.b(type.origin);
            }
        };
        /**
         * @param {(Function|string)} b
         * @param {Text} complete
         * @param {Object} a
         * @return {undefined}
         */
        var $ = function(b, complete, a) {
            self.F.call(this);
            /** @type {(Function|string)} */
            this.b = b;
            b = self.A(complete, 3);
            complete = self.A(complete, 1);
            this.a = new self.cg((0, self.r)(this.c, this), complete);
            /** @type {(Function|string)} */
            this.a.a = b;
            if (a) {
                this.a.da(a.origin, a.source);
            }
            self.I(this, this.a);
        };
        self.p($, self.F);
        /**
         * @param {number} type
         * @return {undefined}
         */
        $.prototype.send = function(type) {
            var e = new Event;
            self.E(e, 4, type);
            this.a.send(opts.bb, e);
        };
        /**
         * @param {?} o
         * @param {boolean} a
         * @return {undefined}
         */
        $.prototype.connect = function(o, a) {
            this.a.da(o, a);
            this.a.send(opts.ab);
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        $.prototype.c = function(type) {
            if (self.A(type, 2) == opts.bb) {
                type = self.C(type, dblclick, 4);
                this.b(type);
            }
        };
        /** @type {function (): undefined} */
        $.prototype.g = self.Da;
        /**
         * @param {(Function|string)} h
         * @param {string} ready
         * @param {?} options
         * @return {undefined}
         */
        var controller = function(h, ready, options) {
            self.F.call(this);
            /** @type {(Function|string)} */
            this.h = h;
            /** @type {null} */
            this.b = this.a = this.c = null;
            if (options) {
                this.b = options.ports[0];
                configure(this);
            } else {
                this.a = new self.cg(self.Da, self.A(ready, 1));
                h = self.A(ready, 3);
                /** @type {(Function|string)} */
                this.a.a = h;
            }
        };
        self.p(controller, self.F);
        /**
         * @return {undefined}
         */
        controller.prototype.B = function() {
            controller.D.B.call(this);
            if (this.b) {
                this.b.close();
            }
            self.H(this.a);
        };
        /**
         * @return {undefined}
         */
        controller.prototype.g = function() {
            this.c = new self.N(this);
            self.I(this, this.c);
            configure(this);
        };
        /**
         * @param {string} ready
         * @return {undefined}
         */
        controller.prototype.send = function(ready) {
            this.b.postMessage(self.D(ready));
        };
        /**
         * @param {?} obj
         * @param {boolean} b
         * @return {undefined}
         */
        controller.prototype.connect = function(obj, b) {
            var channel = new window.MessageChannel;
            this.b = channel.port1;
            configure(this);
            var str = this.a;
            /** @type {null} */
            this.a = null;
            channel = channel.port2;
            str.da(obj, b);
            str.send(opts.Qa, void 0, [channel]);
            self.H(str);
        };
        /**
         * @param {Object} options
         * @return {undefined}
         */
        var configure = function(options) {
            if (options.b) {
                if (options.c) {
                    options.c.listen(options.b, "message", options.j);
                    options.b.start();
                }
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        controller.prototype.j = function(types) {
            types = types.a;
            var ready;
            try {
                ready = new dblclick(types.data);
            } catch (c) {
                return;
            }
            this.h(ready);
        };
        /**
         * @param {string} event
         * @param {string} type
         * @param {string} deepDataAndEvents
         * @return {?}
         */
        var simulate = function(event, type, deepDataAndEvents) {
            /** @type {boolean} */
            var data = false;
            if (self.m(self.A(type, 2))) {
                if (self.A(type, 2) == opts.Qa) {
                    /** @type {boolean} */
                    data = true;
                }
            } else {
                data = tmp;
            }
            return data ? new controller(event, type, deepDataAndEvents) : new $(event, type, deepDataAndEvents);
        };
        var fetch;
        /**
         * @return {undefined}
         */
        self.ig = function() {
            self.L.call(this);
            /** @type {null} */
            this.h = null;
            /** @type {boolean} */
            this.l = false;
            /** @type {Array} */
            this.j = [];
            /** @type {Array} */
            this.g = [];
            self.G(this, this.w, this);
            /** @type {null} */
            this.b = null;
        };
        self.p(self.ig, self.L);
        /**
         * @return {undefined}
         */
        self.ig.prototype.w = function() {
            for (;this.g.length;) {
                this.g.shift()();
            }
        };
        /**
         * @param {Object} options
         * @return {undefined}
         */
        self.kg = function(options) {
            options.w();
            /** @type {Array} */
            options.j = [];
            options.b = new self.Dd;
            options.g.push((0, self.r)(options.b.cancel, options.b));
            /** @type {boolean} */
            options.l = false;
            self.Kd(options.b.h(), function(key) {
                if (onSuccess(this.c) || this.G) {
                    logger(this);
                }
                this.g.push(self.xa(self.H, key));
            }, options);
        };
        self.g = self.ig.prototype;
        /**
         * @param {string} type
         * @return {undefined}
         */
        self.g.sendMessage = function(type) {
            this.xa(self.D(type));
        };
        /**
         * @param {Function} callback
         * @return {undefined}
         */
        self.g.xa = function(callback) {
            if (this.b) {
                var e = new dblclick;
                self.B(e, 1, callback);
                self.Kd(this.b.h(), function(res) {
                    res.send(e);
                });
            }
        };
        /**
         * @param {?} h
         * @return {undefined}
         */
        self.g.$a = function(h) {
            this.h = h;
            self.G(this, function() {
                /** @type {null} */
                this.h = null;
            }, this);
            logger(this);
        };
        /**
         * @return {?}
         */
        self.g.J = function() {
            return this;
        };
        /**
         * @param {?} o
         * @param {boolean} thisObj
         * @param {?} px
         * @return {undefined}
         */
        self.g.da = function(o, thisObj, px) {
            this.a.da(o, thisObj, px);
        };
        /**
         * @param {Object} done
         * @param {Object} config
         * @param {string} recurring
         * @param {Array} key
         * @return {undefined}
         */
        self.mg = function(done, config, recurring, key) {
            self.kg(done);
            fetch(done, config, recurring, key).then(function(args) {
                var ready = new Event;
                self.B(ready, 1, args.pc);
                self.B(ready, 3, recurring);
                ready = simulate((0, self.r)(this.v, this), ready);
                ready.connect(args.origin, config);
                this.b.b(ready);
            }, void 0, done);
        };
        /**
         * @param {Object} c
         * @param {string} a
         * @param {string} recurring
         * @param {Array} keepData
         * @return {?}
         */
        fetch = function(c, a, recurring, keepData) {
            var pc = rand();
            a = (new copyPixel("*", a, recurring, pc, keepData)).c.a;
            c.g.push((0, self.r)(a.cancel, a));
            return a.then(function(origin) {
                return{
                    pc : pc,
                    origin : origin
                };
            });
        };
        /**
         * @param {string} proto
         * @param {string} deepDataAndEvents
         * @return {undefined}
         */
        self.ig.prototype.H = function(proto, deepDataAndEvents) {
            if (self.A(proto, 2) == opts.ab || self.A(proto, 2) == opts.Qa) {
                self.H(this.a);
                /** @type {null} */
                this.a = null;
                this.b.b(simulate((0, self.r)(this.v, this), proto, deepDataAndEvents));
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        self.ig.prototype.v = function(types) {
            this.j.push(types);
            this.A();
        };
        /**
         * @return {undefined}
         */
        self.ig.prototype.A = function() {
            for (;any(this);) {
            }
        };
        /**
         * @param {string} orig
         * @return {?}
         */
        var any = function(orig) {
            if (orig.K || (!orig.l || 0 == orig.j.length)) {
                return false;
            }
            var restoreScript = self.A(orig.j.shift(), 1);
            if (orig.h) {
                orig.h.xa(restoreScript);
            }
            self.M(orig, new close(restoreScript[0], restoreScript));
            return true;
        };
        /**
         * @param {Node} scope
         * @return {undefined}
         */
        var logger = function(scope) {
            if (!scope.l) {
                /** @type {boolean} */
                scope.l = true;
                if (scope.b) {
                    self.Kd(scope.b.h(), function(rgba) {
                        rgba.g();
                        self.Be(this.A, 0, this);
                    }, scope);
                }
            }
        };
        /**
         * @param {?} type
         * @param {string} callback
         * @param {?} opt_attributes
         * @param {boolean} a
         * @return {?}
         */
        self.ig.prototype.listen = function(type, callback, opt_attributes, a) {
            logger(this);
            return self.ig.D.listen.call(this, type, callback, opt_attributes, a);
        };
        /**
         * @param {?} type
         * @param {Object} opt_fromIndex
         * @param {(Range|TextRange)} payload
         * @param {boolean} a
         * @return {?}
         */
        self.ig.prototype.Ma = function(type, opt_fromIndex, payload, a) {
            logger(this);
            return self.ig.D.Ma.call(this, type, opt_fromIndex, payload, a);
        };
        /**
         * @param {string} types
         * @return {?}
         */
        self.ig.prototype.I = function(types) {
            logger(this);
            return self.ig.D.I.call(this, types);
        };
        /**
         * @param {?} b
         * @return {undefined}
         */
        var getColorIndex = function(b) {
            this.b = b;
            /** @type {string} */
            this.g = "empty";
            /** @type {null} */
            this.a = null;
            this.c = {};
        };
        /**
         * @param {?} recurring
         * @return {?}
         */
        getColorIndex.prototype.setVersion = function(recurring) {
            this.g = recurring;
            return this;
        };
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        getColorIndex.prototype.Oa = function(deepDataAndEvents) {
            this.a = deepDataAndEvents;
            return this;
        };
        /**
         * @param {string} value
         * @return {?}
         */
        getColorIndex.prototype.Za = function(value) {
            this.c = self.qb(value);
            return this;
        };
        /**
         * @return {undefined}
         */
        var y = function() {
        };
        /**
         * @return {undefined}
         */
        var model = function() {
        };
        self.p(model, y);
        /**
         * @return {undefined}
         */
        model.prototype.clear = function() {
            var echo = encode(this.ia(true));
            var parent = this;
            (0, self.u)(echo, function(optgroup) {
                parent.remove(optgroup);
            });
        };
        /**
         * @param {?} a
         * @return {undefined}
         */
        var args = function(a) {
            this.a = a;
        };
        self.p(args, model);
        self.g = args.prototype;
        /**
         * @return {?}
         */
        self.g.isAvailable = function() {
            if (!this.a) {
                return false;
            }
            try {
                return this.a.setItem("__sak", "1"), this.a.removeItem("__sak"), true;
            } catch (a) {
                return false;
            }
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @return {undefined}
         */
        self.g.set = function(key, val) {
            try {
                this.a.setItem(key, val);
            } catch (c) {
                if (0 == this.a.length) {
                    throw "Storage mechanism: Storage disabled";
                }
                throw "Storage mechanism: Quota exceeded";
            }
        };
        /**
         * @param {string} key
         * @return {?}
         */
        self.g.get = function(key) {
            key = this.a.getItem(key);
            if (!self.t(key) && null !== key) {
                throw "Storage mechanism: Invalid value was encountered";
            }
            return key;
        };
        /**
         * @param {string} name
         * @return {undefined}
         */
        self.g.remove = function(name) {
            this.a.removeItem(name);
        };
        /**
         * @param {boolean} recurring
         * @return {?}
         */
        self.g.ia = function(recurring) {
            /** @type {number} */
            var i = 0;
            var storage = this.a;
            var stream = new Stream;
            /**
             * @return {?}
             */
            stream.next = function() {
                if (i >= storage.length) {
                    throw delimiterCode;
                }
                var key = storage.key(i++);
                if (recurring) {
                    return key;
                }
                key = storage.getItem(key);
                if (!self.t(key)) {
                    throw "Storage mechanism: Invalid value was encountered";
                }
                return key;
            };
            return stream;
        };
        /**
         * @return {undefined}
         */
        self.g.clear = function() {
            this.a.clear();
        };
        /**
         * @param {number} index
         * @return {?}
         */
        self.g.key = function(index) {
            return this.a.key(index);
        };
        /**
         * @return {undefined}
         */
        var popover = function() {
            /** @type {null} */
            var a = null;
            try {
                a = window.localStorage || null;
            } catch (b) {
            }
            this.a = a;
        };
        self.p(popover, args);
        /**
         * @param {?} compiler
         * @param {?} headGeometry
         * @return {undefined}
         */
        var createObjects = function(compiler, headGeometry) {
            this.c = compiler;
            /** @type {Array} */
            this.a = [];
            /** @type {null} */
            this.b = null;
            if (headGeometry) {
                this.b = new popover;
            }
            if (null != this.b && this.b.isAvailable()) {
                var source = this.b.get("__webmonitoring_RateThrottler_history_hourlyRate");
                if (null != source) {
                    try {
                        this.a = trim(source) || [];
                    } catch (d) {
                    }
                }
            }
            /** @type {boolean} */
            source = false;
            if (!self.Ca(this.a)) {
                /** @type {Array} */
                this.a = [];
                /** @type {boolean} */
                source = true;
            }
            for (;this.a.length > this.c;) {
                this.a.shift();
                /** @type {boolean} */
                source = true;
            }
            if (source) {
                getter(this);
            }
        };
        /**
         * @param {Node} obj
         * @return {undefined}
         */
        var getter = function(obj) {
            if (null != obj.b && obj.b.isAvailable()) {
                try {
                    obj.b.set("__webmonitoring_RateThrottler_history_hourlyRate", self.cc(obj.a));
                } catch (b) {
                }
            }
        };
        /**
         * @param {?} e
         * @return {undefined}
         */
        var fn = function(e) {
            self.F.call(this);
            this.a = e;
            /** @type {boolean} */
            this.h = false;
        };
        self.p(fn, self.F);
        /**
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        fn.prototype.setEnabled = function(dataAndEvents) {
            /** @type {boolean} */
            this.h = dataAndEvents;
        };
        /**
         * @param {?} i
         * @return {undefined}
         */
        var data = function(i) {
            fn.call(this, i);
            /** @type {number} */
            this.w = 100;
            this.v = {};
            /** @type {null} */
            this.R = null;
            /** @type {string} */
            this.c = "";
            /** @type {boolean} */
            this.G = true;
            /** @type {number} */
            this.j = 10;
            /** @type {null} */
            this.b = null;
            /** @type {boolean} */
            this.g = false;
        };
        self.p(data, fn);
        /**
         * @param {?} recurring
         * @return {?}
         */
        data.prototype.pb = function(recurring) {
            this.G = recurring;
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setUseLocalStorage = data.prototype.pb;
        /**
         * @param {?} a
         * @return {?}
         */
        data.prototype.oc = function(a) {
            /** @type {number} */
            this.j = Math.min(a, 100);
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setMaxErrorsPerHour = data.prototype.oc;
        /**
         * @param {string} opt_attributes
         * @return {?}
         */
        data.prototype.nb = function(opt_attributes) {
            /** @type {string} */
            this.w = opt_attributes;
            return this;
        };
        /** @type {function (string): ?} */
        data.prototype.setGlobalSampling = data.prototype.nb;
        /**
         * @param {?} value
         * @param {string} obj
         * @return {?}
         */
        data.prototype.eb = function(value, obj) {
            /** @type {string} */
            this.v[value] = obj;
            return this;
        };
        /** @type {function (?, string): ?} */
        data.prototype.addPerErrorMessageSampling = data.prototype.eb;
        /**
         * @param {string} cur
         * @return {?}
         */
        data.prototype.mb = function(cur) {
            /** @type {string} */
            this.R = cur;
            return this;
        };
        /** @type {function (string): ?} */
        data.prototype.setCallback = data.prototype.mb;
        /**
         * @param {?} compiler
         * @return {?}
         */
        data.prototype.nc = function(compiler) {
            this.c = compiler;
            return this;
        };
        /** @type {function (?): ?} */
        data.prototype.setClientId = data.prototype.nc;
        /**
         * @return {?}
         */
        data.prototype.enable = function() {
            if (this.h) {
                return false;
            }
            this.l = window.onerror;
            window.onerror = this.A.bind(this);
            this.setEnabled(true);
            return true;
        };
        /** @type {function (): ?} */
        data.prototype.enable = data.prototype.enable;
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {(Object|string)} start
         * @param {Object} error
         * @return {undefined}
         */
        data.prototype.jb = function(types, type, expectedNumberOfNonCommentArgs, start, error) {
            var options = self.yf(window.location.href);
            types = null != error && null != error.message ? error.message : types;
            var data;
            if (data = (!self.za(this.R) || this.R(options.a, types, type, expectedNumberOfNonCommentArgs, start, error)) && this.g) {
                /** @type {Array.<string>} */
                data = Object.keys(this.v);
                /** @type {number} */
                var name = 100;
                /** @type {number} */
                var s = 0;
                for (;s < data.length;++s) {
                    /** @type {string} */
                    var value = data[s];
                    if ((new RegExp(value)).test(types)) {
                        name = this.v[value];
                    }
                }
                /** @type {boolean} */
                data = (100 * Math.random()).toFixed(6) >= this.w * name / 100 ? false : true;
            }
            if (data) {
                if (0 <= this.j) {
                    if (null === this.b) {
                        this.b = new createObjects(this.j, this.G);
                    }
                }
                if (null === this.b) {
                    /** @type {number} */
                    data = 0;
                } else {
                    a: {
                        data = this.b;
                        name = (0, self.sa)();
                        if (data.a.length == data.c) {
                            if (data.a[0] < name - 36E5) {
                                data.a.shift();
                            } else {
                                /** @type {boolean} */
                                data = false;
                                break a;
                            }
                        }
                        data.a.push(name);
                        getter(data);
                        /** @type {boolean} */
                        data = true;
                    }
                    /** @type {boolean} */
                    data = !data;
                }
                /** @type {boolean} */
                data = !data;
            }
            if (data) {
                if (this.g) {
                    data = self.qb(this.a.c);
                    data.url = options.a;
                    /** @type {string} */
                    data.type = "JavascriptError";
                    /** @type {string} */
                    data.error_message = types;
                    /** @type {string} */
                    name = "Other";
                    if (versionOffset) {
                        /** @type {string} */
                        name = "Opera";
                    } else {
                        if (self.x) {
                            /** @type {string} */
                            name = "Internet Explorer";
                        } else {
                            if (index) {
                                /** @type {string} */
                                name = "Firefox";
                            } else {
                                if (tmp) {
                                    /** @type {string} */
                                    name = "Chrome";
                                } else {
                                    if (Xb) {
                                        /** @type {string} */
                                        name = "Safari";
                                    }
                                }
                            }
                        }
                    }
                    /** @type {string} */
                    data.browser = name;
                    data.browser_version = self.Vf;
                    /** @type {string} */
                    data.os = self.w("CrOS") ? "Chrome OS" : self.w("Linux") ? "Linux" : self.wb() ? "Windows" : self.w("Android") ? "Android" : fixedPosition() ? "iPhone" : self.w("iPad") ? "iPad" : self.w("iPod") ? "iPod" : self.vb() ? "Mac" : "Unknown";
                    name = self.jb;
                    /** @type {string} */
                    s = "";
                    if (self.wb()) {
                        /** @type {RegExp} */
                        s = /Windows (?:NT|Phone) ([0-9.]+)/;
                        /** @type {string} */
                        s = (name = s.exec(name)) ? name[1] : "0.0";
                    } else {
                        if (fixedPosition() || (self.w("iPad") || self.w("iPod"))) {
                            /** @type {RegExp} */
                            s = /(?:iPhone|iPod|iPad|CPU)\s+OS\s+(\S+)/;
                            /** @type {(null|string)} */
                            s = (name = s.exec(name)) && name[1].replace(/_/g, ".");
                        } else {
                            if (self.vb()) {
                                /** @type {RegExp} */
                                s = /Mac OS X ([0-9_.]+)/;
                                /** @type {string} */
                                s = (name = s.exec(name)) ? name[1].replace(/_/g, ".") : "10";
                            } else {
                                if (self.w("Android")) {
                                    /** @type {RegExp} */
                                    s = /Android\s+([^\);]+)(\)|;)/;
                                    /** @type {(null|string)} */
                                    s = (name = s.exec(name)) && name[1];
                                } else {
                                    if (self.w("CrOS")) {
                                        /** @type {RegExp} */
                                        s = /(?:CrOS\s+(?:i686|x86_64)\s+([0-9.]+))/;
                                        /** @type {(null|string)} */
                                        s = (name = s.exec(name)) && name[1];
                                    }
                                }
                            }
                        }
                    }
                    /** @type {string} */
                    data.os_version = s || "";
                    if (null != this.a.a) {
                        data.channel = this.a.a;
                    }
                    if (!self.Ja(this.c)) {
                        data.guid = this.c;
                    }
                    if (null != type) {
                        /** @type {Function} */
                        data.src = type;
                    }
                    if (null != expectedNumberOfNonCommentArgs) {
                        /** @type {number} */
                        data.line = expectedNumberOfNonCommentArgs;
                    }
                    if (null != start) {
                        /** @type {(Object|string)} */
                        data.column = start;
                    }
                    data.prod = this.a.b;
                    data.ver = this.a.g;
                    type = object(data);
                    error = null != error ? self.Ua(error.stack) : "";
                    if (!self.Ja(types)) {
                        expectedNumberOfNonCommentArgs = error.split("\n");
                        if (-1 < expectedNumberOfNonCommentArgs[0].indexOf(types)) {
                            expectedNumberOfNonCommentArgs.splice(0, 1);
                            error = expectedNumberOfNonCommentArgs.join("\n");
                        }
                    }
                    makeRequest(this, "https://clients2.google.com/cr/staging_report", type, error);
                    options = {
                        product : this.a.b,
                        url : options.a,
                        js_errors_count : "1"
                    };
                    types = self.Ua(this.a.a);
                    if (!self.Ja(types)) {
                        /** @type {string} */
                        options.version = types;
                    }
                    makeRequest(this, "https://clients2.google.com/cr/staging_perf", object(options));
                }
            }
        };
        /** @type {function (string, Function, number, (Object|string), Object): undefined} */
        data.prototype.reportError = data.prototype.jb;
        /**
         * @param {string} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {number} config
         * @param {(Object|string)} arr
         * @param {Error} err
         * @return {undefined}
         */
        data.prototype.A = function(type, expectedNumberOfNonCommentArgs, config, arr, err) {
            this.jb(type, expectedNumberOfNonCommentArgs, config, arr, err);
            if (null != this.l) {
                if (self.za(this.l)) {
                    this.l(type, expectedNumberOfNonCommentArgs, config, arr, err);
                }
            }
        };
        /**
         * @param {Object} map
         * @return {?}
         */
        var object = function(map) {
            /** @type {string} */
            var result = "";
            var letter;
            for (letter in map) {
                result += self.Ja(result) ? "?" : "&";
                result += letter + "=" + (0, window.encodeURIComponent)(map[letter]);
            }
            return result;
        };
        /**
         * @param {Object} options
         * @param {string} i
         * @param {Function} path
         * @param {string} type
         * @return {undefined}
         */
        var makeRequest = function(options, i, path, type) {
            type = type || "";
            /** @type {({Content-Type: string}|{})} */
            var restoreScript = self.Ja(type) ? {} : {
                "Content-Type" : "text/plain"
            };
            self.Je(i + path, options.I.bind(options, i), "POST", type, restoreScript, 6E4, true);
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        data.prototype.I = function(types, type) {
            self.Te(type.target);
        };
        /**
         * @param {?} val
         * @return {undefined}
         */
        var b = function(val) {
            fn.call(this, val);
            /** @type {null} */
            this.R = null;
            /** @type {number} */
            this.b = 0;
        };
        self.p(b, fn);
        /**
         * @param {string} cur
         * @return {?}
         */
        b.prototype.mb = function(cur) {
            /** @type {string} */
            this.R = cur;
            return this;
        };
        /** @type {Array} */
        var people = [{
            age : 6E4,
            name : "1m"
        }, {
            age : 15E5,
            name : "25m"
        }, {
            age : 36E5,
            name : "1h"
        }, {
            age : 36E6,
            name : "10h"
        }];
        /**
         * @return {?}
         */
        b.prototype.enable = function() {
            if (this.h) {
                return false;
            }
            if (window.performance && window.performance.memory) {
                var cycle = new self.ze(6E4);
                self.I(this, cycle);
                var fix = new self.N(this);
                self.I(this, fix);
                fix.listen(cycle, "tick", this.c);
                self.Ae(cycle);
                this.setEnabled(true);
                return true;
            }
            return false;
        };
        /**
         * @return {undefined}
         */
        b.prototype.c = function() {
            this.b += 6E4;
            /** @type {string} */
            var data = "";
            /** @type {number} */
            var i = 0;
            for (;i < people.length;++i) {
                if (this.b == people[i].age) {
                    data = people[i].name;
                    break;
                }
            }
            if ("" != data) {
                /** @type {string} */
                i = rhtml.test(self.jb) ? "" : "32_bit_";
                var res = window.performance.memory;
                if (res.totalJSHeapSize && res.usedJSHeapSize) {
                    /** @type {Array} */
                    data = [{
                        name : "total_" + i + "js_heap_size_mb_" + data + "_raw",
                        displayName : "Total JS heap (MB)",
                        value : Math.round(res.totalJSHeapSize / 1048576)
                    }, {
                        name : "used_" + i + "js_heap_size_mb_" + data + "_raw",
                        displayName : "Used JS heap (MB)",
                        value : Math.round(res.usedJSHeapSize / 1048576)
                    }];
                    i = self.yf(window.location.href).a;
                    res = "?product=" + (0, window.encodeURIComponent)(this.a.b) + "&url=" + (0, window.encodeURIComponent)(i);
                    var j = self.Ua(this.a.a);
                    if (!self.Ja(j)) {
                        res += "&version=" + (0, window.encodeURIComponent)(j);
                    }
                    /** @type {number} */
                    j = 0;
                    for (;j < data.length;++j) {
                        res += "&" + (0, window.encodeURIComponent)(data[j].name) + "=" + data[j].value;
                        if (self.za(this.R)) {
                            this.R(i, data[j].displayName, data[j].value);
                        }
                    }
                    self.Je("https://clients2.google.com/cr/staging_perf" + res, opacity, "POST", "", null, 6E4, true);
                }
            }
        };
        /**
         * @param {Event} d
         * @return {undefined}
         */
        var opacity = function(d) {
            self.Te(d.target);
        };
        /** @type {RegExp} */
        var rhtml = /(x86_64|Win64)/;
        /**
         * @param {?} thisValue
         * @return {undefined}
         */
        var value = function(thisValue) {
            self.F.call(this);
            this.b = new getColorIndex(thisValue);
            this.a = {};
        };
        self.p(value, self.F);
        self.wa("webmonitoring.Monitoring", value);
        /**
         * @param {Object} recurring
         * @return {?}
         */
        value.prototype.setVersion = function(recurring) {
            this.b.setVersion(recurring);
            return this;
        };
        /** @type {function (Object): ?} */
        value.prototype.setVersion = value.prototype.setVersion;
        /**
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        value.prototype.Oa = function(deepDataAndEvents) {
            this.b.Oa(deepDataAndEvents);
            return this;
        };
        /** @type {function (?): ?} */
        value.prototype.setChannel = value.prototype.Oa;
        /**
         * @param {string} value
         * @return {?}
         */
        value.prototype.Za = function(value) {
            this.b.Za(value);
            return this;
        };
        /** @type {function (string): ?} */
        value.prototype.setContext = value.prototype.Za;
        /**
         * @return {?}
         */
        value.prototype.Ka = function() {
            if (!(null != this.a[2])) {
                this.a[2] = new data(this.b);
            }
            return this.a[2];
        };
        /** @type {function (): ?} */
        value.prototype.createJsErrorsReporter = value.prototype.Ka;
        /**
         * @param {?} i
         * @return {undefined}
         */
        var group = function(i) {
            value.call(this, i);
        };
        self.p(group, value);
        /**
         * @return {?}
         */
        group.prototype.Ka = function() {
            var cleanCache = group.D.Ka.call(this);
            /** @type {boolean} */
            cleanCache.g = true;
            return cleanCache;
        };
        /**
         * @return {undefined}
         */
        self.Eg = function() {
            var that = new group("Google_Hangouts_Chrome");
            that.Oa(window.chrome.runtime.id);
            that.setVersion(window.chrome.runtime.getManifest().version);
            if (!(null != that.a[1])) {
                that.a[1] = new b(that.b);
            }
            that.a[1].enable();
            that.Ka().pb(false).nb(100).eb("Script error.", 0).enable();
        };
        /**
         * @param {?} compiler
         * @param {?} texturePath
         * @return {undefined}
         */
        var Model = function(compiler, texturePath) {
            this.c = compiler;
            this.b = texturePath;
            if (!this.constructor.fb) {
                this.constructor.fb = {};
            }
            this.constructor.fb[this.toString()] = this;
        };
        /**
         * @return {?}
         */
        Model.prototype.toString = function() {
            if (!this.a) {
                /** @type {string} */
                this.a = this.c.a + ":" + this.b;
            }
            return this.a;
        };
        /**
         * @param {?} options
         * @param {?} opts
         * @return {undefined}
         */
        var Sprite = function(options, opts) {
            Model.call(this, options, opts);
        };
        self.p(Sprite, Model);
        /**
         * @param {?} array
         * @return {undefined}
         */
        var Buffer = function(array) {
            this.a = array;
        };
        new Buffer("lib");
        var dataAttr;
        var inject;
        var delay;
        var buffer;
        /**
         * @param {Object} data
         * @return {undefined}
         */
        self.Jg = function(data) {
            self.F.call(this);
            this.g = {};
            this.j = {};
            this.l = {};
            this.a = {};
            this.c = {};
            this.I = {};
            this.v = data ? data.J() : new self.L;
            /** @type {boolean} */
            this.P = !data;
            /** @type {null} */
            this.b = null;
            if (data) {
                /** @type {Object} */
                this.b = data;
                this.l = data.l;
                this.a = data.a;
                this.j = data.j;
                this.c = data.c;
            } else {
                (0, self.sa)();
            }
            data = dataAttr(this);
            if (this != data) {
                if (data.h) {
                    data.h.push(this);
                } else {
                    /** @type {Array} */
                    data.h = [this];
                }
            }
        };
        self.p(self.Jg, self.F);
        /** @type {boolean} */
        self.Kg = 0.05 > Math.random();
        /**
         * @param {Object} data
         * @return {?}
         */
        dataAttr = function(data) {
            for (;data.b;) {
                data = data.b;
            }
            return data;
        };
        /**
         * @param {string} key
         * @return {?}
         */
        self.Jg.prototype.get = function(key) {
            var req = self.Lg(this, key);
            if (null == req) {
                throw new Client(key);
            }
            return req;
        };
        /**
         * @param {string} ready
         * @param {string} k
         * @return {?}
         */
        self.Lg = function(ready, k) {
            /** @type {string} */
            var b = ready;
            for (;b;b = b.b) {
                if (b.K) {
                    throw Error("v");
                }
                if (b.g[k]) {
                    return b.g[k][0];
                }
                if (b.I[k]) {
                    break;
                }
            }
            if (b = ready.l[k]) {
                b = b(ready);
                if (null == b) {
                    throw Error("w`" + k);
                }
                self.Ng(ready, k, b);
                return b;
            }
            return null;
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {?}
         */
        self.Jg.prototype.G = function(types, type) {
            var ready = self.Lg(this, types);
            if (null == ready) {
                if (this.c[types]) {
                    var e = this.c[types].h();
                    self.Kd(e, (0, self.r)(this.G, this, types, type));
                    return e;
                }
                throw new fail(types, type, "Module loaded but service or factory not registered with app contexts.");
            }
            return ready.Ta ? (e = new self.Dd, self.Ld(e, ready.Ta()), e.b(ready), self.Kd(e, (0, self.r)(this.w, this, types)), e) : this.w(types);
        };
        /**
         * @param {string} str
         * @return {?}
         */
        self.Jg.prototype.w = function(str) {
            if (this.c[str]) {
                delete this.c[str];
            }
            return this.get(str);
        };
        /**
         * @param {string} a
         * @param {string} mod
         * @param {string} d
         * @return {?}
         */
        self.Jg.prototype.L = function(a, mod, d) {
            return d instanceof self.Ed ? d : new stack(a, mod, d);
        };
        /**
         * @param {Object} data
         * @param {string} name
         * @param {Object} str
         * @param {Object} result
         * @return {?}
         */
        self.Ng = function(data, name, str, result) {
            if (data.K) {
                if (!result) {
                    self.H(str);
                }
            } else {
                /** @type {Array} */
                data.g[name] = [str, !result];
                result = inject(data, data, name);
                /** @type {number} */
                var i = 0;
                for (;i < result.length;i++) {
                    result[i].b(null);
                }
                delete data.j[name];
                return str;
            }
        };
        /**
         * @param {Object} doc
         * @param {string} r
         * @param {string} name
         * @return {?}
         */
        inject = function(doc, r, name) {
            /** @type {Array} */
            var _results = [];
            var header = doc.a[name];
            if (header) {
                self.Za(header, function(a) {
                    var b;
                    a: {
                        b = a.Sa;
                        for (;b;) {
                            if (b == r) {
                                /** @type {boolean} */
                                b = true;
                                break a;
                            }
                            b = b.b;
                        }
                        /** @type {boolean} */
                        b = false;
                    }
                    if (b) {
                        _results.push(a.d);
                        self.gb(header, a);
                    }
                });
                if (0 == header.length) {
                    delete doc.a[name];
                }
            }
            return _results;
        };
        /**
         * @param {Node} object
         * @param {?} type
         * @return {undefined}
         */
        delay = function(object, type) {
            if (object.a) {
                send(object.a, function(haystack, off, buf) {
                    self.Za(haystack, function(panel) {
                        if (panel.Sa == type) {
                            self.gb(haystack, panel);
                        }
                    });
                    if (0 == haystack.length) {
                        delete buf[off];
                    }
                });
            }
        };
        /**
         * @param {?} proto
         * @param {?} d
         * @return {undefined}
         */
        self.Jg.prototype.H = function(proto, d) {
            var which = this.a && this.a[proto];
            if (which) {
                /** @type {number} */
                var i = 0;
                for (;i < which.length;++i) {
                    if (which[i].Sa == this && which[i].d == d) {
                        self.fb(which, i);
                        break;
                    }
                }
                if (0 == which.length) {
                    delete this.a[proto];
                }
            }
        };
        /**
         * @return {undefined}
         */
        self.Jg.prototype.B = function() {
            if (dataAttr(this) == this) {
                var h = this.h;
                if (h) {
                    for (;h.length;) {
                        h[0].Z();
                    }
                }
            } else {
                h = dataAttr(this).h;
                /** @type {number} */
                var i = 0;
                for (;i < h.length;i++) {
                    if (h[i] == this) {
                        h.splice(i, 1);
                        break;
                    }
                }
            }
            var hi;
            for (hi in this.g) {
                h = this.g[hi];
                if (h[1]) {
                    if (h[0].Z) {
                        h[0].Z();
                    }
                }
            }
            /** @type {null} */
            this.g = null;
            if (this.P) {
                this.v.Z();
            }
            delay(this, this);
            /** @type {null} */
            this.a = null;
            self.H(this.O);
            /** @type {null} */
            this.I = this.O = null;
            self.Jg.D.B.call(this);
        };
        /**
         * @return {?}
         */
        self.Jg.prototype.J = function() {
            return this.v;
        };
        /**
         * @param {string} id
         * @return {undefined}
         */
        var Client = function(id) {
            self.Fa.call(this);
            /** @type {string} */
            this.id = id;
            /** @type {string} */
            this.message = 'Service for "' + id + '" is not registered';
        };
        self.p(Client, self.Fa);
        /**
         * @param {string} err
         * @param {string} middleware
         * @param {?} error
         * @return {undefined}
         */
        var stack = function(err, middleware, error) {
            self.Fa.call(this);
            /** @type {string} */
            this.message = 'Module "' + middleware + '" failed to load when requesting the service "' + err + '" [cause: ' + error + "]";
            /** @type {string} */
            this.stack = error.stack + "\nWRAPPED BY:\n" + this.stack;
        };
        self.p(stack, self.Fa);
        /**
         * @param {string} positionError
         * @param {string} operator
         * @param {string} msg
         * @return {undefined}
         */
        var fail = function(positionError, operator, msg) {
            self.Fa.call(this);
            /** @type {string} */
            this.message = 'Configuration error when loading the module "' + operator + '" for the service "' + positionError + '": ' + msg;
        };
        self.p(fail, self.Fa);
        buffer = new Buffer("fva");
        self.Tg = new Sprite(buffer, 1);
    } catch (curr) {
        self._DumpException(curr);
    }
    try {
        self.Ug = "https://support.google.com/chrome/answer/185277?hl=" + window.chrome.i18n.getMessage("@@ui_locale");
        window.chrome.i18n.getMessage("@@ui_locale");
        self.Vg = "https://support.google.com/hangouts/?p=chrome_desktop_app_outdated&hl=" + window.chrome.i18n.getMessage("@@ui_locale");
    } catch (fieldset) {
        self._DumpException(fieldset);
    }
    try {
        var DOW_MAP = {};
        /**
         * @param {string} string
         * @return {?}
         */
        var wrap = function(string) {
            if (DOW_MAP[string]) {
                return DOW_MAP[string];
            }
            /** @type {string} */
            string = String(string);
            if (!DOW_MAP[string]) {
                /** @type {(Array.<string>|null)} */
                var array = /function ([^\(]+)/.exec(string);
                /** @type {string} */
                DOW_MAP[string] = array ? array[1] : "[Anonymous]";
            }
            return DOW_MAP[string];
        };
        /**
         * @param {Object} func
         * @return {?}
         */
        var defer = function(func) {
            /** @type {Error} */
            var err = Error();
            if (Error.captureStackTrace) {
                return Error.captureStackTrace(err, func), String(err.stack);
            }
            try {
                throw err;
            } catch (backtrace) {
                err = backtrace;
            }
            return(func = err.stack) ? String(func) : null;
        };
        /**
         * @param {?} s
         * @return {?}
         */
        var jQuery = function(s) {
            return s.M ? s.M : s.b ? jQuery(s.b) : null;
        };
        /**
         * @param {number} listener
         * @return {undefined}
         */
        var event = function(listener) {
            self.z(this, listener, 0, null);
        };
        var deferred;
        var setter;
        self.p(event, self.y);
        /**
         * @return {?}
         */
        event.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        event.prototype.getTitle = function() {
            return self.A(this, 2);
        };
        /**
         * @param {Object} millis
         * @return {undefined}
         */
        event.prototype.setTitle = function(millis) {
            self.B(this, 2, millis);
        };
        /**
         * @return {?}
         */
        event.prototype.ra = function() {
            return self.A(this, 3);
        };
        /**
         * @param {?} file
         * @param {Object} attributes
         * @return {undefined}
         */
        var hidden = function(file, attributes) {
            self.Jd(file, null, attributes, void 0);
        };
        /**
         * @param {number} opt_depth
         * @return {?}
         */
        var action = function(opt_depth) {
            var result = defer(action);
            if (result) {
                return result;
            }
            /** @type {Array} */
            result = [];
            /** @type {(Function|null)} */
            var fn = arguments.callee.caller;
            /** @type {number} */
            var depth = 0;
            for (;fn && (!opt_depth || depth < opt_depth);) {
                result.push(wrap(fn));
                result.push("()\n");
                try {
                    /** @type {(Function|null)} */
                    fn = fn.caller;
                } catch (e) {
                    result.push("[exception trying to get caller]\n");
                    break;
                }
                depth++;
                if (50 <= depth) {
                    result.push("[...long stack...]");
                    break;
                }
            }
            if (opt_depth && depth >= opt_depth) {
                result.push("[...reached max depth limit...]");
            } else {
                result.push("[end]");
            }
            return result.join("");
        };
        /**
         * @param {Node} scope
         * @param {?} file
         * @param {string} hash
         * @param {string} type
         * @return {undefined}
         */
        var validate = function(scope, file, hash, type) {
            self.Kd(file, function() {
                if (this.A) {
                    this.A.push("loaded(" + hash + "," + type + ")\n" + action());
                }
                return jQuery(this).load(type);
            }, scope);
            hidden(file, (0, self.r)(scope.L, scope, hash, type));
        };
        /**
         * @param {Array} border
         * @return {?}
         */
        var reduce = function(border) {
            var context = {};
            var result = {};
            /** @type {Array} */
            var common = [];
            /** @type {Array} */
            var services = [];
            /**
             * @param {?} v
             * @return {undefined}
             */
            var e = function(v) {
                if (!result[v]) {
                    var i = v instanceof self.O ? v.a : [];
                    result[v] = self.ib(i);
                    (0, self.u)(i, function(zi) {
                        context[zi] = context[zi] || [];
                        context[zi].push(v);
                    });
                    if (!i.length) {
                        common.push(v);
                    }
                    (0, self.u)(i, e);
                }
            };
            (0, self.u)(border, e);
            for (;common.length;) {
                var match = common.shift();
                services.push(match);
                if (context[match]) {
                    (0, self.u)(context[match], function(property) {
                        self.gb(result[property], match);
                        if (!result[property].length) {
                            common.push(property);
                        }
                    });
                }
            }
            var classNames = {};
            /** @type {Array} */
            var _results1 = [];
            (0, self.u)(services, function(c) {
                if (c instanceof self.O) {
                    c = c.b;
                    if (!(null == c)) {
                        if (!classNames[c]) {
                            /** @type {boolean} */
                            classNames[c] = true;
                            _results1.push(c);
                        }
                    }
                }
            });
            return{
                services : services,
                Yb : _results1
            };
        };
        /** @type {Array} */
        var filepaths = [3, 4];
        /**
         * @param {number} textStatus
         * @return {undefined}
         */
        var complete = function(textStatus) {
            self.z(this, textStatus, 0, null);
        };
        self.p(complete, self.y);
        /**
         * @param {number} value
         * @return {undefined}
         */
        var obj = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(obj, self.y);
        /**
         * @return {?}
         */
        obj.prototype.getTitle = function() {
            return self.C(this, event, 9);
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        obj.prototype.setTitle = function(type) {
            self.E(this, 9, type);
        };
        /** @type {Array} */
        var prevData = [1];
        /**
         * @param {number} g
         * @return {undefined}
         */
        var g = function(g) {
            self.z(this, g, 0, null);
        };
        self.p(g, self.y);
        /**
         * @param {(number|string)} arg
         * @return {?}
         */
        var isArray = function(arg) {
            if (arg instanceof self.qc) {
                return arg;
            }
            /** @type {null} */
            var recurring = null;
            if (arg.Wa) {
                recurring = arg.La();
            }
            arg = self.Sa(arg.Xa ? arg.Va() : String(arg));
            return self.rc(arg, recurring);
        };
        /**
         * @param {Object} data
         * @return {?}
         */
        var marked = function(data) {
            if (data instanceof self.qc && (data.constructor === self.qc && data.c === self.pc)) {
                return data.a;
            }
            self.va(data);
            return "type_error:SafeHtml";
        };
        var result_b = {};
        /**
         * @param {?} data
         * @return {?}
         */
        var flatten = function(data) {
            var value;
            if (self.Ca(data)) {
                /** @type {Array} */
                var result = Array(data.length);
                /** @type {number} */
                var key = 0;
                for (;key < data.length;key++) {
                    if (null != (value = data[key])) {
                        result[key] = "object" == typeof value ? flatten(value) : value;
                    }
                }
                return result;
            }
            if (self.gc && data instanceof window.Uint8Array) {
                return new window.Uint8Array(data);
            }
            result = {};
            for (key in data) {
                if (null != (value = data[key])) {
                    result[key] = "object" == typeof value ? flatten(value) : value;
                }
            }
            return result;
        };
        /**
         * @param {Object} options
         * @param {Array} items
         * @return {?}
         */
        var tighten = function(options, items) {
            var statements = {};
            /** @type {Array} */
            var newArgs = [];
            /** @type {Array} */
            var keys = [];
            var doc = self.Lg(options, self.Nf);
            /** @type {number} */
            var k = 0;
            for (;k < items.length;k++) {
                var i = items[k];
                var ready = self.Lg(options, i);
                if (ready) {
                    var html = new self.Dd;
                    statements[i] = html;
                    if (ready.Ta) {
                        self.Ld(html, ready.Ta());
                        self.Kd(html, self.xa(function(a) {
                            return a;
                        }, ready));
                    }
                    html.b(ready);
                } else {
                    var applyArgs;
                    if (i instanceof self.O) {
                        applyArgs = reduce([i]).Yb;
                    } else {
                        if (ready = options.j[i]) {
                            /** @type {Array} */
                            applyArgs = [ready];
                        }
                    }
                    if (applyArgs) {
                        if (doc) {
                            if (i instanceof self.O) {
                                if (doc.Ec()) {
                                    if (self.Kg) {
                                        doc.Fc(self.Tg);
                                    }
                                    doc.Dc(i);
                                }
                            }
                        }
                        newArgs.push.apply(newArgs, applyArgs);
                    }
                    keys.push(i);
                }
            }
            /** @type {number} */
            k = 0;
            for (;k < keys.length;k++) {
                i = keys[k];
                ready = newArgs[k];
                html = new self.Dd((0, self.r)(options.H, options, i));
                statements[i] = html;
                if (!(doc = options.a[i])) {
                    /** @type {Array} */
                    options.a[i] = doc = [];
                }
                if (ready) {
                    validate(options, html, i, ready);
                }
                self.Kd(html, (0, self.r)(options.G, options, i, ready));
                doc.push({
                    Sa : options,
                    d : html
                });
            }
            return statements;
        };
        /**
         * @param {number} data
         * @return {undefined}
         */
        var packet = function(data) {
            self.z(this, data, "capi:clstup", null);
        };
        self.p(packet, self.y);
        /** @type {string} */
        packet.messageId = "capi:clstup";
        /**
         * @param {Object} a
         * @param {?} b
         * @return {undefined}
         */
        var f4 = function(a, b) {
            if ("object" == typeof b) {
                a.b = b;
            } else {
                /** @type {Array} */
                a.b = [b];
            }
        };
        /**
         * @param {number} listener
         * @return {undefined}
         */
        var root = function(listener) {
            self.z(this, listener, "h_cs", null);
        };
        self.p(root, self.y);
        /** @type {string} */
        root.messageId = "h_cs";
        /**
         * @return {?}
         */
        root.prototype.getState = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var z = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(z, self.y);
        /**
         * @param {number} data
         * @return {undefined}
         */
        var template = function(data) {
            self.z(this, data, "uvc", null);
        };
        self.p(template, self.y);
        /** @type {string} */
        template.messageId = "uvc";
        /**
         * @param {Object} recurring
         * @return {undefined}
         */
        template.prototype.setVersion = function(recurring) {
            self.B(this, 1, recurring);
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var prop = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(prop, self.y);
        /**
         * @return {?}
         */
        prop.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} m
         * @return {undefined}
         */
        var C = function(m) {
            self.z(this, m, 0, null);
        };
        self.p(C, self.y);
        /**
         * @return {?}
         */
        C.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} param
         * @return {undefined}
         */
        var string = function(param) {
            self.z(this, param, 0, null);
        };
        self.p(string, self.y);
        /**
         * @return {?}
         */
        string.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        string.prototype.C = function() {
            return self.C(this, complete, 2);
        };
        /**
         * @param {number} y
         * @return {undefined}
         */
        string.prototype.setPosition = function(y) {
            self.E(this, 2, y);
        };
        /**
         * @param {number} fileName
         * @return {undefined}
         */
        var read = function(fileName) {
            self.z(this, fileName, 0, null);
        };
        self.p(read, self.y);
        /**
         * @return {?}
         */
        read.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var msg = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(msg, self.y);
        /**
         * @param {number} selector
         * @return {undefined}
         */
        var selector = function(selector) {
            self.z(this, selector, 0, filepaths);
        };
        self.p(selector, self.y);
        /**
         * @param {number} n
         * @return {undefined}
         */
        var ms = function(n) {
            self.z(this, n, 0, null);
        };
        self.p(ms, self.y);
        /**
         * @param {string} type
         * @param {Object} recurring
         * @return {undefined}
         */
        var is = function(type, recurring) {
            self.B(type, 2, recurring);
        };
        /**
         * @param {string} type
         * @param {Object} recurring
         * @return {undefined}
         */
        var removeFromList = function(type, recurring) {
            self.B(type, 1, recurring);
        };
        /**
         * @param {number} v
         * @return {undefined}
         */
        var filename = function(v) {
            self.z(this, v, 0, null);
        };
        self.p(filename, self.y);
        /**
         * @param {number} event
         * @return {undefined}
         */
        var input = function(event) {
            self.z(this, event, 0, null);
        };
        self.p(input, self.y);
        /**
         * @return {?}
         */
        input.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} obj
         * @return {undefined}
         */
        var i = function(obj) {
            self.z(this, obj, 0, null);
        };
        self.p(i, self.y);
        /**
         * @return {?}
         */
        i.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} context
         * @return {undefined}
         */
        var list = function(context) {
            self.z(this, context, 0, null);
        };
        self.p(list, self.y);
        /**
         * @return {?}
         */
        list.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} arg
         * @return {undefined}
         */
        var arg = function(arg) {
            self.z(this, arg, 0, null);
        };
        self.p(arg, self.y);
        /**
         * @return {?}
         */
        arg.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} context
         * @return {undefined}
         */
        var literal = function(context) {
            self.z(this, context, "capi:f_vsm", null);
        };
        self.p(literal, self.y);
        /** @type {string} */
        literal.messageId = "capi:f_vsm";
        /**
         * @param {number} event
         * @return {undefined}
         */
        var w = function(event) {
            self.z(this, event, "capi:f_cvr", null);
        };
        self.p(w, self.y);
        /** @type {string} */
        w.messageId = "capi:f_cvr";
        /**
         * @param {number} actual
         * @return {undefined}
         */
        var message = function(actual) {
            self.z(this, actual, "capi:f_cfr", null);
        };
        self.p(message, self.y);
        /** @type {string} */
        message.messageId = "capi:f_cfr";
        /**
         * @param {number} e
         * @return {undefined}
         */
        var click = function(e) {
            self.z(this, e, 0, null);
        };
        self.p(click, self.y);
        /**
         * @return {?}
         */
        click.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} func
         * @return {undefined}
         */
        var partial = function(func) {
            self.z(this, func, 0, prevData);
        };
        self.p(partial, self.y);
        /**
         * @return {?}
         */
        partial.prototype.getState = function() {
            return self.C(this, obj, 2);
        };
        /**
         * @param {Object} recurring
         * @return {undefined}
         */
        partial.prototype.setVersion = function(recurring) {
            self.B(this, 3, recurring);
        };
        /**
         * @param {number} g
         * @return {undefined}
         */
        var x = function(g) {
            self.z(this, g, 0, null);
        };
        self.p(x, self.y);
        /**
         * @return {?}
         */
        x.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        x.prototype.C = function() {
            return self.A(this, 2);
        };
        /**
         * @param {number} y
         * @return {undefined}
         */
        x.prototype.setPosition = function(y) {
            self.B(this, 2, y);
        };
        /**
         * @return {?}
         */
        x.prototype.qa = function() {
            return self.A(this, 3);
        };
        /**
         * @param {number} e
         * @return {undefined}
         */
        var keydown = function(e) {
            self.z(this, e, 0, null);
        };
        self.p(keydown, self.y);
        /**
         * @param {number} listener
         * @return {undefined}
         */
        var instance = function(listener) {
            self.z(this, listener, "uv:sn", null);
        };
        self.p(instance, self.y);
        /** @type {string} */
        instance.messageId = "uv:sn";
        /**
         * @return {?}
         */
        instance.prototype.getState = function() {
            return self.C(this, g, 1);
        };
        /**
         * @param {number} data
         * @return {undefined}
         */
        var connection = function(data) {
            self.z(this, data, "uv:smwc", null);
        };
        self.p(connection, self.y);
        /** @type {string} */
        connection.messageId = "uv:smwc";
        /**
         * @param {Document} doc
         * @return {?}
         */
        var emit = function(doc) {
            return doc.parentWindow || doc.defaultView;
        };
        /**
         * @param {?} c
         * @return {?}
         */
        var addClass = function(c) {
            /** @type {number} */
            var val = 0;
            /** @type {string} */
            var msg = "";
            /**
             * @param {number} value
             * @return {undefined}
             */
            var addClass = function(value) {
                if (self.Ca(value)) {
                    (0, self.u)(value, addClass);
                } else {
                    value = isArray(value);
                    msg += marked(value);
                    value = value.La();
                    if (0 == val) {
                        /** @type {number} */
                        val = value;
                    } else {
                        if (0 != value) {
                            if (val != value) {
                                /** @type {null} */
                                val = null;
                            }
                        }
                    }
                }
            };
            (0, self.u)(arguments, addClass);
            return self.rc(msg, val);
        };
        /**
         * @return {undefined}
         */
        var B = function() {
            /** @type {string} */
            this.a = "";
            this.b = result_b;
        };
        /** @type {boolean} */
        B.prototype.Xa = true;
        /**
         * @return {?}
         */
        B.prototype.Va = function() {
            return this.a;
        };
        /** @type {boolean} */
        B.prototype.Wa = true;
        /**
         * @return {?}
         */
        B.prototype.La = function() {
            return 1;
        };
        /**
         * @param {string} ready
         * @return {?}
         */
        var indexOf = function(ready) {
            return new ready.constructor(flatten(self.D(ready)));
        };
        /**
         * @param {string} arg
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {Array} obj
         * @return {undefined}
         */
        var print = function(arg, expectedNumberOfNonCommentArgs, obj) {
            if (!arg.b) {
                arg.b = {};
            }
            obj = obj || [];
            /** @type {Array} */
            var recurring = [];
            /** @type {number} */
            var i = 0;
            for (;i < obj.length;i++) {
                recurring[i] = self.D(obj[i]);
            }
            /** @type {Array} */
            arg.b[expectedNumberOfNonCommentArgs] = obj;
            self.B(arg, expectedNumberOfNonCommentArgs, recurring);
        };
        /**
         * @param {string} obj
         * @param {Function} str
         * @param {number} expectedNumberOfNonCommentArgs
         * @return {?}
         */
        var iterator = function(obj, str, expectedNumberOfNonCommentArgs) {
            if (!obj.b) {
                obj.b = {};
            }
            if (!obj.b[expectedNumberOfNonCommentArgs]) {
                var codeSegments = self.A(obj, expectedNumberOfNonCommentArgs);
                /** @type {Array} */
                var prevSources = [];
                /** @type {number} */
                var i = 0;
                for (;i < codeSegments.length;i++) {
                    prevSources[i] = new str(codeSegments[i]);
                }
                /** @type {Array} */
                obj.b[expectedNumberOfNonCommentArgs] = prevSources;
            }
            str = obj.b[expectedNumberOfNonCommentArgs];
            if (str == self.hc) {
                /** @type {Array} */
                str = obj.b[expectedNumberOfNonCommentArgs] = [];
            }
            return str;
        };
        /** @type {number} */
        var Wh = 0;
        /** @type {Array} */
        var part = ["talkgadget.google.com", "hangouts.google.com", ".hangouts.sandbox.google.com"];
        /**
         * @param {string} ch
         * @param {Object} value
         * @param {Array} stream
         * @return {undefined}
         */
        var repeat = function(ch, value, stream) {
            if (self.Ca(value)) {
                /** @type {number} */
                var j = 0;
                for (;j < value.length;j++) {
                    repeat(ch, String(value[j]), stream);
                }
            } else {
                if (null != value) {
                    stream.push("&", ch, "" === value ? "" : "=", (0, window.encodeURIComponent)(String(value)));
                }
            }
        };
        var a1 = {};
        /**
         * @param {Node} res
         * @return {?}
         */
        var findAll = function(res) {
            res = new self.P(res);
            var b = res.b;
            return!!res.b && -1 != self.cb(part, function(p) {
                    return self.Ia(b, p);
                });
        };
        /**
         * @param {?} item
         * @return {?}
         */
        var isDefined = function(item) {
            item = new self.P(item);
            return!!item.b && ("google.com" == item.b || self.Ia(item.b, ".google.com"));
        };
        var rb = {
            LOADING : 0,
            sb : 1,
            rc : 2,
            xc : 3,
            ERROR : 4,
            LOADED : 5,
            rb : 6
        };
        /**
         * @param {Array} t
         * @param {Object} a
         * @return {?}
         */
        var pad = function(t, a) {
            var prefix;
            for (prefix in a) {
                repeat(prefix, a[prefix], t);
            }
            return t;
        };
        /**
         * @param {Array} obj
         * @param {Object} args
         * @param {number} i
         * @return {?}
         */
        var mixin = function(obj, args, i) {
            i = i || 0;
            for (;i < args.length;i += 2) {
                repeat(args[i], args[i + 1], obj);
            }
            return obj;
        };
        /**
         * @param {Array} buffer
         * @return {?}
         */
        var toString = function(buffer) {
            if (buffer[1]) {
                var baseUri = buffer[0];
                var hashIndex = baseUri.indexOf("#");
                if (0 <= hashIndex) {
                    buffer.push(baseUri.substr(hashIndex));
                    buffer[0] = baseUri = baseUri.substr(0, hashIndex);
                }
                hashIndex = baseUri.indexOf("?");
                if (0 > hashIndex) {
                    /** @type {string} */
                    buffer[1] = "?";
                } else {
                    if (hashIndex == baseUri.length - 1) {
                        buffer[1] = void 0;
                    }
                }
            }
            return buffer.join("");
        };
        /**
         * @param {Function} a
         * @param {Array} buffer
         * @return {?}
         */
        var parse = function(a, buffer) {
            /** @type {Array} */
            var tagNameArr = [];
            if (self.eb(buffer, a)) {
                tagNameArr.push("[...circular reference...]");
            } else {
                if (a && 50 > buffer.length) {
                    tagNameArr.push(wrap(a) + "(");
                    var codeSegments = a.arguments;
                    /** @type {number} */
                    var i = 0;
                    for (;codeSegments && i < codeSegments.length;i++) {
                        if (0 < i) {
                            tagNameArr.push(", ");
                        }
                        var $arg$$5_argDesc;
                        $arg$$5_argDesc = codeSegments[i];
                        switch(typeof $arg$$5_argDesc) {
                            case "object":
                                /** @type {string} */
                                $arg$$5_argDesc = $arg$$5_argDesc ? "object" : "null";
                                break;
                            case "string":
                                break;
                            case "number":
                                /** @type {string} */
                                $arg$$5_argDesc = String($arg$$5_argDesc);
                                break;
                            case "boolean":
                                /** @type {string} */
                                $arg$$5_argDesc = $arg$$5_argDesc ? "true" : "false";
                                break;
                            case "function":
                                $arg$$5_argDesc = ($arg$$5_argDesc = wrap($arg$$5_argDesc)) ? $arg$$5_argDesc : "[fn]";
                                break;
                            default:
                                /** @type {string} */
                                $arg$$5_argDesc = typeof $arg$$5_argDesc;
                        }
                        if (40 < $arg$$5_argDesc.length) {
                            /** @type {string} */
                            $arg$$5_argDesc = $arg$$5_argDesc.substr(0, 40) + "...";
                        }
                        tagNameArr.push($arg$$5_argDesc);
                    }
                    buffer.push(a);
                    tagNameArr.push(")\n");
                    try {
                        tagNameArr.push(parse(a.caller, buffer));
                    } catch (h) {
                        tagNameArr.push("[exception trying to get caller]\n");
                    }
                } else {
                    if (a) {
                        tagNameArr.push("[...long stack...]");
                    } else {
                        tagNameArr.push("[end]");
                    }
                }
            }
            return tagNameArr.join("");
        };
        /**
         * @param {string} o
         * @return {?}
         */
        var forEach = function(o) {
            /** @type {string} */
            var type = typeof o;
            return "object" == type && o || "function" == type ? "o" + (o[self.ra] || (o[self.ra] = ++Wh)) : type.substr(0, 1) + o;
        };
        /**
         * @return {undefined}
         */
        var NodeList = function() {
            this.a = a1;
        };
        /** @type {boolean} */
        NodeList.prototype.Xa = true;
        /**
         * @return {?}
         */
        NodeList.prototype.Va = function() {
            return "";
        };
        /** @type {boolean} */
        NodeList.prototype.Wa = true;
        /**
         * @return {?}
         */
        NodeList.prototype.La = function() {
            return 1;
        };
        /** @type {null} */
        var ii = null;
        var vals = {
            zc : 1,
            yc : 2,
            vc : 3,
            qc : 4,
            uc : 6,
            wc : 10
        };
        /** @type {Array} */
        var origin = [1E4, 12E4, 144E5];
        /**
         * @param {?} data
         * @return {?}
         */
        var queue = function(data) {
            var object = new self.P(data);
            return isDefined(data) && (!!object.a && self.Ha(object.a, "/voice/"));
        };
        /**
         * @param {?} pattern
         * @return {?}
         */
        var escape = function(pattern) {
            var s = new self.P(pattern);
            return isDefined(pattern) && (!!s.a && self.Ha(s.a, "/client-channel"));
        };
        /**
         * @param {?} e
         * @return {?}
         */
        var readFile = function(e) {
            var p = new self.P(e);
            return findAll(e) && (!!p.a && (self.Ha(p.a, "/webchat/frame") || self.Ha(p.a, "/webchat/u/0/frame")));
        };
        /**
         * @param {?} item
         * @return {?}
         */
        var isFunction = function(item) {
            var obj = new self.P(item);
            return isDefined(item) && (!!obj.a && self.Ha(obj.a, "/webchat/extension-start"));
        };
        var requestFilter = {
            urls : ["https://*.talkgadget.google.com/*", "https://hangouts.google.com/*", "https://*.hangouts.sandbox.google.com/*"]
        };
        var appender = {
            urls : ["https://*.google.com/*"]
        };
        /**
         * @param {?} l
         * @return {undefined}
         */
        var I = function(l) {
            self.L.call(this);
            this.l = l;
            /** @type {number} */
            this.b = rb.LOADING;
            /** @type {boolean} */
            this.h = false;
            /** @type {boolean} */
            this.a = true;
            this.j = new self.N(this);
            self.I(this, this.j);
            this.j.listen(this.l.J(), "h_cs", this.v);
        };
        self.p(I, self.L);
        /**
         * @return {?}
         */
        I.prototype.getState = function() {
            return this.b;
        };
        /**
         * @return {?}
         */
        I.prototype.g = function() {
            return this.h;
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        I.prototype.v = function(types) {
            types = new root(types.S);
            var h = types.getState();
            if (null != h) {
                if (this.b != h) {
                    this.b = h;
                    self.M(this, "d");
                }
            }
            h = self.A(types, 2);
            if (null != h) {
                if (this.h != h) {
                    this.h = h;
                    self.M(this, "c");
                }
            }
            types = self.A(types, 3);
            if (null != types) {
                if (this.a != types) {
                    /** @type {string} */
                    this.a = types;
                    self.M(this, "b");
                }
            }
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var empty = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(empty, self.y);
        /**
         * @param {?} data
         * @return {undefined}
         */
        empty.prototype.Ba = function(data) {
            self.B(this, 13, data);
        };
        /**
         * @param {number} frame
         * @return {undefined}
         */
        var hit = function(frame) {
            self.z(this, frame, 0, null);
        };
        self.p(hit, self.y);
        /**
         * @param {number} value
         * @return {undefined}
         */
        var RegExp = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(RegExp, self.y);
        /**
         * @param {number} prop
         * @return {undefined}
         */
        var fx = function(prop) {
            self.z(this, prop, 0, null);
        };
        self.p(fx, self.y);
        /**
         * @return {?}
         */
        fx.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {undefined}
         */
        deferred = function() {
            for (;self.Ie.length;) {
                self.Ie.pop().Z();
            }
        };
        /**
         * @param {?} val
         * @param {string} value
         * @return {?}
         */
        setter = function(val, value) {
            return toString(2 == arguments.length ? mixin([val], arguments[1], 0) : mixin([val], arguments, 1));
        };
        /**
         * @param {?} id
         * @return {undefined}
         */
        self.Rd = function(id) {
            this.id = id;
        };
        /**
         * @return {?}
         */
        self.Rd.prototype.toString = function() {
            return this.id;
        };
        /**
         * @param {?} object
         * @return {undefined}
         */
        var getEnumerableProperties = function(object) {
            /** @type {boolean} */
            self.Sc = true;
            var obj = (0, self.r)(object.b, object);
            /** @type {number} */
            var i = 0;
            for (;i < self.Qc.length;i++) {
                self.Qc[i](obj);
            }
            self.Rc.push(object);
        };
        /**
         * @param {Function} extra
         * @return {?}
         */
        var inherit = function(extra) {
            var result;
            if (!(result = defer(extra || inherit))) {
                result = parse(extra || arguments.callee.caller, []);
            }
            return result;
        };
        /**
         * @param {?} logFunc
         * @param {boolean} ui
         * @param {Node} target
         * @return {undefined}
         */
        var open = function(logFunc, ui, target) {
            target = target || self.l;
            /** @type {function (string, Function, number, (Object|string), Error): ?} */
            var fn = target.onerror;
            /** @type {boolean} */
            var retVal = !!ui;
            if (self.Db) {
                if (!self.Pb("535.3")) {
                    /** @type {boolean} */
                    retVal = !retVal;
                }
            }
            /**
             * @param {string} types
             * @param {Function} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @param {(Object|string)} arr
             * @param {Error} err
             * @return {?}
             */
            target.onerror = function(types, type, expectedNumberOfNonCommentArgs, arr, err) {
                if (fn) {
                    fn(types, type, expectedNumberOfNonCommentArgs, arr, err);
                }
                logFunc({
                    message : types,
                    /** @type {Function} */
                    fileName : type,
                    line : expectedNumberOfNonCommentArgs,
                    Bc : arr,
                    error : err
                });
                return retVal;
            };
        };
        /**
         * @param {(Array|string)} values
         * @return {undefined}
         */
        var stream = function(values) {
            this.a = new self.J;
            if (values) {
                values = self.Mc(values);
                var valuesLen = values.length;
                /** @type {number} */
                var i = 0;
                for (;i < valuesLen;i++) {
                    var ar = values[i];
                    this.a.set(forEach(ar), ar);
                }
            }
        };
        self.g = stream.prototype;
        /**
         * @param {Object} values
         * @return {undefined}
         */
        self.g.removeAll = function(values) {
            values = self.Mc(values);
            var valuesLen = values.length;
            /** @type {number} */
            var i = 0;
            for (;i < valuesLen;i++) {
                this.remove(values[i]);
            }
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.g.remove = function(name) {
            return this.a.remove(forEach(name));
        };
        /**
         * @return {undefined}
         */
        self.g.clear = function() {
            this.a.clear();
        };
        /**
         * @param {string} name
         * @return {?}
         */
        self.g.contains = function(name) {
            return self.Lc(this.a, forEach(name));
        };
        /**
         * @return {?}
         */
        self.g.V = function() {
            return this.a.V();
        };
        /**
         * @return {?}
         */
        self.g.ia = function() {
            return this.a.ia(false);
        };
        /**
         * @param {Node} element
         * @param {string} text
         * @return {undefined}
         */
        var text = function(element, text) {
            if ("textContent" in element) {
                /** @type {string} */
                element.textContent = text;
            } else {
                if (3 == element.nodeType) {
                    /** @type {string} */
                    element.data = text;
                } else {
                    if (element.firstChild && 3 == element.firstChild.nodeType) {
                        for (;element.lastChild != element.firstChild;) {
                            element.removeChild(element.lastChild);
                        }
                        /** @type {string} */
                        element.firstChild.data = text;
                    } else {
                        self.Cc(element);
                        element.appendChild(self.uc(element).createTextNode(String(text)));
                    }
                }
            }
        };
        /**
         * @param {Text} element
         * @return {undefined}
         */
        var swap = function(element) {
            if (element) {
                if (element.parentNode) {
                    element.parentNode.removeChild(element);
                }
            }
        };
        /**
         * @param {?} pixelWidth
         * @param {?} pixelHeight
         * @return {undefined}
         */
        var setSize = function(pixelWidth, pixelHeight) {
            this.width = pixelWidth;
            this.height = pixelHeight;
        };
        /**
         * @return {?}
         */
        setSize.prototype.ceil = function() {
            /** @type {number} */
            this.width = Math.ceil(this.width);
            /** @type {number} */
            this.height = Math.ceil(this.height);
            return this;
        };
        /**
         * @return {?}
         */
        setSize.prototype.floor = function() {
            /** @type {number} */
            this.width = Math.floor(this.width);
            /** @type {number} */
            this.height = Math.floor(this.height);
            return this;
        };
        /**
         * @return {?}
         */
        setSize.prototype.round = function() {
            /** @type {number} */
            this.width = Math.round(this.width);
            /** @type {number} */
            this.height = Math.round(this.height);
            return this;
        };
        /**
         * @param {string} ready
         * @param {string} orig
         * @return {undefined}
         */
        var Rect = function(ready, orig) {
            this.x = self.m(ready) ? ready : 0;
            this.y = self.m(orig) ? orig : 0;
        };
        /**
         * @return {?}
         */
        Rect.prototype.ceil = function() {
            /** @type {number} */
            this.x = Math.ceil(this.x);
            /** @type {number} */
            this.y = Math.ceil(this.y);
            return this;
        };
        /**
         * @return {?}
         */
        Rect.prototype.floor = function() {
            /** @type {number} */
            this.x = Math.floor(this.x);
            /** @type {number} */
            this.y = Math.floor(this.y);
            return this;
        };
        /**
         * @return {?}
         */
        Rect.prototype.round = function() {
            /** @type {number} */
            this.x = Math.round(this.x);
            /** @type {number} */
            this.y = Math.round(this.y);
            return this;
        };
        /**
         * @param {string} ready
         * @param {string} orig
         * @return {?}
         */
        var addEvent = function(ready, orig) {
            return ready == orig || !(!ready || !orig) && (ready instanceof orig.constructor && self.lc(self.D(ready), self.D(orig)));
        };
        /**
         * @param {Array} value
         * @return {?}
         */
        var min = function(value) {
            /** @type {number} */
            var argLength = arguments.length;
            if (1 == argLength && self.Ca(arguments[0])) {
                return min.apply(null, arguments[0]);
            }
            var result = {};
            /** @type {number} */
            var i = 0;
            for (;i < argLength;i++) {
                /** @type {boolean} */
                result[arguments[i]] = true;
            }
            return result;
        };
        /**
         * @param {Array} arg1
         * @return {?}
         */
        var func = function(arg1) {
            /** @type {number} */
            var argLength = arguments.length;
            if (1 == argLength && self.Ca(arguments[0])) {
                return func.apply(null, arguments[0]);
            }
            if (argLength % 2) {
                throw Error("b");
            }
            var m = {};
            /** @type {number} */
            var i = 0;
            for (;i < argLength;i += 2) {
                m[arguments[i]] = arguments[i + 1];
            }
            return m;
        };
        /**
         * @param {string} obj
         * @param {Function} callback
         * @return {undefined}
         */
        var ajax = function(obj, callback) {
            /** @type {number} */
            var c = 0;
            self.Za(obj, function(operation, value) {
                if (callback.call(void 0, operation, value, obj)) {
                    if (self.fb(obj, value)) {
                        c++;
                    }
                }
            });
        };
        /**
         * @param {?} $script
         * @return {undefined}
         */
        var $goog$globalEval$ = function($script) {
            if (self.l.execScript) {
                self.l.execScript($script, "JavaScript");
            } else {
                if (self.l.eval) {
                    if (null == ii) {
                        if (self.l.eval("var _evalTest_ = 1;"), "undefined" != typeof self.l._evalTest_) {
                            try {
                                delete self.l._evalTest_;
                            } catch (d) {
                            }
                            /** @type {boolean} */
                            ii = true;
                        } else {
                            /** @type {boolean} */
                            ii = false;
                        }
                    }
                    if (ii) {
                        self.l.eval($script);
                    } else {
                        var $doc$$ = self.l.document;
                        var script = $doc$$.createElement("SCRIPT");
                        /** @type {string} */
                        script.type = "text/javascript";
                        /** @type {boolean} */
                        script.defer = false;
                        script.appendChild($doc$$.createTextNode($script));
                        $doc$$.body.appendChild(script);
                        $doc$$.body.removeChild(script);
                    }
                } else {
                    throw Error("a");
                }
            }
        };
        /**
         * @param {Function} val
         * @return {undefined}
         */
        var encodeUriSegment = function(val) {
            /**
             * @return {?}
             */
            val.za = function() {
                return val.ib ? val.ib : val.ib = new val;
            };
        };
        /**
         * @return {?}
         */
        var evaluate = function() {
            var root = window;
            if (!root.location) {
                try {
                    self.cc(root);
                } catch (c) {
                }
            }
            var ready = root.location && root.location.ancestorOrigins;
            if (self.m(ready)) {
                return ready && ready.length ? ready[ready.length - 1] == root.location.origin : true;
            }
            try {
                return self.m(root.top.location.href);
            } catch (c) {
                return false;
            }
        };
        var processors = {};
        /**
         * @return {?}
         */
        var poll = function() {
            var obj = {};
            obj.location = window.document.location.toString();
            if (evaluate()) {
                try {
                    obj["top.location"] = window.top.location.toString();
                } catch (c) {
                    /** @type {string} */
                    obj["top.location"] = "[external]";
                }
            } else {
                /** @type {string} */
                obj["top.location"] = "[external]";
            }
            var name;
            for (name in processors) {
                try {
                    obj[name] = processors[name].call();
                } catch (ex) {
                    /** @type {string} */
                    obj[name] = "[error] " + ex.message;
                }
            }
            return obj;
        };
        /**
         * @return {undefined}
         */
        var onComplete = function() {
            self.F.call(this);
        };
        self.p(onComplete, self.F);
        /**
         * @param {?} value
         * @return {undefined}
         */
        var isUndefinedOrNull = function(value) {
            var elem = target;
            elem.b = value;
            listen(elem);
        };
        /**
         * @param {string} res
         * @return {undefined}
         */
        var handleError = function(res) {
            var el = target;
            if (!el.K) {
                if (!(res instanceof self.Ed)) {
                    if (el.b) {
                        clean(el.b, res, null);
                    } else {
                        if (el.a) {
                            if (10 > el.a.length) {
                                el.a.push([null, res]);
                            }
                        }
                    }
                }
            }
        };
        /**
         * @param {Node} src
         * @return {undefined}
         */
        var listen = function(src) {
            if (src.a) {
                (0, self.u)(src.a, function(te) {
                    clean(this.b, te[1], te[0]);
                }, src);
                /** @type {null} */
                src.a = null;
            }
        };
        var target = new onComplete;
        /**
         * @param {string} name
         * @return {undefined}
         */
        var getLogger = function(name) {
            handleError(name);
        };
        /**
         * @param {?} b
         * @return {undefined}
         */
        var Span = function(b) {
            this.b = b;
            this.c = {};
            /** @type {Array} */
            this.a = [];
        };
        /**
         * @param {Object} obj
         * @param {string} result
         * @param {string} item
         * @return {undefined}
         */
        var clean = function(obj, result, item) {
            var cycle = poll();
            cycle["call-stack"] = inherit();
            if (item) {
                /** @type {string} */
                cycle.message = item;
            }
            var ready;
            if (result instanceof Error) {
                /** @type {string} */
                ready = result;
            } else {
                ready = result || "";
            }
            var i;
            /** @type {string} */
            result = "";
            if (ready) {
                /** @type {string} */
                result = ready.message || "unknown";
                /** @type {number} */
                var j = item = 0;
                for (;j < result.length;++j) {
                    /** @type {number} */
                    item = 31 * item + result.charCodeAt(j) >>> 0;
                }
                /** @type {string} */
                result = item;
            }
            /** @type {string} */
            item = "";
            for (i in cycle) {
                /** @type {string} */
                item = item + i + ":" + cycle[i] + ":";
            }
            /** @type {string} */
            i = result + "::" + item;
            result = obj.c[i];
            if (!result) {
                result = {
                    time : 0,
                    count : 0
                };
                /** @type {string} */
                obj.c[i] = result;
            }
            if (1E4 > (0, self.sa)() - result.time) {
                result.count++;
                if (1 == result.count) {
                    cycle = poll();
                    /** @type {string} */
                    cycle.message = "Throttling: " + i;
                    obj.b.b(ready, cycle);
                }
            } else {
                if (result.count) {
                    /** @type {number} */
                    cycle["dropped-instances"] = result.count;
                }
                result.time = (0, self.sa)();
                /** @type {number} */
                i = result.count = 0;
                for (;i < obj.a.length;i++) {
                    obj.a[i](ready, cycle);
                }
                obj.b.b(ready, cycle);
            }
        };
        /**
         * @param {?} g
         * @return {undefined}
         */
        var m = function(g) {
            self.F.call(this);
            this.g = g;
            /** @type {boolean} */
            this.c = true;
            /** @type {boolean} */
            this.a = false;
        };
        self.p(m, self.F);
        /**
         * @param {string} types
         * @return {?}
         */
        m.prototype.b = function(types) {
            return hasOwn(this, types);
        };
        /**
         * @param {?} obj
         * @param {boolean} recurring
         * @return {?}
         */
        var isUndefined = function(obj, recurring) {
            return(recurring ? "__wrapper_" : "__protected_") + (obj[self.ra] || (obj[self.ra] = ++Wh)) + "__";
        };
        /**
         * @param {?} obj
         * @param {Function} config
         * @return {?}
         */
        var hasOwn = function(obj, config) {
            var x = isUndefined(obj, true);
            if (!config[x]) {
                /** @type {Function} */
                (config[x] = navigate(obj, config))[isUndefined(obj, false)] = config;
            }
            return config[x];
        };
        /**
         * @param {Object} obj
         * @param {Function} matcherFunction
         * @return {?}
         */
        var navigate = function(obj, matcherFunction) {
            /**
             * @return {?}
             */
            var initialize = function() {
                if (obj.K) {
                    return matcherFunction.apply(this, arguments);
                }
                try {
                    return matcherFunction.apply(this, arguments);
                } catch (orig) {
                    if (!(orig && ("object" === typeof orig && (orig.message && 0 == orig.message.indexOf("Error in protected function: "))) || "string" === typeof orig && 0 == orig.indexOf("Error in protected function: "))) {
                        obj.g(orig);
                        if (!obj.c) {
                            throw obj.a && ("object" === typeof orig && (orig && "message" in orig) ? orig.message = "Error in protected function: " + orig.message : orig = "Error in protected function: " + orig), orig;
                        }
                        throw new onError(orig);
                    }
                } finally {
                }
            };
            /** @type {Function} */
            initialize[isUndefined(obj, false)] = matcherFunction;
            return initialize;
        };
        /**
         * @param {?} value
         * @return {undefined}
         */
        var isArguments = function(value) {
            var win = self.Ea("window");
            /** @type {Array} */
            var fnNames = ["requestAnimationFrame", "mozRequestAnimationFrame", "webkitAnimationFrame", "msRequestAnimationFrame"];
            /** @type {number} */
            var i = 0;
            for (;i < fnNames.length;i++) {
                var fnName = fnNames[i];
                if (fnNames[i] in win) {
                    hasKey(value, fnName);
                }
            }
        };
        /**
         * @param {?} obj
         * @param {string} fnName
         * @return {undefined}
         */
        var hasKey = function(obj, fnName) {
            var w = self.Ea("window");
            var originalFn = w[fnName];
            /**
             * @param {Function} key
             * @param {?} time
             * @return {?}
             */
            w[fnName] = function(key, time) {
                if (self.t(key)) {
                    key = self.xa($goog$globalEval$, key);
                }
                key = hasOwn(obj, key);
                if (originalFn.apply) {
                    return originalFn.apply(this, arguments);
                }
                /** @type {Function} */
                var newMatchersClass = key;
                if (2 < arguments.length) {
                    /** @type {Array.<?>} */
                    var args = Array.prototype.slice.call(arguments, 2);
                    /**
                     * @return {undefined}
                     */
                    newMatchersClass = function() {
                        key.apply(this, args);
                    };
                }
                return originalFn(newMatchersClass, time);
            };
            w[fnName][isUndefined(obj, false)] = originalFn;
        };
        /**
         * @return {undefined}
         */
        m.prototype.B = function() {
            var obj = self.Ea("window");
            var context;
            context = obj.setTimeout;
            context = context[isUndefined(this, false)] || context;
            obj.setTimeout = context;
            context = obj.setInterval;
            context = context[isUndefined(this, false)] || context;
            obj.setInterval = context;
            m.D.B.call(this);
        };
        /**
         * @param {Object} error
         * @return {undefined}
         */
        var onError = function(error) {
            self.Fa.call(this, "Error in protected function: " + (error && error.message ? String(error.message) : String(error)));
            if (error = error && error.stack) {
                if (self.t(error)) {
                    /** @type {Object} */
                    this.stack = error;
                }
            }
        };
        self.p(onError, self.Fa);
        /**
         * @param {?} positions
         * @param {Node} height
         * @param {?} success
         * @return {undefined}
         */
        var show = function(positions, height, success) {
            self.L.call(this);
            this.h = height || null;
            this.g = {};
            /** @type {function (string, Function, number, (Object|string)): undefined} */
            this.l = off;
            this.j = positions;
            if (!success) {
                /** @type {null} */
                this.a = null;
                if (self.x && !self.Pb("10")) {
                    open((0, self.r)(this.b, this), false, null);
                } else {
                    this.a = new m((0, self.r)(this.b, this));
                    hasKey(this.a, "setTimeout");
                    hasKey(this.a, "setInterval");
                    isArguments(this.a);
                    getEnumerableProperties(this.a);
                }
            }
        };
        self.p(show, self.L);
        /**
         * @param {?} message
         * @param {?} context
         * @return {undefined}
         */
        var warn = function(message, context) {
            self.K.call(this, "g");
            this.error = message;
            this.context = context;
        };
        self.p(warn, self.K);
        /**
         * @param {string} types
         * @param {Function} type
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {(Object|string)} callback
         * @return {undefined}
         */
        var off = function(types, type, expectedNumberOfNonCommentArgs, callback) {
            self.Je(types, null, type, expectedNumberOfNonCommentArgs, callback);
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        show.prototype.b = function(types, type) {
            var ready;
            ready = self.Ea("window.location.href");
            if (self.t(types)) {
                ready = {
                    message : types,
                    name : "Unknown error",
                    lineNumber : "Not available",
                    fileName : ready,
                    stack : "Not available"
                };
            } else {
                var fix;
                var trace;
                /** @type {boolean} */
                var lastFrame = false;
                try {
                    fix = types.lineNumber || (types.line || "Not available");
                } catch (Q) {
                    /** @type {string} */
                    fix = "Not available";
                    /** @type {boolean} */
                    lastFrame = true;
                }
                try {
                    trace = types.fileName || (types.filename || (types.sourceURL || (self.l.$googDebugFname || ready)));
                } catch (Q) {
                    /** @type {string} */
                    trace = "Not available";
                    /** @type {boolean} */
                    lastFrame = true;
                }
                ready = !lastFrame && (types.lineNumber && (types.fileName && (types.stack && (types.message && types.name)))) ? types : {
                    message : types.message || "Not available",
                    name : types.name || "UnknownError",
                    lineNumber : fix,
                    fileName : trace,
                    stack : types.stack || "Not available"
                };
            }
            fix = type ? self.qb(type) : {};
            if (this.h) {
                try {
                    this.h(ready, fix);
                } catch (Q) {
                }
            }
            lastFrame = ready.message.substring(0, 1900);
            /** @type {string} */
            trace = ready.stack;
            try {
                var orig = setter(this.j, "script", ready.fileName, "error", lastFrame, "line", ready.lineNumber);
                var node;
                a: {
                    var g = this.g;
                    var sGroup;
                    for (sGroup in g) {
                        /** @type {boolean} */
                        node = false;
                        break a;
                    }
                    /** @type {boolean} */
                    node = true;
                }
                if (!node) {
                    orig = toString(pad([orig], this.g));
                }
                node = {};
                /** @type {string} */
                node.trace = trace;
                if (fix) {
                    var name;
                    for (name in fix) {
                        node["context." + name] = fix[name];
                    }
                }
                var expectedNumberOfNonCommentArgs;
                var attr = pad([], node);
                /** @type {string} */
                attr[0] = "";
                expectedNumberOfNonCommentArgs = attr.join("");
                if (self.Aa(null)) {
                    expectedNumberOfNonCommentArgs = expectedNumberOfNonCommentArgs.substring(0, null);
                }
                this.l(orig, "POST", expectedNumberOfNonCommentArgs, this.v);
            } catch (Q) {
            }
            try {
                self.M(this, new warn(ready, fix));
            } catch (Q) {
            }
        };
        /**
         * @return {undefined}
         */
        show.prototype.B = function() {
            self.H(this.a);
            show.D.B.call(this);
        };
        /**
         * @return {undefined}
         */
        var parseModel = function() {
            /** @type {Array} */
            target.a = [];
            var a = new show("_/jserror", void 0, true);
            a = new Span(a);
            node.b = a;
            isUndefinedOrNull(a);
            /** @type {null} */
            var type = null;
            /**
             * @param {string} types
             * @return {undefined}
             */
            a = function(types) {
                if (self.l.$googDebugFname) {
                    if (types) {
                        if (types.message) {
                            if (!types.fileName) {
                                types.message += " in " + self.l.$googDebugFname;
                            }
                        }
                    }
                }
                if (type) {
                    if (types) {
                        if (types.message) {
                            types.message += " [Possibly caused by: " + type + "]";
                        }
                    }
                } else {
                    /** @type {string} */
                    type = String(types);
                }
                handleError(types);
            };
            self.wa("_DumpException", a);
            self.wa("_B_err", a);
            (0, self.u)([self.l].concat([]), self.xa(open, self.xa(onerror, false), true));
            if (!self.x || self.Pb(10)) {
                a = new m(getLogger);
                /** @type {boolean} */
                a.c = true;
                /** @type {boolean} */
                a.a = true;
                isArguments(a);
                hasKey(a, "setTimeout");
                hasKey(a, "setInterval");
                getEnumerableProperties(a);
                node.a = a;
            }
        };
        /**
         * @param {?} er
         * @param {Object} e
         * @return {undefined}
         */
        var onerror = function(er, e) {
            if (!(-1 != e.message.indexOf("Error in protected function: "))) {
                if (e.error && e.error.stack) {
                    handleError(e.error);
                } else {
                    if (!er) {
                        handleError(e);
                    }
                }
            }
        };
        /**
         * @return {undefined}
         */
        var KlassC = function() {
        };
        /** @type {null} */
        KlassC.prototype.a = null;
        /** @type {null} */
        KlassC.prototype.b = null;
        var node = new KlassC;
        /**
         * @param {string} formatString
         * @param {?} var_args
         * @return {undefined}
         */
        var format = function(formatString, var_args) {
            self.N.call(this);
            if (this.j = !!var_args) {
                if (parseModel(), this.a = node.b) {
                    this.b = new self.J;
                    this.c = (0, self.r)(this.w, this);
                    this.a.a.push(this.c);
                }
            } else {
                this.h = new show(formatString, void 0, void 0);
                this.listen(this.h, "g", this.l);
            }
        };
        self.p(format, self.N);
        /**
         * @return {undefined}
         */
        format.prototype.B = function() {
            self.H(this.h);
            if (this.j) {
                self.H(node.a);
                if (this.a) {
                    self.gb(this.a.a, this.c);
                }
            }
            format.D.B.call(this);
        };
        /**
         * @param {?} str
         * @param {Object} e
         * @return {undefined}
         */
        format.prototype.w = function(str, e) {
            var MSG_CLOSURE_CUSTOM_COLOR_BUTTON = str.name || str.message ? str.name + ": " + str.message : e.message;
            var storageKey = "Browser stack: " + str.stack + "\nContext stack:\n" + e["call-stack"];
            if (self.Lc(this.b, storageKey)) {
                this.b.get(storageKey).lc++;
            } else {
                this.b.set(storageKey, {
                    title : MSG_CLOSURE_CUSTOM_COLOR_BUTTON,
                    stack : storageKey,
                    timestamp : (0, self.sa)(),
                    lc : 1
                });
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        format.prototype.l = function(types) {
            if (types = types.error) {
                try {
                    /** @type {string} */
                    var ready = "Message: " + types.message + "\nFile name: " + types.fileName + "\nLine number: " + types.lineNumber + "\n\nBrowser stack:\n" + types.stack + "\n\nJS stack traversal:\n" + inherit();
                    if (window.document.defaultView) {
                        if (window.document.defaultView.console) {
                            window.document.defaultView.console.error(ready);
                        }
                    }
                } catch (c) {
                }
            }
        };
        /**
         * @param {?} origin
         * @param {boolean} c
         * @return {undefined}
         */
        var Particle = function(origin, c) {
            this.b = self.ib(origin);
            /** @type {number} */
            this.a = 0;
            this.c = c || false;
        };
        /**
         * @return {undefined}
         */
        Particle.prototype.reset = function() {
            /** @type {number} */
            this.a = 0;
        };
        /**
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        Particle.prototype.N = function(dataAndEvents) {
            var a = this.b[this.a];
            /** @type {number} */
            a = (0.5 + 1 * Math.random()) * a;
            if (dataAndEvents) {
                intersectObject(this);
            }
            return a;
        };
        /**
         * @param {Object} object
         * @return {undefined}
         */
        var intersectObject = function(object) {
            if (!(object.c && object.a == object.b.length - 1)) {
                object.a++;
            }
        };
        var sandboxIn = {};
        /**
         * @return {undefined}
         */
        var EVENT_READY = function() {
            delete sandboxIn.R;
        };
        /**
         * @return {undefined}
         */
        var oncomplete = function() {
            sandboxIn.R(2);
        };
        /**
         * @return {undefined}
         */
        var restoreScript = function() {
            sandboxIn.R(3);
        };
        /**
         * @param {?} arg
         * @param {?} b
         * @param {string} string
         * @return {undefined}
         */
        var Color = function(arg, b, string) {
            self.F.call(this);
            this.c = null != string ? (0, self.r)(arg, string) : arg;
            this.b = b;
            this.R = (0, self.r)(this.jc, this);
            /** @type {Array} */
            this.a = [];
        };
        self.p(Color, self.F);
        self.g = Color.prototype;
        /** @type {boolean} */
        self.g.Pa = false;
        /** @type {null} */
        self.g.sa = null;
        /**
         * @param {string} opt_attributes
         * @return {undefined}
         */
        self.g.Pb = function(opt_attributes) {
            /** @type {Arguments} */
            this.a = arguments;
            if (this.sa) {
                /** @type {boolean} */
                this.Pa = true;
            } else {
                arrayRemove(this);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.stop = function() {
            if (this.sa) {
                self.Ce(this.sa);
                /** @type {null} */
                this.sa = null;
                /** @type {boolean} */
                this.Pa = false;
                /** @type {Array} */
                this.a = [];
            }
        };
        /**
         * @return {undefined}
         */
        self.g.B = function() {
            Color.D.B.call(this);
            this.stop();
        };
        /**
         * @return {undefined}
         */
        self.g.jc = function() {
            /** @type {null} */
            this.sa = null;
            if (this.Pa) {
                /** @type {boolean} */
                this.Pa = false;
                arrayRemove(this);
            }
        };
        /**
         * @param {Object} item
         * @return {undefined}
         */
        var arrayRemove = function(item) {
            item.sa = self.Be(item.R, item.b);
            item.c.apply(null, item.a);
        };
        var searchTerm;
        var rtv;
        /**
         * @return {?}
         */
        var getElementById = function() {
            if (!rtv) {
                rtv = new self.L;
            }
            return rtv;
        };
        /**
         * @param {Function} onComplete
         * @param {boolean} recurring
         * @param {Object} value
         * @return {undefined}
         */
        var finish = function(onComplete, recurring, value) {
            if (value && (searchTerm && value != searchTerm)) {
                onComplete();
            } else {
                call(self.xa(cont, onComplete, recurring));
            }
        };
        /**
         * @param {Function} property
         * @return {undefined}
         */
        var addListener = function(property) {
            window.chrome.identity.onSignInChanged.addListener(win);
            window.chrome.alarms.onAlarm.addListener(element);
            window.addEventListener("online", completed);
            cont(property);
        };
        /**
         * @return {undefined}
         */
        var fix = function() {
            window.chrome.alarms.clear("auth-token");
            window.chrome.alarms.onAlarm.removeListener(element);
            window.chrome.identity.onSignInChanged.removeListener(win);
            match(void 0);
            $ul.stop();
            window.removeEventListener("online", completed);
        };
        var _this = new Particle([1, 2, 3, 4], true);
        var $ul = new Color(self.xa(finish, self.Da, false), 6E4);
        var completed = (0, self.r)($ul.Pb, $ul);
        /**
         * @param {Object} opt_attributes
         * @return {undefined}
         */
        var match = function(opt_attributes) {
            if (opt_attributes || searchTerm != opt_attributes) {
                /** @type {Object} */
                searchTerm = opt_attributes;
                self.M(getElementById(), "h");
            }
        };
        /**
         * @param {Function} style
         * @param {?} index
         * @return {undefined}
         */
        var cont = function(style, index) {
            if (window.chrome.identity) {
                self.S("Getting new auth token.");
                window.chrome.identity.getAuthToken({
                    interactive : !!index
                }, function(key) {
                    if (key) {
                        self.S("Got auth token.");
                        _this.reset();
                    } else {
                        self.S("Didn't get auth token: %s", window.chrome.runtime.lastError.message);
                    }
                    match(key);
                    /** @type {number} */
                    key = searchTerm ? 15 : Math.max(1, _this.N(true));
                    self.S("Setting up an auth token alarm for %s minutes.", key);
                    window.chrome.alarms.create("auth-token", {
                        periodInMinutes : key
                    });
                    style();
                });
            } else {
                style();
            }
        };
        /**
         * @param {Function} callback
         * @return {undefined}
         */
        var call = function(callback) {
            if (window.chrome.identity) {
                if (searchTerm) {
                    self.S("Removing cached auth token.");
                    window.chrome.identity.removeCachedAuthToken({
                        token : searchTerm
                    }, callback);
                } else {
                    callback();
                }
            } else {
                callback();
            }
        };
        /**
         * @param {Function} settings
         * @return {undefined}
         */
        var element = function(settings) {
            if ("auth-token" == settings.name) {
                cont(self.Da);
            }
        };
        /**
         * @param {?} xxx
         * @param {?} position
         * @return {undefined}
         */
        var win = function(xxx, position) {
            if (position) {
                self.S("Signing in detected.");
                finish(oncomplete, false);
            } else {
                self.S("Signing out detected.");
                call(restoreScript);
                match(void 0);
            }
        };
        var storageKey = new self.O("a");
        var modalInstance = new self.O("b");
        var pageId = new self.O("c");
        var actualKey = new self.O("d");
        var elem = new self.O("e");
        var blockId = new self.O("f");
        var term = new self.O("g");
        var callee = new self.O("h");
        var k = new self.O("i");
        var key = new self.O("j");
        var it = new self.O("k");
        var arrayBuf = new self.O("l");
        var key2 = new self.O("m");
        var KEY = new self.O("n");
        var subKey = new self.O("o");
        var callbackId = new self.O("p");
        /**
         * @return {undefined}
         */
        var child = function() {
            self.F.call(this);
            this.a = new stream;
        };
        self.p(child, self.F);
        /**
         * @param {?} arg
         * @param {boolean} recurring
         * @return {?}
         */
        child.prototype.pa = function(arg, recurring) {
            var index;
            var codeSegments = this.a.V();
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                try {
                    index = codeSegments[i](arg, recurring);
                } catch (j) {
                    self.S(j);
                }
            }
            return index;
        };
        /**
         * @param {Function} name
         * @return {undefined}
         */
        child.prototype.addListener = function(name) {
            this.a.a.set(forEach(name), name);
        };
        /**
         * @param {Function} name
         * @return {undefined}
         */
        child.prototype.removeListener = function(name) {
            this.a.remove(name);
        };
        /**
         * @return {undefined}
         */
        child.prototype.B = function() {
            this.a.clear();
            child.D.B.call(this);
        };
        /**
         * @param {?} b
         * @param {number} g
         * @param {?} children
         * @return {undefined}
         */
        var Node = function(b, g, children) {
            self.F.call(this);
            this.b = b;
            this.g = g || 0;
            this.c = children;
            this.R = (0, self.r)(this.h, this);
        };
        self.p(Node, self.F);
        /** @type {number} */
        Node.prototype.a = 0;
        /**
         * @return {undefined}
         */
        Node.prototype.B = function() {
            Node.D.B.call(this);
            this.stop();
            delete this.b;
            delete this.c;
        };
        /**
         * @param {Object} node
         * @param {string} ready
         * @return {undefined}
         */
        var generate = function(node, ready) {
            node.stop();
            node.a = self.Be(node.R, self.m(ready) ? ready : node.g);
        };
        /**
         * @return {undefined}
         */
        Node.prototype.stop = function() {
            if (0 != this.a) {
                self.Ce(this.a);
            }
            /** @type {number} */
            this.a = 0;
        };
        /**
         * @return {undefined}
         */
        Node.prototype.h = function() {
            /** @type {number} */
            this.a = 0;
            if (this.b) {
                this.b.call(this.c);
            }
        };
        /**
         * @param {?} top
         * @param {?} a
         * @param {?} b
         * @param {?} left
         * @return {undefined}
         */
        var Bounds = function(top, a, b, left) {
            this.top = top;
            this.a = a;
            this.b = b;
            this.left = left;
        };
        /**
         * @param {Object} parent
         * @return {?}
         */
        Bounds.prototype.contains = function(parent) {
            return this && parent ? parent instanceof Bounds ? parent.left >= this.left && (parent.a <= this.a && (parent.top >= this.top && parent.b <= this.b)) : parent.x >= this.left && (parent.x <= this.a && (parent.y >= this.top && parent.y <= this.b)) : false;
        };
        /**
         * @return {?}
         */
        Bounds.prototype.ceil = function() {
            /** @type {number} */
            this.top = Math.ceil(this.top);
            /** @type {number} */
            this.a = Math.ceil(this.a);
            /** @type {number} */
            this.b = Math.ceil(this.b);
            /** @type {number} */
            this.left = Math.ceil(this.left);
            return this;
        };
        /**
         * @return {?}
         */
        Bounds.prototype.floor = function() {
            /** @type {number} */
            this.top = Math.floor(this.top);
            /** @type {number} */
            this.a = Math.floor(this.a);
            /** @type {number} */
            this.b = Math.floor(this.b);
            /** @type {number} */
            this.left = Math.floor(this.left);
            return this;
        };
        /**
         * @return {?}
         */
        Bounds.prototype.round = function() {
            /** @type {number} */
            this.top = Math.round(this.top);
            /** @type {number} */
            this.a = Math.round(this.a);
            /** @type {number} */
            this.b = Math.round(this.b);
            /** @type {number} */
            this.left = Math.round(this.left);
            return this;
        };
        /**
         * @param {string} ui
         * @param {number} f
         * @param {?} val
         * @param {?} h
         * @return {undefined}
         */
        var slide = function(ui, f, val, h) {
            /** @type {string} */
            this.left = ui;
            /** @type {number} */
            this.top = f;
            this.width = val;
            this.height = h;
        };
        /**
         * @param {Object} a
         * @param {Object} b
         * @return {?}
         */
        var assertEquals = function(a, b) {
            return a == b ? true : a && b ? a.left == b.left && (a.width == b.width && (a.top == b.top && a.height == b.height)) : false;
        };
        /**
         * @param {Object} rect
         * @return {?}
         */
        slide.prototype.contains = function(rect) {
            return rect instanceof Rect ? rect.x >= this.left && (rect.x <= this.left + this.width && (rect.y >= this.top && rect.y <= this.top + this.height)) : this.left <= rect.left && (this.left + this.width >= rect.left + rect.width && (this.top <= rect.top && this.top + this.height >= rect.top + rect.height));
        };
        /**
         * @param {?} b
         * @param {Object} a
         * @return {?}
         */
        var scrollTo = function(b, a) {
            /** @type {number} */
            var z0 = a.x < b.left ? b.left - a.x : Math.max(a.x - (b.left + b.width), 0);
            /** @type {number} */
            var z1 = a.y < b.top ? b.top - a.y : Math.max(a.y - (b.top + b.height), 0);
            return z0 * z0 + z1 * z1;
        };
        /**
         * @return {?}
         */
        slide.prototype.ceil = function() {
            /** @type {number} */
            this.left = Math.ceil(this.left);
            /** @type {number} */
            this.top = Math.ceil(this.top);
            /** @type {number} */
            this.width = Math.ceil(this.width);
            /** @type {number} */
            this.height = Math.ceil(this.height);
            return this;
        };
        /**
         * @return {?}
         */
        slide.prototype.floor = function() {
            /** @type {number} */
            this.left = Math.floor(this.left);
            /** @type {number} */
            this.top = Math.floor(this.top);
            /** @type {number} */
            this.width = Math.floor(this.width);
            /** @type {number} */
            this.height = Math.floor(this.height);
            return this;
        };
        /**
         * @return {?}
         */
        slide.prototype.round = function() {
            /** @type {number} */
            this.left = Math.round(this.left);
            /** @type {number} */
            this.top = Math.round(this.top);
            /** @type {number} */
            this.width = Math.round(this.width);
            /** @type {number} */
            this.height = Math.round(this.height);
            return this;
        };
        /**
         * @param {?} j
         * @param {?} ctx
         * @param {?} n
         * @return {undefined}
         */
        var f = function(j, ctx, n) {
            self.F.call(this);
            this.j = j;
            /** @type {boolean} */
            this.G = !!ctx;
            this.Ga = n;
            /** @type {Array} */
            this.v = [];
            /** @type {null} */
            this.b = this.c = this.a = null;
            this.onBoundsChanged = new child;
            self.I(this, this.onBoundsChanged);
            /** @type {null} */
            this.I = null;
            if (ctx) {
                if (self.Hb) {
                    this.I = new merge(this);
                }
            }
            if (fk) {
                this.H = new Node(this.Fa, 5E3, this);
            }
        };
        self.p(f, self.F);
        var fk = self.Gb || false;
        /**
         * @return {undefined}
         */
        f.prototype.B = function() {
            if (this.b) {
                if (!this.b.closed) {
                    lookupIterator(this);
                    /** @type {null} */
                    this.b = null;
                }
            }
            if (this.a) {
                var rvar = this.a;
                /** @type {null} */
                this.a = null;
                rvar.close();
            }
            if (this.c) {
                rvar = this.c;
                /** @type {null} */
                this.c = null;
                window.chrome.windows.remove(rvar, function() {
                });
            }
            f.D.B.call(this);
        };
        /**
         * @param {string} options
         * @return {undefined}
         */
        f.prototype.create = function(options) {
            if (window.chrome.app.window) {
                animate(this, options);
            } else {
                if (window.chrome.windows) {
                    success(this, options);
                }
            }
        };
        /**
         * @param {Object} obj
         * @param {Object} options
         * @return {undefined}
         */
        var animate = function(obj, options) {
            var opts = {};
            (0, self.u)("id focused visibleOnAllWorkspaces alwaysOnTop frame alphaEnabled hidden resizable state".split(" "), function(item) {
                if (null != options[item]) {
                    opts[item] = options[item];
                }
            });
            opts.outerBounds = {
                width : Math.round(options.width),
                height : Math.round(options.height)
            };
            if (null != options.top) {
                if (null != options.left) {
                    /** @type {number} */
                    opts.outerBounds.top = Math.round(options.top);
                    /** @type {number} */
                    opts.outerBounds.left = Math.round(options.left);
                }
            }
            if (obj.G) {
                opts.id = obj.Ga;
            }
            if (!(null != opts.focused)) {
                /** @type {boolean} */
                opts.focused = false;
            }
            sort(obj.I, !!opts.alwaysOnTop);
            window.chrome.app.window.create(options.url, opts, (0, self.r)(obj.Ha, obj, opts));
        };
        /**
         * @param {?} key
         * @param {Object} args
         * @return {undefined}
         */
        var success = function(key, args) {
            var opts = {
                type : "popup"
            };
            (0, self.u)(["url", "focused", "state", "type"], function(i) {
                if (null != args[i]) {
                    opts[i] = args[i];
                }
            });
            (0, self.u)(["left", "top", "width", "height"], function(i) {
                if (null != args[i]) {
                    /** @type {number} */
                    opts[i] = Math.round(args[i]);
                }
            });
            opts.url += "?" + args.id;
            elements[args.id] = (0, self.r)(key.Ca, key, opts);
            window.chrome.windows.create(opts, (0, self.r)(key.Ia, key, opts));
        };
        /**
         * @param {?} elem
         * @param {Object} data
         * @return {undefined}
         */
        f.prototype.Ha = function(elem, data) {
            var value = this;
            if (data) {
                if (this.K) {
                    data.close();
                } else {
                    /** @type {Object} */
                    this.a = data;
                    this.b = data.contentWindow;
                    var options = elem.outerBounds;
                    if (!data.id) {
                        if (options) {
                            if (null != options.left) {
                                if (null != options.left) {
                                    data.outerBounds.setPosition(options.left, options.top);
                                }
                            }
                        }
                    }
                    data.onBoundsChanged.addListener(function() {
                        return lookupIterator(value);
                    });
                    data.onClosed.addListener(function() {
                        if (value.a) {
                            if (!value.K) {
                                value.A();
                            }
                        }
                    });
                    var doc = this.b.document;
                    if ("complete" == doc.readyState) {
                        check(this);
                    } else {
                        doc.addEventListener("readystatechange", function() {
                            if ("complete" == doc.readyState) {
                                check(value);
                            }
                        });
                    }
                }
            } else {
                self.S("Failed to create an app window: %s", window.chrome.runtime.lastError && window.chrome.runtime.lastError.message || "");
            }
        };
        /**
         * @param {?} v12
         * @param {Element} m
         * @return {undefined}
         */
        f.prototype.Ia = function(v12, m) {
            var value = this;
            if (m) {
                if (this.K) {
                    window.chrome.windows.remove(m.id);
                } else {
                    this.c = m.id;
                    var cycle = new self.ze(1E4);
                    self.I(this, cycle);
                    cycle.listen("tick", function() {
                        return lookupIterator(value);
                    });
                    /**
                     * @param {(Object|string)} dt
                     * @return {undefined}
                     */
                    var tick = function(dt) {
                        if (dt == value.c) {
                            self.Ae(cycle);
                        } else {
                            if (cycle.b) {
                                cycle.stop();
                                lookupIterator(value);
                            }
                        }
                    };
                    /**
                     * @param {(Object|string)} clazz
                     * @return {undefined}
                     */
                    var any = function(clazz) {
                        if (value.c) {
                            if (clazz == value.c) {
                                if (!value.K) {
                                    value.A();
                                }
                            }
                        }
                    };
                    window.chrome.windows.onFocusChanged.addListener(tick);
                    window.chrome.windows.onRemoved.addListener(any);
                    self.G(this, function() {
                        window.chrome.windows.onFocusChanged.removeListener(tick);
                        window.chrome.windows.onRemoved.removeListener(any);
                    });
                    check(this);
                }
            } else {
                self.H(this);
            }
        };
        /**
         * @param {?} fn
         * @param {?} s
         * @return {undefined}
         */
        f.prototype.Ca = function(fn, s) {
            if (this.K) {
                s.close();
            } else {
                this.b = s;
                check(this);
            }
        };
        /**
         * @param {Object} object
         * @return {undefined}
         */
        var check = function(object) {
            if ((object.a || object.c) && object.b) {
                if (isObject(object)) {
                    self.H(object);
                } else {
                    var element = object.b.document.body;
                    var options = object.j;
                    if (element) {
                        if (options = "number" == typeof options ? 0 < options ? 1 : 0 > options ? -1 : 0 : null == options ? null : options ? -1 : 1) {
                            /** @type {string} */
                            element.style.textAlign = -1 == options ? "right" : "left";
                            /** @type {string} */
                            element.dir = -1 == options ? "rtl" : "ltr";
                        }
                    }
                    if (null != object.v) {
                        for (;object.v.length && !isObject(object);) {
                            object.v.shift().call(object, object);
                        }
                        /** @type {null} */
                        object.v = null;
                    }
                }
            }
        };
        /**
         * @param {Object} object
         * @param {Function} value
         * @return {undefined}
         */
        var set = function(object, value) {
            if (isObject(object)) {
                self.H(object);
            } else {
                if (null == object.v) {
                    value.call(object, object);
                } else {
                    object.v.push(value);
                }
            }
        };
        /**
         * @param {Object} value
         * @return {?}
         */
        var isObject = function(value) {
            return!(!value.b || !value.b.closed);
        };
        /**
         * @param {Object} a
         * @return {?}
         */
        var equal = function(a) {
            return!a.b || isObject(a) ? null : a.b;
        };
        /**
         * @param {Object} options
         * @return {?}
         */
        var when = function(options) {
            return go(options.I) ? self.od(false) : options.b && (options.b.document && options.b.document.hasFocus()) ? self.od(true) : options.a ? self.od(!options.a.isMinimized() && options.a.isAlwaysOnTop()) : options.c ? createOptions(options).then(function(attrs) {
                return attrs ? "minimized" != attrs.state && attrs.alwaysOnTop || attrs.focused : false;
            }) : self.od(false);
        };
        /**
         * @param {Object} options
         * @return {?}
         */
        var createOptions = function(options) {
            return new self.kd(function(deepDataAndEvents) {
                return window.chrome.windows.get(options.c, {}, deepDataAndEvents);
            });
        };
        /**
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        f.prototype.focus = function(dataAndEvents) {
            var failuresLink = this;
            if (dataAndEvents) {
                focus(this);
            } else {
                set(this, function() {
                    return focus(failuresLink);
                });
            }
        };
        /**
         * @param {Object} el
         * @return {undefined}
         */
        var focus = function(el) {
            if (el.a) {
                el.a.focus();
            } else {
                if (el.c) {
                    window.chrome.windows.update(el.c, {
                        focused : true
                    });
                }
            }
        };
        /**
         * @param {Object} node
         * @param {boolean} o
         * @return {undefined}
         */
        var compile = function(node, o) {
            set(node, function() {
                if (o) {
                    if (node.H) {
                        generate(node.H);
                    }
                }
                if (node.a) {
                    if (o) {
                        node.a.drawAttention();
                    } else {
                        node.a.clearAttention();
                    }
                } else {
                    if (node.c) {
                        window.chrome.windows.update(node.c, {
                            drawAttention : o
                        });
                    }
                }
            });
        };
        /**
         * @return {undefined}
         */
        f.prototype.Fa = function() {
            var node = this;
            window.chrome.idle.queryState(Math.max(15, 10), function(total) {
                if ("active" == total) {
                    compile(node, false);
                } else {
                    generate(node.H);
                }
            });
        };
        /**
         * @param {Object} obj
         * @param {?} n
         * @return {undefined}
         */
        var size = function(obj, n) {
            set(obj, function() {
                var result;
                if (result = obj.a && obj.a.setShape) {
                    if (result = obj.I) {
                        result.g = n;
                        /** @type {boolean} */
                        result = !result.a;
                    } else {
                        /** @type {boolean} */
                        result = true;
                    }
                }
                if (result) {
                    obj.a.setShape({
                        rects : n
                    });
                }
            });
        };
        /**
         * @param {Object} obj
         * @param {boolean} files
         * @return {undefined}
         */
        var json = function(obj, files) {
            set(obj, function() {
                if (sort(obj.I, files)) {
                    if (obj.a) {
                        obj.a.setAlwaysOnTop(files);
                    }
                }
            });
        };
        /**
         * @param {Object} root
         * @param {?} options
         * @return {undefined}
         */
        var getVersion = function(root, options) {
            set(root, function() {
                return root.a && root.a.setVisibleOnAllWorkspaces(options);
            });
        };
        /**
         * @return {undefined}
         */
        f.prototype.show = function() {
            var opts = this;
            set(this, function() {
                return opts.a && opts.a.show(false);
            });
        };
        /**
         * @return {undefined}
         */
        f.prototype.hide = function() {
            var row = this;
            set(this, function() {
                return row.a && row.a.hide();
            });
        };
        /**
         * @param {Object} item
         * @return {?}
         */
        var getSize = function(item) {
            return item.a ? (item = item.a.outerBounds, new slide(item.left, item.top, item.width, item.height)) : item.b && (item.b.outerWidth && item.b.outerHeight) ? new slide(item.b.screenX, item.b.screenY, item.b.outerWidth, item.b.outerHeight) : null;
        };
        /**
         * @param {number} x
         * @param {number} y
         * @return {undefined}
         */
        f.prototype.setPosition = function(x, y) {
            if (this.a) {
                /** @type {number} */
                this.a.outerBounds.left = x;
                /** @type {number} */
                this.a.outerBounds.top = y;
            } else {
                if (this.c) {
                    window.chrome.windows.update(this.c, {
                        left : x,
                        top : y
                    });
                }
            }
        };
        /**
         * @param {Object} obj
         * @param {number} keypath
         * @return {undefined}
         */
        var setModel = function(obj, keypath) {
            set(obj, function() {
                return obj.b.resizeBy(keypath, 0);
            });
        };
        /**
         * @param {Object} obj
         * @param {string} type
         * @return {undefined}
         */
        var expect = function(obj, type) {
            set(obj, function() {
                /** @type {number} */
                var W = Math.round(self.A(type, 3));
                /** @type {number} */
                var H = Math.round(self.A(type, 4));
                obj.b.resizeTo(W, H);
                obj.setPosition(Math.round(self.A(type, 1)), Math.round(self.A(type, 2)));
            });
        };
        /**
         * @param {string} str
         * @return {undefined}
         */
        f.prototype.Ba = function(str) {
            var elem = this;
            set(this, function() {
                return elem.b.document.title = str;
            });
        };
        /**
         * @param {?} value
         * @return {undefined}
         */
        var lookupIterator = function(value) {
            if (!value.K) {
                value.onBoundsChanged.pa(value);
            }
        };
        /**
         * @return {undefined}
         */
        f.prototype.A = function() {
            lookupIterator(this);
            /** @type {null} */
            this.a = this.c = this.b = null;
            self.H(this);
        };
        var elements = {};
        self.wa("__onExtWindowLoad", function(doc) {
            var i = doc.location.search.substring(1);
            if (elements[i]) {
                elements[i](doc);
                delete elements[i];
            } else {
                doc.close();
            }
        });
        /**
         * @param {?} b
         * @return {undefined}
         */
        var merge = function(b) {
            var args = this;
            /** @type {boolean} */
            this.a = false;
            this.b = b;
            /** @type {Array} */
            this.g = [];
            /** @type {boolean} */
            this.c = true;
            set(this.b, function() {
                return self.ge(equal(args.b), "focus", function() {
                    var a;
                    if (args && args.a) {
                        /** @type {boolean} */
                        args.a = false;
                        args.b.a.setShape({
                            rects : args.g
                        });
                        args.b.a.setAlwaysOnTop(args.c);
                        /** @type {boolean} */
                        a = false;
                    } else {
                        /** @type {boolean} */
                        a = true;
                    }
                    return a;
                });
            });
        };
        /**
         * @param {Node} object
         * @return {?}
         */
        var go = function(object) {
            return!(!object || !object.a);
        };
        /**
         * @param {Object} obj
         * @param {boolean} list
         * @return {?}
         */
        var sort = function(obj, list) {
            if (!obj) {
                return true;
            }
            /** @type {boolean} */
            obj.c = list;
            return!obj.a;
        };
        var recurring = self.eb(window.chrome.runtime.getManifest().permissions, "webview");
        var identity = self.eb(window.chrome.runtime.getManifest().permissions, "identity");
        var cookies = self.eb(window.chrome.runtime.getManifest().permissions, "cookies");
        var Ck = self.eb(window.chrome.runtime.getManifest().permissions, "app.window.shape");
        /**
         * @param {Node} target
         * @return {undefined}
         */
        var activate = function(target) {
            var object = this;
            self.L.call(this);
            this.g = target.get(elem);
            this.b = target.get(actualKey);
            /** @type {null} */
            this.a = null;
            self.G(this, function() {
                return self.H(object.a);
            });
            onLoad(this);
        };
        self.p(activate, self.L);
        var out = new self.Rd("i");
        /** @type {Array} */
        var bytenew = ["SSID", "SAPISID"];
        /**
         * @param {Node} c
         * @return {undefined}
         */
        var colorChanged = function(c) {
            if (identity) {
                finish(function() {
                }, true);
            } else {
                if (cookies) {
                    if (c.a) {
                        c.a.focus(true);
                    } else {
                        c.a = new f(c.b.j());
                        c.a.create({
                            url : c.g.j,
                            type : "normal",
                            width : 800,
                            height : 800
                        });
                        self.G(c.a, function() {
                            return c.a = null;
                        });
                    }
                }
            }
        };
        /**
         * @param {string} ready
         * @return {undefined}
         */
        var onLoad = function(ready) {
            if (identity) {
                getElementById().listen("h", function() {
                    return self.M(ready, out);
                });
                addListener(function() {
                });
                self.G(ready, fix);
            } else {
                if (cookies) {
                    /**
                     * @param {Object} data
                     * @return {undefined}
                     */
                    var init = function(data) {
                        if (!data.removed) {
                            if (".google.com" == data.cookie.domain) {
                                if (self.eb(bytenew, data.cookie.name)) {
                                    self.H(ready.a);
                                    self.M(ready, out);
                                }
                            }
                        }
                    };
                    window.chrome.cookies.onChanged.addListener(init);
                    self.G(ready, function() {
                        return window.chrome.cookies.onChanged.removeListener(init);
                    });
                }
            }
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var checked = function(value) {
            self.z(this, value, "capi:oa_tc", null);
        };
        self.p(checked, self.y);
        /** @type {string} */
        checked.messageId = "capi:oa_tc";
        /**
         * @param {Node} watch
         * @return {undefined}
         */
        var Player = function(watch) {
            this.a = watch.get(self.T);
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var val = function(value) {
            var a = this;
            self.L.call(this);
            /** @type {number} */
            this.g = value;
            this.a = value.get(self.T);
            /** @type {number} */
            this.j = 0;
            self.G(this, function() {
                return self.Ce(a.j);
            });
            this.b = value.get(key);
            this.A = new Player(value);
            /** @type {string} */
            this.h = "";
            window.chrome.runtime.onUpdateAvailable.addListener(function(data) {
                self.S("Update available; details: '%s'", JSON.stringify(data));
                if (a.h != data.version) {
                    a.h = data.version;
                    compare(a, false, 864E5 + 288E5 * Math.random());
                    every(a, true);
                }
            });
            var cycle = new self.N(this);
            self.I(this, cycle);
            cycle.listen(this.a.J(), "uv_r_rr", this.T);
            cycle.listen(this.a.J(), "uv_r_rdr", this.M);
            cycle.listen(this.a.J(), "uv_r_rrr", this.W);
            cycle.listen(this.a.J(), "h_cs", this.l);
            this.l();
            cycle.listen(this.a.J(), "uv_r_hrd", this.O);
            cycle.listen(this.a.J(), "uvc", this.P);
            cycle.listen(this.a.J(), "h_odd", this.L);
            cycle.listen(getElementById(), "h", this.v);
            cycle.listen(value.get(elem), ready, this.w);
            every(this, true);
        };
        self.p(val, self.L);
        /**
         * @param {string} types
         * @return {undefined}
         */
        val.prototype.T = function(types) {
            types = self.A(new self.If(types.S), 1);
            this.b.set("UvReloadState", types).then(function() {
                return window.chrome.runtime.reload();
            });
        };
        /**
         * @return {undefined}
         */
        val.prototype.M = function() {
            var recurring = this.b.get("UvReloadState");
            var dblclick = new self.Hf;
            self.B(dblclick, 1, recurring);
            this.a.sendMessage(dblclick);
        };
        /**
         * @return {undefined}
         */
        val.prototype.W = function() {
            this.b.remove("UvReloadState");
        };
        /**
         * @param {Node} b
         * @param {boolean} recurring
         * @param {number} a
         * @return {undefined}
         */
        var compare = function(b, recurring, a) {
            var source = b.K ? null : b.g.get(elem);
            if (source && (source.g() && (1 != source.a && (3 != source.a && 6 != source.a)))) {
                source = new self.Jf;
                self.B(source, 1, recurring);
                b.a.sendMessage(source);
            } else {
                /** @type {boolean} */
                recurring = true;
            }
            /** @type {number} */
            a = Math.max(a, 1E4);
            self.Be(function() {
                if (recurring) {
                    window.chrome.runtime.reload();
                } else {
                    compare(this, true, 1E4);
                }
            }, a, b);
        };
        /**
         * @return {undefined}
         */
        val.prototype.l = function() {
            self.Ce(this.j);
            this.j = self.Be(this.H, 3E4, this);
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        val.prototype.P = function(types) {
            this.b.set("UvReloadConfig", types.S);
            if (!every(this, false)) {
                M(this);
            }
        };
        /**
         * @return {undefined}
         */
        val.prototype.L = function() {
            self.M(this, "k");
        };
        /**
         * @param {Node} t
         * @param {boolean} recurring
         * @return {?}
         */
        var every = function(t, recurring) {
            var ready = new template(t.b.get("UvReloadConfig") || []);
            var v = window.chrome.runtime.getManifest().version;
            var attributes = self.A(ready, 1) || 1;
            if (0 <= self.Xa(v, attributes)) {
                return true;
            }
            if (!t.h || 0 > self.Xa(t.h, attributes)) {
                return recurring && push(t.g.get(elem), 10), false;
            }
            ready = self.C(ready, z, 2);
            v = self.A(ready, 1) + Math.random() * self.A(ready, 3);
            var loose = self.A(ready, 2) + Math.random() * self.A(ready, 3);
            self.Be(function() {
                return compare(t, false, loose);
            }, v);
            return true;
        };
        /**
         * @return {undefined}
         */
        val.prototype.H = function() {
            if (!removeNode(this.g.get(actualKey).a)) {
                oncomplete();
            }
        };
        /**
         * @return {undefined}
         */
        val.prototype.O = function() {
            self.M(this, "j");
        };
        /**
         * @return {undefined}
         */
        val.prototype.v = function() {
            if (searchTerm) {
                var w = this.A;
                var recurring = searchTerm;
                var dblclick = new checked;
                self.B(dblclick, 1, recurring);
                w.a.sendMessage(dblclick);
            }
        };
        /**
         * @return {undefined}
         */
        val.prototype.w = function() {
            if (4 == this.g.get(elem).a) {
                this.v();
            }
        };
        /**
         * @param {Node} a
         * @return {undefined}
         */
        var M = function(a) {
            window.chrome.runtime.requestUpdateCheck(function(key, msg) {
                self.S("Update check status: '%s', details: '%s'", key, JSON.stringify(msg));
                if (!msg.version) {
                    every(a, true);
                }
            });
        };
        /**
         * @param {Element} message
         * @return {undefined}
         */
        var info = function(message) {
            self.L.call(this);
            /** @type {number} */
            this.a = 1;
            /** @type {null} */
            this.j = this.h = null;
            message = message.get(self.T);
            message.J().listen("h_exts", this.l, void 0, this);
            this.b = new I(message);
            self.I(this, this.b);
            this.b.listen("d", this.w, void 0, this);
            this.b.listen("b", this.v, void 0, this);
        };
        self.p(info, self.L);
        var cycle = new self.Rd("l");
        var ready = new self.Rd("m");
        /**
         * @return {undefined}
         */
        info.prototype.v = function() {
            if (!this.b.a) {
                push(this, 4);
                self.M(this, cycle);
            }
        };
        /**
         * @return {undefined}
         */
        info.prototype.w = function() {
            switch(this.b.getState()) {
                case rb.rb:
                    push(this, 3);
                    break;
                case rb.sb:
                    if (!this.b.a) {
                        push(this, 4);
                        self.M(this, cycle);
                    }
                    ;
            }
        };
        /**
         * @param {string} orig
         * @param {number} opt_attributes
         * @return {undefined}
         */
        var push = function(orig, opt_attributes) {
            if (orig.a != opt_attributes) {
                self.S("Wabel changing status from: %s to: %s", orig.a, opt_attributes);
                /** @type {number} */
                orig.a = opt_attributes;
                self.M(orig, ready);
                self.M(orig, cycle);
            }
        };
        /**
         * @return {?}
         */
        info.prototype.g = function() {
            return this.b.g();
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        info.prototype.l = function(types) {
            types = new self.Rf(types.S);
            if (self.A(types, 3)) {
                this.h = self.A(types, 3);
            }
            if (self.A(types, 4)) {
                this.j = self.A(types, 4);
            }
            if (!self.A(types, 1)) {
                push(this, 3);
            }
            self.M(this, cycle);
        };
        /**
         * @param {number} entity
         * @return {undefined}
         */
        var which = function(entity) {
            self.z(this, entity, "cpai:w_hsc", null);
        };
        self.p(which, self.y);
        /** @type {string} */
        which.messageId = "cpai:w_hsc";
        /**
         * @param {number} value
         * @return {undefined}
         */
        var token = function(value) {
            self.z(this, value, "h_icr", null);
        };
        self.p(token, self.y);
        /** @type {string} */
        token.messageId = "h_icr";
        /**
         * @return {?}
         */
        token.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} name
         * @return {undefined}
         */
        var atom = function(name) {
            self.z(this, name, "h_opc", null);
        };
        self.p(atom, self.y);
        /** @type {string} */
        atom.messageId = "h_opc";
        /**
         * @param {number} event
         * @return {undefined}
         */
        var focusin = function(event) {
            self.z(this, event, "h_sc", null);
        };
        self.p(focusin, self.y);
        /** @type {string} */
        focusin.messageId = "h_sc";
        /**
         * @param {number} key
         * @return {undefined}
         */
        var date = function(key) {
            self.z(this, key, "h_po", null);
        };
        self.p(date, self.y);
        /** @type {string} */
        date.messageId = "h_po";
        /**
         * @param {(Node|string)} c
         * @return {undefined}
         */
        var c = function(c) {
            var options = this;
            self.N.call(this);
            /** @type {Array} */
            this.j = [];
            /**
             * @param {string} types
             * @return {undefined}
             */
            this.I = function(types) {
                self.gb(options.j, types);
                types.onMessage.removeListener(options.A);
                types.onDisconnect.removeListener(options.I);
            };
            /**
             * @param {string} type
             * @param {number} expectedNumberOfNonCommentArgs
             * @return {undefined}
             */
            this.A = function(type, expectedNumberOfNonCommentArgs) {
                var result = validator(options);
                if ("getCentralJid" == type.eventType) {
                    expectedNumberOfNonCommentArgs.postMessage({
                        jid : result
                    });
                } else {
                    if ("rhsc" == type.eventType) {
                        /** @type {*} */
                        var camelKey = JSON.parse(type.jid);
                        /** @type {string} */
                        result = "";
                        if (self.Lc(options.a, camelKey)) {
                            result = options.a.get(camelKey).context;
                        }
                        expectedNumberOfNonCommentArgs.postMessage({
                            jid : result
                        });
                    } else {
                        if ("po" == type.eventType) {
                            camelKey = type.jid;
                            result = new date;
                            self.B(result, 1, camelKey);
                            options.c.sendMessage(result);
                        } else {
                            if (result) {
                                result = type.jid;
                                switch(type.eventType) {
                                    case "ocfp":
                                        camelKey = new atom(JSON.parse(result)[3]);
                                        break;
                                    case "showChat":
                                        camelKey = new focusin;
                                        self.B(camelKey, 1, result);
                                }
                                if (camelKey) {
                                    options.c.sendMessage(camelKey);
                                }
                            }
                        }
                    }
                }
            };
            /**
             * @param {string} types
             * @return {undefined}
             */
            this.G = function(types) {
                var name = validator(options);
                if (types.name != window.chrome.runtime.id && (types.name && types.name != name)) {
                    types.disconnect();
                } else {
                    options.j.push(types);
                    types.onMessage.addListener(options.A);
                    types.onDisconnect.addListener(options.I);
                }
            };
            /** @type {boolean} */
            this.h = false;
            this.b = c.get(elem);
            this.c = c.get(self.T);
            c = c.get(k);
            this.listen(c, "j", this.w);
            this.w();
            this.a = new self.J;
            this.listen(this.c.J(), "cpai:w_hsc", this.H);
            this.listen(this.b, cycle, this.l);
            self.G(this, this.l, this);
            this.l();
        };
        self.p(c, self.N);
        /**
         * @param {?} options
         * @return {?}
         */
        var validator = function(options) {
            return!options.K && (!options.b.K && (options.b.g() && (4 == options.b.a && options.b.h))) || "";
        };
        /**
         * @return {undefined}
         */
        c.prototype.w = function() {
            var result = new token;
            self.B(result, 2, true);
            this.c.sendMessage(result);
        };
        /**
         * @param {Object} now
         * @return {undefined}
         */
        c.prototype.H = function(now) {
            var suiteView = this;
            var ready = new which(now.S);
            var camelKey = self.A(ready, 1);
            if (camelKey) {
                wrapper(this, camelKey);
                now = new timeout(self.A(ready, 2));
                this.a.set(camelKey, now);
                ready = self.A(ready, 3) || 6E4;
                now.qb = self.Be(function() {
                    return wrapper(suiteView, camelKey);
                }, ready);
            }
        };
        /**
         * @param {?} obj
         * @param {string} opt_key
         * @return {undefined}
         */
        var wrapper = function(obj, opt_key) {
            if (self.Lc(obj.a, opt_key)) {
                var key = obj.a.get(opt_key);
                self.Ce(key.qb);
                obj.a.remove(opt_key);
            }
        };
        /**
         * @return {undefined}
         */
        c.prototype.l = function() {
            if (validator(this)) {
                if (!this.h) {
                    window.chrome.runtime.onConnectExternal.addListener(this.G);
                    /** @type {boolean} */
                    this.h = true;
                }
            } else {
                if (this.h) {
                    window.chrome.runtime.onConnectExternal.removeListener(this.G);
                    (0, self.u)([].concat(this.j), function(connect) {
                        return connect.disconnect();
                    });
                    /** @type {number} */
                    this.j.length = 0;
                    /** @type {boolean} */
                    this.h = false;
                }
            }
        };
        /**
         * @param {?} context
         * @return {undefined}
         */
        var timeout = function(context) {
            this.context = context;
            /** @type {null} */
            this.qb = null;
        };
        /**
         * @param {?} p1
         * @param {?} property
         * @param {boolean} file
         * @param {string} style
         * @param {boolean} operator
         * @return {undefined}
         */
        var result = function(p1, property, file, style, operator) {
            self.F.call(this);
            this.a = p1;
            this.fa = property;
            /** @type {string} */
            this.b = typeof property;
            this.Zb = file ? file : false;
            /** @type {boolean} */
            this.ea = true;
            this.Sb = style ? style : false;
            this.ja = operator ? operator : false;
        };
        self.p(result, self.F);
        self.g = result.prototype;
        /**
         * @return {undefined}
         */
        self.g.B = function() {
            result.D.B.call(this);
            /** @type {string} */
            this.a = "";
            /** @type {null} */
            this.fa = null;
            /** @type {string} */
            this.b = "";
        };
        /**
         * @return {?}
         */
        self.g.id = function() {
            return this.a;
        };
        /**
         * @return {?}
         */
        self.g.type = function() {
            return this.b;
        };
        /**
         * @return {?}
         */
        self.g.N = function() {
            return this.fa;
        };
        /**
         * @param {boolean} dataAndEvents
         * @return {undefined}
         */
        self.g.setEnabled = function(dataAndEvents) {
            /** @type {boolean} */
            this.ea = dataAndEvents;
        };
        /**
         * @param {Object} next_callback
         * @return {undefined}
         */
        var Point = function(next_callback) {
            self.F.call(this);
            this.a = new self.J;
            this.b = self.Sf("https://hangouts.google.com").toString();
            if (next_callback) {
                change(this, next_callback);
            }
        };
        self.p(Point, self.F);
        /**
         * @return {undefined}
         */
        Point.prototype.B = function() {
            Point.D.B.call(this);
            var codeSegments = this.a.X();
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;++i) {
                self.H(this.a.get(codeSegments[i]));
            }
            delete this.a;
        };
        /**
         * @param {string} opt_key
         * @return {?}
         */
        Point.prototype.getItem = function(opt_key) {
            return self.Lc(this.a, opt_key) ? this.a.get(opt_key) : null;
        };
        /**
         * @param {Object} header
         * @return {undefined}
         */
        Point.prototype.setItem = function(header) {
            this.a.set(header.id(), header);
        };
        /**
         * @param {?} d
         * @param {Object} callback
         * @return {undefined}
         */
        var change = function(d, callback) {
            var storageKey;
            for (storageKey in callback) {
                var data = callback[storageKey];
                var ar;
                if (self.Lc(d.a, storageKey)) {
                    ar = d.a.get(storageKey);
                    ar.fa = data.value;
                    if (data.changed) {
                        ar.ja = data.changed;
                    }
                } else {
                    ar = new result(storageKey, data.value, data.multiple, data.inverse, data.changed);
                    ar.setEnabled(!data.disabled);
                }
                d.a.set(storageKey, ar);
            }
        };
        /**
         * @param {?} s
         * @return {?}
         */
        var query = function(s) {
            var codeSegments = s.a.X();
            var map = {};
            /** @type {number} */
            var i = 0;
            for (;i < codeSegments.length;i++) {
                var storageKey = codeSegments[i];
                var self = s.a.get(storageKey);
                map[storageKey] = {
                    value : self.N(),
                    type : self.type(),
                    multiple : self.Zb,
                    disabled : !self.ea,
                    inverse : self.Sb,
                    changed : self.ja
                };
            }
            return map;
        };
        /**
         * @param {Object} node
         * @return {?}
         */
        var eval = function(node) {
            return(node = node.a.get("ServerName")) && node.ea ? node.N() : null;
        };
        /**
         * @param {Object} node
         * @return {?}
         */
        var removeNode = function(node) {
            return(node = node.a.get("IgnoreUnresponsiveWebview")) && node.ea ? node.N() : false;
        };
        /**
         * @param {Object} node
         * @return {?}
         */
        var clear = function(node) {
            return(node = node.a.get("locale")) && node.ea ? node.N() : "";
        };
        /**
         * @param {Object} x
         * @return {?}
         */
        var toFixed = function(x) {
            x = (0,eval)(x) ? self.Sf("https://" + (0,eval)(x)).toString() : x.b;
            self.S("getApplicationPath: " + x);
            return x;
        };
        /**
         * @param {Object} m
         * @param {Object} d
         * @return {undefined}
         */
        var E = function(m, d) {
            this.a = new self.J;
            this.b = new self.L;
            if (m) {
                var storageKey;
                for (storageKey in m) {
                    this.a.set(storageKey, m[storageKey]);
                }
            }
            if (d) {
                for (storageKey in d) {
                    this.a.set(storageKey, d[storageKey]);
                }
            }
            window.chrome.storage.onChanged.addListener((0, self.r)(this.bc, this));
        };
        var t = new self.Rd("n");
        /**
         * @return {?}
         */
        var commitDirty = function() {
            return new self.kd(function(done) {
                window.chrome.storage.local.get(null, function(err) {
                    if (window.chrome.runtime.lastError) {
                        self.S("chrome local storage read error %s", window.chrome.runtime.lastError);
                    }
                    window.chrome.storage.sync.get(null, function(dataAndEvents) {
                        if (window.chrome.runtime.lastError) {
                            self.S("chrome sync storage read error %s", window.chrome.runtime.lastError);
                        }
                        done(new E(err, dataAndEvents));
                    });
                });
            });
        };
        self.g = E.prototype;
        /**
         * @param {string} key
         * @return {?}
         */
        self.g.get = function(key) {
            return this.a.get(key, null);
        };
        /**
         * @param {string} key
         * @return {?}
         */
        self.g.has = function(key) {
            return self.Lc(this.a, key);
        };
        /**
         * @param {string} key
         * @param {Object} val
         * @param {boolean} deepDataAndEvents
         * @return {?}
         */
        self.g.set = function(key, val, deepDataAndEvents) {
            var object = self.qd();
            this.a.set(key, val);
            var camelKey = {};
            /** @type {Object} */
            camelKey[key] = val;
            (deepDataAndEvents ? window.chrome.storage.sync : window.chrome.storage.local).set(camelKey, function() {
                if (window.chrome.runtime.lastError) {
                    self.S("chrome storage set key: %s, error: %s", key, window.chrome.runtime.lastError);
                }
                object.b(void 0);
            });
            return object.a;
        };
        /**
         * @param {string} name
         * @param {Function} keepData
         * @return {undefined}
         */
        self.g.remove = function(name, keepData) {
            this.a.remove(name);
            (keepData ? window.chrome.storage.sync : window.chrome.storage.local).remove(name, function() {
                if (window.chrome.runtime.lastError) {
                    self.S("chrome storage remove key: %s, error: %s", name, window.chrome.runtime.lastError);
                }
            });
        };
        /**
         * @return {undefined}
         */
        self.g.clear = function() {
            this.a.clear();
            window.chrome.storage.local.clear(function() {
                if (window.chrome.runtime.lastError) {
                    self.S("chrome local storage clear error: %s", window.chrome.runtime.lastError);
                }
            });
            window.chrome.storage.sync.clear(function() {
                if (window.chrome.runtime.lastError) {
                    self.S("chrome sync storage clear error: %s", window.chrome.runtime.lastError);
                }
            });
        };
        /**
         * @return {?}
         */
        self.g.J = function() {
            return this.b;
        };
        /**
         * @param {Object} options
         * @param {string} dataAndEvents
         * @return {undefined}
         */
        self.g.bc = function(options, dataAndEvents) {
            if ("sync" == dataAndEvents) {
                var opt_key;
                for (opt_key in options) {
                    var v = options[opt_key];
                    if (null != v.newValue) {
                        this.a.set(opt_key, v.newValue);
                    } else {
                        this.a.remove(opt_key);
                    }
                    self.M(this.b, t);
                }
            }
        };
        /** @type {Array} */
        var codeSegments = [25, 33, 50, 66, 75, 90, 100, 110, 125, 150, 175, 200, 250, 300, 400, 500];
        /** @type {number} */
        var max = codeSegments.length - 1;
        /**
         * @param {number} x
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        var rest = function(x, dataAndEvents) {
            return self.Aa(x) && (0, window.isFinite)(x) ? 0 > x ? dataAndEvents ? 6 : 0 : x > max ? dataAndEvents ? 6 : max : x : 6;
        };
        /**
         * @param {number} opt_attributes
         * @return {?}
         */
        var crossProduct = function(opt_attributes) {
            return codeSegments[rest(opt_attributes, true)];
        };
        /**
         * @param {Object} el
         * @return {undefined}
         */
        var move = function(el) {
            self.L.call(this);
            this.a = new Point;
            this.l = {};
            this.b = el.get(key);
            this.h = el.get(self.T);
            var cycle = new self.N(this);
            self.I(this, cycle);
            cycle.listen(this.b.J(), t, this.g);
            cycle.listen(this.h.J(), "uv:sn", this.v);
            cycle.listen(this.h.J(), "uv:sr", this.g);
            tighten(el, [k])[k].then(function(fix) {
                cycle.listen(fix, "j", this.g);
            }, void 0, this);
            insert(this);
        };
        self.p(move, self.L);
        /**
         * @param {Node} me
         * @return {undefined}
         */
        var insert = function(me) {
            me.a.setItem(new result("UnobfuscatedMode", false));
            me.a.setItem(new result("ServerName", null));
            me.a.setItem(new result("experimentz", null));
            me.a.setItem(new result("UseBrightBackground", false));
            me.a.setItem(new result("IgnoreUnresponsiveWebview", false));
            me.a.setItem(new result("locale", ""));
            var restoreScript = me.b.get("settings");
            if (restoreScript) {
                change(me.a, restoreScript);
            }
            me.b.set("settings", query(me.a));
            me.g();
        };
        /**
         * @return {?}
         */
        move.prototype.j = function() {
            var selectors = clear(this.a) || window.chrome.i18n.getMessage("@@ui_locale");
            return self.oc.test(selectors);
        };
        /**
         * @param {string} ready
         * @param {string} orig
         * @return {undefined}
         */
        var F = function(ready, orig) {
            var li = apply(ready, true);
            var load = indexOf(li);
            /** @type {boolean} */
            var e = false;
            /** @type {boolean} */
            var f = false;
            /** @type {boolean} */
            var r = false;
            /** @type {boolean} */
            var k = false;
            /** @type {boolean} */
            var n = false;
            /** @type {boolean} */
            var q = false;
            if (null != self.A(orig, 1)) {
                /** @type {boolean} */
                e = self.A(load, 1) != self.A(orig, 1);
                var t = self.A(orig, 1);
                self.B(load, 1, t);
            }
            if (null != self.C(orig, complete, 2)) {
                t = self.C(orig, complete, 2);
                self.E(load, 2, t);
            }
            if (null != self.A(orig, 3)) {
                t = self.A(orig, 3);
                self.B(load, 3, t);
            }
            if (null != self.A(orig, 4)) {
                t = self.A(orig, 4);
                self.B(load, 4, t);
            }
            if (null != self.A(orig, 14)) {
                /** @type {boolean} */
                n = self.A(load, 14) != self.A(orig, 14);
                t = self.A(orig, 14);
                self.B(load, 14, t);
            }
            if (null != self.A(orig, 5) && (r = self.A(load, 5) != self.A(orig, 5), t = self.A(orig, 5), self.B(load, 5, t), r)) {
                t = self.C(load, complete, 2) || _createWindow(!!self.A(orig, 5));
                var v1 = null != self.A(load, 14) ? self.A(load, 14) : 6;
                /** @type {number} */
                v1 = crossProduct(v1) / crossProduct(6);
                /** @type {number} */
                var recurring = v1 * (self.A(load, 5) ? 350 : 700);
                self.B(t, 3, recurring);
                self.B(t, 4, 560 * v1);
                self.E(load, 2, t);
                ready.b.remove("mwp");
                ping(ready);
                t = self.A(orig, 5);
                self.B(orig, 6, t);
            }
            if (null != self.A(orig, 6)) {
                /** @type {boolean} */
                f = self.A(load, 6) != self.A(orig, 6);
                t = self.A(orig, 6);
                self.B(load, 6, t);
            }
            if (null != self.A(orig, 7)) {
                t = self.A(orig, 7);
                self.B(load, 7, t);
            }
            if (null != self.A(orig, 8)) {
                t = self.A(orig, 8);
                self.B(load, 8, t);
            }
            if (null != self.A(orig, 9)) {
                t = self.A(orig, 9);
                self.B(load, 9, t);
            }
            if (null != self.A(orig, 13)) {
                /** @type {boolean} */
                k = self.A(load, 13) != self.A(orig, 13);
                t = self.A(orig, 13);
                self.B(load, 13, t);
            }
            if (null != self.A(orig, 15)) {
                /** @type {boolean} */
                q = self.A(load, 15) != self.A(orig, 15);
                t = self.A(orig, 15);
                self.B(load, 15, t);
            }
            if (!addEvent(li, load)) {
                ready.b.set("uv_settings", self.D(load));
                if (e) {
                    self.M(ready, "p");
                }
                if (f) {
                    self.M(ready, "o");
                }
                if (r) {
                    self.M(ready, "r");
                }
                if (k) {
                    self.M(ready, "s");
                }
                if (n) {
                    self.M(ready, "t");
                }
                if (q) {
                    self.M(ready, "q");
                }
                ready.g();
            }
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        move.prototype.v = function(type) {
            type = (new instance(type.S)).getState();
            var ready = new RegExp;
            var attr = self.A(type, 1);
            self.B(ready, 1, attr);
            attr = self.A(type, 3);
            self.B(ready, 3, attr);
            attr = self.A(type, 4);
            self.B(ready, 4, attr);
            attr = self.A(type, 2);
            self.B(ready, 5, attr);
            attr = self.A(type, 6);
            self.B(ready, 7, attr);
            attr = self.A(type, 7);
            self.B(ready, 8, attr);
            attr = self.A(type, 8);
            self.B(ready, 9, attr);
            attr = self.A(type, 11);
            self.B(ready, 13, attr);
            attr = self.A(type, 12);
            self.B(ready, 6, attr);
            attr = self.A(type, 15);
            self.B(ready, 14, attr);
            attr = self.A(type, 19);
            self.B(ready, 15, attr);
            F(this, ready);
            ready = new hit;
            type = self.C(type, keydown, 14);
            self.E(ready, 1, type);
            type = after(this, true);
            if (!(null == self.C(ready, keydown, 1))) {
                if (!addEvent(self.C(ready, keydown, 1), self.C(type, keydown, 1))) {
                    ready = self.C(ready, keydown, 1);
                    self.E(type, 1, ready);
                }
            }
            this.b.set("app_sync_settings", self.D(type), true);
            this.g();
        };
        /**
         * @param {string} type
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        var apply = function(type, dataAndEvents) {
            var ready = type.b.get("uv_settings");
            ready = ready ? new RegExp(ready) : new RegExp;
            if (dataAndEvents) {
                return ready;
            }
            if (null == self.A(ready, 5)) {
                self.B(ready, 5, false);
            }
            if (type.j()) {
                self.B(ready, 5, false);
            }
            /** @type {boolean} */
            var recurring = !!self.A(ready, 5);
            if (null == self.A(ready, 1)) {
                self.B(ready, 1, !recurring);
            }
            if (!self.C(ready, complete, 2)) {
                var operator = _createWindow(recurring);
                self.E(ready, 2, operator);
            }
            if (null == self.A(ready, 4)) {
                self.B(ready, 4, recurring);
            }
            if (null == self.A(ready, 6)) {
                self.B(ready, 6, recurring);
            }
            if (null == self.A(ready, 3)) {
                self.B(ready, 3, true);
            }
            if (null == self.A(ready, 7)) {
                self.B(ready, 7, true);
            }
            if (null == self.A(ready, 8)) {
                self.B(ready, 8, true);
            }
            if (null == self.A(ready, 9)) {
                self.B(ready, 9, true);
            }
            if (null == self.A(ready, 13)) {
                self.B(ready, 13, true);
            }
            if (null == self.A(ready, 14)) {
                self.B(ready, 14, 6);
            }
            if (null == self.A(ready, 15)) {
                self.B(ready, 15, false);
            }
            return ready;
        };
        /**
         * @param {Node} t
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        var after = function(t, dataAndEvents) {
            var ready = t.b.get("app_sync_settings");
            ready = ready ? new hit(ready) : new hit;
            if (dataAndEvents) {
                return ready;
            }
            if (null == self.C(ready, keydown, 1)) {
                var complete = new keydown;
                self.E(ready, 1, complete);
            }
            return ready;
        };
        /**
         * @return {undefined}
         */
        move.prototype.g = function() {
            var complete = apply(this);
            var ready = after(this);
            var type = new g;
            var version = self.A(complete, 1);
            self.B(type, 1, version);
            self.B(type, 17, millis);
            version = self.A(complete, 5) && millis;
            self.B(type, 2, version);
            version = self.A(complete, 3);
            self.B(type, 3, version);
            version = self.A(complete, 4);
            self.B(type, 4, version);
            version = self.A(complete, 5) && qsaAvail;
            self.B(type, 5, version);
            version = self.A(complete, 7);
            self.B(type, 6, version);
            version = self.A(complete, 9);
            self.B(type, 8, version);
            version = self.A(complete, 8);
            self.B(type, 7, version);
            self.B(type, 10, beforeObj);
            version = self.A(complete, 13);
            self.B(type, 11, version);
            self.B(type, 18, parentName);
            version = self.A(complete, 6) && parentName;
            self.B(type, 12, version);
            version = window.chrome.runtime.getManifest().version;
            self.B(type, 13, version);
            self.B(type, 20, window.chrome.runtime.id);
            self.B(type, 16, recurring);
            version = recurring ? self.A(complete, 14) : 6;
            self.B(type, 15, version);
            ready = self.C(ready, keydown, 1);
            self.E(type, 14, ready);
            complete = self.A(complete, 15);
            self.B(type, 19, complete);
            complete = new instance;
            self.E(complete, 1, type);
            this.h.sendMessage(complete);
        };
        /**
         * @param {Text} complete
         * @return {?}
         */
        var require = function(complete) {
            complete = apply(complete);
            return self.A(complete, 1);
        };
        /**
         * @param {Text} complete
         * @return {?}
         */
        var makeArray = function(complete) {
            if (!parentName) {
                return false;
            }
            complete = apply(complete);
            return self.A(complete, 6);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        var parseFloat = function(value) {
            return millis ? self.zb(value.l, "UseShapedWindow", function() {
                return self.A(apply(value), 5);
            }) : false;
        };
        /**
         * @param {Node} node
         * @return {?}
         */
        var params = function(node) {
            if (!node.b.has("auto_ui")) {
                node.b.set("auto_ui", true);
            }
            return node.b.get("auto_ui");
        };
        /**
         * @param {Node} self
         * @return {?}
         */
        var stop = function(self) {
            if (!self.b.has("wvpp")) {
                self.b.set("wvpp", "-" + Math.random());
            }
            return "uv" + self.b.get("wvpp");
        };
        /**
         * @param {string} options
         * @return {?}
         */
        var ping = function(options) {
            if (!options.b.has("mwp")) {
                options.b.set("mwp", "-" + Math.random());
            }
            return options.b.get("mwp");
        };
        /**
         * @param {Text} complete
         * @return {?}
         */
        var Anim = function(complete) {
            complete = apply(complete);
            return self.A(complete, 13);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        var hex = function(value) {
            return recurring ? self.A(apply(value), 14) : 6;
        };
        /**
         * @param {?} a
         * @param {number} millis
         * @return {undefined}
         */
        var wait = function(a, millis) {
            if (recurring) {
                var l = new RegExp;
                self.B(l, 14, millis);
                F(a, l);
            }
        };
        /**
         * @param {?} recurring
         * @return {?}
         */
        var _createWindow = function(recurring) {
            var screen = window.screen;
            var dblclick = new complete;
            /** @type {number} */
            var millis = Math.min(screen.availHeight, 560);
            self.B(dblclick, 4, millis);
            self.B(dblclick, 2, (screen.availTop + screen.availHeight - millis) / 2);
            if (recurring) {
                /** @type {number} */
                recurring = Math.min(screen.availWidth, 350);
                self.B(dblclick, 3, recurring);
                self.B(dblclick, 1, screen.availLeft + screen.availWidth - recurring);
            } else {
                /** @type {number} */
                recurring = Math.min(screen.availWidth, 700);
                self.B(dblclick, 3, recurring);
                self.B(dblclick, 1, (screen.availLeft + screen.availWidth - recurring) / 2);
            }
            return dblclick;
        };
        var millis = Ck && (true && !self.vb());
        var qsaAvail = millis && (self.w("CrOS") || self.wb());
        var beforeObj = window.chrome.app.window ? window.chrome.app.window.canSetVisibleOnAllWorkspaces() : false;
        /** @type {boolean} */
        var parentName = !!window.chrome.app.window;
        /**
         * @param {Object} elem
         * @return {?}
         */
        var _removeClass = function(elem) {
            if (elem.classList) {
                return elem.classList;
            }
            elem = elem.className;
            return self.t(elem) && elem.match(/\S+/g) || [];
        };
        /**
         * @param {Element} elem
         * @param {string} node
         * @return {?}
         */
        var _hasClass = function(elem, node) {
            return elem.classList ? elem.classList.contains(node) : self.eb(_removeClass(elem), node);
        };
        /**
         * @param {Element} element
         * @param {string} className
         * @return {undefined}
         */
        var _addClass = function(element, className) {
            if (element.classList) {
                element.classList.add(className);
            } else {
                if (!_hasClass(element, className)) {
                    element.className += 0 < element.className.length ? " " + className : className;
                }
            }
        };
        /**
         * @param {Element} elem
         * @param {string} name
         * @return {undefined}
         */
        var select = function(elem, name) {
            if (elem.classList) {
                elem.classList.remove(name);
            } else {
                if (_hasClass(elem, name)) {
                    elem.className = (0, self.$a)(_removeClass(elem), function(c) {
                        return c != name;
                    }).join(" ");
                }
            }
        };
        /**
         * @param {Element} element
         * @return {undefined}
         */
        var removeClass = function(element) {
            /** @type {Array} */
            var pdataOld = ["CzdJYb", "LQr1Lc"];
            if (element.classList) {
                (0, self.u)(pdataOld, function(from) {
                    select(element, from);
                });
            } else {
                element.className = (0, self.$a)(_removeClass(element), function(length) {
                    return!self.eb(pdataOld, length);
                }).join(" ");
            }
        };
        /**
         * @param {?} obj
         * @param {string} node
         * @param {boolean} recurring
         * @return {undefined}
         */
        var replace = function(obj, node, recurring) {
            if (recurring) {
                _addClass(obj, node);
            } else {
                select(obj, node);
            }
        };
        /**
         * @param {Object} fix
         * @param {string} str
         * @param {Document} doc
         * @param {?} name
         * @param {?} contentHTML
         * @return {undefined}
         */
        var initialize = function(fix, str, doc, name, contentHTML) {
            var result = this;
            self.L.call(this);
            /** @type {Object} */
            this.g = fix;
            this.b = fix.get(actualKey);
            this.a = recurring ? doc.createElement("webview") : doc.createElement("iframe");
            if (recurring) {
                this.a.setAttribute("partition", stop(this.b));
                if (this.a.setZoomMode) {
                    this.a.setZoomMode("per-origin");
                    this.cb();
                }
                if (contentHTML) {
                    this.a.setAttribute("allowtransparency", true);
                }
            }
            this.w = this.b.j();
            /** @type {null} */
            this.h = null;
            doc.body.appendChild(this.a);
            if (self.t(str)) {
                /** @type {string} */
                this.a.src = str;
            } else {
                str.attach(this.a);
            }
            if (name) {
                this.a.id = name;
                this.a.name = name;
            }
            fix = new self.N(this);
            self.I(this, fix);
            this.j = new Node(this.a.terminate || self.Da, 5E3, this.a);
            self.I(this, this.j);
            if (this.a.request) {
                this.a.request.onBeforeSendHeaders.addListener(save, appender, onInput);
                /**
                 * @param {string} types
                 * @return {?}
                 */
                this.v = function(types) {
                    return error(result, types);
                };
                this.a.request.onCompleted.addListener(this.v, appender);
                this.a.request.onErrorOccurred.addListener(cb, appender);
                /**
                 * @param {string} types
                 * @return {undefined}
                 */
                this.l = function(types) {
                    var a;
                    if (a = "xmlhttprequest" == types.type && (isFunction(types.url) && isFunction(types.redirectUrl))) {
                        a = result.b.a;
                        types = new self.P(types.redirectUrl);
                        if (findAll(types) && (new self.P(a.b)).b != types.b) {
                            a.b = self.Sf("https://" + types.b).toString();
                            /** @type {boolean} */
                            a = true;
                        } else {
                            /** @type {boolean} */
                            a = false;
                        }
                    }
                    if (a) {
                        result.g.get(storageKey).b();
                    }
                };
                this.a.request.onBeforeRedirect.addListener(this.l, requestFilter);
            }
            fix.listen(this.a, "newwindow", this.A).listen(this.a, "permissionrequest", this.hc).listen(this.a, "close", this.cc).listen(this.a, "responsive", this.H).listen(this.a, "unresponsive", this.L).listen(this.a, "exit", opt_attributes).listen(this.a, "loadabort", attributes).listen(this.b, "t", this.cb);
            if (doc.defaultView) {
                fix.listen(doc.defaultView, "focus", this.kc);
            }
        };
        self.p(initialize, self.L);
        /** @type {Array} */
        var iniX = [1E3, 5E3, 3E4, 3E5];
        var server = new self.J;
        /** @type {Array} */
        var onInput = ["blocking", "requestHeaders"];
        /**
         * @param {boolean} recurring
         * @return {undefined}
         */
        initialize.prototype.show = function(recurring) {
            replace(this.a, "QWxtS", recurring);
        };
        /**
         * @return {undefined}
         */
        initialize.prototype.H = function() {
            this.j.stop();
        };
        /**
         * @return {undefined}
         */
        initialize.prototype.L = function() {
            if (!removeNode(this.b.a)) {
                generate(this.j);
            }
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        initialize.prototype.A = function(type) {
            var t = type.a;
            var name = t.targetUrl;
            if (readFile(name) || "about:blank" == name) {
                type.c();
                if ("centralRoster" != t.name) {
                    if ("gtn-roster-iframe-id" != t.name) {
                        refresh(this, t);
                    }
                }
            } else {
                t.window.discard();
                window.chrome.browser.openTab({
                    url : name
                }, function() {
                    if (window.chrome.runtime.lastError) {
                        self.S("Failed to open a new tab: %s, %s", name, window.chrome.runtime.lastError.message);
                    }
                });
            }
        };
        /**
         * @param {string} orig
         * @param {Object} t
         * @return {undefined}
         */
        var refresh = function(orig, t) {
            var ready = new f(orig.w);
            self.I(orig, ready);
            ready.create({
                url : "dialog.html",
                width : t.initialWidth,
                height : t.initialHeight
            });
            self.G(ready, function() {
                return orig.g.get(self.T).sendMessage(new connection);
            });
            set(ready, (0, self.r)(function() {
                var str = new initialize(this.g, t.window, equal(ready).document);
                self.G(ready, function() {
                    return self.H(str);
                });
                self.ne(str, "u", function() {
                    return self.H(ready);
                });
                ready.focus();
            }, orig));
        };
        self.g = initialize.prototype;
        /**
         * @param {Object} e
         * @return {undefined}
         */
        self.g.hc = function(e) {
            e = e.a;
            switch(e.permission) {
                case "media":
                    ;
                case "filesystem":
                    if (isDefined(e.url)) {
                        e.request.allow();
                    }
                    break;
                case "loadplugin":
                    if ("google-talk" == e.identifier) {
                        e.request.allow();
                    }
                    break;
                default:
                    self.S("Unknown permission request for '%s'", e.permission);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.cc = function() {
            self.M(this, "u");
        };
        /**
         * @return {undefined}
         */
        self.g.B = function() {
            if (this.a.request) {
                try {
                    this.a.request.onBeforeSendHeaders.removeListener(save);
                    this.a.request.onCompleted.removeListener(this.v);
                    this.a.request.onErrorOccurred.removeListener(cb);
                    this.a.request.onBeforeRedirect.removeListener(this.l);
                } catch (a) {
                }
            }
            swap(this.a);
            initialize.D.B.call(this);
        };
        /**
         * @return {undefined}
         */
        self.g.kc = function() {
            if (this.a.ownerDocument.activeElement != this.a) {
                this.a.focus();
            }
        };
        /**
         * @return {undefined}
         */
        self.g.cb = function() {
            if (this.a.setZoom) {
                this.a.setZoom(codeSegments[rest(hex(this.b), true)] / 100);
            }
        };
        /**
         * @param {Object} options
         * @return {?}
         */
        var save = function(options) {
            var key = options.url;
            if ((0, self.Dc)(key)) {
                self.S("Before sending headers for request: %s", key);
            }
            var ar = searchTerm;
            if (ar) {
                return server.set(options.requestId, ar), options = [].concat(options.requestHeaders || []), ajax(options, function(el) {
                    return "authorization" == el.name.toLowerCase();
                }), options.push({
                    name : "Authorization",
                    value : "Bearer " + ar
                }), (0, self.Dc)(key) && self.S("Setting auth token for request: %s", key), {
                    requestHeaders : options
                };
            }
            self.S("No auth token available for the request: %s", key);
        };
        /**
         * @param {Node} h
         * @param {Object} data
         * @return {undefined}
         */
        var error = function(h, data) {
            var opt_key = data.requestId;
            var types = data.statusCode;
            var url = data.url;
            if (400 <= types || 300 <= types && (0, self.Dc)(url)) {
                self.S("Detected %s status code for request %s", types, url);
            }
            var udataCur = server.get(opt_key);
            server.remove(opt_key);
            switch(types) {
                case 400:
                    if (!readFile(url) || !recurring) {
                        break;
                    }
                    self.Be(function() {
                        return h.a.reload();
                    }, hue(h));
                    break;
                case 401:
                    if (queue(url)) {
                        break;
                    }
                    finish(function() {
                        if (!(window.chrome.identity ? searchTerm : 1)) {
                            oncomplete();
                        }
                    }, false, udataCur);
                    break;
                case 403:
                    if (escape(url)) {
                        finish(self.Da, false, udataCur);
                        oncomplete();
                    }
                    ;
            }
        };
        /**
         * @param {Node} obj
         * @return {undefined}
         */
        var opt_attributes = function(obj) {
            self.S("Exit: %s", obj.a.reason);
            oncomplete();
        };
        /**
         * @param {Object} opts
         * @return {undefined}
         */
        var attributes = function(opts) {
            opts = opts.a;
            self.S("Load abort: %s, isTopLevel: %s, %s", opts.reason, opts.isTopLevel, opts.url);
            if (opts.isTopLevel) {
                sandboxIn.R(1);
            }
        };
        /**
         * @param {Object} req
         * @return {undefined}
         */
        var cb = function(req) {
            server.remove(req.requestId);
            switch(req.type) {
                case "sub_frame":
                    self.S("Request error for a sub-frame: %s", req.url);
                    break;
                case "main_frame":
                    self.S("Request error for the main frame: %s", req.url);
                    sandboxIn.R(1);
                    break;
                case "script":
                    self.S("Request error for a script: %s", req.url);
                    if (0 == req.frameId) {
                        sandboxIn.R(1);
                    }
                    ;
            }
        };
        /**
         * @param {Node} h
         * @return {?}
         */
        var hue = function(h) {
            if (!h.h) {
                h.h = new Particle(iniX, true);
            }
            return h.h.N(true);
        };
        /**
         * @param {Object} ready
         * @param {string} property
         * @param {?} y
         * @return {undefined}
         */
        var init = function(ready, property, y) {
            var e = this;
            self.L.call(this);
            Zl++;
            this.h = y;
            /** @type {string} */
            this.j = property;
            this.b = "39C9B5E" + Zl;
            this.g = ready.get(self.T);
            self.kg(this.g);
            var text = new self.Kf;
            self.B(text, 1, this.b);
            self.B(text, 2, this.j);
            self.B(text, 3, this.h.locale);
            self.B(text, 4, this.h.a);
            property = recurring ? self.zf(this.j, "/webchat/blank") : new self.P("host.html");
            self.pf(property, text.l());
            ready = new initialize(ready, property.toString(), window.document, "centralRoster");
            self.I(this, ready);
            this.a = ready.a;
            ready = new self.N(this);
            self.I(this, ready);
            self.Ze(ready, this.a, ["loadstop", "load"], function() {
                self.mg(e.g, e.a.contentWindow, e.b, recurring ? "google.com" : window.chrome.runtime.id);
                if (recurring) {
                    load(e, text);
                }
            });
            ready.listen(this.a, "consolemessage", this.l);
        };
        self.p(init, self.L);
        /** @type {number} */
        var Zl = 0;
        /**
         * @param {string} ready
         * @param {string} callback
         * @return {undefined}
         */
        var load = function(ready, callback) {
            var restoreScript = searchTerm;
            /** @type {(undefined|{Authorization: string})} */
            restoreScript = restoreScript ? {
                Authorization : "Bearer " + restoreScript
            } : void 0;
            self.Je(self.Tf(callback), function(result) {
                result = result.target;
                if (self.Te(result)) {
                    dispatchEvent(ready, self.Ue(result));
                } else {
                    self.S("Error fetching host bundle: (" + result.j + ") " + (self.t(result.h) ? result.h : String(result.h)));
                    sandboxIn.R(1);
                }
            }, void 0, void 0, restoreScript, 25E3);
            self.G(ready, deferred);
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        init.prototype.l = function(types) {
            window.console.log("***", types.a.message);
        };
        /**
         * @param {Node} args
         * @param {string} properties
         * @return {undefined}
         */
        var dispatchEvent = function(args, properties) {
            args.a.executeScript({
                code : properties + "//# sourceURL=bundle.js"
            }, function() {
                return args.a.executeScript({
                    file : "scripts/host.js"
                }, function() {
                });
            });
        };
        /**
         * @param {?} a
         * @param {string} req
         * @return {undefined}
         */
        var l = function(a, req) {
            this.a = a;
            /** @type {string} */
            this.locale = req;
        };
        /**
         * @param {Object} fix
         * @return {undefined}
         */
        var run = function(fix) {
            self.F.call(this);
            /** @type {Object} */
            this.l = fix;
            this.a = fix.get(elem);
            this.w = fix.get(subKey);
            this.v = fix.get(actualKey);
            /** @type {null} */
            this.g = null;
            this.G = new Color(this.b, 6E4, this);
            self.I(this, this.G);
            sandboxIn.R = (0, self.r)(this.ec, this);
            self.G(this, EVENT_READY);
            this.h = new Particle(origin, true);
            this.j = new Node(this.mc, this.h.N(), this);
            self.I(this, this.j);
            this.c = new Node(this.kb, 4E4, this);
            self.I(this, this.c);
            fix = new self.N(this);
            self.I(this, fix);
            fix.listen(this.a, ready, this.Bb);
            fix.listen(this.w, out, this.ac);
            fix = self.zf(toFixed(this.v.a), "/webchat/_/jserror").toString();
            if (null != fix) {
                new format(fix, void 0);
            }
            if (10 != this.a.a) {
                self.S("Starting application.");
                this.b();
            }
        };
        self.p(run, self.F);
        /**
         * @return {undefined}
         */
        run.prototype.B = function() {
            normalize(this);
            run.D.B.call(this);
        };
        /**
         * @return {undefined}
         */
        run.prototype.b = function() {
            if (!this.K) {
                self.S("Reloading host...");
                push(this.a, 2);
                normalize(this);
                var graph = this.l;
                var c = this.v.a;
                var g = toFixed(c);
                var key = c.a.get("UnobfuscatedMode");
                /** @type {string} */
                key = key && (key.ea && key.N()) ? "du" : "";
                if (!(0,eval)(c)) {
                    new self.P(toFixed(c));
                }
                var map = c.a.get("experimentz");
                if (map) {
                    if (map.ea) {
                        map.N();
                    }
                }
                if (!(c = clear(c))) {
                    c = window.chrome.i18n.getMessage("@@ui_locale");
                }
                this.g = new init(graph, g, new l(key, c));
                self.I(this, this.g);
            }
        };
        /**
         * @param {Node} b
         * @return {undefined}
         */
        var normalize = function(b) {
            self.H(b.g);
            /** @type {null} */
            b.g = null;
        };
        self.g = run.prototype;
        /**
         * @return {undefined}
         */
        self.g.Bb = function() {
            this.j.stop();
            this.c.stop();
            switch(this.a.a) {
                case 2:
                    generate(this.c);
                    break;
                case 6:
                    generate(this.j, this.h.N());
                    break;
                case 4:
                    this.h.reset();
                    break;
                case 10:
                    normalize(this);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.mc = function() {
            intersectObject(this.h);
            this.b();
        };
        /**
         * @param {?} dataAndEvents
         * @return {undefined}
         */
        self.g.ec = function(dataAndEvents) {
            normalize(this);
            switch(dataAndEvents) {
                case 1:
                    this.kb();
                    break;
                case 2:
                    if (3 == this.a.a) {
                        break;
                    }
                    compare(this.l.get(k), true, 1E4);
                    break;
                case 3:
                    push(this.a, 3);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.kb = function() {
            if (!this.K) {
                this.c.stop();
                push(this.a, 6);
            }
        };
        /**
         * @return {undefined}
         */
        self.g.ac = function() {
            if (3 == this.a.a) {
                this.b();
            }
        };
        /**
         * @param {Node} keys
         * @param {?} radius
         * @return {undefined}
         */
        var process = function(keys, radius) {
            self.F.call(this);
            this.b = keys.get(actualKey);
            this.g = keys.get(key);
            this.h = radius;
            var context = window.document;
            if (this.h) {
                context = equal(this.h).document;
            }
            this.a = new self.tc(context);
            this.c = new self.N(this);
            self.I(this, this.c);
            next(this);
            context = this.b.a;
            this.a.F("jsmodeField").checked = context.getItem("UnobfuscatedMode").N();
            this.a.F("experimentz").value = context.getItem("experimentz").N();
            this.a.F("brightBackgroundField").checked = context.getItem("UseBrightBackground").N();
            this.a.F("ignoreUnresponsiveWebviewField").checked = context.getItem("IgnoreUnresponsiveWebview").N();
            this.a.F("localeField").value = context.getItem("locale").N();
            this.c.listen(this.a.F("serverSelect"), "change", this.v);
            this.c.listen(this.a.F("doneButton"), "click", this.l);
            this.c.listen(this.a.F("cancelButton"), "click", this.j);
        };
        self.p(process, self.F);
        /** @type {Array.<string>} */
        var resultItems = "autopush.hangouts.sandbox.google.com preprod.hangouts.sandbox.google.com daily-0.hangouts.sandbox.google.com daily-1.hangouts.sandbox.google.com daily-2.hangouts.sandbox.google.com daily-3.hangouts.sandbox.google.com daily-4.hangouts.sandbox.google.com daily-5.hangouts.sandbox.google.com daily-6.hangouts.sandbox.google.com devpool-00.hangouts.sandbox.google.com devpool-01.hangouts.sandbox.google.com devpool-02.hangouts.sandbox.google.com devpool-03.hangouts.sandbox.google.com devpool-04.hangouts.sandbox.google.com devpool-05.hangouts.sandbox.google.com devpool-06.hangouts.sandbox.google.com devpool-07.hangouts.sandbox.google.com devpool-08.hangouts.sandbox.google.com devpool-09.hangouts.sandbox.google.com devpool-10.hangouts.sandbox.google.com devpool-11.hangouts.sandbox.google.com devpool-12.hangouts.sandbox.google.com devpool-13.hangouts.sandbox.google.com devpool-14.hangouts.sandbox.google.com devpool-15.hangouts.sandbox.google.com devpool-16.hangouts.sandbox.google.com devpool-17.hangouts.sandbox.google.com devpool-18.hangouts.sandbox.google.com devpool-19.hangouts.sandbox.google.com devpool-20.hangouts.sandbox.google.com devpool-21.hangouts.sandbox.google.com devpool-22.hangouts.sandbox.google.com devpool-23.hangouts.sandbox.google.com devpool-24.hangouts.sandbox.google.com".split(" ");
        /**
         * @param {Node} element
         * @param {string} value
         * @param {string} prefix
         * @param {string} val
         * @param {boolean} recurring
         * @return {?}
         */
        var highlight = function(element, value, prefix, val, recurring) {
            element = element.a.a.createElement("option");
            /** @type {string} */
            element.innerText = value;
            /** @type {string} */
            element.id = prefix;
            /** @type {string} */
            element.value = val;
            if (recurring) {
                /** @type {string} */
                element.selected = "selected";
            }
            return element;
        };
        /**
         * @param {Node} c
         * @return {undefined}
         */
        var next = function(c) {
            var el = c.a.F("serverSelect");
            var i = highlight(c, "hangouts.google.com (default)", "", "", true);
            el.insertBefore(i, c.a.F("otherOption"));
            /** @type {number} */
            i = 0;
            for (;i < resultItems.length;i++) {
                /** @type {string} */
                var result = resultItems[i];
                result = highlight(c, result, result, result, false);
                el.insertBefore(result, c.a.F("otherOption"));
            }
            var value = c.b.a.getItem("ServerName").N();
            if (value) {
                if (self.db(resultItems, function(el) {
                        return el == value;
                    })) {
                    /** @type {string} */
                    c.a.F(value).selected = "selected";
                } else {
                    if ("hangouts.google.com" != value) {
                        i = highlight(c, value, value, value, true);
                        el.insertBefore(i, c.a.F("otherOption"));
                    }
                }
            }
        };
        /**
         * @return {undefined}
         */
        process.prototype.v = function() {
            var x = this.a.F("serverSelect");
            var testElement = this.a.F("otherLabel");
            if (x.value) {
                /** @type {string} */
                this.a.F("error").style.display = "none";
            }
            /** @type {string} */
            testElement.style.display = x.value == this.a.F("otherOption").value ? "block" : "none";
        };
        /**
         * @return {undefined}
         */
        process.prototype.l = function() {
            var val = this.a.F("serverSelect").value;
            var options = this.a.F("error");
            if (val == this.a.F("otherOption").value && (val = this.a.F("otherField").value, "" == val)) {
                /** @type {string} */
                options.style.display = "block";
                return;
            }
            /** @type {string} */
            options.style.display = "none";
            /** @type {boolean} */
            options = false;
            var key = this.b.a;
            var value = this.a.F("jsmodeField").checked;
            var cookie = key.getItem("UnobfuscatedMode");
            /** @type {boolean} */
            var name = false;
            if (value != cookie.N()) {
                cookie.fa = value;
                /** @type {boolean} */
                cookie.ja = true;
                key.setItem(cookie);
                /** @type {boolean} */
                name = true;
            }
            value = key.getItem("ServerName");
            /** @type {boolean} */
            cookie = false;
            if (val != value.N()) {
                value.fa = val;
                /** @type {boolean} */
                value.ja = true;
                key.setItem(value);
                /** @type {boolean} */
                cookie = true;
            }
            if (name || cookie) {
                this.g.set("settings", query(key));
                /** @type {boolean} */
                val = true;
            } else {
                /** @type {boolean} */
                val = false;
            }
            options = val || options;
            val = this.b.a;
            key = val.getItem("experimentz");
            name = this.a.F("experimentz").value;
            if (name != key.N()) {
                key.fa = name;
                /** @type {boolean} */
                key.ja = true;
                val.setItem(key);
                this.g.set("settings", query(val));
                /** @type {boolean} */
                val = true;
            } else {
                /** @type {boolean} */
                val = false;
            }
            options = val || options;
            options = store(this, "brightBackgroundField", "UseBrightBackground") || options;
            options = store(this, "ignoreUnresponsiveWebviewField", "IgnoreUnresponsiveWebview") || options;
            val = this.b.a;
            key = val.getItem("locale");
            name = this.a.F("localeField").value;
            if (name != key.N()) {
                key.fa = name;
                /** @type {boolean} */
                key.ja = true;
                val.setItem(key);
                this.g.set("settings", query(val));
                /** @type {boolean} */
                val = true;
            } else {
                /** @type {boolean} */
                val = false;
            }
            options = val || options;
            emit(this.a.a).close();
            if (options) {
                window.chrome.runtime.reload();
            }
        };
        /**
         * @param {Node} object
         * @param {(Object|string)} name
         * @param {Object} key
         * @return {?}
         */
        var store = function(object, name, key) {
            var c = object.b.a;
            name = object.a.F(name).checked;
            key = c.getItem(key);
            return name != key.N() ? (key.fa = name, key.ja = true, c.setItem(key), object.g.set("settings", query(c)), true) : false;
        };
        /**
         * @return {undefined}
         */
        process.prototype.j = function() {
            emit(this.a.a).close();
        };
        /**
         * @param {?} selfObj
         * @return {undefined}
         */
        var bind = function(selfObj) {
            var defaults = {
                width : 520,
                height : 320,
                top : Math.floor((window.screen.height - 320) / 2),
                left : Math.floor((window.screen.width - 520) / 2),
                id : "settingsdialog.html",
                url : "settingsdialog.html",
                visibleOnAllWorkspaces : true
            };
            var ready = new f(false);
            ready.create(defaults);
            set(ready, function() {
                var str = new process(selfObj, ready);
                self.G(ready, function() {
                    return self.H(str);
                });
            });
        };
        /**
         * @param {?} container
         * @param {?} array
         * @return {undefined}
         */
        var map = function(container, array) {
            self.N.call(this);
            this.c = container;
            this.a = array;
            this.b = this.c.get(elem);
            Class(this);
        };
        self.p(map, self.N);
        /**
         * @param {Object} args
         * @return {undefined}
         */
        var Class = function(args) {
            var input = args.a.F("restartButton");
            /** @type {string} */
            input.textContent = "Restart";
            args.listen(input, "click", function() {
                return window.chrome.runtime.reload();
            });
            input = args.a.F("settingsButton");
            /** @type {string} */
            input.textContent = "Settings";
            args.listen(input, "click", self.xa(bind, args.c));
            input = args.a.F("wabelStatus");
            var key;
            for (key in vals) {
                var data = args.a.a.createElement("option");
                data.value = vals[key];
                /** @type {string} */
                data.text = key;
                input.add(data);
            }
            args.listen(input, "change", args.j);
            args.listen(args.b, ready, args.h);
            args.h();
        };
        /**
         * @return {undefined}
         */
        map.prototype.j = function() {
            push(this.b, (0, window.parseInt)(this.a.F("wabelStatus").value, 10));
        };
        /**
         * @return {undefined}
         */
        map.prototype.h = function() {
            this.a.F("wabelStatus").value = this.b.a;
        };
        /** @type {null} */
        var orig = null;
        /**
         * @param {?} a
         * @return {undefined}
         */
        var reset = function(a) {
            if (orig) {
                orig.focus();
            } else {
                var defaults = {
                    width : 190,
                    height : 190,
                    top : Math.floor((window.screen.height - 190) / 2),
                    left : Math.floor((window.screen.width - 190) / 2),
                    id : "debugdialog.html190190",
                    url : "debugdialog.html",
                    visibleOnAllWorkspaces : true
                };
                orig = new f(false);
                orig.create(defaults);
                set(orig, function() {
                    var e = equal(orig).document;
                    var str = new map(a, new self.tc(e));
                    self.G(orig, function() {
                        /** @type {null} */
                        orig = null;
                        self.H(str);
                    });
                });
            }
        };
        /**
         * @param {number} event
         * @return {undefined}
         */
        var prev = function(event) {
            self.z(this, event, "capi:ds_sc", login_resource);
        };
        self.p(prev, self.y);
        /** @type {Array} */
        var login_resource = [1];
        /** @type {string} */
        prev.messageId = "capi:ds_sc";
        /**
         * @param {Node} fn
         * @return {undefined}
         */
        var proxy = function(fn) {
            this.a = fn.get(self.T);
        };
        /**
         * @param {string} ready
         * @param {(number|string)} obj
         * @return {undefined}
         */
        var reject = function(ready, obj) {
            var structure = self.A(ready, 1) || [];
            if (!self.eb(structure, obj)) {
                structure.push(obj);
                self.B(ready, 1, structure || []);
            }
        };
        /**
         * @param {?} type
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        var changePage = function(type, dataAndEvents) {
            if (null == type.getState() && dataAndEvents) {
                var instance = new obj;
                self.E(type, 2, instance);
            }
            return type.getState();
        };
        /**
         * @param {?} e
         * @param {number} offset
         * @return {undefined}
         */
        var extractStacktrace = function(e, offset) {
            reject(e, 16);
            var n = changePage(e, true);
            var stack = new string;
            stack.setPosition(offset);
            self.E(n, 15, stack);
        };
        /**
         * @param {string} ready
         * @return {?}
         */
        var _clone = function(ready) {
            return new slide(self.A(ready, 1), self.A(ready, 2), self.A(ready, 3), self.A(ready, 4));
        };
        /**
         * @param {?} obj
         * @return {?}
         */
        var inspect = function(obj) {
            var deferred = new complete;
            self.B(deferred, 1, obj.left);
            self.B(deferred, 2, obj.top);
            self.B(deferred, 3, obj.width);
            self.B(deferred, 4, obj.height);
            return deferred;
        };
        /**
         * @param {Function} fix
         * @return {undefined}
         */
        var test = function(fix) {
            self.L.call(this);
            this.b = fix.get(key);
            this.j = new proxy(fix);
            /** @type {Array} */
            this.a = [];
            var cycle = new self.N(this);
            self.I(this, cycle);
            fix = fix.get(k);
            cycle.listen(fix, "j", this.g);
            if (this.b.has("display")) {
                this.a = (0, self.ab)(this.b.get("display"), function(dataAndEvents) {
                    return new complete(dataAndEvents);
                });
                this.g();
            } else {
                fix = new complete;
                self.B(fix, 1, window.screen.availLeft);
                self.B(fix, 2, window.screen.availTop);
                self.B(fix, 3, window.screen.availWidth);
                self.B(fix, 4, window.screen.availHeight);
                /** @type {Array} */
                this.a = [fix];
            }
            fix = new self.ze(5E3);
            self.I(this, fix);
            cycle.listen(fix, "tick", this.h);
            self.Ae(fix);
            this.h();
        };
        self.p(test, self.L);
        /**
         * @param {?} source
         * @param {Object} y
         * @return {?}
         */
        var raycast_mesh = function(source, y) {
            var intersection;
            /** @type {number} */
            var minDot = Number.MAX_VALUE;
            /** @type {number} */
            var i = 0;
            for (;i < source.a.length;i++) {
                var oldconfig = _clone(source.a[i]);
                var dot = scrollTo(oldconfig, y);
                if (dot < minDot) {
                    minDot = dot;
                    intersection = oldconfig;
                }
            }
            return intersection;
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        test.prototype.l = function(types) {
            /** @type {Array} */
            var a = [];
            /** @type {boolean} */
            var thisp = false;
            (0, self.u)(types, function(widget) {
                if (widget.isEnabled) {
                    var type = new complete;
                    self.B(type, 1, widget.workArea.left);
                    self.B(type, 2, widget.workArea.top);
                    self.B(type, 3, widget.workArea.width);
                    self.B(type, 4, widget.workArea.height);
                    a.push(type);
                    thisp = thisp || !(0, self.bb)(this.a, function(li) {
                            return addEvent(li, type);
                        });
                }
            }, this);
            if (thisp = thisp || this.a.length != a.length) {
                /** @type {Array} */
                this.a = a;
                this.b.set("display", (0, self.ab)(a, function(ready) {
                    return self.D(ready);
                }));
                self.M(this, "change");
                this.g();
            }
        };
        /**
         * @return {undefined}
         */
        test.prototype.h = function() {
            window.chrome.system.display.getInfo((0, self.r)(this.l, this));
        };
        /**
         * @return {undefined}
         */
        test.prototype.g = function() {
            var w = this.j;
            var suiteView = this.a;
            var text = new prev;
            print(text, 1, suiteView);
            w.a.sendMessage(text);
        };
        /**
         * @param {number} context
         * @return {undefined}
         */
        var detach = function(context) {
            self.z(this, context, "h_ssf", null);
        };
        self.p(detach, self.y);
        /** @type {string} */
        detach.messageId = "h_ssf";
        /**
         * @param {(Node|string)} b
         * @return {undefined}
         */
        var step = function(b) {
            self.N.call(this);
            b = b.get(self.T);
            this.listen(b.J(), "h_ssf", this.a);
        };
        self.p(step, self.N);
        /** @type {(number|string)} */
        var loanProductId = 0 <= self.Xa(self.Vf, 54) ? 83052 : "83052";
        /**
         * @param {string} types
         * @return {undefined}
         */
        step.prototype.a = function(types) {
            /** @type {Array} */
            var paramsArray = [{
                key : "appVersion",
                value : window.chrome.runtime.getManifest().version
            }, {
                key : "appId",
                value : window.chrome.runtime.id
            }];
            types = new detach(types.S);
            if (self.A(types, 1)) {
                /** @type {*} */
                types = JSON.parse(self.A(types, 1));
                var k;
                for (k in types) {
                    paramsArray.push({
                        key : k,
                        value : types[k]
                    });
                }
            }
            window.chrome.runtime.sendMessage("gfdkimpbcpahaombhbimeihdjnejgicl", {
                requestFeedback : true,
                feedbackInfo : {
                    productId : loanProductId,
                    categoryTag : "Hangouts App",
                    systemInformation : paramsArray
                }
            });
        };
        /**
         * @param {number} context
         * @return {undefined}
         */
        var block = function(context) {
            self.z(this, context, "capi:fsc", null);
        };
        self.p(block, self.y);
        /** @type {string} */
        block.messageId = "capi:fsc";
        /**
         * @param {Object} recurring
         * @return {undefined}
         */
        block.prototype.setVersion = function(recurring) {
            self.B(this, 2, recurring);
        };
        /**
         * @param {Node} response
         * @param {string} total
         * @param {string} end
         * @return {undefined}
         */
        var done = function(response, total, end) {
            this.c = total || "e";
            this.g = end || "en";
            this.a = new self.kd(function($sanitize) {
                response.get(self.T).J().listen("capi:fsc", function(exports) {
                    $sanitize(new block(exports.S));
                });
            });
            self.G(this, this.a.cancel, this.a);
        };
        self.p(done, self.F);
        /**
         * @param {Object} obj
         * @param {Node} data
         * @return {?}
         */
        var build = function(obj, data) {
            return obj.a.then(function(ready) {
                var property = self.zf(self.A(ready, 1), "frame2");
                self.R(property, "v", self.A(ready, 2));
                self.R(property, "pvt", self.A(ready, 10));
                self.R(property, "prop", self.A(ready, 11));
                if (null == property.toString().match(/\/u\/\d+\//)) {
                    self.R(property, "authuser", self.A(ready, 4));
                }
                self.R(property, "hl", this.g);
                self.pf(property, this.c + data.l());
                return property.toString();
            }, void 0, obj);
        };
        /**
         * @param {number} segment
         * @return {undefined}
         */
        var resume = function(segment) {
            var p3 = clear(segment.get(actualKey).a) || window.chrome.i18n.getMessage("@@ui_locale");
            done.call(this, segment, "p", p3);
            /** @type {number} */
            this.b = segment;
        };
        self.p(resume, done);
        /**
         * @param {Object} value
         * @param {Node} resolved
         * @param {string} _callback
         * @param {Array} callback
         * @return {?}
         */
        var then = function(value, resolved, _callback, callback) {
            return build(value, resolved).then(function(dataAndEvents) {
                return new initialize(value.b, dataAndEvents, _callback, void 0, callback);
            });
        };
        /**
         * @param {Node} fn
         * @return {undefined}
         */
        var watch = function(fn) {
            self.F.call(this);
            if (window.chrome.gcm) {
                window.chrome.gcm.register(["1021803392181"], function(div1) {
                    if (div1) {
                        self.S("Got GCM registration id");
                        window.chrome.gcm.onMessage.addListener(function() {
                        });
                        window.chrome.gcm.onMessagesDeleted.addListener(function() {
                            self.S("GCM deleted message");
                        });
                        self.Be(function() {
                            /** @type {Array} */
                            var a = [div1, window.chrome.runtime.id, "1021803392181"];
                            var exports = new self.Wf("p");
                            /** @type {string} */
                            exports.a = "erd";
                            f4(exports, a);
                            /** @type {string} */
                            exports.c = "";
                            /** @type {string} */
                            exports.g = "e";
                            a = new self.Wf("p");
                            /** @type {string} */
                            a.a = "onSendHostMessage";
                            f4(a, JSON.stringify(exports.message()));
                            /** @type {string} */
                            a.c = "";
                            /** @type {string} */
                            a.g = "e";
                            exports = new self.Gf;
                            /** @type {string} */
                            a = JSON.stringify(a.message());
                            self.B(exports, 1, a);
                            fn.get(self.T).sendMessage(exports);
                        }, 2E3);
                    } else {
                        self.S("Didn't get GCM registration id: %s", window.chrome.runtime.lastError.message);
                    }
                });
            }
        };
        self.p(watch, self.F);
        /**
         * @param {number} stat
         * @return {undefined}
         */
        var label = function(stat) {
            self.z(this, stat, "capi:id_sc", null);
        };
        self.p(label, self.y);
        /** @type {string} */
        label.messageId = "capi:id_sc";
        /**
         * @param {Node} arr
         * @return {undefined}
         */
        var last = function(arr) {
            self.L.call(this);
            var ready = new self.N(this);
            self.I(ready, ready);
            this.a = arr.get(self.T);
        };
        self.p(last, self.L);
        /**
         * @param {?} row
         * @param {string} type
         * @return {undefined}
         */
        var setActive = function(row, type) {
            var dblclick = new label;
            switch(type) {
                case "idle":
                    self.B(dblclick, 1, 0);
                    break;
                case "active":
                    self.B(dblclick, 1, 1);
            }
            row.a.sendMessage(dblclick);
        };
        /**
         * @param {?} dt
         * @return {undefined}
         */
        var tick = function(dt) {
            var cleanCache = this;
            self.F.call(this);
            this.h = new last(dt);
            /** @type {boolean} */
            this.c = false;
            this.a = new self.ze;
            self.I(this, this.a);
            this.b = new self.ze;
            self.I(this, this.b);
            window.chrome.idle.setDetectionInterval(300);
            /**
             * @return {?}
             */
            var name = function() {
                return cleanCache.g;
            };
            window.chrome.idle.onStateChanged.addListener(name);
            self.G(this, function() {
                return window.chrome.idle.onStateChanged.removeListener(name);
            });
            this.a.setInterval(5E3);
            this.a.listen("tick", this.g, void 0, this);
            self.Ae(this.a);
            this.b.setInterval(9E5);
            this.b.listen("tick", this.l, void 0, this);
            self.Ae(this.b);
        };
        self.p(tick, self.F);
        /**
         * @return {undefined}
         */
        tick.prototype.l = function() {
            /** @type {boolean} */
            this.c = true;
            this.g();
        };
        /**
         * @return {undefined}
         */
        tick.prototype.g = function() {
            var item = this;
            window.chrome.idle.queryState(300, function(j) {
                if (item.c || j != item.j) {
                    switch(item.c = false, item.j = j, j) {
                        case "idle":
                            ;
                        case "locked":
                            setActive(item.h, "idle");
                            break;
                        case "active":
                            setActive(item.h, "active");
                    }
                }
            });
        };
        /**
         * @param {?} array
         * @return {undefined}
         */
        var Float32Array = function(array) {
            this.a = array;
        };
        var seen = {};
        /**
         * @param {?} opt_node
         * @param {?} success
         * @param {?} array
         * @return {undefined}
         */
        var all = function(opt_node, success, array) {
            self.K.call(this, opt_node, success);
            this.a = array;
        };
        self.p(all, self.K);
        /**
         * @param {number} object
         * @return {?}
         */
        var defineProperties = function(object) {
            if (self.Cb) {
                object = ref(object);
            } else {
                if (self.Gb && self.Db) {
                    a: {
                        switch(object) {
                            case 93:
                                /** @type {number} */
                                object = 91;
                                break a;
                        }
                    }
                }
            }
            return object;
        };
        /**
         * @param {number} value
         * @return {?}
         */
        var ref = function(value) {
            switch(value) {
                case 61:
                    return 187;
                case 59:
                    return 186;
                case 173:
                    return 189;
                case 224:
                    return 91;
                case 0:
                    return 224;
                default:
                    return value;
            }
        };
        var iterable = {
            8 : "backspace",
            9 : "tab",
            13 : "enter",
            16 : "shift",
            17 : "ctrl",
            18 : "alt",
            19 : "pause",
            20 : "caps-lock",
            27 : "esc",
            32 : "space",
            33 : "pg-up",
            34 : "pg-down",
            35 : "end",
            36 : "home",
            37 : "left",
            38 : "up",
            39 : "right",
            40 : "down",
            45 : "insert",
            46 : "delete",
            48 : "0",
            49 : "1",
            50 : "2",
            51 : "3",
            52 : "4",
            53 : "5",
            54 : "6",
            55 : "7",
            56 : "8",
            57 : "9",
            59 : "semicolon",
            61 : "equals",
            65 : "a",
            66 : "b",
            67 : "c",
            68 : "d",
            69 : "e",
            70 : "f",
            71 : "g",
            72 : "h",
            73 : "i",
            74 : "j",
            75 : "k",
            76 : "l",
            77 : "m",
            78 : "n",
            79 : "o",
            80 : "p",
            81 : "q",
            82 : "r",
            83 : "s",
            84 : "t",
            85 : "u",
            86 : "v",
            87 : "w",
            88 : "x",
            89 : "y",
            90 : "z",
            93 : "context",
            96 : "num-0",
            97 : "num-1",
            98 : "num-2",
            99 : "num-3",
            100 : "num-4",
            101 : "num-5",
            102 : "num-6",
            103 : "num-7",
            104 : "num-8",
            105 : "num-9",
            106 : "num-multiply",
            107 : "num-plus",
            109 : "num-minus",
            110 : "num-period",
            111 : "num-division",
            112 : "f1",
            113 : "f2",
            114 : "f3",
            115 : "f4",
            116 : "f5",
            117 : "f6",
            118 : "f7",
            119 : "f8",
            120 : "f9",
            121 : "f10",
            122 : "f11",
            123 : "f12",
            186 : "semicolon",
            187 : "equals",
            189 : "dash",
            188 : ",",
            190 : ".",
            191 : "/",
            192 : "`",
            219 : "open-square-bracket",
            220 : "\\",
            221 : "close-square-bracket",
            222 : "single-quote",
            224 : "win"
        };
        /**
         * @param {?} e
         * @return {undefined}
         */
        var setup = function(e) {
            self.L.call(this);
            this.b = this.g = {};
            /** @type {number} */
            this.j = 0;
            this.O = min(pdataOld);
            this.T = min(udataCur);
            /** @type {boolean} */
            this.w = true;
            /** @type {boolean} */
            this.v = false;
            /** @type {null} */
            this.l = null;
            this.a = e;
            self.ge(this.a, "keydown", this.h, void 0, this);
            if (self.Hb) {
                self.ge(this.a, "keypress", this.H, void 0, this);
            }
            self.ge(this.a, "keyup", this.A, void 0, this);
        };
        var elems;
        self.p(setup, self.L);
        /**
         * @param {boolean} cb
         * @return {undefined}
         */
        var makeCallback = function(cb) {
            this.a = cb || null;
            /** @type {(null|{})} */
            this.next = cb ? null : {};
        };
        /** @type {Array} */
        var pdataOld = [27, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 19];
        /** @type {Array.<string>} */
        var udataCur = "color date datetime datetime-local email month number password search tel text time url week".split(" ");
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        setup.prototype.P = function(types, type) {
            frame(this.g, getArgs(arguments), types);
        };
        /**
         * @param {Array} args
         * @return {?}
         */
        var getArgs = function(args) {
            if (self.t(args[1])) {
                args = (0, self.ab)(getKeys(args[1]), function(event) {
                    return event.keyCode & 255 | event.Xb << 8;
                });
            } else {
                /** @type {Array} */
                var spec = args;
                /** @type {number} */
                var i = 1;
                if (self.Ca(args[1])) {
                    spec = args[1];
                    /** @type {number} */
                    i = 0;
                }
                /** @type {Array} */
                args = [];
                for (;i < spec.length;i += 2) {
                    args.push(spec[i] & 255 | spec[i + 1] << 8);
                }
            }
            return args;
        };
        /**
         * @return {undefined}
         */
        setup.prototype.B = function() {
            setup.D.B.call(this);
            this.g = {};
            self.oe(this.a, "keydown", this.h, false, this);
            if (self.Hb) {
                self.oe(this.a, "keypress", this.H, false, this);
            }
            self.oe(this.a, "keyup", this.A, false, this);
            /** @type {null} */
            this.a = null;
        };
        /**
         * @param {string} hash
         * @return {?}
         */
        var getKeys = function(hash) {
            hash = hash.replace(/[ +]*\+[ +]*/g, "+").replace(/[ ]+/g, " ").toLowerCase();
            hash = hash.split(" ");
            /** @type {Array} */
            var rv = [];
            var dep;
            /** @type {number} */
            var require = 0;
            for (;dep = hash[require];require++) {
                var keys = dep.split("+");
                /** @type {null} */
                var elem = null;
                /** @type {number} */
                dep = 0;
                var key;
                /** @type {number} */
                var j = 0;
                for (;key = keys[j];j++) {
                    switch(key) {
                        case "shift":
                            dep |= 1;
                            continue;
                        case "ctrl":
                            dep |= 2;
                            continue;
                        case "alt":
                            dep |= 4;
                            continue;
                        case "meta":
                            dep |= 8;
                            continue;
                    }
                    keys = key;
                    if (!elems) {
                        elem = {};
                        key = void 0;
                        for (key in iterable) {
                            elem[iterable[key]] = defineProperties((0, window.parseInt)(key, 10));
                        }
                        elems = elem;
                    }
                    elem = elems[keys];
                    break;
                }
                rv.push({
                    keyCode : elem,
                    Xb : dep
                });
            }
            return rv;
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        setup.prototype.A = function(type) {
            if (self.Cb) {
                initialise(this, type);
            }
            if (self.Hb) {
                if (!this.L) {
                    if (self.Hb) {
                        if (type.j) {
                            if (type.h) {
                                this.h(type);
                            }
                        }
                    }
                }
            }
        };
        /**
         * @param {Node} s
         * @param {string} ready
         * @return {undefined}
         */
        var initialise = function(s, ready) {
            if (self.Gb) {
                if (224 == ready.keyCode) {
                    /** @type {boolean} */
                    s.M = true;
                    self.Be(function() {
                        /** @type {boolean} */
                        this.M = false;
                    }, 400, s);
                    return;
                }
                var jQuery = ready.C || s.M;
                if (!(67 != ready.keyCode && (88 != ready.keyCode && 86 != ready.keyCode))) {
                    if (!!jQuery) {
                        ready.C = jQuery;
                        s.h(ready);
                    }
                }
            }
            if (32 == s.l) {
                if (32 == ready.keyCode) {
                    ready.c();
                }
            }
            /** @type {null} */
            s.l = null;
        };
        /**
         * @param {?} spy
         * @return {undefined}
         */
        setup.prototype.H = function(spy) {
            if (32 < spy.keyCode) {
                if (self.Hb) {
                    if (spy.j) {
                        if (spy.h) {
                            /** @type {boolean} */
                            this.L = true;
                        }
                    }
                }
            }
        };
        /**
         * @param {?} obj
         * @param {Array} next
         * @param {string} context
         * @return {undefined}
         */
        var frame = function(obj, next, context) {
            var key = next.shift();
            var value = obj[key];
            if (value && (0 == next.length || value.a)) {
                throw Error("x");
            }
            if (next.length) {
                key = key.toString();
                value = new makeCallback;
                value = key in obj ? obj[key] : obj[key] = value;
                frame(value.next, next, context);
            } else {
                obj[key] = new makeCallback(context);
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        setup.prototype.h = function(types) {
            var l;
            l = types.keyCode;
            if (16 == l || (17 == l || 18 == l)) {
                /** @type {boolean} */
                l = false;
            } else {
                var element = types.target;
                /** @type {boolean} */
                var t = "TEXTAREA" == element.tagName || ("INPUT" == element.tagName || ("BUTTON" == element.tagName || "SELECT" == element.tagName));
                var suppressDebug = !t && (element.isContentEditable || element.ownerDocument && "on" == element.ownerDocument.designMode);
                /** @type {boolean} */
                l = !t && !suppressDebug || (this.O[l] || this.v) ? true : suppressDebug ? false : types.h || (types.j || types.C) ? true : "INPUT" == element.tagName && this.T[element.type] ? 13 == l : "INPUT" == element.tagName || "BUTTON" == element.tagName ? 32 != l : false;
            }
            if (l) {
                if ("keydown" == types.type && (self.Hb && (types.j && types.h))) {
                    /** @type {boolean} */
                    this.L = false;
                } else {
                    l = defineProperties(types.keyCode);
                    /** @type {number} */
                    element = l & 255 | ((types.v ? 1 : 0) | (types.j ? 2 : 0) | (types.h ? 4 : 0) | (types.C ? 8 : 0)) << 8;
                    if (!this.b[element] || 1500 <= (0, self.sa)() - this.j) {
                        this.b = this.g;
                        this.j = (0, self.sa)();
                    }
                    if (element = this.b[element]) {
                        if (element.next) {
                            this.b = element.next;
                            this.j = (0, self.sa)();
                            types.c();
                        } else {
                            this.b = this.g;
                            this.j = (0, self.sa)();
                            if (this.w) {
                                types.c();
                            }
                            element = element.a;
                            t = types.target;
                            suppressDebug = self.M(this, new up("shortcut", element, t));
                            if (!(suppressDebug &= self.M(this, new up("shortcut_" + element, element, t)))) {
                                types.c();
                            }
                            if (self.Cb) {
                                this.l = l;
                            }
                        }
                    }
                }
            }
        };
        /**
         * @param {?} evt
         * @param {?} a
         * @param {?} inEvent
         * @return {undefined}
         */
        var up = function(evt, a, inEvent) {
            self.K.call(this, evt, inEvent);
            this.a = a;
        };
        self.p(up, self.K);
        /**
         * @param {?} substitution
         * @param {?} opts
         * @param {?} v
         * @return {undefined}
         */
        var d = function(substitution, opts, v) {
            self.L.call(this);
            this.j = v;
            this.b = new self.N(this);
            self.I(this, this.b);
            /** @type {Array} */
            this.a = [];
            this.g = self.qb(seen);
            self.sb(this.g, opts);
        };
        self.p(d, self.L);
        /**
         * @return {undefined}
         */
        d.prototype.B = function() {
            d.D.B.call(this);
            (0, self.u)(this.a, function(object) {
                self.H(object.a);
            });
        };
        /**
         * @param {Object} data
         * @param {?} thisObj
         * @return {undefined}
         */
        var play = function(data, thisObj) {
            var copies = self.db(data.a, function(event) {
                return event.b == thisObj;
            });
            if (null != copies) {
                fire(data, thisObj);
            }
            var doc = new setup(thisObj);
            doc.v = data.j;
            /** @type {boolean} */
            doc.w = false;
            groupBy(data, doc);
            copies = new _overlap_interval(thisObj, doc);
            data.a.push(copies);
            data.b.listen(doc, "shortcut", data.h);
        };
        /**
         * @param {Object} data
         * @param {?} thisObj
         * @return {undefined}
         */
        var fire = function(data, thisObj) {
            var doc = self.db(data.a, function(event) {
                return event.b == thisObj;
            });
            if (null != doc) {
                data.b.ta(doc.a, "shortcut", data.h);
                self.H(doc.a);
                self.gb(data.a, doc);
            }
        };
        /**
         * @param {Object} obj
         * @param {?} val
         * @return {undefined}
         */
        var groupBy = function(obj, val) {
            self.mb(obj.g, function(d, ready) {
                if (d.a) {
                    val.P(ready, d.a);
                }
            });
        };
        /**
         * @param {string} types
         * @return {?}
         */
        d.prototype.h = function(types) {
            var g = this.g[types.a];
            self.M(this, new all("v", types.target, g));
            return self.M(this, new all("w", types.target, g));
        };
        /**
         * @param {?} a
         * @param {?} b
         * @return {undefined}
         */
        var _overlap_interval = function(a, b) {
            this.b = a;
            this.a = b;
        };
        /** @type {number} */
        var cy = self.Gb ? 8 : 2;
        /** @type {number} */
        var y2 = cy | 1;
        var array = new Float32Array([38, y2, 38, y2, 40, y2, 40, y2, 37, y2, 39, y2, 37, y2, 39, y2, 68, y2, 68, y2]);
        var fa = new Float32Array([48, cy]);
        var _size = new Float32Array([107, cy]);
        var vec = new Float32Array([187, cy]);
        var uvArray = new Float32Array([187, cy | 1]);
        var coordArray = new Float32Array([109, cy]);
        var floatSubArray = new Float32Array([189, cy]);
        var newPos = new Float32Array([189, cy | 1]);
        /**
         * @return {undefined}
         */
        var unknown = function() {
            d.call(this, 0, pos, true);
        };
        self.p(unknown, d);
        var pos = {
            tb : array,
            Ab : fa,
            ub : _size,
            vb : vec,
            wb : uvArray,
            xb : coordArray,
            yb : floatSubArray,
            zb : newPos
        };
        /**
         * @param {number} context
         * @param {?} b
         * @return {undefined}
         */
        var constructor = function(context, b) {
            var args = this;
            self.N.call(this);
            /** @type {number} */
            this.h = context;
            this.b = b;
            this.a = context.get(actualKey);
            this.l = context.get(elem);
            this.c = context.get(modalInstance);
            this.j = context.get(k);
            set(this.b, function() {
                args.b.onBoundsChanged.addListener(function() {
                    var expected = getSize(args.b);
                    if (expected) {
                        var a = args.a;
                        expected = inspect(expected);
                        var value = new RegExp;
                        self.E(value, 2, expected);
                        F(a, value);
                    }
                });
                args.listen(args.c, "change", function() {
                    if (self.wb() && parseFloat(args.a)) {
                        compare(args.j, true, 1E4);
                    } else {
                        if (!args.l.g()) {
                            var expected = getSize(args.b);
                            var e = new slide(expected.left, expected.top, expected.width, expected.height);
                            var a;
                            a = args.a;
                            var dblclick = apply(a);
                            var value = self.A(dblclick, 4);
                            if (value) {
                                if (!self.A(dblclick, 5)) {
                                    dblclick = new RegExp;
                                    self.B(dblclick, 4, false);
                                    F(a, dblclick);
                                    /** @type {boolean} */
                                    value = false;
                                }
                            }
                            a = value;
                            dblclick = require(args.a);
                            var options;
                            if (a) {
                                value = args.c;
                                var left = new Rect(expected.left + expected.width / 2, expected.top + expected.height / 2);
                                /** @type {number} */
                                var minDot = Number.MAX_VALUE;
                                /** @type {number} */
                                var i = 0;
                                for (;i < value.a.length;i++) {
                                    var rect = _clone(value.a[i]);
                                    if (!dblclick) {
                                        rect.left += rect.width;
                                    }
                                    /** @type {number} */
                                    rect.width = 0;
                                    rect = scrollTo(rect, left);
                                    if (rect < minDot) {
                                        minDot = rect;
                                        options = _clone(value.a[i]);
                                    }
                                }
                            } else {
                                options = raycast_mesh(args.c, new Rect(expected.left + expected.width / 2, expected.top + expected.height / 2));
                            }
                            value = options.left;
                            /** @type {number} */
                            left = options.left + options.width - expected.width;
                            if (a) {
                                e.left = dblclick ? value : left;
                            }
                            scroll(e, options, dblclick);
                            if (!assertEquals(expected, e)) {
                                expect(args.b, inspect(e));
                            }
                        }
                    }
                });
            });
            var cycle = context.get(key2);
            this.listen(cycle, "w", this.w);
            this.listen(this.j, "k", function() {
                return reset(args.h);
            });
        };
        self.p(constructor, self.N);
        /**
         * @param {string} str
         * @return {undefined}
         */
        constructor.prototype.w = function(str) {
            var p = str.target;
            if (self.ya(p) && 0 < p.nodeType) {
                switch(p = pos, str.a) {
                    case p.tb:
                        reset(this.h);
                        break;
                    case p.Ab:
                        wait(this.a, 6);
                        break;
                    case p.ub:
                        ;
                    case p.vb:
                        ;
                    case p.wb:
                        wait(this.a, rest(hex(this.a) + 1));
                        break;
                    case p.xb:
                        ;
                    case p.yb:
                        ;
                    case p.zb:
                        wait(this.a, rest(hex(this.a) - 1));
                }
            }
        };
        /**
         * @param {?} item
         * @param {number} el
         * @param {number} t
         * @return {undefined}
         */
        var scroll = function(item, el, t) {
            var val = el.left;
            /** @type {number} */
            var olen = el.left + el.width - item.width;
            /** @type {number} */
            item.left = t ? Math.max(item.left, val) : Math.min(item.left, olen);
            t = el.top;
            /** @type {number} */
            el = el.top + el.height - item.height;
            if (item.top < t) {
                /** @type {number} */
                item.top = t;
            } else {
                if (item.top > el) {
                    /** @type {number} */
                    item.top = el;
                }
            }
        };
        if (self.x) {
            self.Pb(8);
        }
        var LI = {};
        var preferredDoc = {};
        var style = {};
        var title = {};
        /**
         * @return {?}
         */
        var v = function() {
            throw Error("y");
        };
        /** @type {null} */
        v.prototype.a = null;
        /**
         * @return {?}
         */
        v.prototype.qa = function() {
            return this.content;
        };
        /**
         * @return {?}
         */
        v.prototype.toString = function() {
            return this.content;
        };
        /**
         * @param {Object} p
         * @return {?}
         */
        var convert = function(p) {
            var a = self.vc();
            p = p(path, void 0, void 0);
            var text = regExpEscape(p);
            if (p instanceof v) {
                if (p.ka === title) {
                    p = isArray(p.toString());
                } else {
                    if (p.ka !== LI) {
                        throw Error("z");
                    }
                    p = self.rc(p.toString(), p.a || null);
                }
            } else {
                p = self.rc(text, null);
            }
            a = a.a;
            /** @type {Object} */
            text = p;
            p = a.createElement("DIV");
            if (self.x) {
                text = addClass(self.sc, text);
                p.innerHTML = marked(text);
                p.removeChild(p.firstChild);
            } else {
                p.innerHTML = marked(text);
            }
            if (1 == p.childNodes.length) {
                a = p.removeChild(p.firstChild);
            } else {
                a = a.createDocumentFragment();
                for (;p.firstChild;) {
                    a.appendChild(p.firstChild);
                }
            }
            return a;
        };
        /**
         * @param {Node} first
         * @param {string} opt_attributes
         * @param {Node} fragment
         * @return {?}
         */
        var fragment = function(first, opt_attributes, fragment) {
            a: {
                if (first = first(opt_attributes || path, void 0, void 0), fragment = (fragment || self.vc()).a.createElement("DIV"), first = regExpEscape(first), fragment.innerHTML = first, 1 == fragment.childNodes.length && (first = fragment.firstChild, 1 == first.nodeType)) {
                    /** @type {Node} */
                    fragment = first;
                    break a;
                }
            }
            return fragment;
        };
        /**
         * @param {?} s
         * @return {?}
         */
        var regExpEscape = function(s) {
            if (!self.ya(s)) {
                return String(s);
            }
            if (s instanceof v) {
                if (s.ka === LI) {
                    return s.qa();
                }
                if (s.ka === title) {
                    return self.Sa(s.qa());
                }
            }
            return "zSoyz";
        };
        var path = {};
        /**
         * @return {undefined}
         */
        var type = function() {
            v.call(this);
        };
        self.p(type, v);
        type.prototype.ka = LI;
        var createMultiple = function(constructor) {
            /**
             * @param {string} content
             * @return {undefined}
             */
            function temp(content) {
                /** @type {string} */
                this.content = content;
            }
            temp.prototype = constructor.prototype;
            return function(opt_message, opacity) {
                var obj = new temp(String(opt_message));
                if (void 0 !== opacity) {
                    /** @type {number} */
                    obj.a = opacity;
                }
                return obj;
            };
        }(type);
        (function(constructor) {
            /**
             * @param {string} content
             * @return {undefined}
             */
            function temp(content) {
                /** @type {string} */
                this.content = content;
            }
            temp.prototype = constructor.prototype;
            return function(obj, i) {
                /** @type {string} */
                var t = String(obj);
                if (!t) {
                    return "";
                }
                t = new temp(t);
                if (void 0 !== i) {
                    /** @type {number} */
                    t.a = i;
                }
                return t;
            };
        })(type);
        /**
         * @param {string} s
         * @return {?}
         */
        var escapeHtml = function(s) {
            return null != s && s.ka === LI ? String(String(s.qa()).replace(rclass, "").replace(badChars, "&lt;")).replace(rQuot, tokenizeEvaluate) : self.Sa(String(s));
        };
        /**
         * @param {string} a
         * @return {?}
         */
        var extend = function(a) {
            if (null != a && a.ka === preferredDoc || null != a && a.ka === style) {
                /** @type {string} */
                a = String(a).replace(r20, span);
            } else {
                if (a instanceof B) {
                    if (a instanceof B && (a.constructor === B && a.b === result_b)) {
                        a = a.a;
                    } else {
                        self.va(a);
                        /** @type {string} */
                        a = "type_error:SafeUrl";
                    }
                    /** @type {string} */
                    a = String(a).replace(r20, span);
                } else {
                    if (a instanceof NodeList) {
                        if (a instanceof NodeList && (a.constructor === NodeList && a.a === a1)) {
                            /** @type {string} */
                            a = "";
                        } else {
                            self.va(a);
                            /** @type {string} */
                            a = "type_error:TrustedResourceUrl";
                        }
                        /** @type {string} */
                        a = String(a).replace(r20, span);
                    } else {
                        /** @type {string} */
                        a = String(a);
                        /** @type {string} */
                        a = rchecked.test(a) ? a.replace(r20, span) : "about:invalid#zSoyz";
                    }
                }
            }
            return a;
        };
        var escapes = {
            "\x00" : "&#0;",
            "\t" : "&#9;",
            "\n" : "&#10;",
            "\x0B" : "&#11;",
            "\f" : "&#12;",
            "\r" : "&#13;",
            " " : "&#32;",
            '"' : "&quot;",
            "&" : "&amp;",
            "'" : "&#39;",
            "-" : "&#45;",
            "/" : "&#47;",
            "<" : "&lt;",
            "=" : "&#61;",
            ">" : "&gt;",
            "`" : "&#96;",
            "\u0085" : "&#133;",
            "\u00a0" : "&#160;",
            "\u2028" : "&#8232;",
            "\u2029" : "&#8233;"
        };
        /**
         * @param {?} match
         * @return {?}
         */
        var tokenizeEvaluate = function(match) {
            return escapes[match];
        };
        var scheduledFunctions = {
            "\x00" : "%00",
            "\u0001" : "%01",
            "\u0002" : "%02",
            "\u0003" : "%03",
            "\u0004" : "%04",
            "\u0005" : "%05",
            "\u0006" : "%06",
            "\u0007" : "%07",
            "\b" : "%08",
            "\t" : "%09",
            "\n" : "%0A",
            "\x0B" : "%0B",
            "\f" : "%0C",
            "\r" : "%0D",
            "\u000e" : "%0E",
            "\u000f" : "%0F",
            "\u0010" : "%10",
            "\u0011" : "%11",
            "\u0012" : "%12",
            "\u0013" : "%13",
            "\u0014" : "%14",
            "\u0015" : "%15",
            "\u0016" : "%16",
            "\u0017" : "%17",
            "\u0018" : "%18",
            "\u0019" : "%19",
            "\u001a" : "%1A",
            "\u001b" : "%1B",
            "\u001c" : "%1C",
            "\u001d" : "%1D",
            "\u001e" : "%1E",
            "\u001f" : "%1F",
            " " : "%20",
            '"' : "%22",
            "'" : "%27",
            "(" : "%28",
            ")" : "%29",
            "<" : "%3C",
            ">" : "%3E",
            "\\" : "%5C",
            "{" : "%7B",
            "}" : "%7D",
            "\u007f" : "%7F",
            "\u0085" : "%C2%85",
            "\u00a0" : "%C2%A0",
            "\u2028" : "%E2%80%A8",
            "\u2029" : "%E2%80%A9",
            "\uff01" : "%EF%BC%81",
            "\uff03" : "%EF%BC%83",
            "\uff04" : "%EF%BC%84",
            "\uff06" : "%EF%BC%86",
            "\uff07" : "%EF%BC%87",
            "\uff08" : "%EF%BC%88",
            "\uff09" : "%EF%BC%89",
            "\uff0a" : "%EF%BC%8A",
            "\uff0b" : "%EF%BC%8B",
            "\uff0c" : "%EF%BC%8C",
            "\uff0f" : "%EF%BC%8F",
            "\uff1a" : "%EF%BC%9A",
            "\uff1b" : "%EF%BC%9B",
            "\uff1d" : "%EF%BC%9D",
            "\uff1f" : "%EF%BC%9F",
            "\uff20" : "%EF%BC%A0",
            "\uff3b" : "%EF%BC%BB",
            "\uff3d" : "%EF%BC%BD"
        };
        /**
         * @param {?} timeoutKey
         * @return {?}
         */
        var span = function(timeoutKey) {
            return scheduledFunctions[timeoutKey];
        };
        /** @type {RegExp} */
        var rQuot = /[\x00\x22\x27\x3c\x3e]/g;
        /** @type {RegExp} */
        var r20 = /[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g;
        /** @type {RegExp} */
        var rchecked = /^(?![^#?]*\/(?:\.|%2E){2}(?:[\/?#]|$))(?:(?:https?|mailto):|[^&:\/?#]*(?:[\/?#]|$))/i;
        /** @type {RegExp} */
        var rclass = /<(?:!|\/?([a-zA-Z][a-zA-Z0-9:\-]*))(?:[^>'"]|"[^"]*"|'[^']*')*>/g;
        /** @type {RegExp} */
        var badChars = /</g;
        /**
         * @return {?}
         */
        var one = function() {
            return createMultiple('<div class="tLanXe yTYtyc"></div><div class="glECie FJoqob OzKC3"></div><div class="hvD8ob"><div class="gIdGUb d22q4d"><div class="ZF9l5d Ox4l4"></div></div></div>');
        };
        /**
         * @param {(number|string)} value
         * @param {boolean} recurring
         * @return {?}
         */
        var round = function(value, recurring) {
            if ("number" == typeof value) {
                /** @type {string} */
                value = (recurring ? Math.round(value) : value) + "px";
            }
            return value;
        };
        /**
         * @param {Element} element
         * @param {boolean} value
         * @return {undefined}
         */
        var html = function(element, value) {
            /** @type {string} */
            element.style.display = value ? "" : "none";
        };
        /**
         * @return {undefined}
         */
        var Foo = function() {
        };
        encodeUriSegment(Foo);
        /** @type {number} */
        Foo.prototype.a = 0;
        /**
         * @param {string} timeObject
         * @return {undefined}
         */
        var A = function(timeObject) {
            self.L.call(this);
            this.L = timeObject || self.vc();
            /** @type {null} */
            this.P = null;
            /** @type {boolean} */
            this.la = false;
            /** @type {null} */
            this.b = null;
            this.g = void 0;
            /** @type {null} */
            this.A = this.H = this.j = null;
        };
        self.p(A, self.L);
        A.prototype.Y = Foo.za();
        /**
         * @return {?}
         */
        A.prototype.a = function() {
            return this.P || (this.P = ":" + (this.Y.a++).toString(36));
        };
        /**
         * @return {?}
         */
        A.prototype.F = function() {
            return this.b;
        };
        /**
         * @param {string} options
         * @param {string} d
         * @return {?}
         */
        var objectToString = function(options, d) {
            return options.b ? self.xc(d, options.b || options.L.a) : null;
        };
        /**
         * @param {?} e
         * @return {?}
         */
        var watchErr = function(e) {
            if (!e.g) {
                e.g = new self.N(e);
            }
            return e.g;
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        A.prototype.I = function(types) {
            if (this.j && this.j != types) {
                throw Error("C");
            }
            A.D.I.call(this, types);
        };
        /**
         * @return {?}
         */
        A.prototype.ya = function() {
            return this.L;
        };
        /**
         * @return {undefined}
         */
        A.prototype.na = function() {
            this.b = this.L.a.createElement("DIV");
        };
        /**
         * @param {Object} item
         * @param {Element} obj
         * @return {undefined}
         */
        var addItem = function(item, obj) {
            if (item.la) {
                throw Error("D");
            }
            if (!item.b) {
                item.na();
            }
            if (obj) {
                obj.insertBefore(item.b, null);
            } else {
                item.L.a.body.appendChild(item.b);
            }
            if (!(item.j && !item.j.la)) {
                item.ca();
            }
        };
        /**
         * @return {undefined}
         */
        A.prototype.ca = function() {
            /** @type {boolean} */
            this.la = true;
            isElement(this, function(n) {
                if (!n.la) {
                    if (n.F()) {
                        n.ca();
                    }
                }
            });
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        var removeChild = function(obj) {
            isElement(obj, function(walkers) {
                if (walkers.la) {
                    removeChild(walkers);
                }
            });
            if (obj.g) {
                obj.g.removeAll();
            }
            /** @type {boolean} */
            obj.la = false;
        };
        /**
         * @return {undefined}
         */
        A.prototype.B = function() {
            if (this.la) {
                removeChild(this);
            }
            if (this.g) {
                this.g.Z();
                delete this.g;
            }
            isElement(this, function(xyz) {
                xyz.Z();
            });
            if (this.b) {
                swap(this.b);
            }
            /** @type {null} */
            this.j = this.b = this.A = this.H = null;
            A.D.B.call(this);
        };
        /**
         * @param {Object} obj
         * @param {Function} value
         * @return {undefined}
         */
        var isElement = function(obj, value) {
            if (obj.H) {
                (0, self.u)(obj.H, value, void 0);
            }
        };
        /**
         * @param {string} child
         * @param {?} opt_unrender
         * @return {?}
         */
        A.prototype.removeChild = function(child, opt_unrender) {
            if (child) {
                var id = self.t(child) ? child : child.a();
                var done;
                if (this.A && id) {
                    done = this.A;
                    done = (null !== done && id in done ? done[id] : void 0) || null;
                } else {
                    /** @type {null} */
                    done = null;
                }
                child = done;
                if (id && child) {
                    done = this.A;
                    if (id in done) {
                        delete done[id];
                    }
                    self.gb(this.H, child);
                    if (opt_unrender) {
                        removeChild(child);
                        if (child.b) {
                            swap(child.b);
                        }
                    }
                    /** @type {string} */
                    id = child;
                    if (null == id) {
                        throw Error("B");
                    }
                    /** @type {null} */
                    id.j = null;
                    A.D.I.call(id, null);
                }
            }
            if (!child) {
                throw Error("E");
            }
            return child;
        };
        /**
         * @param {?} value
         * @return {undefined}
         */
        var Literal = function(value) {
            A.call(this, value);
        };
        self.p(Literal, A);
        /**
         * @return {undefined}
         */
        Literal.prototype.na = function() {
            this.b = fragment(one, void 0, this.ya());
        };
        /**
         * @return {undefined}
         */
        Literal.prototype.ca = function() {
            Literal.D.ca.call(this);
            text(objectToString(this, "glECie"), window.chrome.i18n.getMessage("CHROME_APP_CONNECTION_ERROR_DETAIL"));
            text(objectToString(this, "tLanXe"), window.chrome.i18n.getMessage("CHROME_APP_CONNECTION_ERROR_TITLE"));
            text(objectToString(this, "ZF9l5d"), window.chrome.i18n.getMessage("CHROME_APP_CONNECTION_ERROR_BUTTON"));
            watchErr(this).listen(objectToString(this, "gIdGUb"), "click", this.h);
        };
        /**
         * @return {undefined}
         */
        Literal.prototype.h = function() {
            window.chrome.runtime.reload();
        };
        /**
         * @return {?}
         */
        var stylesheets = function() {
            return createMultiple('<div class="KwUJl za99jd"><div class="ftkzZd EAIhBe"></div><div class="PdFW3"><div class="YZ17Mb"><div class="C64E9d MDvFI"></div><div class="L7IVff YZ1oWe"><div class="nkDZ6 cHJjp ydkj6c QMBe3c"></div><div class="bE4Lge cHJjp ydkj6c Heq7Jf"></div><div class="elCDOd rKV8Yb cHJjp ydkj6c"></div></div></div></div></div><div class="zkRavd cHJjp YSvmaf"></div><div class="G1uPUb s54bbb"></div><div id="uv_drag_region" class="xVwMu"><div class="HNBNSb"></div></div>');
        };
        /**
         * @return {?}
         */
        var images = function() {
            return createMultiple('<div class="KwUJl UgQOTe"><div class="nK6Nye"><div class="L3c0u U2TDqc"></div></div><div class="EmPSFe"><span class="WxquGe"></span> <a class="vkUZWc" target="_blank"></a></div><button class="rHCn5c xWbrW"></button></div>');
        };
        /**
         * @param {?} stel
         * @return {?}
         */
        var statsTemplate = function(stel) {
            return createMultiple('<div class="NfTrme yTYtyc"></div><div class="FJoqob UeuQKb"><span class="Kq8xGf"></span> <a class="PhCVj" target="_blank" href="' + escapeHtml(extend(stel.Ya)) + '"></a></div>');
        };
        /**
         * @param {?} val
         * @return {undefined}
         */
        var Comment = function(val) {
            A.call(this, val);
        };
        self.p(Comment, A);
        /**
         * @return {undefined}
         */
        Comment.prototype.na = function() {
            this.b = fragment(statsTemplate, {
                Ya : self.Vg
            }, this.ya());
        };
        /**
         * @return {undefined}
         */
        Comment.prototype.ca = function() {
            Comment.D.ca.call(this);
            text(objectToString(this, "NfTrme"), window.chrome.i18n.getMessage("CHROME_APP_OUTDATED_TITLE"));
            text(objectToString(this, "Kq8xGf"), window.chrome.i18n.getMessage("CHROME_APP_OUTDATED_DETAIL"));
            text(objectToString(this, "PhCVj"), window.chrome.i18n.getMessage("CHROME_APP_LEARN_MORE_MESSAGE"));
        };
        /**
         * @param {?} req
         * @return {?}
         */
        var scripts = function(req) {
            return createMultiple('<div class="PtUZ5e"><div class="v0h5Oe"></div><div class="yp1vyc NpbhUd"></div><div class="evTYnd"><span class="vLaJkf"></span> <a href="' + escapeHtml(extend(req.Ya)) + '" target="_blank" class="TTGEHc"></a></div><button class="SHZ6h WvHdHd"></button></div>');
        };
        /**
         * @param {Node} mod
         * @param {?} name
         * @return {undefined}
         */
        var module = function(mod, name) {
            A.call(this, name);
            this.h = mod.get(subKey);
        };
        self.p(module, A);
        /**
         * @return {undefined}
         */
        module.prototype.na = function() {
            this.b = fragment(scripts, {
                Ya : self.Ug
            }, this.ya());
        };
        /**
         * @return {undefined}
         */
        module.prototype.ca = function() {
            module.D.ca.call(this);
            text(objectToString(this, "yp1vyc"), window.chrome.i18n.getMessage("CHROME_APP_WELCOME_MESSAGE"));
            text(objectToString(this, "vLaJkf"), window.chrome.i18n.getMessage("CHROME_APP_CHROME_SIGNIN_MESSAGE"));
            text(objectToString(this, "TTGEHc"), window.chrome.i18n.getMessage("CHROME_APP_LEARN_MORE_MESSAGE"));
            text(objectToString(this, "SHZ6h"), window.chrome.i18n.getMessage("CHROME_APP_SIGNIN_BUTTON"));
            watchErr(this).listen(objectToString(this, "SHZ6h"), "click", this.l);
        };
        /**
         * @return {undefined}
         */
        module.prototype.l = function() {
            colorChanged(this.h);
        };
        /**
         * @return {?}
         */
        var nodes = function() {
            return createMultiple('<div class="zMUWEe"><div class="Anoad CeGTP"><div class="e2kKGc"></div></div><div class="eOXgae NRBRtc"><div class="xMMbVb"></div></div><div class="f37ZYc X1zc7"><div class="kV5Myd"></div></div><div class="C86Dyb HVpWnd"><div class="L1ipvb"></div></div><div class="UUGBcf ykhkZb xVwMu s1mHsd"><div class="G3qHP"></div></div><div class="KwZMgc jkvMQb xVwMu s1mHsd"><div class="HNBNSb"></div></div><div class="TbAvif xVwMu s1mHsd"><div class="HNBNSb"></div></div></div>');
        };
        var types = {
            23 : [[{
                left : 19,
                top : -3,
                width : 8,
                height : 1
            }, {
                left : 14,
                top : -2,
                width : 18,
                height : 1
            }, {
                left : 11,
                top : -1,
                width : 23,
                height : 1
            }, {
                left : 9,
                top : 0,
                width : 28,
                height : 1
            }, {
                left : 8,
                top : 1,
                width : 30,
                height : 1
            }, {
                left : 6,
                top : 2,
                width : 34,
                height : 1
            }, {
                left : 5,
                top : 3,
                width : 36,
                height : 1
            }, {
                left : 4,
                top : 4,
                width : 38,
                height : 1
            }, {
                left : 3,
                top : 5,
                width : 40,
                height : 1
            }, {
                left : 2,
                top : 6,
                width : 42,
                height : 1
            }, {
                left : 1,
                top : 7,
                width : 44,
                height : 1
            }, {
                left : 0,
                top : 8,
                width : 46,
                height : 1
            }, {
                left : -1,
                top : 9,
                width : 48,
                height : 1
            }, {
                left : -2,
                top : 10,
                width : 50,
                height : 2
            }, {
                left : -3,
                top : 12,
                width : 52,
                height : 2
            }, {
                left : -4,
                top : 14,
                width : 54,
                height : 2
            }, {
                left : -5,
                top : 16,
                width : 56,
                height : 3
            }, {
                left : -6,
                top : 19,
                width : 57,
                height : 1
            }, {
                left : -6,
                top : 20,
                width : 58,
                height : 13
            }, {
                left : -6,
                top : 33,
                width : 57,
                height : 1
            }, {
                left : -5,
                top : 34,
                width : 56,
                height : 3
            }, {
                left : -4,
                top : 37,
                width : 54,
                height : 2
            }, {
                left : -3,
                top : 39,
                width : 52,
                height : 2
            }, {
                left : -2,
                top : 41,
                width : 50,
                height : 2
            }, {
                left : -1,
                top : 43,
                width : 48,
                height : 1
            }, {
                left : 0,
                top : 44,
                width : 46,
                height : 1
            }, {
                left : 1,
                top : 45,
                width : 44,
                height : 1
            }, {
                left : 2,
                top : 46,
                width : 42,
                height : 1
            }, {
                left : 3,
                top : 47,
                width : 40,
                height : 1
            }, {
                left : 4,
                top : 48,
                width : 38,
                height : 1
            }, {
                left : 5,
                top : 49,
                width : 36,
                height : 1
            }, {
                left : 6,
                top : 50,
                width : 34,
                height : 1
            }, {
                left : 8,
                top : 51,
                width : 30,
                height : 1
            }, {
                left : 9,
                top : 52,
                width : 28,
                height : 1
            }, {
                left : 11,
                top : 53,
                width : 23,
                height : 1
            }, {
                left : 14,
                top : 54,
                width : 18,
                height : 1
            }, {
                left : 19,
                top : 55,
                width : 8,
                height : 1
            }], [{
                left : 19,
                top : 0,
                width : 8,
                height : 1
            }, {
                left : 15,
                top : 1,
                width : 16,
                height : 1
            }, {
                left : 13,
                top : 2,
                width : 20,
                height : 1
            }, {
                left : 11,
                top : 3,
                width : 24,
                height : 1
            }, {
                left : 10,
                top : 4,
                width : 26,
                height : 1
            }, {
                left : 8,
                top : 5,
                width : 30,
                height : 1
            }, {
                left : 7,
                top : 6,
                width : 32,
                height : 1
            }, {
                left : 6,
                top : 7,
                width : 34,
                height : 1
            }, {
                left : 5,
                top : 8,
                width : 36,
                height : 2
            }, {
                left : 4,
                top : 10,
                width : 38,
                height : 1
            }, {
                left : 3,
                top : 11,
                width : 40,
                height : 2
            }, {
                left : 2,
                top : 13,
                width : 42,
                height : 2
            }, {
                left : 1,
                top : 15,
                width : 44,
                height : 4
            }, {
                left : 0,
                top : 19,
                width : 46,
                height : 8
            }, {
                left : 1,
                top : 27,
                width : 44,
                height : 4
            }, {
                left : 2,
                top : 31,
                width : 42,
                height : 2
            }, {
                left : 3,
                top : 33,
                width : 40,
                height : 2
            }, {
                left : 4,
                top : 35,
                width : 38,
                height : 2
            }, {
                left : 5,
                top : 37,
                width : 36,
                height : 1
            }, {
                left : 6,
                top : 38,
                width : 34,
                height : 1
            }, {
                left : 7,
                top : 39,
                width : 32,
                height : 1
            }, {
                left : 8,
                top : 40,
                width : 30,
                height : 1
            }, {
                left : 9,
                top : 41,
                width : 28,
                height : 1
            }, {
                left : 11,
                top : 42,
                width : 24,
                height : 1
            }, {
                left : 13,
                top : 43,
                width : 20,
                height : 1
            }, {
                left : 15,
                top : 44,
                width : 16,
                height : 1
            }, {
                left : 18,
                top : 45,
                width : 10,
                height : 1
            }]],
            "13.5" : [[{
                left : 11,
                top : -3,
                width : 5,
                height : 1
            }, {
                left : 7,
                top : -2,
                width : 13,
                height : 1
            }, {
                left : 4,
                top : -1,
                width : 19,
                height : 1
            }, {
                left : 3,
                top : 0,
                width : 21,
                height : 1
            }, {
                left : 1,
                top : 1,
                width : 25,
                height : 1
            }, {
                left : 0,
                top : 2,
                width : 27,
                height : 1
            }, {
                left : -1,
                top : 3,
                width : 29,
                height : 1
            }, {
                left : -2,
                top : 4,
                width : 31,
                height : 1
            }, {
                left : -3,
                top : 5,
                width : 33,
                height : 2
            }, {
                left : -4,
                top : 7,
                width : 35,
                height : 2
            }, {
                left : -5,
                top : 9,
                width : 37,
                height : 3
            }, {
                left : -6,
                top : 12,
                width : 39,
                height : 10
            }, {
                left : -5,
                top : 22,
                width : 37,
                height : 3
            }, {
                left : -4,
                top : 25,
                width : 35,
                height : 2
            }, {
                left : -3,
                top : 27,
                width : 33,
                height : 2
            }, {
                left : -2,
                top : 29,
                width : 31,
                height : 1
            }, {
                left : -1,
                top : 30,
                width : 29,
                height : 1
            }, {
                left : 0,
                top : 31,
                width : 27,
                height : 1
            }, {
                left : 1,
                top : 32,
                width : 25,
                height : 1
            }, {
                left : 3,
                top : 33,
                width : 21,
                height : 1
            }, {
                left : 4,
                top : 34,
                width : 19,
                height : 1
            }, {
                left : 7,
                top : 35,
                width : 13,
                height : 1
            }, {
                left : 11,
                top : 36,
                width : 5,
                height : 1
            }], [{
                left : 10,
                top : 0,
                width : 7,
                height : 1
            }, {
                left : 7,
                top : 1,
                width : 13,
                height : 1
            }, {
                left : 6,
                top : 2,
                width : 15,
                height : 1
            }, {
                left : 4,
                top : 3,
                width : 19,
                height : 1
            }, {
                left : 3,
                top : 4,
                width : 21,
                height : 2
            }, {
                left : 2,
                top : 6,
                width : 23,
                height : 1
            }, {
                left : 1,
                top : 7,
                width : 25,
                height : 3
            }, {
                left : 0,
                top : 10,
                width : 27,
                height : 7
            }, {
                left : 1,
                top : 17,
                width : 25,
                height : 3
            }, {
                left : 2,
                top : 20,
                width : 23,
                height : 2
            }, {
                left : 3,
                top : 22,
                width : 21,
                height : 1
            }, {
                left : 4,
                top : 23,
                width : 19,
                height : 1
            }, {
                left : 5,
                top : 24,
                width : 17,
                height : 1
            }, {
                left : 7,
                top : 25,
                width : 13,
                height : 1
            }, {
                left : 10,
                top : 26,
                width : 7,
                height : 1
            }]],
            "11.5" : [[{
                left : 10,
                top : -3,
                width : 3,
                height : 1
            }, {
                left : 5,
                top : -2,
                width : 13,
                height : 1
            }, {
                left : 3,
                top : -1,
                width : 17,
                height : 1
            }, {
                left : 1,
                top : 0,
                width : 21,
                height : 1
            }, {
                left : 0,
                top : 1,
                width : 23,
                height : 1
            }, {
                left : -1,
                top : 2,
                width : 25,
                height : 1
            }, {
                left : -2,
                top : 3,
                width : 27,
                height : 1
            }, {
                left : -3,
                top : 4,
                width : 29,
                height : 2
            }, {
                left : -4,
                top : 6,
                width : 31,
                height : 2
            }, {
                left : -5,
                top : 8,
                width : 33,
                height : 3
            }, {
                left : -6,
                top : 11,
                width : 35,
                height : 8
            }, {
                left : -5,
                top : 19,
                width : 33,
                height : 3
            }, {
                left : -4,
                top : 22,
                width : 31,
                height : 2
            }, {
                left : -3,
                top : 24,
                width : 29,
                height : 2
            }, {
                left : -2,
                top : 26,
                width : 27,
                height : 1
            }, {
                left : -1,
                top : 27,
                width : 25,
                height : 1
            }, {
                left : 0,
                top : 28,
                width : 23,
                height : 1
            }, {
                left : 1,
                top : 29,
                width : 21,
                height : 1
            }, {
                left : 3,
                top : 30,
                width : 17,
                height : 1
            }, {
                left : 5,
                top : 31,
                width : 13,
                height : 1
            }, {
                left : 10,
                top : 32,
                width : 3,
                height : 1
            }], [{
                left : 8,
                top : 0,
                width : 7,
                height : 1
            }, {
                left : 6,
                top : 1,
                width : 11,
                height : 1
            }, {
                left : 4,
                top : 2,
                width : 15,
                height : 1
            }, {
                left : 3,
                top : 3,
                width : 17,
                height : 1
            }, {
                left : 2,
                top : 4,
                width : 19,
                height : 2
            }, {
                left : 1,
                top : 6,
                width : 21,
                height : 2
            }, {
                left : 0,
                top : 8,
                width : 23,
                height : 7
            }, {
                left : 1,
                top : 15,
                width : 21,
                height : 2
            }, {
                left : 2,
                top : 17,
                width : 19,
                height : 2
            }, {
                left : 3,
                top : 19,
                width : 17,
                height : 1
            }, {
                left : 4,
                top : 20,
                width : 15,
                height : 1
            }, {
                left : 6,
                top : 21,
                width : 11,
                height : 1
            }, {
                left : 8,
                top : 22,
                width : 7,
                height : 1
            }]],
            10 : [[{
                left : 8,
                top : -4,
                width : 4,
                height : 1
            }, {
                left : 5,
                top : -3,
                width : 10,
                height : 1
            }, {
                left : 3,
                top : -2,
                width : 14,
                height : 1
            }, {
                left : 1,
                top : -1,
                width : 18,
                height : 1
            }, {
                left : 0,
                top : 0,
                width : 20,
                height : 1
            }, {
                left : -1,
                top : 1,
                width : 22,
                height : 2
            }, {
                left : -2,
                top : 3,
                width : 24,
                height : 2
            }, {
                left : -3,
                top : 5,
                width : 26,
                height : 3
            }, {
                left : -4,
                top : 8,
                width : 28,
                height : 4
            }, {
                left : -3,
                top : 12,
                width : 26,
                height : 3
            }, {
                left : -2,
                top : 15,
                width : 24,
                height : 2
            }, {
                left : -1,
                top : 17,
                width : 22,
                height : 2
            }, {
                left : 0,
                top : 19,
                width : 20,
                height : 1
            }, {
                left : 1,
                top : 20,
                width : 18,
                height : 1
            }, {
                left : 3,
                top : 21,
                width : 14,
                height : 1
            }, {
                left : 5,
                top : 22,
                width : 10,
                height : 1
            }, {
                left : 8,
                top : 23,
                width : 4,
                height : 1
            }], [{
                left : 8,
                top : 0,
                width : 4,
                height : 1
            }, {
                left : 5,
                top : 1,
                width : 10,
                height : 1
            }, {
                left : 4,
                top : 2,
                width : 12,
                height : 1
            }, {
                left : 3,
                top : 3,
                width : 14,
                height : 1
            }, {
                left : 2,
                top : 4,
                width : 16,
                height : 1
            }, {
                left : 1,
                top : 5,
                width : 18,
                height : 3
            }, {
                left : 0,
                top : 8,
                width : 20,
                height : 4
            }, {
                left : 1,
                top : 12,
                width : 18,
                height : 3
            }, {
                left : 2,
                top : 15,
                width : 16,
                height : 1
            }, {
                left : 3,
                top : 16,
                width : 14,
                height : 1
            }, {
                left : 4,
                top : 17,
                width : 12,
                height : 1
            }, {
                left : 5,
                top : 18,
                width : 10,
                height : 1
            }, {
                left : 8,
                top : 19,
                width : 4,
                height : 1
            }]],
            5 : [[{
                left : 1,
                top : -1,
                width : 8,
                height : 1
            }, {
                left : -1,
                top : 0,
                width : 12,
                height : 1
            }, {
                left : -2,
                top : 1,
                width : 14,
                height : 1
            }, {
                left : -3,
                top : 2,
                width : 16,
                height : 1
            }, {
                left : -4,
                top : 3,
                width : 18,
                height : 3
            }, {
                left : -5,
                top : 6,
                width : 20,
                height : 5
            }, {
                left : -4,
                top : 11,
                width : 18,
                height : 3
            }, {
                left : -3,
                top : 14,
                width : 16,
                height : 1
            }, {
                left : -2,
                top : 15,
                width : 14,
                height : 1
            }, {
                left : -1,
                top : 16,
                width : 12,
                height : 1
            }, {
                left : 1,
                top : 17,
                width : 8,
                height : 1
            }], [{
                left : 3,
                top : 0,
                width : 4,
                height : 1
            }, {
                left : 2,
                top : 1,
                width : 6,
                height : 1
            }, {
                left : 1,
                top : 2,
                width : 8,
                height : 1
            }, {
                left : 0,
                top : 3,
                width : 10,
                height : 4
            }, {
                left : 1,
                top : 7,
                width : 8,
                height : 1
            }, {
                left : 2,
                top : 8,
                width : 6,
                height : 1
            }, {
                left : 3,
                top : 9,
                width : 4,
                height : 1
            }]],
            4 : [[{
                left : 1,
                top : -2,
                width : 6,
                height : 1
            }, {
                left : 0,
                top : -1,
                width : 8,
                height : 1
            }, {
                left : -1,
                top : 0,
                width : 10,
                height : 1
            }, {
                left : -2,
                top : 1,
                width : 12,
                height : 6
            }, {
                left : -1,
                top : 7,
                width : 10,
                height : 1
            }, {
                left : 0,
                top : 8,
                width : 8,
                height : 1
            }, {
                left : 1,
                top : 9,
                width : 6,
                height : 1
            }], [{
                left : 2,
                top : 0,
                width : 4,
                height : 1
            }, {
                left : 1,
                top : 1,
                width : 6,
                height : 1
            }, {
                left : 0,
                top : 2,
                width : 8,
                height : 4
            }, {
                left : 1,
                top : 6,
                width : 6,
                height : 1
            }, {
                left : 2,
                top : 7,
                width : 4,
                height : 1
            }]]
        };
        /** @type {Array} */
        var idsToCancel = [{
            left : 16,
            top : -3,
            width : 1,
            height : 1
        }, {
            left : 2,
            top : -2,
            width : 27,
            height : 1
        }, {
            left : 0,
            top : -1,
            width : 31,
            height : 1
        }, {
            left : -2,
            top : 0,
            width : 35,
            height : 1
        }, {
            left : -3,
            top : 1,
            width : 37,
            height : 1
        }, {
            left : -4,
            top : 2,
            width : 39,
            height : 2
        }, {
            left : -5,
            top : 4,
            width : 41,
            height : 2
        }, {
            left : -6,
            top : 6,
            width : 43,
            height : 13
        }, {
            left : -7,
            top : 19,
            width : 45,
            height : 1
        }, {
            left : -6,
            top : 20,
            width : 43,
            height : 11
        }, {
            left : -5,
            top : 31,
            width : 41,
            height : 2
        }, {
            left : -4,
            top : 33,
            width : 39,
            height : 2
        }, {
            left : -3,
            top : 35,
            width : 37,
            height : 1
        }, {
            left : -2,
            top : 36,
            width : 35,
            height : 1
        }, {
            left : 0,
            top : 37,
            width : 31,
            height : 1
        }, {
            left : 2,
            top : 38,
            width : 27,
            height : 1
        }, {
            left : 16,
            top : 39,
            width : 1,
            height : 1
        }];
        var bbox = new setSize(31, 29);
        /**
         * @param {?} x
         * @param {number} y
         * @param {number} c
         * @param {number} left
         * @param {number} right
         * @param {number} b
         * @param {Array} a
         * @return {undefined}
         */
        var sub = function(x, y, c, left, right, b, a) {
            x *= right;
            y *= right;
            c = new slide(Math.floor(x), Math.floor(y), Math.ceil(x + c * right) - Math.floor(x), Math.ceil(y + left * right) - Math.floor(y));
            a: {
                /** @type {number} */
                left = Math.max(c.left, b.left);
                /** @type {number} */
                right = Math.min(c.left + c.width, b.left + b.width);
                if (left <= right && (y = Math.max(c.top, b.top), b = Math.min(c.top + c.height, b.top + b.height), y <= b)) {
                    /** @type {number} */
                    c.left = left;
                    /** @type {number} */
                    c.top = y;
                    /** @type {number} */
                    c.width = right - left;
                    /** @type {number} */
                    c.height = b - y;
                    /** @type {boolean} */
                    b = true;
                    break a;
                }
                /** @type {boolean} */
                b = false;
            }
            if (b = !b || (1 > c.width || 1 > c.height) ? null : {
                    left : c.left,
                    top : c.top,
                    width : c.width,
                    height : c.height
                }) {
                a.push(b);
            }
        };
        /**
         * @param {string} type
         * @param {boolean} event
         * @param {number} value
         * @param {number} needle
         * @param {Array} x
         * @return {undefined}
         */
        var resize = function(type, event, value, needle, x) {
            (0, self.u)(iterator(type, filename, 4) || [], function(type) {
                var px = self.A(type, 1);
                var yOffset = self.A(type, 2);
                var width = self.A(type, 3);
                var height = self.A(type, 4);
                type = self.A(type, 5);
                if (!(1 > width || 1 > height)) {
                    if (!event || (width < bbox.width || height < bbox.height)) {
                        if (!(1 > width || 1 > height)) {
                            var data = types[type];
                            if (data) {
                                if (data = data[1], width -= 2 * type, !(0 > width)) {
                                    /** @type {number} */
                                    var j = (data.length - 1) / 2;
                                    /** @type {number} */
                                    var that = 0;
                                    /** @type {number} */
                                    var i = 0;
                                    for (;i < data.length;i++) {
                                        var pos = data[i];
                                        /** @type {number} */
                                        var offset = 0;
                                        if (i == j && (offset = height - 2 * type, 0 > offset)) {
                                            continue;
                                        }
                                        sub(pos.left + px, pos.top + yOffset + that, pos.width + width, pos.height + offset, value, needle, x);
                                        that += offset;
                                    }
                                }
                            } else {
                                sub(px, yOffset, width, height, value, needle, x);
                            }
                        }
                    } else {
                        if (type = width - bbox.width, !(0 > type)) {
                            /** @type {number} */
                            data = 0;
                            /** @type {number} */
                            width = (idsToCancel.length - 1) / 2;
                            /** @type {number} */
                            j = 0;
                            for (;j < idsToCancel.length;j++) {
                                that = idsToCancel[j];
                                /** @type {number} */
                                i = 0;
                                if (j == width && (i = height - bbox.height, 0 > i)) {
                                    continue;
                                }
                                sub(that.left + px, that.top + yOffset + data, that.width + type, that.height + i, value, needle, x);
                                data += i;
                            }
                        }
                    }
                }
            });
            (0, self.u)(iterator(type, ms, 3) || [], function(complete) {
                var resultItems = self.A(complete, 3);
                var c = self.A(complete, 1);
                complete = self.A(complete, 2);
                var i = types[Math.round(2 * resultItems) / 2];
                if (i) {
                    c -= resultItems;
                    complete -= resultItems;
                    resultItems = i[event ? 0 : 1];
                    /** @type {number} */
                    i = 0;
                    for (;i < resultItems.length;i++) {
                        var result = resultItems[i];
                        sub(result.left + c, result.top + complete, result.width, result.height, value, needle, x);
                    }
                } else {
                    resultItems += 2;
                    /** @type {number} */
                    i = 0;
                    result = resultItems;
                    /** @type {number} */
                    resultItems = 3 - 2 * resultItems;
                    for (;i < result;) {
                        if (0 <= resultItems) {
                            --result;
                            sub(-i + c, -result + complete, 2 * i + 1, 1, value, needle, x);
                            sub(-i + c, result - 1 + complete, 2 * i + 1, 1, value, needle, x);
                            sub(-result + c, -i + complete, 1, 2 * i, value, needle, x);
                            sub(result - 1 + c, -i + complete, 1, 2 * i, value, needle, x);
                            resultItems += -4 * result + 4;
                        }
                        resultItems += 4 * i + 6;
                        i += 1;
                    }
                    x.pop();
                    c = x.pop();
                    c.height++;
                    /** @type {number} */
                    c.width = c.height - 1;
                    sub(c.left, c.top, c.width, c.height, value, needle, x);
                }
            });
        };
        /**
         * @param {Element} item
         * @param {string} x
         * @return {undefined}
         */
        var onMove = function(item, x) {
            if (item) {
                var result = item.getBoundingClientRect();
                if (!(1 > result.width || 1 > result.height)) {
                    var ready = new ms;
                    /** @type {number} */
                    var i = result.width / 2;
                    self.B(ready, 1, result.left + i);
                    self.B(ready, 2, result.top + i);
                    self.B(ready, 3, i);
                    result = iterator(x, ms, 3);
                    result.push(ready);
                    print(x, 3, result);
                }
            }
        };
        /**
         * @param {?} opt
         * @param {?} val
         * @return {undefined}
         */
        var options = function(opt, val) {
            A.call(this, val);
            this.T = opt;
            this.W = opt.get(actualKey);
            this.M = this.T.get(elem);
            /** @type {null} */
            this.v = null;
            self.G(this, function() {
                self.Ce(this.v);
            }, this);
            /** @type {null} */
            this.h = null;
            self.G(this, function() {
                self.Ce(this.h);
            }, this);
            /** @type {null} */
            this.l = null;
            self.G(this, function() {
                self.Ce(this.l);
            }, this);
            /** @type {boolean} */
            this.w = false;
        };
        self.p(options, A);
        self.g = options.prototype;
        /**
         * @return {undefined}
         */
        self.g.na = function() {
            this.b = fragment(nodes, void 0, this.ya());
        };
        /**
         * @return {undefined}
         */
        self.g.ca = function() {
            options.D.ca.call(this);
            if (parseFloat(this.W)) {
                watchErr(this).listen(objectToString(this, "UUGBcf"), "click", this.$);
                watchErr(this).listen(this.F(), "mouseleave", this.gc);
                watchErr(this).listen(this.F(), "mouseenter", this.fc);
                watchErr(this).listen(objectToString(this, "KwZMgc"), "mouseenter", this.dc);
            }
            watchErr(this).listen(this.M, cycle, this.O);
            this.O();
        };
        /**
         * @return {undefined}
         */
        self.g.gc = function() {
            if (!_hasClass(objectToString(this, "KwZMgc"), "vjWrIb")) {
                convertTrans(this, false);
                patch(this, false);
                self.Ce(this.h);
                /** @type {null} */
                this.h = null;
            }
        };
        /**
         * @return {undefined}
         */
        self.g.dc = function() {
            replace(objectToString(this, "KwZMgc"), "vjWrIb", true);
        };
        /**
         * @return {undefined}
         */
        self.g.fc = function() {
            convertTrans(this, true);
            buildParams(this);
            patch(this, true);
            self.Ce(this.h);
            this.h = self.Be(function() {
                patch(this, false);
                /** @type {null} */
                this.h = null;
            }, 3E3, this);
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        var buildParams = function(obj) {
            self.Ce(obj.v);
            obj.v = self.Be(function() {
                if (this.w) {
                    buildParams(this);
                } else {
                    convertTrans(this, false);
                }
            }, 3E3, obj);
        };
        /**
         * @return {undefined}
         */
        options.prototype.aa = function() {
            if (_hasClass(objectToString(this, "KwZMgc"), "AvEm8e")) {
                /** @type {boolean} */
                this.w = true;
                self.Ce(this.l);
                this.l = self.Be(function() {
                    self.M(this, "z");
                    /** @type {boolean} */
                    this.w = false;
                    convertTrans(this, false);
                }, 1E3, this);
            } else {
                /** @type {boolean} */
                this.w = false;
            }
        };
        /**
         * @param {string} ready
         * @param {boolean} recurring
         * @return {undefined}
         */
        var convertTrans = function(ready, recurring) {
            var suiteView = objectToString(ready, "KwZMgc");
            replace(suiteView, "AvEm8e", recurring);
            if (!recurring) {
                replace(suiteView, "vjWrIb", false);
                self.M(ready, "z");
                /** @type {boolean} */
                ready.w = false;
                self.Ce(ready.l);
                /** @type {null} */
                ready.l = null;
                self.Ce(ready.v);
                /** @type {null} */
                ready.v = null;
            }
            self.M(ready, "x");
        };
        /**
         * @param {string} ready
         * @param {boolean} recurring
         * @return {undefined}
         */
        var patch = function(ready, recurring) {
            replace(objectToString(ready, "UUGBcf"), "sPAbv", recurring);
            self.M(ready, "x");
        };
        /**
         * @return {undefined}
         */
        options.prototype.O = function() {
            /** @type {Array} */
            var failuresLink = ["f37ZYc", "Anoad", "eOXgae", "C86Dyb"];
            switch(this.M.a) {
                case 1:
                    ;
                case 2:
                    getComputedStyle(this, "Anoad", failuresLink);
                    break;
                case 3:
                    getComputedStyle(this, "eOXgae", failuresLink);
                    break;
                case 6:
                    getComputedStyle(this, "f37ZYc", failuresLink);
                    break;
                case 10:
                    getComputedStyle(this, "C86Dyb", failuresLink);
                    break;
                default:
                    getComputedStyle(this, "", failuresLink);
            }
        };
        /**
         * @param {?} t
         * @param {string} property
         * @param {Array} el
         * @return {undefined}
         */
        var getComputedStyle = function(t, property, el) {
            (0, self.u)(el, function(dateTime) {
                replace(objectToString(this, dateTime), "RWqoGd", dateTime == property);
            }, t);
        };
        /**
         * @param {Node} context
         * @return {undefined}
         */
        options.prototype.$ = function(context) {
            context.l();
            self.M(this, "y");
        };
        /**
         * @param {?} opt_data
         * @param {string} x
         * @return {undefined}
         */
        var grid = function(opt_data, x) {
            (0, self.u)(["UUGBcf", "KwZMgc"], function(dateTime) {
                onMove(objectToString(this, dateTime), x);
            }, opt_data);
        };
        /**
         * @param {?} ctx
         * @param {string} a
         * @return {undefined}
         */
        var draw = function(ctx, a) {
            var orig = this;
            self.N.call(this);
            this.l = ctx;
            /** @type {string} */
            this.a = a;
            this.j = this.l.get(elem);
            this.b = ctx.get(actualKey);
            this.A = ctx.get(subKey);
            /** @type {null} */
            this.c = null;
            this.G = new constructor(this.l, this.a);
            self.I(this, this.G);
            ctx.get(self.T);
            set(this.a, function() {
                var elem = equal(orig.a);
                var item = elem.document.body;
                var cycle = parseFloat(orig.b);
                item.appendChild(convert(cycle ? stylesheets : images));
                if (cycle) {
                    item = self.vc(equal(orig.a));
                    cycle = new module(orig.l, item);
                    self.I(orig, cycle);
                    var copy = log(orig, "bE4Lge");
                    addItem(cycle, copy);
                    cycle = new Literal(item);
                    self.I(orig, cycle);
                    copy = log(orig, "nkDZ6");
                    addItem(cycle, copy);
                    cycle = new Comment(item);
                    self.I(orig, cycle);
                    copy = log(orig, "elCDOd");
                    addItem(cycle, copy);
                    orig.c = new options(orig.l, item);
                    item = log(orig, "C64E9d");
                    addItem(orig.c, item);
                    orig.listen(orig.c, "z", orig.H);
                    orig.listen(orig.c.F(), "click", orig.P);
                    orig.listen(orig.c, "x", orig.h);
                    orig.listen(orig.c, "y", orig.O);
                    orig.a.onBoundsChanged.addListener((0, self.r)(orig.c.aa, orig.c));
                    orig.listen(orig.b, "p", orig.M);
                } else {
                    orig.listen(log(orig, "rHCn5c"), "click", orig.T);
                }
                orig.listen(orig.j, ready, orig.w);
                orig.w();
                orig.listen(elem, ["focus", "blur"], orig.I);
                orig.I();
                orig.listen(orig.b, "r", orig.Y);
                orig.listen(orig.b, "t", orig.L);
                orig.listen(orig.b, "q", orig.W);
            });
        };
        self.p(draw, self.N);
        /**
         * @return {undefined}
         */
        draw.prototype.T = function() {
            switch(this.j.a) {
                case 3:
                    colorChanged(this.A);
                    break;
                case 6:
                    window.chrome.runtime.reload();
            }
        };
        /**
         * @return {undefined}
         */
        draw.prototype.I = function() {
            var that = equal(this.a);
            if (that) {
                if (that.document) {
                    replace(that.document.body, "SgJDAe", that.document.hasFocus());
                }
            }
        };
        /**
         * @return {undefined}
         */
        draw.prototype.Y = function() {
            window.chrome.runtime.reload();
        };
        /**
         * @param {Node} source
         * @return {?}
         */
        var link = function(source) {
            var elem = log(source, "L7IVff");
            var hasBody = _hasClass(elem, "e48oQe");
            return(0, self.bb)(["nkDZ6", "bE4Lge", "elCDOd"], function(params) {
                params = log(this, params);
                return hasBody && "none" != params.style.display;
            }, source);
        };
        /**
         * @param {Object} s
         * @param {boolean} recurring
         * @return {undefined}
         */
        var runTests = function(s, recurring) {
            var suiteView = log(s, "L7IVff");
            replace(suiteView, "e48oQe", recurring);
            if (recurring) {
                var wanted;
                switch(s.j.a) {
                    case 3:
                        /** @type {string} */
                        wanted = "bE4Lge";
                        break;
                    case 6:
                        /** @type {string} */
                        wanted = "nkDZ6";
                        break;
                    case 10:
                        /** @type {string} */
                        wanted = "elCDOd";
                }
                (0, self.u)(["nkDZ6", "bE4Lge", "elCDOd"], function(type) {
                    var activeClassName = log(this, type);
                    html(activeClassName, type == wanted);
                }, s);
            }
            s.h();
        };
        /**
         * @param {Element} object
         * @return {undefined}
         */
        var translate = function(object) {
            var activeClassName;
            /** @type {string} */
            var el = "";
            /** @type {string} */
            var title = "";
            /** @type {string} */
            var path = "";
            /** @type {string} */
            var later = "";
            switch(object.j.a) {
                case 2:
                    /** @type {string} */
                    activeClassName = "CzdJYb";
                    break;
                case 3:
                    el = window.chrome.i18n.getMessage("CHROME_APP_CHROME_SIGNIN_MESSAGE");
                    title = window.chrome.i18n.getMessage("CHROME_APP_LEARN_MORE_MESSAGE");
                    path = self.Ug;
                    later = window.chrome.i18n.getMessage("CHROME_APP_SIGNIN_BUTTON");
                    break;
                case 6:
                    /** @type {string} */
                    activeClassName = "LQr1Lc";
                    el = window.chrome.i18n.getMessage("CHROME_APP_CONNECTION_ERROR_DETAIL");
                    later = window.chrome.i18n.getMessage("CHROME_APP_CONNECTION_ERROR_BUTTON");
                    break;
                case 10:
                    el = window.chrome.i18n.getMessage("CHROME_APP_OUTDATED_DETAIL");
                    title = window.chrome.i18n.getMessage("CHROME_APP_LEARN_MORE_MESSAGE");
                    path = self.Vg;
            }
            var node = log(object, "U2TDqc");
            removeClass(node);
            if (activeClassName) {
                _addClass(node, activeClassName);
            }
            activeClassName = log(object, "WxquGe");
            text(activeClassName, el);
            html(activeClassName, !!el);
            el = log(object, "vkUZWc");
            text(el, title);
            html(el, !!title);
            el.href = path;
            object = log(object, "rHCn5c");
            text(object, later);
            html(object, !!later);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.P = function() {
            runTests(this, !link(this));
            this.b.b.set("auto_ui", false);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.M = function() {
            if (equal(this.a)) {
                var doc = equal(this.a).document;
                replace(doc.body, "uLlKUd", require(this.b));
                if (!_hasClass(log(this, "KwUJl"), "hhxgvd")) {
                    this.h();
                }
            }
        };
        /**
         * @param {Node} obj
         * @param {string} str
         * @return {?}
         */
        var log = function(obj, str) {
            var object = self.vc(equal(obj.a));
            return self.xc(str, object.a);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.H = function() {
            var s = this.G;
            if (!(self.w("CrOS") && 1 < s.c.a.length)) {
                var e = getSize(s.b);
                var start = this.c.F().getBoundingClientRect();
                e = require(s.a) ? start.left + start.width / 2 : e.width - start.left - start.width / 2;
                var r = new Rect(start.left + start.width / 2, start.top + start.height / 2);
                var c = new Bounds(0, e, 0, e);
                start = getSize(s.b);
                e = new slide(start.left, start.top, start.width, start.height);
                var b = new Rect(start.left + r.x, start.top + r.y);
                r = require(s.a);
                /** @type {number} */
                var a = b.x - c.left;
                var x = b.x + c.a;
                var p = raycast_mesh(s.c, b);
                c = p.left;
                /** @type {number} */
                b = p.left + p.width - start.width;
                if (x - start.width < c) {
                    /** @type {boolean} */
                    r = true;
                    /** @type {number} */
                    e.left = a;
                }
                if (a + start.width > b) {
                    /** @type {boolean} */
                    r = false;
                    /** @type {number} */
                    e.left = x - start.width;
                }
                a = s.a;
                x = r;
                var l = new RegExp;
                self.B(l, 1, x);
                F(a, l);
                scroll(e, p, r);
                /** @type {boolean} */
                p = true;
                if (r && e.left > c || !r && e.left < b) {
                    /** @type {boolean} */
                    p = false;
                }
                r = s.a;
                /** @type {boolean} */
                c = p;
                b = new RegExp;
                self.B(b, 4, c);
                F(r, b);
                if (!assertEquals(start, e)) {
                    expect(s.b, inspect(e));
                }
            }
        };
        /**
         * @return {undefined}
         */
        draw.prototype.O = function() {
            self.H(this.a);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.w = function() {
            var s = this;
            if (!this.a.K && equal(this.a)) {
                var suiteView = parseFloat(this.b);
                var useQSA = qsaAvail && parseFloat(this.b);
                var doc = equal(this.a).document;
                replace(doc.body, "uLlKUd", require(this.b));
                replace(doc.body, "MaeNme", suiteView && !useQSA);
                replace(doc.body, "hGMvJe", !suiteView);
                /** @type {boolean} */
                var recurring = 4 == this.j.a;
                foo(this.a, recurring);
                self.Yc(function() {
                    return replace(log(s, "KwUJl"), "hhxgvd", recurring);
                });
                if (suiteView) {
                    suiteView = log(this, "L7IVff");
                    replace(suiteView, "e48oQe", false);
                    if (!recurring) {
                        if (params(this.b)) {
                            runTests(this, true);
                        }
                    }
                    this.h();
                    self.Yc(function() {
                        return s.h();
                    });
                } else {
                    translate(this);
                }
            }
        };
        /**
         * @param {?} num
         * @param {string} value
         * @return {undefined}
         */
        var toHex = function(num, value) {
            (0, self.u)(["bE4Lge", "nkDZ6", "elCDOd", "ftkzZd"], function(type) {
                var style = log(this, type);
                if (style) {
                    var s = style.getBoundingClientRect();
                    type = new filename;
                    removeFromList(type, s.left);
                    is(type, s.top);
                    self.B(type, 3, s.width);
                    self.B(type, 4, s.height);
                    a: {
                        s = self.uc(style);
                        if (s.defaultView && (s.defaultView.getComputedStyle && (style = s.defaultView.getComputedStyle(style, null)))) {
                            style = style.borderRadius || (style.getPropertyValue("borderRadius") || "");
                            break a;
                        }
                        /** @type {string} */
                        style = "";
                    }
                    self.B(type, 5, (0, window.parseInt)(style, 10));
                    style = iterator(value, filename, 4);
                    style.push(type);
                    print(value, 4, style);
                }
            }, num);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.h = function() {
            if (parseFloat(this.b) && equal(this.a)) {
                var v = new selector;
                grid(this.c, v);
                onMove(log(this, "C64E9d"), v);
                toHex(this, v);
                add(this.a, v);
            }
        };
        /**
         * @return {undefined}
         */
        draw.prototype.L = function() {
            self.Yc(this.h, this);
        };
        /**
         * @return {undefined}
         */
        draw.prototype.W = function() {
            var dblclick = apply(this.b);
            if (!self.A(dblclick, 15)) {
                dblclick = getSize(this.a).width;
                if (!(640 <= dblclick)) {
                    setModel(this.a, 640 - dblclick);
                }
            }
        };
        /**
         * @param {number} method
         * @return {undefined}
         */
        var sync = function(method) {
            self.z(this, method, "ca:tu", bodyHead);
        };
        self.p(sync, self.y);
        /** @type {Array} */
        var bodyHead = [1, 2, 3];
        /** @type {string} */
        sync.messageId = "ca:tu";
        /**
         * @param {number} code
         * @return {undefined}
         */
        var a = function(code) {
            self.z(this, code, 0, null);
        };
        self.p(a, self.y);
        /**
         * @return {?}
         */
        a.prototype.getTitle = function() {
            return self.A(this, 1);
        };
        /**
         * @param {Object} millis
         * @return {undefined}
         */
        a.prototype.setTitle = function(millis) {
            self.B(this, 1, millis);
        };
        /**
         * @return {?}
         */
        a.prototype.ra = function() {
            return self.A(this, 3);
        };
        /**
         * @param {number} doc
         * @return {undefined}
         */
        var option = function(doc) {
            self.z(this, doc, 0, null);
        };
        self.p(option, self.y);
        /**
         * @return {?}
         */
        option.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        option.prototype.ra = function() {
            return self.A(this, 3);
        };
        /**
         * @param {number} height
         * @return {undefined}
         */
        var marker = function(height) {
            self.z(this, height, 0, vbarDrag);
        };
        self.p(marker, self.y);
        /** @type {Array} */
        var vbarDrag = [4];
        /**
         * @return {?}
         */
        marker.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        marker.prototype.Aa = function() {
            return self.C(this, a, 3);
        };
        /**
         * @param {string} easing
         * @return {undefined}
         */
        marker.prototype.ob = function(easing) {
            self.E(this, 3, easing);
        };
        /**
         * @param {number} error
         * @return {undefined}
         */
        var req = function(error) {
            self.z(this, error, 0, null);
        };
        self.p(req, self.y);
        /**
         * @return {?}
         */
        req.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @return {?}
         */
        req.prototype.Aa = function() {
            return self.C(this, a, 2);
        };
        /**
         * @param {string} easing
         * @return {undefined}
         */
        req.prototype.ob = function(easing) {
            self.E(this, 2, easing);
        };
        /**
         * @param {number} value
         * @return {undefined}
         */
        var component = function(value) {
            self.z(this, value, 0, null);
        };
        self.p(component, self.y);
        /**
         * @return {?}
         */
        component.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {number} reqUrl
         * @return {undefined}
         */
        var Events = function(reqUrl) {
            self.z(this, reqUrl, "ca:pta", null);
        };
        self.p(Events, self.y);
        /** @type {string} */
        Events.messageId = "ca:pta";
        /**
         * @return {?}
         */
        Events.prototype.a = function() {
            return self.A(this, 1);
        };
        /**
         * @param {Node} context
         * @return {undefined}
         */
        var r = function(context) {
            self.N.call(this);
            this.c = context.get(elem);
            this.b = context.get(self.T);
            this.A = context.get(callee);
            this.a = new self.J;
            this.listen(this.b.J(), "ca:tu", this.G);
            var name = (0, self.r)(this.w, this);
            window.chrome.notifications.onClosed.addListener(name);
            var rvar = (0, self.r)(this.l, this);
            window.chrome.notifications.onClicked.addListener(rvar);
            var eventName = (0, self.r)(this.j, this);
            window.chrome.notifications.onButtonClicked.addListener(eventName);
            self.G(this, function() {
                window.chrome.notifications.onClosed.removeListener(name);
                window.chrome.notifications.onClicked.removeListener(rvar);
                window.chrome.notifications.onButtonClicked.removeListener(eventName);
            });
        };
        self.p(r, self.N);
        /**
         * @param {string} types
         * @return {undefined}
         */
        r.prototype.G = function(types) {
            types = new sync(types.S);
            (0, self.u)(iterator(types, marker, 1), this.h, this);
            (0, self.u)(iterator(types, req, 2), this.I, this);
            (0, self.u)(iterator(types, component, 3), function(fnc) {
                write(this, fnc.a());
            }, this);
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        r.prototype.h = function(types) {
            var camelKey = types.a();
            var suiteView = types.Aa();
            if (!has(this, camelKey, suiteView)) {
                resolve(this.A, self.A(types, 5)).then(function(dataAndEvents) {
                    if (!dataAndEvents) {
                        this.a.set(camelKey, types);
                        if (self.w("CrOS")) {
                            window.chrome.notifications.clear(camelKey, (0, self.r)(function(dataAndEvents) {
                                if (!dataAndEvents) {
                                    binding(this, camelKey);
                                }
                            }, this));
                        } else {
                            binding(this, camelKey);
                        }
                    }
                }, void 0, this);
            }
        };
        /**
         * @param {Object} obj
         * @param {string} value
         * @return {undefined}
         */
        var binding = function(obj, value) {
            window.chrome.notifications.create(value, update(obj, value), function(key) {
                if (key != value) {
                    self.S("Created a notification %s instead of %s", key, value);
                }
            });
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        r.prototype.I = function(types) {
            var storageKey = types.a();
            types = types.Aa();
            if (!has(this, storageKey, types)) {
                removeItem(this, storageKey, types);
                window.chrome.notifications.update(storageKey, update(this, storageKey), function(dataAndEvents) {
                    if (!dataAndEvents) {
                        self.S("Couldn't find a matching notification to update: %s", storageKey);
                    }
                });
            }
        };
        /**
         * @param {Node} object
         * @param {string} key
         * @return {undefined}
         */
        var write = function(object, key) {
            object.a.remove(key);
            window.chrome.notifications.clear(key, function() {
            });
        };
        /**
         * @param {string} str
         * @param {boolean} event
         * @return {undefined}
         */
        r.prototype.w = function(str, event) {
            var _w_ = (0, self.r)(function() {
                if (this.a.remove(str)) {
                    color(this, str, 2);
                }
            }, this);
            var throttledUpdate = (0, self.r)(function() {
                if (self.Lc(this.a, str)) {
                    binding(this, str);
                }
            }, this);
            if (event || !self.Lc(this.a, str)) {
                _w_();
            } else {
                window.chrome.idle.queryState(25, function(total) {
                    if ("active" == total) {
                        _w_();
                    } else {
                        throttledUpdate();
                    }
                });
            }
        };
        /**
         * @param {string} types
         * @return {undefined}
         */
        r.prototype.l = function(types) {
            write(this, types);
            color(this, types, 1);
        };
        /**
         * @param {string} types
         * @param {Function} type
         * @return {undefined}
         */
        r.prototype.j = function(types, type) {
            write(this, types);
            color(this, types, 3, "" + type);
        };
        /**
         * @param {Node} user
         * @param {string} context
         * @param {number} expectedNumberOfNonCommentArgs
         * @param {string} source
         * @return {undefined}
         */
        var color = function(user, context, expectedNumberOfNonCommentArgs, source) {
            var dblclick = new Events;
            self.B(dblclick, 1, context);
            self.B(dblclick, 2, expectedNumberOfNonCommentArgs);
            self.B(dblclick, 3, source || "");
            user.b.sendMessage(dblclick);
        };
        /**
         * @param {Object} obj
         * @param {string} storageKey
         * @return {?}
         */
        var update = function(obj, storageKey) {
            var value = obj.a.get(storageKey);
            var dblclick = value.Aa();
            value = iterator(value, option, 4);
            dblclick = {
                type : "basic",
                iconUrl : dblclick.ra() || window.chrome.runtime.getURL(window.chrome.runtime.getManifest().icons["128"]),
                title : dblclick.getTitle(),
                message : self.A(dblclick, 2),
                contextMessage : obj.c.h,
                priority : 2,
                requireInteraction : true,
                eventTime : (0, self.sa)(),
                isClickable : !!self.A(dblclick, 4)
            };
            if (value) {
                dblclick.buttons = (0, self.ab)(value, function(ready) {
                    return{
                        title : self.A(ready, 2),
                        iconUrl : ready.ra()
                    };
                });
            }
            return dblclick;
        };
        /**
         * @param {Object} type
         * @param {string} storageKey
         * @param {string} obj
         * @return {?}
         */
        var has = function(type, storageKey, obj) {
            type = type.a.get(storageKey);
            return type ? (type = type.Aa()) && (type.ra() == obj.ra() && (type.getTitle() == obj.getTitle() && (self.A(type, 2) == self.A(obj, 2) && self.A(type, 5) == self.A(obj, 5)))) : false;
        };
        /**
         * @param {Node} item
         * @param {string} storageKey
         * @param {string} callback
         * @return {undefined}
         */
        var removeItem = function(item, storageKey, callback) {
            var ar = item.a.get(storageKey);
            if (!ar) {
                ar = new marker;
                item.a.set(storageKey, ar);
            }
            ar.ob(callback);
        };
        /**
         * @param {number} context
         * @return {undefined}
         */
        var helper = function(context) {
            self.z(this, context, "capii_c_ucmc", null);
        };
        self.p(helper, self.y);
        /** @type {string} */
        helper.messageId = "capii_c_ucmc";
        /**
         * @param {Function} fix
         * @return {undefined}
         */
        var animation = function(fix) {
            self.L.call(this);
            this.b = fix.get(self.T);
            /** @type {number} */
            this.a = this.j = 0;
            /** @type {boolean} */
            this.h = this.g = false;
            fix = new self.N(this);
            self.I(this, fix);
            fix.listen(this.b.J(), "capii_c_ucmc", this.l);
            this.b.sendMessage(new helper);
        };
        self.p(animation, self.L);
        /**
         * @param {string} types
         * @return {undefined}
         */
        animation.prototype.l = function(types) {
            types = new helper(types.S);
            if (!this.g) {
                /** @type {boolean} */
                this.g = true;
                if (this.h) {
                    self.B(types, 5, true);
                    /** @type {boolean} */
                    this.h = false;
                }
            }
            /** @type {boolean} */
            var b = this.j != self.A(types, 1);
            /** @type {number} */
            this.j = Number(self.A(types, 1));
            /** @type {boolean} */
            var c = this.a != self.A(types, 2);
            /** @type {number} */
            this.a = Number(self.A(types, 2));
            if (self.A(types, 5)) {
                self.M(this, "A");
            } else {
                if (b) {
                    self.M(this, "C");
                }
                if (c) {
                    self.M(this, "B");
                }
            }
        };
        /**
         * @param {Object} data
         * @return {undefined}
         */
        var handler = function(data) {
            if (window.chrome.browserAction) {
                window.chrome.browserAction.setIcon(finished(data, true));
            }
            if (window.chrome.systemIndicator) {
                window.chrome.systemIndicator.setIcon(finished(data));
            }
            var eventName;
            switch(data.a.K ? 1 : data.a.a) {
                case 2:
                    eventName = window.chrome.i18n.getMessage("QUASAR_TIP_CONNECTING");
                    break;
                case 3:
                    eventName = window.chrome.i18n.getMessage("QUASAR_TIP_NOT_LOGIN");
                    break;
                case 4:
                    eventName = data.a.g() ? data.b.a ? window.chrome.i18n.getMessage("CHROME_EXT_TIP_CONNECTED_MISSED_MESSAGES") : window.chrome.i18n.getMessage("QUASAR_TIP_CONNECTED") : window.chrome.i18n.getMessage("QUASAR_TIP_NOT_LOGIN");
                    break;
                case 6:
                    eventName = window.chrome.i18n.getMessage("QUASAR_TIP_NOT_LOAD");
                    break;
                default:
                    eventName = window.chrome.i18n.getMessage("QUASAR_TIP_DISCONNECTED");
            }
            /** @type {string} */
            data = (data = data.a.K ? "" : data.a.h) ? "\n" + data : "";
            /** @type {string} */
            eventName = window.chrome.i18n.getMessage("CHROME_HANGOUTS_SHORT_NAME") + data + "\n" + eventName;
            if (window.chrome.browserAction) {
                window.chrome.browserAction.setTitle({
                    title : eventName
                });
            }
        };
        /**
         * @param {Node} data
         * @param {boolean} dataAndEvents
         * @return {?}
         */
        var finished = function(data, dataAndEvents) {
            /** @type {string} */
            var sign = !dataAndEvents && self.vb() ? "_mac_" : "_";
            var d = messageHandler(data);
            var e = window.chrome.runtime.getURL("images_4/presence/" + d + sign + "19.png");
            sign = window.chrome.runtime.getURL("images_4/presence/" + d + sign + "38.png");
            return{
                path : {
                    19 : e,
                    38 : sign
                }
            };
        };
        /**
         * @param {Node} d
         * @return {?}
         */
        var messageHandler = function(d) {
            switch(d.a.K ? 1 : d.a.a) {
                case 2:
                    return "offline_working";
                case 4:
                    return d.a.g() ? d.b.a ? "available_missed" : "available" : "offline";
                case 6:
                    return "offline_unknown";
                default:
                    return "offline";
            }
        };
        /**
         * @param {(Object|string)} dblclick
         * @param {string} ready
         * @param {?} dx
         * @param {?} val
         * @return {undefined}
         */
        var s = function(dblclick, ready, dx, val) {
            var value = this;
            /** @type {(Object|string)} */
            this.l = dblclick;
            /** @type {string} */
            this.T = ready;
            this.Ra = dx;
            this.g = this.l.get(actualKey);
            this.W = this.l.get(elem);
            f.call(this, this.g.j(), val, "uv_main_window" + ping(this.g));
            this.Da = dblclick.get(modalInstance);
            this.Ea = dblclick.get(self.T);
            this.h = crossProduct(hex(this.g));
            dblclick = self.C(ready, complete, 2);
            this.P = self.A(dblclick, 3);
            this.M = self.A(dblclick, 4);
            this.O = this.h;
            /** @type {number} */
            this.$ = 0;
            /** @type {null} */
            this.va = null;
            this.ua = self.qd();
            prepare(this, self.A(ready, 17));
            /** @type {null} */
            this.w = null;
            this.L = self.qd();
            set(this, function() {
                return callback(value);
            });
            this.g.listen("s", this.ha, void 0, this);
            this.g.listen("o", this.aa, void 0, this);
            self.G(this, function() {
                value.g.ta("s", value.ha, void 0, value);
                value.g.ta("o", value.aa, void 0, value);
            });
        };
        self.p(s, f);
        /**
         * @param {Object} obj
         * @param {string} e
         * @return {undefined}
         */
        var add = function(obj, e) {
            var expected;
            if ((obj.a || obj.c) && obj.b) {
                expected = getSize(obj);
                expected = new slide(0, 0, expected.width, expected.height);
            } else {
                expected = new slide(0, 0, 1, 1);
            }
            /** @type {Array} */
            var c = [];
            resize(e, qsaAvail && parseFloat(obj.g), obj.h / 100, expected, c);
            if (c.length) {
                size(obj, c);
            }
        };
        /**
         * @return {undefined}
         */
        s.prototype.focus = function() {
            var body = this;
            if (!(2E3 > (0, self.sa)() - this.$)) {
                set(this, function() {
                    body.$ = (0, self.sa)();
                    focus(body);
                    var input = self.vc(equal(body).document);
                    if (input = self.wc(input.a, "webview", void 0, void 0)[0]) {
                        if (equal(body).document.activeElement != input) {
                            input.focus();
                        }
                    }
                });
            }
        };
        /**
         * @param {Object} obj
         * @param {string} ready
         * @return {undefined}
         */
        var prepare = function(obj, ready) {
            if (ready) {
                /** @type {string} */
                obj.va = ready;
                obj.ua.b(ready);
            }
        };
        /**
         * @param {?} c
         * @param {boolean} recurring
         * @return {undefined}
         */
        var foo = function(c, recurring) {
            c.L.a.then(function(content) {
                return content.show(recurring);
            });
        };
        /**
         * @return {undefined}
         */
        s.prototype.A = function() {
            if (this.G) {
                this.l.get(key).set("mwo", false);
            }
            s.D.A.call(this);
        };
        /**
         * @param {string} element
         * @param {string} ready
         * @return {undefined}
         */
        var postLink = function(element, ready) {
            set(element, function() {
                var el = self.vc(equal(this).document).F("uv_drag_region");
                if (el) {
                    var top = self.A(ready, 1);
                    var tipOffset = self.A(ready, 2);
                    var left;
                    if (top instanceof Rect) {
                        left = top.x;
                        top = top.y;
                    } else {
                        left = top;
                        top = tipOffset;
                    }
                    el.style.left = round(left, false);
                    el.style.top = round(top, false);
                    left = self.A(ready, 3);
                    top = self.A(ready, 4);
                    if (left instanceof setSize) {
                        top = left.height;
                        left = left.width;
                    } else {
                        if (void 0 == top) {
                            throw Error("A");
                        }
                    }
                    el.style.width = round(left, true);
                    el.style.height = round(top, true);
                    /** @type {boolean} */
                    left = 0 < self.A(ready, 3) && 0 < self.A(ready, 4);
                    html(el, left);
                }
            });
        };
        /**
         * @param {string} orig
         * @return {undefined}
         */
        var callback = function(orig) {
            var target = equal(orig).document.body;
            /** @type {string} */
            target.style.zoom = orig.h + "%";
            output(orig);
            orig.onBoundsChanged.addListener(function() {
                return output(orig);
            });
            orig.g.listen("t", orig.Y, false, orig);
            orig.Y();
            var memory = orig.l.get(key2);
            play(memory, target);
            self.G(orig, function() {
                return fire(memory, target);
            });
            if (self.A(orig.T, 13)) {
                orig.Ba(self.A(orig.T, 13));
            }
            orig.W.listen(ready, orig.ga, void 0, orig);
            orig.ga();
            if (orig.G) {
                orig.l.get(key).set("mwo", true);
            }
        };
        /**
         * @param {Object} data
         * @return {undefined}
         */
        var output = function(data) {
            var opts = getSize(data);
            if (opts) {
                var offset = inspect(opts);
                var e = new partial;
                extractStacktrace(e, offset);
                btoa(data, e);
                data.P = opts.width;
                data.M = opts.height;
                data.O = data.h;
            }
        };
        /**
         * @return {undefined}
         */
        s.prototype.Y = function() {
            if (!this.K && !isObject(this)) {
                var radius = crossProduct(hex(this.g));
                if (radius != this.h) {
                    if (this.G) {
                        if (parseFloat(this.g)) {
                            this.O = crossProduct(6);
                            /** @type {number} */
                            this.P = 350;
                            /** @type {number} */
                            this.M = 560;
                        }
                    }
                    var offset = getSize(this);
                    var src = raycast_mesh(this.Da, new Rect(offset.left, offset.top));
                    /** @type {number} */
                    var height = radius / this.O;
                    /** @type {number} */
                    var width = Math.min(src.width, Math.round(this.P * height));
                    /** @type {number} */
                    height = Math.min(src.height, Math.round(this.M * height));
                    this.setPosition(Math.min(offset.left, src.left + src.width - width), Math.min(offset.top, src.top + src.height - height));
                    this.b.resizeTo(width, height);
                    /** @type {string} */
                    equal(this).document.body.style.zoom = radius + "%";
                    this.h = radius;
                }
            }
        };
        /**
         * @return {undefined}
         */
        s.prototype.ha = function() {
            getVersion(this, Anim(this.g));
        };
        /**
         * @return {undefined}
         */
        s.prototype.aa = function() {
            json(this, makeArray(this.g));
        };
        /**
         * @param {Object} data
         * @param {string} e
         * @return {undefined}
         */
        var btoa = function(data, e) {
            data.ua.a.then(function(n) {
                var complete = new fx;
                self.B(complete, 1, n);
                n = indexOf(e);
                self.E(complete, 2, n);
                n = new literal;
                self.E(n, 1, complete);
                data.Ea.sendMessage(n);
            });
        };
        /**
         * @return {undefined}
         */
        s.prototype.ga = function() {
            if (4 == this.W.a) {
                loadFile(this);
            } else {
                if (this.G) {
                    self.H(this.w);
                    /** @type {null} */
                    this.w = null;
                    this.L = self.qd();
                } else {
                    self.H(this);
                }
            }
        };
        /**
         * @param {string} orig
         * @return {undefined}
         */
        var loadFile = function(orig) {
            if (equal(orig)) {
                /** @type {boolean} */
                var restoreScript = !!self.A(orig.T, 3);
                then(orig.l.get(KEY), orig.Ra, equal(orig).document, restoreScript).then(function(w) {
                    promise(orig);
                    /** @type {string} */
                    orig.w = w;
                    self.I(orig, orig.w);
                    orig.L.b(orig.w);
                    if (restoreScript) {
                        w.a.addEventListener("loadstop", function() {
                            var code = orig.g.a.a.get("UseBrightBackground");
                            /** @type {string} */
                            code = code && (code.ea && code.N()) ? "body {background-color: red;}" : "body {background-color: transparent;}";
                            w.a.insertCSS({
                                code : code
                            });
                        });
                    }
                });
            }
        };
        /**
         * @param {Object} obj
         * @return {undefined}
         */
        var promise = function(obj) {
            if (100 != obj.h && recurring) {
                var keys = equal(obj);
                /**
                 * @return {undefined}
                 */
                var resolve = function() {
                    keys.resizeBy(1, 0);
                    keys.resizeBy(-1, 0);
                };
                if (obj.G && (parseFloat(obj.g) && self.wb())) {
                    resolve();
                } else {
                    self.Yc(resolve);
                }
            }
        };
        /**
         * @param {Object} target
         * @return {undefined}
         */
        var get = function(target) {
            var object = this;
            self.L.call(this);
            /** @type {Object} */
            this.h = target;
            this.a = new self.J;
            this.l = target.get(self.T);
            this.b = target.get(actualKey);
            this.j = target.get(elem);
            this.g = new self.N(this);
            self.I(this, this.g);
            target = this.l.J();
            this.g.listen(target, "capi:f_cfr", this.A);
            this.g.listen(target, "capi:f_cvr", this.w);
            this.g.listen(this.j, cycle, this.v);
            self.G(this, function() {
                return object.a.forEach(self.H);
            });
            if (0 != this.h.get(key).get("mwo")) {
                D(this);
            }
        };
        self.p(get, self.L);
        /**
         * @param {string} d
         * @param {string} ready
         * @return {?}
         */
        var start = function(d, ready) {
            if (!ready) {
                return{
                    url : d,
                    state : "normal"
                };
            }
            /** @type {Array} */
            var that = ["url", d];
            var dblclick = self.C(ready, complete, 2);
            if (dblclick) {
                if (null != self.A(dblclick, 1)) {
                    that.push("left", self.A(dblclick, 1));
                }
                if (null != self.A(dblclick, 2)) {
                    that.push("top", self.A(dblclick, 2));
                }
                if (null != self.A(dblclick, 3)) {
                    that.push("width", self.A(dblclick, 3));
                }
                if (null != self.A(dblclick, 4)) {
                    that.push("height", self.A(dblclick, 4));
                }
            }
            that.push("frame", self.A(ready, 3) ? "none" : "chrome");
            if (self.A(ready, 3)) {
                that.push("alphaEnabled", self.A(ready, 3));
            }
            if (null != self.A(ready, 5)) {
                that.push("hidden", self.A(ready, 5));
            }
            if (null != self.A(ready, 6)) {
                that.push("resizable", self.A(ready, 6));
            }
            if (null != self.A(ready, 7)) {
                that.push("alwaysOnTop", self.A(ready, 7));
            }
            if (null != self.A(ready, 8)) {
                that.push("focused", self.A(ready, 8));
            } else {
                that.push("focused", false);
            }
            if (null != self.A(ready, 15)) {
                that.push("id", self.A(ready, 15));
            }
            dblclick = self.A(ready, 16);
            that.push("visibleOnAllWorkspaces", null != dblclick ? dblclick : true);
            return func(that);
        };
        /**
         * @param {Object} obj
         * @param {string} orig
         * @param {string} a
         * @param {string} value
         * @return {undefined}
         */
        var remove = function(obj, orig, a, value) {
            var opt_key = self.A(orig, 15);
            /** @type {boolean} */
            var store = "uv_main_window" == opt_key;
            var record = store && parseFloat(obj.b);
            /** @type {boolean} */
            var k = false;
            if (record) {
                if (!self.A(orig, 5)) {
                    /** @type {boolean} */
                    k = true;
                    self.B(orig, 5, true);
                }
            }
            var type = new s(obj.h, orig, value, store);
            obj.a.set(opt_key, type);
            if (record) {
                value = new selector;
                record = new filename;
                var ready = self.C(orig, complete, 2);
                removeFromList(record, self.A(ready, 1) + self.A(ready, 3));
                is(record, self.A(ready, 2) + self.A(ready, 4));
                self.B(record, 3, 0);
                self.B(record, 4, 0);
                self.B(record, 5, 0);
                ready = iterator(value, filename, 4);
                ready.push(record);
                print(value, 4, ready);
                add(type, value);
            }
            type.create(start(a, orig));
            self.G(type, function() {
                return obj.a.remove(opt_key);
            });
            if (k) {
                set(type, function() {
                    return type.show();
                });
            }
            if (store) {
                self.I(type, new draw(obj.h, type));
            }
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        get.prototype.A = function(type) {
            type = self.C(new message(type.S), x, 1);
            h(this, type);
        };
        /**
         * @param {Object} obj
         * @param {string} orig
         * @return {undefined}
         */
        var h = function(obj, orig) {
            var type = self.C(orig, partial, 6);
            var load = type && type.getState();
            var ready;
            switch(orig.qa()) {
                case 2:
                    /** @type {string} */
                    ready = "uv_main_window";
                    break;
                case 5:
                    ready = orig.a();
                    break;
                case 6:
                    ;
                case 8:
                    ready = (type = load && self.C(load, input, 6)) && self.A(type, 3);
            }
            if (!self.Lc(obj.a, ready)) {
                /** @type {string} */
                var end = "dialog.html";
                type = new empty;
                self.B(type, 15, ready);
                ready = orig.a();
                self.B(type, 17, ready);
                ready = makeArray(obj.b);
                self.B(type, 7, ready);
                ready = Anim(obj.b);
                self.B(type, 16, ready);
                if (ready = load && self.C(load, i, 4)) {
                    if (null != self.A(ready, 2)) {
                        ready = self.A(ready, 2);
                        self.B(type, 8, ready);
                    }
                }
                switch(orig.qa()) {
                    case 2:
                        end = parseFloat(obj.b);
                        load = indexOf(self.C(apply(obj.b), complete, 2));
                        self.E(type, 2, load);
                        self.B(type, 3, end);
                        self.B(type, 6, !end);
                        type.Ba(flag(obj));
                        /** @type {string} */
                        end = "mainapp.html";
                        break;
                    case 5:
                        load = indexOf((load && self.C(load, string, 15)).C());
                        /** @type {number} */
                        ready = crossProduct(hex(obj.b)) / crossProduct(6);
                        /** @type {number} */
                        var recurring = ready * self.A(load, 3);
                        self.B(load, 3, recurring);
                        ready *= self.A(load, 4);
                        self.B(load, 4, ready);
                        self.E(type, 2, load);
                        self.B(type, 3, true);
                        self.B(type, 7, true);
                        self.B(type, 5, true);
                        self.B(type, 6, false);
                        break;
                    case 6:
                        ;
                    case 8:
                        self.B(type, 5, false);
                        self.B(type, 6, true);
                        self.B(type, 3, false);
                        /** @type {number} */
                        load = crossProduct(hex(obj.b)) / crossProduct(6) * 290;
                        ready = indexOf(self.C(apply(obj.b), complete, 2));
                        recurring = require(obj.b) ? self.A(ready, 1) + self.A(ready, 3) : self.A(ready, 1) - load;
                        self.B(ready, 1, recurring);
                        self.B(ready, 3, load);
                        self.E(type, 2, ready);
                }
                remove(obj, type, end, orig);
            }
        };
        /**
         * @param {string} str
         * @return {undefined}
         */
        get.prototype.w = function(str) {
            var ready = self.C(new w(str.S), click, 1);
            if (str = interpolate(this, ready.a())) {
                if (ready = self.C(ready, partial, 2), null != ready) {
                    var orig = self.C(changePage(ready), list, 2);
                    if (orig && (self.A(orig, 2) && str.w)) {
                        self.H(str);
                    } else {
                        orig = (orig = self.C(changePage(ready), arg, 1)) && self.A(orig, 2);
                        if (null != orig) {
                            compile(str, orig);
                        }
                        orig = (orig = self.C(changePage(ready), read, 10)) && self.A(orig, 2);
                        if (null != orig) {
                            if (orig) {
                                str.show();
                            } else {
                                str.hide();
                            }
                        }
                        if (orig = self.C(changePage(ready), i, 4)) {
                            if (self.A(orig, 2)) {
                                str.focus();
                            }
                        }
                        orig = (orig = changePage(ready).getTitle()) && orig.getTitle();
                        if (null != orig) {
                            str.Ba(orig);
                        }
                        orig = (orig = self.C(changePage(ready), C, 14)) && self.C(orig, complete, 2);
                        if (null != orig) {
                            postLink(str, orig);
                        }
                        orig = (orig = self.C(changePage(ready), string, 15)) && orig.C();
                        if (null != orig) {
                            expect(str, orig);
                        }
                        var load;
                        load = (ready = self.C(changePage(ready), prop, 12)) && self.C(ready, msg, 2);
                        if (null != load && self.C(load, selector, 2)) {
                            add(str, self.C(load, selector, 2));
                            ready = new partial;
                            orig = new msg;
                            load = self.A(load, 1);
                            self.B(orig, 1, load);
                            reject(ready, 12);
                            load = changePage(ready, true);
                            var part = new prop;
                            self.E(part, 2, orig);
                            self.E(load, 12, part);
                            btoa(str, ready);
                        }
                    }
                }
            }
        };
        /**
         * @param {Object} data
         * @return {undefined}
         */
        var D = function(data) {
            var e = new partial;
            var type = new obj;
            self.E(e, 2, type);
            extractStacktrace(e, indexOf(self.C(apply(data.b), complete, 2)));
            reject(e, 6);
            type = changePage(e, true);
            var dblclick = new read;
            self.B(dblclick, 2, true);
            self.E(type, 10, dblclick);
            type = new x;
            self.B(type, 3, 2);
            self.B(type, 1, "uv_main_window");
            self.E(type, 6, e);
            h(data, type);
        };
        /**
         * @param {Object} h
         * @return {undefined}
         */
        var rect = function(h) {
            D(h);
            h.a.get("uv_main_window").focus();
        };
        /**
         * @param {Node} target
         * @return {undefined}
         */
        var dispatch = function(target) {
            var udataCur = target.a.X();
            self.gb(udataCur, "uv_main_window");
            (0, self.u)(udataCur, function(opt_key) {
                self.H(this.a.get(opt_key));
                this.a.remove(opt_key);
            }, target);
        };
        /**
         * @param {Node} db
         * @return {undefined}
         */
        var getBuffer = function(db) {
            var str = db.a.get("uv_main_window");
            db.a.remove("uv_main_window");
            self.H(str);
        };
        /**
         * @param {Node} a
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        var resolve = function(a, deepDataAndEvents) {
            var e = interpolate(a, deepDataAndEvents);
            return e ? when(e) : self.od(false);
        };
        /**
         * @param {Node} object
         * @param {?} deepDataAndEvents
         * @return {?}
         */
        var interpolate = function(object, deepDataAndEvents) {
            /** @type {null} */
            var result = null;
            object.a.forEach(function(subKey) {
                if (subKey.va == deepDataAndEvents) {
                    /** @type {null} */
                    result = subKey;
                }
            });
            return result;
        };
        /**
         * @param {Object} h
         * @return {?}
         */
        var flag = function(h) {
            return(h = h.j.h) ? window.chrome.i18n.getMessage("CHROME_APP_WINDOW_TITLE", h) : window.chrome.i18n.getMessage("CHROME_HANGOUTS_SHORT_NAME");
        };
        /**
         * @return {undefined}
         */
        get.prototype.v = function() {
            if (10 == this.j.a) {
                dispatch(this);
            }
            var uv_main_window = this.a.get("uv_main_window");
            if (uv_main_window) {
                uv_main_window.Ba(flag(this));
            }
        };
        /**
         * @param {Object} x
         * @return {undefined}
         */
        var css = function(x) {
            (0, self.u)(pdataCur, function(p) {
                return self.Ng(x, p.id, new p.U(x));
            });
        };
        /** @type {Array} */
        var pdataCur = [{
            id : self.T,
            U : self.ig
        }, {
            id : actualKey,
            /** @type {function (Object): undefined} */
            U : move
        }, {
            id : KEY,
            /** @type {function (number): undefined} */
            U : resume
        }, {
            id : elem,
            /** @type {function (Element): undefined} */
            U : info
        }, {
            id : subKey,
            /** @type {function (Node): undefined} */
            U : activate
        }, {
            id : k,
            /** @type {function (number): undefined} */
            U : val
        }, {
            id : pageId,
            /** @type {function (?): undefined} */
            U : tick
        }, {
            id : modalInstance,
            /** @type {function (Function): undefined} */
            U : test
        }, {
            id : term,
            /** @type {function ((Node|string)): undefined} */
            U : c
        }, {
            id : it,
            /** @type {function ((Node|string)): undefined} */
            U : step
        }, {
            id : key2,
            /** @type {function (): undefined} */
            U : unknown
        }, {
            id : callee,
            /** @type {function (Object): undefined} */
            U : get
        }, {
            id : arrayBuf,
            /** @type {function (Node): undefined} */
            U : r
        }, {
            id : self.Of,
            /** @type {function (Function): undefined} */
            U : animation
        }, {
            id : blockId,
            /**
             * @param {string} orig
             * @return {undefined}
             */
            U : function(orig) {
                var pdataCur = this;
                this.a = orig.get(elem);
                this.b = orig.get(self.Of);
                /**
                 * @return {?}
                 */
                var ready = function() {
                    return handler(pdataCur);
                };
                this.a.listen(cycle, ready);
                this.b.listen("B", ready);
                self.G(orig, ready);
                if (window.chrome.systemIndicator) {
                    window.chrome.systemIndicator.enable();
                }
                handler(this);
            }
        }, {
            id : callbackId,
            /** @type {function (Node): undefined} */
            U : watch
        }, {
            id : storageKey,
            /** @type {function (Object): undefined} */
            U : run
        }];
        /**
         * @param {number} options
         * @return {undefined}
         */
        var o = function(options) {
            self.z(this, options, "h_ec", null);
        };
        self.p(o, self.y);
        /** @type {string} */
        o.messageId = "h_ec";
        /**
         * @param {(Node|string)} obj
         * @return {undefined}
         */
        var copy = function(obj) {
            self.L.call(this);
            obj = obj.get(self.T);
            var ready = new self.N(this);
            self.I(ready, ready);
            /** @type {string} */
            this.a = "u";
            ready.listen(obj.J(), "capi:clstup", this.b);
            ready.listen(obj.J(), "h_fr", this.h);
            ready.listen(obj.J(), "h_ec", this.g);
        };
        self.p(copy, self.L);
        /**
         * @param {string} types
         * @return {undefined}
         */
        copy.prototype.b = function(types) {
            /** @type {string} */
            var a = "r";
            if (1 == self.A(new packet(types.S), 1)) {
                /** @type {string} */
                a = "u";
            }
            md5_cmn(this, a);
        };
        /**
         * @param {string} ready
         * @param {?} a
         * @return {undefined}
         */
        var md5_cmn = function(ready, a) {
            /** @type {boolean} */
            var result = a != ready.a;
            ready.a = a;
            if (result) {
                self.M(ready, "E");
            }
        };
        /**
         * @return {undefined}
         */
        copy.prototype.h = function() {
            md5_cmn(this, "u");
        };
        /**
         * @param {string} type
         * @return {undefined}
         */
        copy.prototype.g = function(type) {
            type = new o(type.S);
            type = new keys(!!self.A(type, 1));
            self.M(this, type);
        };
        /**
         * @param {?} object
         * @return {undefined}
         */
        var keys = function(object) {
            self.K.call(this, "F");
            this.a = object;
        };
        self.p(keys, self.K);
        /**
         * @return {undefined}
         */
        var indents = function() {
            /** @type {null} */
            this.a = null;
            this.b = commitDirty();
        };
        encodeUriSegment(indents);
        /**
         * @param {Node} args
         * @param {boolean} recurring
         * @return {undefined}
         */
        var search = function(args, recurring) {
            args.b.then(function(result) {
                result.set("running", true);
                self.S("Loaded storage");
                if (!args.a) {
                    args.a = new self.Jg;
                    self.Ng(args.a, key, result, false);
                    css(args.a);
                    tighten(args.a, [self.T])[self.T].then(function() {
                        return(new copy(args.a)).listen("F", function(done) {
                            return runTest(args, done);
                        });
                    });
                }
                if (recurring) {
                    rect(args.a.get(callee));
                }
            });
        };
        /**
         * @param {Node} args
         * @param {Node} fn
         * @return {undefined}
         */
        var runTest = function(args, fn) {
            if (fn.a) {
                getBuffer(args.a.get(callee));
            } else {
                self.H(args.a);
                /** @type {null} */
                args.a = null;
                args.b.then(function(namespace) {
                    return namespace.set("running", false);
                });
                self.S("Stopped");
            }
        };
        /**
         * @return {undefined}
         */
        var find = function() {
            var value = indents.za();
            value.b.then(function(ready) {
                var c;
                if (!(c = 0 != ready.get("running"))) {
                    if (ready = ready.get("uv_settings")) {
                        ready = new RegExp(ready);
                        c = "boolean" == typeof self.A(ready, 9) ? self.A(ready, 9) : true;
                    } else {
                        /** @type {boolean} */
                        c = true;
                    }
                }
                if (c) {
                    search(value, false);
                }
            });
        };
        window.chrome.runtime.onInstalled.addListener(function(err) {
            self.S("onInstalled event: %s", JSON.stringify(err));
            find();
        });
        window.chrome.runtime.onStartup.addListener(function() {
            self.S("onStartup event");
            find();
        });
        if (window.chrome.app) {
            if (window.chrome.app.runtime) {
                window.chrome.app.runtime.onLaunched.addListener(function(key) {
                    self.S("runtime.onLaunched event: %s", key);
                    if (key = key && key.source) {
                        if (-1 != "new_tab_page command_line keyboard extensions_page management_api app_launcher system_tray about_page".split(" ").indexOf(key)) {
                            search(indents.za(), true);
                        }
                    }
                });
            }
        }
        if (window.chrome.systemIndicator) {
            window.chrome.systemIndicator.onClicked.addListener(function() {
                self.S("systemIndicator.onClicked event");
                search(indents.za(), true);
            });
        }
        if (window.chrome.browserAction) {
            window.chrome.browserAction.onClicked.addListener(function() {
                self.S("browserAction.onClicked event");
                search(indents.za(), true);
            });
        }
        self.Eg();
        self.S("Welcome to Google Hangouts");
        find();
    } catch (tableview) {
        self._DumpException(tableview);
    }
}).call(this, this.default_main);
